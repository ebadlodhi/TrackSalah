package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface addexemptedcurrent {
    @FormUrlEncoded
    @POST("addexemptedcurrent.php")
    Call<String> getDetails(@Field("userid") String userid, @Field("startdate") String startdate, @Field("startdaynamazname") String startdaynamazname, @Field("enaddate") String enaddate
            , @Field("enddatenamazname") String enddatenamazname);
}
