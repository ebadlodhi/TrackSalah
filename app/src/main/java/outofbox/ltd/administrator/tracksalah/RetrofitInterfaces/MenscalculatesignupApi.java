package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;

import outofbox.ltd.administrator.tracksalah.UpdateProfile.Example;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface MenscalculatesignupApi {
    @FormUrlEncoded
    @POST("menscalculatesignup.php")
    Call<String> getDetails(@Field("userid") String userid,@Field("lastmonthdate") String lastmonthdate,@Field("dateofbirth") String dateofbirth,@Field("periodcycle") String periodcycle
            ,@Field("noofdays") String noofdays);

}
