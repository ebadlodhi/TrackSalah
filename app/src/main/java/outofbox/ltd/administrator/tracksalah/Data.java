package outofbox.ltd.administrator.tracksalah;

public class Data {
    public String getDatelist() {
        return datelist;
    }

    public void setDatelist(String datelist) {
        this.datelist = datelist;
    }

    public Data(String datelist) {
        this.datelist = datelist;
    }

    String datelist;
}
