package outofbox.ltd.administrator.tracksalah;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import outofbox.ltd.administrator.tracksalah.POJOQadahCheck.Example;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.QadahCheckrecord;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

public class QaddahEstimationActivity extends AppCompatActivity {
    public static final String ROOT_URL = "http://185.123.99.207/~android215/TrackSalah/";
    TextView totqadah,res,num,days,t1,t2,dd ;
    Spinner qadahestimator;
    String totalqahad;
    private AdView mAdView;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qaddah_estimation);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        mAdView = (AdView) findViewById(R.id.adView);
        MobileAds.initialize(this, "ca-app-pub-2825367626708661~7620129751");
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        totqadah = findViewById(R.id.totqadahnnn);
        res= findViewById(R.id.res);
        num = findViewById(R.id.numnamaz);
        days = findViewById(R.id.daynamaz);
        t1 =findViewById(R.id.t1);
        t2 = findViewById(R.id.t2);
        dd = findViewById(R.id.dd);
        qadahestimator = findViewById(R.id.qadahestimator);
        qadahestimator.setPopupBackgroundResource(R.color.White);
        qadahestimator.setPopupBackgroundResource(R.color.White);
        Bundle bundle = getIntent().getExtras();
         totalqahad = bundle.getString("TotalQadah");
        if(totalqahad == null) {
            Toast.makeText(this, "Data not loaded\nKindly Go Back.", Toast.LENGTH_LONG).show();
            qadahestimator.setEnabled(false);
        }
            totqadah.setText("   "+totalqahad+"   ");
        String[] items = new String[]{"0","1", "2", "3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        qadahestimator.setPopupBackgroundResource(R.color.White);
        qadahestimator.setAdapter(adapter);
        qadahestimator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(Integer.parseInt((String) qadahestimator.getSelectedItem())==0)
                {
                    res.setText("Kindly Select Days from above list");
                    num.setVisibility(View.GONE);
                    days.setVisibility(View.GONE);
                    t1.setVisibility(View.GONE);
                    t2.setVisibility(View.GONE);
                    dd.setVisibility(View.GONE);

                }
                else
                {
                    res.setVisibility(View.VISIBLE);
                    num.setVisibility(View.VISIBLE);
                    days.setVisibility(View.VISIBLE);
                    t1.setVisibility(View.VISIBLE);
                    t2.setVisibility(View.VISIBLE);
                    dd.setVisibility(View.VISIBLE);
                    int non = Integer.parseInt((String) qadahestimator.getSelectedItem());
                    int non1 = non*5;
                    int totnamazyears = (Integer.parseInt(totalqahad)/non1);
                    double totnamazyears1 = totnamazyears/365;
                    res.setText("If You Offer");
                  num.setText("   "+Integer.toString(non)+"   ");
                  days.setText("   "+Integer.toString(totnamazyears)+"   ");


                   // res.setText("If You Offer "+wordtoSpan+" Qadah Namaz after every Namaz \nSo You Will Offer your all Qadah Namaz in "+wordtoSpan2+" Days.");

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                res.setText("Kindly Select Days from above list");
            }
        });




    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }





}
