package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface PendingApi {
    @FormUrlEncoded
    @POST("AddToPending.php")
    Call<String> getDetails(@Field("name") String name,
                            @Field("email") String email,
                            @Field("password") String password,
                            @Field("gender") String gender);
}
