package outofbox.ltd.administrator.tracksalah.POJOFORQADAHTIMEPERIOD;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class _0 {
    @SerializedName("TotalQadahNamaz")
    @Expose
    private String totalQadahNamaz;
    @SerializedName("Namazfajar")
    @Expose
    private String namazfajar;
    @SerializedName("NamazZohar")
    @Expose
    private String namazZohar;
    @SerializedName("NamazAsar")
    @Expose
    private String namazAsar;
    @SerializedName("NamazMaghrib")
    @Expose
    private String namazMaghrib;
    @SerializedName("NamazIsha")
    @Expose
    private String namazIsha;

    public String getTotalQadahNamaz() {
        return totalQadahNamaz;
    }

    public void setTotalQadahNamaz(String totalQadahNamaz) {
        this.totalQadahNamaz = totalQadahNamaz;
    }

    public String getNamazfajar() {
        return namazfajar;
    }

    public void setNamazfajar(String namazfajar) {
        this.namazfajar = namazfajar;
    }

    public String getNamazZohar() {
        return namazZohar;
    }

    public void setNamazZohar(String namazZohar) {
        this.namazZohar = namazZohar;
    }

    public String getNamazAsar() {
        return namazAsar;
    }

    public void setNamazAsar(String namazAsar) {
        this.namazAsar = namazAsar;
    }

    public String getNamazMaghrib() {
        return namazMaghrib;
    }

    public void setNamazMaghrib(String namazMaghrib) {
        this.namazMaghrib = namazMaghrib;
    }

    public String getNamazIsha() {
        return namazIsha;
    }

    public void setNamazIsha(String namazIsha) {
        this.namazIsha = namazIsha;
    }
}
