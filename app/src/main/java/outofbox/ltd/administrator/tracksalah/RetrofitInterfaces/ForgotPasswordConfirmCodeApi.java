package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ForgotPasswordConfirmCodeApi {

    @FormUrlEncoded
    @POST("ForgotPasswordConfirmCode.php")
    Call<String> getDetails(@Field("Email") String email, @Field("Code") String code, @Field("Password") String password);
}
