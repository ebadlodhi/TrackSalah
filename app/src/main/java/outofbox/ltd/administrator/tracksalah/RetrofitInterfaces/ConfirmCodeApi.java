package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ConfirmCodeApi {

    @FormUrlEncoded
    @POST("ConfirmCode.php")
    Call<String> getDetails(@Field("Name") String name, @Field("Email") String email,
                            @Field("Password") String password, @Field("Code") String code);
}
