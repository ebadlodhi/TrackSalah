package outofbox.ltd.administrator.tracksalah;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.UserHandle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.RegisterApi;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class SignupActivity extends AppCompatActivity {

    String res ;
    TextView datepicker;
     TextView tvDisplayDate,namazfarzdate,udob,mensstartdate,mensenddate,salahstartdate;
    private DatePicker dpResult,dpResult1;
    private Button btnChangeDate;
    private PopupWindow mPopupWindow;

    private int myear,mfarzyear;
    private int mmonth,mfarzmonth;
    private int mday,mfarzday;
    Date mydate = null;
    Date newdate = null;
    String danew="";
    long longdate;
    String tenyear = "";
    String tenyearServer = "";
    String uname,upass,uemail;
    String username="";
    String userpassword="";
    String useremail = "";
    String UserDOB = "";
    String UserDOBSeerver = "";

    String Creationtime = "";
    String Gender = "";
    String nfarzdate = "";

    String nstartdate = "";

    String mstartdate = "0";

    String menddate = "0";

    Date Currenttime;
    String currentDateandTime;
    TextView textView;
    static final int DATE_DIALOG_ID = 999;
    public static ProgressBar p1;
    public RadioGroup radioGroup;
    public RadioButton radioButton,rbutton1,rbutton2;
    private AdView mAdView;
    public static final String ROOT_URL = "http://185.123.99.207/~android215/TrackSalah/";
    ConstraintLayout layoutmens;
    int day;
    Calendar myCalendar;
    DatePickerDialog datePickerDialog;
    Date startdate = null;
    Date startnewdate = null;
    String Nstartdate = "";
    long diff;
    int Days,TotalQadahNamaz,Qadahfajar,QadahZohar,QadahAsar,Qadahmaghrib,Qadahisha = 0;
    private Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_signup);
        mAdView = (AdView) findViewById(R.id.adView);
        MobileAds.initialize(this, "ca-app-pub-2825367626708661~7620129751");
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdView.setVisibility(View.VISIBLE);
            }
        });
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Signup");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("TrackSalahUser")
                .setAction("User id")
                .setLabel(DashboardActivity.Userid)
                .setValue(1)
                .build());
        datepicker = findViewById(R.id.editText_date);
        Bundle bundle = getIntent().getExtras();

        uname= (bundle.get("name").toString());

        upass=(bundle.get("password").toString());

        uemail=(bundle.get("email").toString());

        layoutmens = findViewById(R.id.layoutmens);



        Gender = bundle.getString("Gender");
       if(Gender.equals("Male")) {
           layoutmens.setVisibility(View.GONE);
       }
       else
       {
           layoutmens.setVisibility(View.VISIBLE);
       }
        //  Toast.makeText(this, ""+Gender, Toast.LENGTH_LONG).show();
        udob = findViewById(R.id.editText_date);
       // udob.setText(bundle.get("dob").toString());

        namazfarzdate = findViewById(R.id.namazfarzdatenew);
        mensstartdate = findViewById(R.id.mensstartdate);
        mensenddate = findViewById(R.id.mensenddate);
        salahstartdate = findViewById(R.id.salahstartdate);
        dpResult1 = (DatePicker) findViewById(R.id.dpResult);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        try {
//            mydate = simpleDateFormat.parse(getUserDOB);
//            Calendar c = Calendar.getInstance();
//            c.setTime(mydate);
//            c.add(Calendar.YEAR, 14);
//            newdate = c.getTime();
//            longdate = newdate.getTime();
//            tenyear = simpleDateFormat.format(newdate);
//            namazfarzdate.setText(tenyear);
//
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }


        p1 = (ProgressBar) findViewById(R.id.signprogress);
        p1.setVisibility(View.GONE);
        setCurrentDateOnView();
        addListenerOnButton();
        mensstartdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogDatestart();
            }
        });
        mensenddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogDateend();
            }
        });


        namazfarzdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCurrentDateOnViewfarz();
                addListenerOnButtonfarz();
               // Toast.makeText(SignupActivity.this, "hahha", Toast.LENGTH_SHORT).show();
            }
        });



    }
    public void setCurrentDateOnView() {

        tvDisplayDate = findViewById(R.id.editText_date);
        namazfarzdate = findViewById(R.id.namazfarzdatenew);
        dpResult = (DatePicker) findViewById(R.id.dpResult);
        dpResult.setVisibility(View.GONE);

        final Calendar c = Calendar.getInstance();
        Currenttime = c.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        currentDateandTime = sdf.format(new Date());
        myear = c.get(Calendar.YEAR);
        mmonth = c.get(Calendar.MONTH);
        mday = c.get(Calendar.DAY_OF_MONTH);

        // set current date into textview
//        tvDisplayDate.setText(new StringBuilder()
//                // Month is 0 based, just add 1
//                .append(myear).append("-") .append(mmonth + 1).append("-").append(mday)
//        );

        // set current date into datepicker
        dpResult.init(myear, mmonth, mday, null);

    }

    public void addListenerOnButton() {



        datepicker.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                showDialog(DATE_DIALOG_ID);

            }


        });

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID: {
                DatePickerDialog _datefarz = new DatePickerDialog(this, datePickerListener, myear, mmonth,
                        mday) {
                    @Override
                    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                        if (year > myear) {
//                            Toast.makeText(getApplicationContext(), "Year can't be greater then current Year", Toast.LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }
//                        if (monthOfYear > mmonth && year == myear) {
//                            Toast.makeText(getApplicationContext(), "Month can't be greater then current Month", Toast.LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }
//                        if (dayOfMonth > mday && year == myear && monthOfYear == mmonth) {
//                            Toast.makeText(getApplicationContext(), "Date can't be greater then current date", Toast.LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }

                    }
                };
                return _datefarz;
            }
            case 111: {
                DatePickerDialog _datefarz = new DatePickerDialog(this, datePickerListenerfarz, myear, mmonth,
                        mday) {
                    @Override
                    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                        if (year > myear) {
//                            Toast.makeText(getApplicationContext(), "Year can't be greater then current Year", Toast.LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }
//                        if (monthOfYear > mmonth && year == myear) {
//                            Toast.makeText(getApplicationContext(), "Month can't be greater then current Month", Toast.LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }
//                        if (dayOfMonth > mday && year == myear && monthOfYear == mmonth) {
//                            Toast.makeText(getApplicationContext(), "Date can't be greater then current date", Toast.LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }
                    }
                };
                return _datefarz;
            }

        }


        return null;
    }
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            myear = selectedYear;
            mmonth = selectedMonth;
            mday = selectedDay;
            String sMonth = "";
            // set selected date into textview
            if (mmonth == 0)
                sMonth = "Jan";
            else if (mmonth == 1)
                sMonth = "Feb";
            else if (mmonth == 2)
                sMonth = "Mar";
            else if (mmonth == 3)
                sMonth = "Apr";
            else if (mmonth == 4)
                sMonth = "May";
            else if (mmonth == 5)
                sMonth = "Jun";
            else if (mmonth == 6)
                sMonth = "Jul";
            else if (mmonth == 7)
                sMonth = "Aug";
            else if (mmonth == 8)
                sMonth = "Sep";
            else if (mmonth == 9)
                sMonth = "Oct";
            else if (mmonth == 10)
                sMonth = "Nov";
            else if (mmonth == 11)
                sMonth = "Dec";
            tvDisplayDate.setText(new StringBuilder()
                    .append(mday).append(" ") .append(sMonth).append(" ").append(myear).append(" "));
            // set selected date into datepicker also
            dpResult.init(myear, mmonth, mday, null);

            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy");
                SimpleDateFormat simpleDateFormattenyear = new SimpleDateFormat("yyyy-MM-dd");
                mydate = simpleDateFormat.parse(tvDisplayDate.getText().toString());
                Calendar c = Calendar.getInstance();
                c.setTime(mydate);
                c.add(Calendar.YEAR, 14);
                newdate = c.getTime();
                longdate = newdate.getTime();
                tenyear = simpleDateFormat.format(newdate);
                tenyearServer = simpleDateFormattenyear.format(newdate);
                namazfarzdate.setText(tenyear);
                mfarzyear = Calendar.YEAR;
                mfarzmonth = Calendar.MONTH;
                mfarzday = Calendar.DAY_OF_MONTH;


            } catch (ParseException e) {
                e.printStackTrace();
            }


        }
    };

    public void setCurrentDateOnViewfarz() {


        namazfarzdate = findViewById(R.id.namazfarzdatenew);
        dpResult = (DatePicker) findViewById(R.id.dpResult);
        dpResult.setVisibility(View.GONE);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy");
        try {
            mydate = simpleDateFormat.parse(tvDisplayDate.getText().toString());
            c.setTime(mydate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        currentDateandTime = sdf.format(new Date());
        myear = c.get(Calendar.YEAR);
        mmonth = c.get(Calendar.MONTH);
        mday = c.get(Calendar.DAY_OF_MONTH);

        // set current date into textview
//        tvDisplayDate.setText(new StringBuilder()
//                // Month is 0 based, just add 1
//                .append(myear).append("-") .append(mmonth + 1).append("-").append(mday)
//        );

        // set current date into datepicker
        dpResult.init(myear, mmonth, mday, null);

    }

    public void addListenerOnButtonfarz() {



        namazfarzdate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                showDialog(111);

            }

        });

    }


    private DatePickerDialog.OnDateSetListener datePickerListenerfarz = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            myear = selectedYear;
            mmonth = selectedMonth;
            mday = selectedDay;
            String sMonth = "";
            // set selected date into textview
            if (mmonth == 0)
                sMonth = "Jan";
            else if (mmonth == 1)
                sMonth = "Feb";
            else if (mmonth == 2)
                sMonth = "Mar";
            else if (mmonth == 3)
                sMonth = "Apr";
            else if (mmonth == 4)
                sMonth = "May";
            else if (mmonth == 5)
                sMonth = "Jun";
            else if (mmonth == 6)
                sMonth = "Jul";
            else if (mmonth == 7)
                sMonth = "Aug";
            else if (mmonth == 8)
                sMonth = "Sep";
            else if (mmonth == 9)
                sMonth = "Oct";
            else if (mmonth == 10)
                sMonth = "Nov";
            else if (mmonth == 11)
                sMonth = "Dec";
             danew = myear+"-"+mmonth+"-"+mday;

            namazfarzdate.setText(new StringBuilder()
                    .append(mday).append(" ") .append(sMonth).append(" ").append(myear).append(" "));
            // set selected date into datepicker also
            dpResult.init(myear, mmonth, mday, null);
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy");
                SimpleDateFormat simpleDateFormattenyear = new SimpleDateFormat("yyyy-MM-dd");
                mydate = simpleDateFormat.parse(namazfarzdate.getText().toString());
                Calendar c = Calendar.getInstance();
                c.setTime(mydate);
                newdate = c.getTime();
                longdate = newdate.getTime();
                tenyear = simpleDateFormat.format(newdate);
                tenyearServer = simpleDateFormattenyear.format(newdate);
                namazfarzdate.setText(tenyear);
                mfarzyear = Calendar.YEAR;
                mfarzmonth = Calendar.MONTH;
                mfarzday = Calendar.DAY_OF_MONTH;


            } catch (ParseException e) {
                e.printStackTrace();
            }


        }
    };


    public void registerr(View view)
    {
        UserDOBSeerver = udob.getText().toString();
        SimpleDateFormat simpleDateFormat11 = new SimpleDateFormat("dd MMM yyyy");
        SimpleDateFormat simpleDateFormat22 = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;

        try {
            date = simpleDateFormat11.parse(UserDOBSeerver);
            UserDOB = simpleDateFormat22.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }



        if(UserDOB.equals(""))
{
    Toast.makeText(this, "Enter Your Date Of Birth First", Toast.LENGTH_SHORT).show();
    udob.requestFocus();
    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
}

        else if(mensstartdate.getText().toString().equals(""))
        {
            Toast.makeText(this, "Enter mensturation start date.", Toast.LENGTH_SHORT).show();
        }

        else if(mensenddate.getText().toString().equals(""))
        {
            Toast.makeText(this, "Enter mensturation end date.", Toast.LENGTH_SHORT).show();
        }
else {
    username = uname.toString();
    userpassword = upass.toString();
    useremail = uemail.toString();
            Calendar calendar1 = Calendar.getInstance();

            SimpleDateFormat simpleDateFormatctime = new SimpleDateFormat("yyyy-MM-dd");

            Date ss = calendar1.getTime();


            String cdate = simpleDateFormatctime.format(ss);
            Creationtime = cdate.toString()+" "+currentDateandTime;

    nfarzdate = namazfarzdate.getText().toString().trim();
    nstartdate = salahstartdate.getText().toString().trim();
    mstartdate = mensstartdate.getText().toString().trim();
    menddate = mensenddate.getText().toString().trim();
   // Toast.makeText(this, ""+nstartdate, Toast.LENGTH_SHORT).show();

    p1.setVisibility(View.VISIBLE);

    if(Gender.equals("Male")) {
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");



        //            startdate = simpleDateFormat.parse(nstartdate.toString());

//            calendar.setTime(startdate);
            startnewdate = calendar1.getTime();
        //Date changedate = simpleDateFormat.format(startnewdate);
        cdate = simpleDateFormat.format(startnewdate);
        Nstartdate = simpleDateFormat2.format(startnewdate);
        //  Toast.makeText(this, "farz date = "+tenyear+"\nstart date = "+cdate, Toast.LENGTH_LONG).show();
//        List<String> dates = getDates( tenyear.toString().trim(),cdate.toString().trim());
//        for(String date:dates)
//            Log.d("date",date.toString());

        diff = startnewdate.getTime() - newdate.getTime();
        Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        TotalQadahNamaz = Days * 5;
        Qadahfajar = TotalQadahNamaz / 5;
        QadahZohar = TotalQadahNamaz / 5;
        QadahAsar = TotalQadahNamaz / 5;
        Qadahmaghrib = TotalQadahNamaz / 5;
        Qadahisha = TotalQadahNamaz / 5;



        Gson gson = new Gson();
//        String dtlist = gson.toJson(dates);
//        Log.d("array date",dtlist);
      //  Toast.makeText(this, TotalQadahNamaz+"\n"+Qadahfajar+"\n"+QadahZohar+"\n"+QadahAsar+"\n"+Qadahmaghrib+"\n"+Qadahisha, Toast.LENGTH_SHORT).show();
       // Toast.makeText(this, TotalQadahNamaz+"\n"+Qadahfajar+"\n"+QadahZohar+"\n"+QadahAsar+"\n"+Qadahmaghrib+"\n"+Qadahisha, Toast.LENGTH_SHORT).show();
        //  CalculateQadah(uid, cdate, TotalQadahNamaz, TotalQadahNamaz, Qadahfajar, QadahZohar, QadahAsar, Qadahmaghrib, Qadahisha,tenyear);
        if(TotalQadahNamaz <0)
        {
            Qadahfajar = 0;
            TotalQadahNamaz = 0;
            QadahZohar = 0;
            QadahAsar = 0;
            Qadahmaghrib = 0;
            Qadahisha = 0;
            Toast.makeText(this, "Namaz not Obligatory on you!..\nWill Obligatory From "+tenyear, Toast.LENGTH_SHORT).show();
            p1.setVisibility(View.GONE);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ROOT_URL) //Setting the Root URL
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            RegisterApi api = retrofit.create(RegisterApi.class);
            Call<String> call = api.getDetails(username, userpassword, useremail, Gender, UserDOB, Creationtime, cdate, Integer.toString(TotalQadahNamaz), Integer.toString(TotalQadahNamaz), Integer.toString(Qadahfajar), Integer.toString(QadahZohar), Integer.toString(QadahAsar), Integer.toString(Qadahmaghrib), Integer.toString(Qadahisha),tenyearServer,"0","0");
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    Log.i("Response", response.body().toString());
                    p1.setVisibility(View.GONE);
                    if (response.isSuccessful()) {

                        if (response.body() != null) {
                            String ret = response.body().toString().trim();
                            if (ret.contains("Server Issue")) {
                                Toast.makeText(SignupActivity.this, "Email already registered", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(), "Congratulations!\nAccount Created", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }
                        //                    else{
                        //                        Toast.makeText(SignupActivity.this, "Server issue Kindly retry later.", Toast.LENGTH_SHORT).show(); }
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    p1.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "Congratulations!\nAccount Created", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                    p1.setVisibility(View.GONE);
                    finish();
                }
            });



        }
        else
        {     Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(ROOT_URL) //Setting the Root URL
                            .addConverterFactory(ScalarsConverterFactory.create())
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    RegisterApi api = retrofit.create(RegisterApi.class);
                    Call<String> call = api.getDetails(username, userpassword, useremail, Gender, UserDOB, Creationtime, cdate, Integer.toString(TotalQadahNamaz), Integer.toString(TotalQadahNamaz), Integer.toString(Qadahfajar), Integer.toString(QadahZohar), Integer.toString(QadahAsar), Integer.toString(Qadahmaghrib), Integer.toString(Qadahisha),tenyearServer,"0","0");
                    call.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            Log.i("Response", response.body().toString());
                            p1.setVisibility(View.GONE);
                            if (response.isSuccessful()) {

                                if (response.body() != null) {
                                    String ret = response.body().toString().trim();
                                    if (ret.contains("Server Issue")) {
                                        Toast.makeText(SignupActivity.this, "Email already registered", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Congratulations!\nAccount Created", Toast.LENGTH_LONG).show();
                                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
        //                    else{
        //                        Toast.makeText(SignupActivity.this, "Server issue Kindly retry later.", Toast.LENGTH_SHORT).show(); }
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            p1.setVisibility(View.GONE);
                            Toast.makeText(getApplicationContext(), "Congratulations!\nAccount Created", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(intent);
                            p1.setVisibility(View.GONE);
                            finish();
                        }
                    });
         }


    }
    else
    {
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        // startdate = simpleDateFormat.parse(nstartdate.toString());

//            calendar.setTime(startdate);
        startnewdate = calendar1.getTime();
        //Date changedate = simpleDateFormat.format(startnewdate);
         cdate = simpleDateFormat.format(startnewdate);
        Nstartdate = simpleDateFormat2.format(startnewdate);
      //  Toast.makeText(this, "farz date = "+tenyear+"\nstart date = "+cdate, Toast.LENGTH_LONG).show();
//        List<String> dates = getDates( tenyear.toString().trim(),cdate.toString().trim());
//        for(String date:dates)
//            Log.d("date",date.toString());
        diff = startnewdate.getTime() - newdate.getTime();
        Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        String mdayss = Integer.toString(day);
        int mdays = Integer.parseInt(mdayss);
        int minyear = mdays*12;
        int totyear = Days/365;
        int totminday = minyear*totyear;
        int Totalqadahmin = Days-totminday;
        TotalQadahNamaz = Totalqadahmin * 5;
        Qadahfajar = TotalQadahNamaz / 5;
        QadahZohar = TotalQadahNamaz / 5;
        QadahAsar = TotalQadahNamaz / 5;
        Qadahmaghrib = TotalQadahNamaz / 5;
        Qadahisha = TotalQadahNamaz / 5;
//        Gson gson = new Gson();
//        String dtlist = gson.toJson(dates);
//        Log.d("array date",dtlist);
        // CalculateQadah(uid, cdate, TotalQadahNamaz, TotalQadahNamaz, Qadahfajar, QadahZohar, QadahAsar, Qadahmaghrib, Qadahisha,tenyear);
        if(TotalQadahNamaz <0)
        {
            Qadahfajar = 0;
            TotalQadahNamaz = 0;
            QadahZohar = 0;
            QadahAsar = 0;
            Qadahmaghrib = 0;
            Qadahisha = 0;
            Toast.makeText(this, "Namaz not Obligatory on you!..\nWill Obligatory From "+tenyear, Toast.LENGTH_SHORT).show();
            p1.setVisibility(View.GONE);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ROOT_URL) //Setting the Root URL
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            RegisterApi api = retrofit.create(RegisterApi.class);
            Call<String> call = api.getDetails(username, userpassword, useremail, Gender, UserDOB, Creationtime, cdate, Integer.toString(TotalQadahNamaz), Integer.toString(TotalQadahNamaz), Integer.toString(Qadahfajar), Integer.toString(QadahZohar), Integer.toString(QadahAsar), Integer.toString(Qadahmaghrib), Integer.toString(Qadahisha),tenyearServer, mensstartdate.getText().toString().trim(), mensenddate.getText().toString().trim());
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    Log.i("Response", response.body().toString());
                    p1.setVisibility(View.GONE);
                    if (response.isSuccessful()) {

                        if (response.body() != null) {
                            String ret = response.body().toString().trim();
                            if (ret.contains("Server Issue")) {
                                Toast.makeText(SignupActivity.this, "Email already registered", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(), "Congratulations!\nAccount Created", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }
                        //                    else{
                        //                        Toast.makeText(SignupActivity.this, "Server issue Kindly retry later.", Toast.LENGTH_SHORT).show(); }
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    p1.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "Congratulations!\nAccount Created", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                    p1.setVisibility(View.GONE);
                    finish();
                }
            });
        }
        else
        {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ROOT_URL) //Setting the Root URL
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            RegisterApi api = retrofit.create(RegisterApi.class);
            Call<String> call = api.getDetails(username, userpassword, useremail, Gender, UserDOB, Creationtime, cdate, Integer.toString(TotalQadahNamaz), Integer.toString(TotalQadahNamaz), Integer.toString(Qadahfajar), Integer.toString(QadahZohar), Integer.toString(QadahAsar), Integer.toString(Qadahmaghrib), Integer.toString(Qadahisha),tenyearServer, mensstartdate.getText().toString().trim(), mensenddate.getText().toString().trim());
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    Log.i("Response", response.body().toString());



                        if (response.body() != null) {
                            String ret = response.body().toString().trim();
                            if (ret.contains("Account Created")) {
                                Toast.makeText(getApplicationContext(), "Congratulations!\nAccount Created", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                startActivity(intent);
                                p1.setVisibility(View.GONE);
                                finish();
                            }
                            } else {
                            Toast.makeText(SignupActivity.this, "Email already registered", Toast.LENGTH_SHORT).show();
                                p1.setVisibility(View.GONE);

                        }
        //                    else{
        //                        Toast.makeText(SignupActivity.this, "Server issue Kindly retry later.", Toast.LENGTH_SHORT).show(); }

                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Congratulations!\nAccount Created", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                    p1.setVisibility(View.GONE);
                    finish();
                }
            });
        }


    }


}
    }

    private static ArrayList<String> getDates(String dateString1, String dateString2)
    {
        ArrayList<String> dates = new ArrayList<String>();
        DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = df1.parse(dateString1);
            date2 = df1.parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);


        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while(!cal1.after(cal2))
        {
            Date pdate = cal1.getTime();
            String sdate = df1.format(pdate);
            dates.add(sdate.toString());
            cal1.add(Calendar.DATE, 1);
        }
        return dates;
    }

    public void showDialogDatestart()
    {
        final String[] mThumbIds = {
                "1","2","3","4","5","6","7",
                "8","9","10","11","12","13","14",
                "15","16","17","18","19","20","21",
                "22","23","24","25","26","27","28","29","30"
        };

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.customselectdate, null);
        dialogBuilder.setView(dialogView);
        View view = null;



        final GridView gridViewcustom = dialogView.findViewById(R.id.dateadgridview);
        Button adddrink = dialogView.findViewById(R.id.addatebuton);




        dialogBuilder.setTitle(" Select Mensturation Start Date ");
        dialogBuilder.setCancelable(false);
        final View finalView = view;
        final String[] selectednum = new String[1];
        selectednum[0] = mThumbIds[0];
        gridViewcustom.setAdapter(new TextViewAdapter(this, mThumbIds));
        gridViewcustom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                selectednum[0] = mThumbIds[i];
//                Toast.makeText(DashboardActivity.this, ""+i, Toast.LENGTH_SHORT).show();


            }
        });
        final AlertDialog b = dialogBuilder.create();

//        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int whichButton) {
//
//            }
//        });
//        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int whichButton) {
//                //pass
//            }
//        });

        b.show();
        adddrink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                String url = selectednum[0];
                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd ");
                String strDate = mdformat.format(calendar.getTime());

                 mensstartdate.setText(url);


//                Toast.makeText(getApplicationContext(), "url is "+url, Toast.LENGTH_SHORT).show();
                b.dismiss();
            }
        });
    }
    public void showDialogDateend()
    {
        final String[] mThumbIds = {
                "1","2","3","4","5","6","7",
                "8","9","10","11","12","13","14",
                "15","16","17","18","19","20","21",
                "22","23","24","25","26","27","28","29","30"
        };

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.customselectdate, null);
        dialogBuilder.setView(dialogView);
        View view = null;



        final GridView gridViewcustom = dialogView.findViewById(R.id.dateadgridview);
        Button adddrink = dialogView.findViewById(R.id.addatebuton);





        dialogBuilder.setTitle(" Select Mensturation End Date ");
        dialogBuilder.setCancelable(false);
        final View finalView = view;
        final String[] selectednum = new String[1];
        selectednum[0] = mThumbIds[0];
        gridViewcustom.setAdapter(new TextViewAdapter(this, mThumbIds));
        gridViewcustom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                selectednum[0] = mThumbIds[i];
//                Toast.makeText(DashboardActivity.this, ""+i, Toast.LENGTH_SHORT).show();


            }
        });
        final AlertDialog b = dialogBuilder.create();

//        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int whichButton) {
//
//            }
//        });
//        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int whichButton) {
//                //pass
//            }
//        });

        b.show();
        adddrink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                String url = selectednum[0];
                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd ");
                String strDate = mdformat.format(calendar.getTime());


                mensenddate.setText(url);

                int startdate =Integer.parseInt( mensstartdate.getText().toString().trim());

                int endtdate =Integer.parseInt( mensenddate.getText().toString().trim());
                if(startdate<=endtdate)
                {
                     day = endtdate - startdate +1;
                    Toast.makeText(SignupActivity.this, "Your menstuaration days for every month is = "+day+" days", Toast.LENGTH_SHORT).show();
                }
                if(startdate>=endtdate)
                {
                     day = (30 - startdate)+endtdate+1;
                    Toast.makeText(SignupActivity.this, "Your menstuaration days for every month is = "+day+" days", Toast.LENGTH_SHORT).show();
                }



//                Toast.makeText(getApplicationContext(), "url is "+url, Toast.LENGTH_SHORT).show();
                b.dismiss();
            }
        });
    }

    public class TextViewAdapter extends BaseAdapter {
        private Context context;
        private final String[] textViewValues;

        public TextViewAdapter(Context context, String[] textViewValues) {
            this.context = context;
            this.textViewValues = textViewValues;
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View gridView;

            if (convertView == null) {

                gridView = new View(context);

                // get layout from mobile.xml
                gridView = inflater.inflate(R.layout.dayitem, null);

                // set value into textview
                TextView textView = (TextView) gridView
                        .findViewById(R.id.tvadapterday);
                textView.setText(textViewValues[position]);
            } else {
                gridView = (View) convertView;
            }

            return gridView;
        }

        @Override
        public int getCount() {
            return textViewValues.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

    }

}
