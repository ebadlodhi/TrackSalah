package outofbox.ltd.administrator.tracksalah.POJO2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Month_ {

    @SerializedName("number")
    @Expose
    private int number;
    @SerializedName("en")
    @Expose
    private String en;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getEn() {
        return en;
    }

    public void setEn(String en) {
        this.en = en;
    }


}
