package outofbox.ltd.administrator.tracksalah;

import android.content.Context;
import android.content.Intent;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.List;
import java.util.Locale;

import outofbox.ltd.administrator.tracksalah.POJO2.Example;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QiblaDirectionActivity extends AppCompatActivity implements SensorEventListener,LocationListener {

    private ImageView image , image2;
    private Tracker mTracker;
    LocationManager locationManager;
    public static Double latti = 52.4771894, longi = 2.0037147,altti;
    TextView tvHeading;

    private float currentDegree = 0f;
    private float currentDegreeNeedle = 0f;
    Context context;
    Location userLoc=new Location("service Provider");
    // device sensor manager
    private static SensorManager mSensorManager ;
    private Sensor sensor;

    private AdView mAdView;
    RotateAnimation ra, ra1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qibla_direction);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        image = (ImageView) findViewById(R.id.compass);
        image2 = (ImageView) findViewById(R.id.uperimg);
        tvHeading = (TextView) findViewById(R.id.qiblatext);
        this.setTitle("Qibla Location");


try {
    userLoc.setLongitude(SplashActivity.longi);
    userLoc.setLatitude(SplashActivity.latti);
    userLoc.setAltitude(SplashActivity.altti);
    AnalyticsApplication application = (AnalyticsApplication) getApplication();
    mTracker = application.getDefaultTracker();
    mTracker.setScreenName("Qibla Direction");
    mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    mTracker.send(new HitBuilders.EventBuilder()
            .setCategory("TrackSalahUser")
            .setAction("User id")
            .setLabel(DashboardActivity.Userid)
            .setValue(1)
            .build());
    mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
    sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
    if (sensor != null) {
        // for the system's orientation sensor registered listeners
        mSensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_GAME);//SensorManager.SENSOR_DELAY_Fastest
    } else {
//        Toast.makeText(context, "Not Supported", Toast.LENGTH_SHORT).show();
        tvHeading.setText("Your device doesn't contain Compass Sensor  ");
    }
    // initialize your android device sensor capabilities
    this.context = context;
    mAdView = (AdView) findViewById(R.id.adView);
    MobileAds.initialize(this, "ca-app-pub-2825367626708661~7620129751");
    AdRequest adRequest = new AdRequest.Builder().build();
    mAdView.loadAd(adRequest);
    mAdView.setAdListener(new AdListener() {
        @Override
        public void onAdLoaded() {
            super.onAdLoaded();
            mAdView.setVisibility(View.VISIBLE);
        }
    });
}
catch (Exception e)
{
    getLocation();
}
    }


    void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, (LocationListener) this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        latti = location.getLatitude();
        longi = location.getLongitude();
        altti = location.getAltitude();
        userLoc.setLongitude(longi);
        userLoc.setLatitude(latti);
        userLoc.setAltitude(altti);
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Qibla Direction");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("TrackSalahUser")
                .setAction("User id")
                .setLabel(DashboardActivity.Userid)
                .setValue(1)
                .build());
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
        if (sensor != null) {
            // for the system's orientation sensor registered listeners
            mSensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_GAME);//SensorManager.SENSOR_DELAY_Fastest
        } else {
            //Toast.makeText(context, "Not Supported", Toast.LENGTH_SHORT).show();
            tvHeading.setText("Your device doesn't contain Compass Sensor  ");
        }
        // initialize your android device sensor capabilities
        this.context = context;
        mAdView = (AdView) findViewById(R.id.adView);
        MobileAds.initialize(this, "ca-app-pub-2825367626708661~7620129751");
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdView.setVisibility(View.VISIBLE);
            }
        });



        //Toast.makeText(getApplicationContext(),"Latitude: " + location.getLatitude() + "\n Longitude: " + location.getLongitude(),Toast.LENGTH_SHORT).show();

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            latti = location.getLatitude();
            longi = location.getLongitude();

        } catch (Exception e) {

        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(getApplicationContext(), "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();


    }








    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
    @Override
    protected void onResume() {
        super.onResume();
try {
    mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION), SensorManager.SENSOR_DELAY_GAME);

}
catch (NullPointerException e)
{
   // Toast.makeText(getApplicationContext(), "Sensor not available kindly restart you device", Toast.LENGTH_SHORT).show();
//    Intent intent = new Intent(getApplicationContext(),DashboardActivity.class);
//    startActivity(intent);
//    finish();
    tvHeading.setText("Your device doesn't contain Compass Sensor  ");

}
    }
    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }
    @Override
    protected void onPause() {
        super.onPause();
//        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION), SensorManager.SENSOR_DELAY_GAME);
//        mSensorManager.unregisterListener(this);
    }
    @Override
    public void onSensorChanged(SensorEvent event) {

        Location destinationLoc = new Location("service Provider");


        destinationLoc.setLatitude(21.4224829); //kaaba latitude setting
        destinationLoc.setLongitude(39.8240889); //kaaba longitude setting
        float bearTo=userLoc.bearingTo(destinationLoc);


        GeomagneticField geoField = new GeomagneticField( Double.valueOf( userLoc.getLatitude() ).floatValue(), Double.valueOf( userLoc.getLongitude() ).floatValue(),
                Double.valueOf( userLoc.getAltitude() ).floatValue(),
                System.currentTimeMillis() );
        float head = - geoField.getDeclination(); // converts magnetic north into true north

        if (bearTo < 0) {
            bearTo = bearTo + 360;
            //bearTo = -100 + 360  = 260;
        }

        float direction = bearTo - head;

        if (direction < 0) {
            direction = direction + 360;
        }
        float degree = Math.round(event.values[0]);
       // tvHeading.setText("Heading: " + Float.toString(direction)+" "+Float.toString(head) + " degrees" );
        RotateAnimation raQibla = new RotateAnimation(currentDegreeNeedle, direction, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        raQibla.setDuration(210);
        raQibla.setFillAfter(true);
        image.startAnimation(raQibla);
        currentDegreeNeedle =(direction + currentDegree ) ;
        direction = (int) direction;
        tvHeading.setText(direction+"° N");
        //tvHeading.setText("Qibla Direction From Your Location: " + Float.toString(direction)+"\nHeading towards "+Float.toString(degree) + " Degrees.");
        RotateAnimation ra = new RotateAnimation(currentDegree, -degree, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

// how long the animation will take place
        ra.setDuration(210);


// set the animation after the end of the reservation status
        ra.setFillAfter(true);

// Start the animation
        image2.startAnimation(ra);

        currentDegree = -degree;
//        // get the angle around the z-axis rotated
//        float degree = Math.round(event.values[0]);
//        tvHeading.setText("Heading: " + Float.toString(degree) + " degrees");
//        // create a rotation animation (reverse turn degree degrees)
//        ra = new RotateAnimation(currentDegree, -110, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
//                0.5f);
//        ra1 = new RotateAnimation(currentDegree, -320, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
//                0.5f);
//        ra.setDuration(210);
//        ra1.setDuration(210);
//        // set the animation after the end of the reservation status
//        ra.setFillAfter(true);
//        ra1.setFillAfter(true);
//        // Start the animation
//        image.startAnimation(ra);
//        image2.startAnimation(ra1);
//        currentDegree = -degree;
    }
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}