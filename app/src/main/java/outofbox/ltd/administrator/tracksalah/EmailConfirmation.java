package outofbox.ltd.administrator.tracksalah;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;

import outofbox.ltd.administrator.tracksalah.POJOuserid.Example;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.ConfirmCodeApi;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.PendingApi;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.RegisterApi;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.Registeronsignupapi;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.ResendCodeApi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static outofbox.ltd.administrator.tracksalah.LoginActivity.loginpref;

public class EmailConfirmation extends AppCompatActivity {
    TextView datepicker;
    private TextView tvDisplayDate,namazfarzdate;
    private DatePicker dpResult;
    private Button btnChangeDate;
    private PopupWindow mPopupWindow;
    Date Currenttime;
    String currentDateandTime;
    Date mydate = null;
    Date newdate = null;
    String danew="";
    long longdate;
    String tenyear = "";
    String tenyearServer = "";
    static final int DATE_DIALOG_ID = 999;
    private int myear,mfarzyear;
    private int mmonth,mfarzmonth;
    private int mday,mfarzday;
    String user_name = "", user_email = "", user_pass = "";
    TextInputEditText Name, Email, Password;
    Button button;
    String popUpContents[];
    PopupWindow popupWindowDogs;
    EditText buttonShowDropDown;
    String Gender = "Female";
    public RadioGroup radioGroup;
    public RadioButton radioButton;
    boolean flag=true;
    TextView dateofb;
    TextView reg;
    ProgressBar progressBar;
    private Tracker mTracker;
    int day;
    Calendar myCalendar;
    DatePickerDialog datePickerDialog;
    Date startdate = null;
    Date startnewdate = null;
    String Nstartdate = "";
    long diff;
    int Days,TotalQadahNamaz,Qadahfajar,QadahZohar,QadahAsar,Qadahmaghrib,Qadahisha = 0;
    List<Example> useridlatest = new ArrayList<>();
    String uname,upass,uemail;
    String username="";
    String userpassword="";
    String useremail = "";
    String UserDOB = "a";
    String namazfarazdate = "";
    String UserDOBSeerver = "";

    String Creationtime = "";

    String nfarzdate = "";

    String nstartdate = "";

    String mstartdate = "0";

    String menddate = "0";

    public static SharedPreferences sharedpreferences;

    public static final String ROOT_URL = "http://185.123.99.207/~android215/TrackSalah/";
    @SuppressLint("ClickableViewAccessibility")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_confirmation);
        getSupportActionBar().hide();
        Name = (TextInputEditText) findViewById(R.id.full_name);
        Email = (TextInputEditText) findViewById(R.id.email);
        Password = (TextInputEditText) findViewById(R.id.password);
        namazfarzdate = findViewById(R.id.textView45);
        reg = findViewById(R.id.register);
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Email Confirmation");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("TrackSalahUser")
                .setAction("User id")
                .setLabel(DashboardActivity.Userid)
                .setValue(1)
                .build());
        sharedpreferences = getSharedPreferences(loginpref, Context.MODE_PRIVATE);
        dpResult = (DatePicker) findViewById(R.id.dpResult1);
        progressBar = findViewById(R.id.progressBar2);
        progressBar.setVisibility(View.GONE);
            dateofb  = findViewById(R.id.textView43);
        setCurrentDateOnView();
            dateofb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDialog(111);
                }
            });


        Spannable word = new SpannableString(" Log in");
        word.setSpan(new ForegroundColorSpan(Color.rgb(212,92,84)), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        reg.append(word);
        button=(Button)findViewById(R.id.button_sign_in);
        //fetchEmail();
//        Email.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                final int DRAWABLE_RIGHT = 2;
//                if(event.getAction() == MotionEvent.ACTION_UP) {
//                    if(event.getRawX() >= (Email.getRight() - Email.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
//                        // your action here
//                        popupWindowDogs.showAsDropDown(v, -5, 0);
//                        return true;
//                    }
//                }
//                return false;
//            }
//        });

        namazfarzdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCurrentDateOnViewfarz();
                addListenerOnButtonfarz();
                // Toast.makeText(SignupActivity.this, "hahha", Toast.LENGTH_SHORT).show();
            }
        });


    }
    public void addListenerOnButtonfarz() {

                showDialog(DATE_DIALOG_ID);

    }
    public void setCurrentDateOnViewfarz() {


        namazfarzdate = findViewById(R.id.textView45);
        dpResult = (DatePicker) findViewById(R.id.dpResult1);
        dpResult.setVisibility(View.GONE);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy");
        try {
            mydate = simpleDateFormat.parse(dateofb.getText().toString());
            c.setTime(mydate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        currentDateandTime = sdf.format(new Date());
        myear = c.get(Calendar.YEAR);
        mmonth = c.get(Calendar.MONTH);
        mday = c.get(Calendar.DAY_OF_MONTH);

        // set current date into textview
//        tvDisplayDate.setText(new StringBuilder()
//                // Month is 0 based, just add 1
//                .append(myear).append("-") .append(mmonth + 1).append("-").append(mday)
//        );

        // set current date into datepicker
        dpResult.init(myear, mmonth, mday, null);

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {

            case DATE_DIALOG_ID: {
                DatePickerDialog _datefarz = new DatePickerDialog(this, datePickerListener, myear, mmonth,
                        mday) {
                    @Override
                    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                        if (year > myear) {
//                            Toast.makeText(getApplicationContext(), "Year can't be greater then current Year", Toast.LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }
//                        if (monthOfYear > mmonth && year == myear) {
//                            Toast.makeText(getApplicationContext(), "Month can't be greater then current Month", Toast.LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }
//                        if (dayOfMonth > mday && year == myear && monthOfYear == mmonth) {
//                            Toast.makeText(getApplicationContext(), "Date can't be greater then current date", Toast.LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }

                    }
                };
                return _datefarz;
            }


            case 111: {
                DatePickerDialog _datefarz = new DatePickerDialog(this, datePickerListenerfarz, myear, mmonth,
                        mday) {
                    @Override
                    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                        if (year > myear) {
//                            Toast.makeText(getApplicationContext(), "Year can't be greater then current Year", Toast.LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }
//                        if (monthOfYear > mmonth && year == myear) {
//                            Toast.makeText(getApplicationContext(), "Month can't be greater then current Month", Toast.LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }
//                        if (dayOfMonth > mday && year == myear && monthOfYear == mmonth) {
//                            Toast.makeText(getApplicationContext(), "Date can't be greater then current date", Toast.LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }
                    }
                };
                return _datefarz;
            }

        }


        return null;
    }
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            myear = selectedYear;
            mmonth = selectedMonth;
            mday = selectedDay;
            String sMonth = "";
            // set selected date into textview
            if (mmonth == 0)
                sMonth = "Jan";
            else if (mmonth == 1)
                sMonth = "Feb";
            else if (mmonth == 2)
                sMonth = "Mar";
            else if (mmonth == 3)
                sMonth = "Apr";
            else if (mmonth == 4)
                sMonth = "May";
            else if (mmonth == 5)
                sMonth = "Jun";
            else if (mmonth == 6)
                sMonth = "Jul";
            else if (mmonth == 7)
                sMonth = "Aug";
            else if (mmonth == 8)
                sMonth = "Sep";
            else if (mmonth == 9)
                sMonth = "Oct";
            else if (mmonth == 10)
                sMonth = "Nov";
            else if (mmonth == 11)
                sMonth = "Dec";
            namazfarzdate.setText(new StringBuilder()
                    .append(mday).append(" ") .append(sMonth).append(" ").append(myear).append(" "));
            // set selected date into datepicker also
            dpResult.init(myear, mmonth, mday, null);

//            try {
//                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy");
//                SimpleDateFormat simpleDateFormattenyear = new SimpleDateFormat("yyyy-MM-dd");
//                mydate = simpleDateFormat.parse(dateofb.getText().toString());
//                Calendar c = Calendar.getInstance();
//                c.setTime(mydate);
//                c.add(Calendar.YEAR, 14);
//                newdate = c.getTime();
//                longdate = newdate.getTime();
//                tenyear = simpleDateFormat.format(newdate);
//                tenyearServer = simpleDateFormattenyear.format(newdate);
//                namazfarzdate.setText(tenyear);
//                mfarzyear = Calendar.YEAR;
//                mfarzmonth = Calendar.MONTH;
//                mfarzday = Calendar.DAY_OF_MONTH;
//
//
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }


        }
    };
    public void setCurrentDateOnView() {



        final Calendar c = Calendar.getInstance();
        Currenttime = c.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        currentDateandTime = sdf.format(new Date());
        myear = c.get(Calendar.YEAR);
        mmonth = c.get(Calendar.MONTH);
        mday = c.get(Calendar.DAY_OF_MONTH);

        // set current date into textview
//        tvDisplayDate.setText(new StringBuilder()
//                // Month is 0 based, just add 1
//                .append(myear).append("-") .append(mmonth + 1).append("-").append(mday)
//        );

        // set current date into datepicker
        dpResult.init(myear, mmonth, mday, null);

    }

    private DatePickerDialog.OnDateSetListener datePickerListenerfarz = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            myear = selectedYear;
            mmonth = selectedMonth;
            mday = selectedDay;
            String sMonth = "";
            // set selected date into textview
            if (mmonth == 0)
                sMonth = "Jan";
            else if (mmonth == 1)
                sMonth = "Feb";
            else if (mmonth == 2)
                sMonth = "Mar";
            else if (mmonth == 3)
                sMonth = "Apr";
            else if (mmonth == 4)
                sMonth = "May";
            else if (mmonth == 5)
                sMonth = "Jun";
            else if (mmonth == 6)
                sMonth = "Jul";
            else if (mmonth == 7)
                sMonth = "Aug";
            else if (mmonth == 8)
                sMonth = "Sep";
            else if (mmonth == 9)
                sMonth = "Oct";
            else if (mmonth == 10)
                sMonth = "Nov";
            else if (mmonth == 11)
                sMonth = "Dec";
            dateofb.setText(new StringBuilder()
                    .append(mday).append(" ") .append(sMonth).append(" ").append(myear).append(" "));
            // set selected date into datepicker also
            dpResult.init(myear, mmonth, mday, null);

            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy");
                SimpleDateFormat simpleDateFormattenyear = new SimpleDateFormat("yyyy-MM-dd");
                mydate = simpleDateFormat.parse(dateofb.getText().toString());
                Calendar c = Calendar.getInstance();
                c.setTime(mydate);
                c.add(Calendar.YEAR, 14);
                newdate = c.getTime();
                longdate = newdate.getTime();
                tenyear = simpleDateFormat.format(newdate);
                tenyearServer = simpleDateFormattenyear.format(newdate);
                namazfarzdate.setText(tenyear);
                mfarzyear = Calendar.YEAR;
                mfarzmonth = Calendar.MONTH;
                mfarzday = Calendar.DAY_OF_MONTH;


            } catch (ParseException e) {
                e.printStackTrace();
            }



        }
    };






    public void takePermission()
    {
        int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 0;
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.GET_ACCOUNTS)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.GET_ACCOUNTS)) {

            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.GET_ACCOUNTS},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            }
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
//    public void fetchEmail()
//    {
//        takePermission();
//        //String possibleEmail="";
//        try{
//            Account[] accounts = AccountManager.get(this).getAccountsByType("com.google");
//            List<String> dogsList = new ArrayList<>();
//            for(Account account: accounts)
//            {
//                dogsList.add(account.name);
//            }
////            possibleEmail= accounts[0].name;
////            Email.setText(possibleEmail);
//            // convert to simple array
//            popUpContents = new String[dogsList.size()];
//            dogsList.toArray(popUpContents);
//            // initialize pop up window
//            popupWindowDogs = popupWindowDogs();
//
////            View.OnClickListener handler = new View.OnClickListener() {
////                public void onClick(View v) {
////
////                    switch (v.getId()) {
////                        case R.id.email:
////                            // show the list view as dropdown
////                            popupWindowDogs.showAsDropDown(v, -5, 0);
////
////                            break;
////                    }
////                }
////            };
//
//            // our button
//            buttonShowDropDown = (EditText) findViewById(R.id.email);
//            //buttonShowDropDown.setOnClickListener(handler);
//
//        }
//        catch (Exception e)
//        {
//            Toast.makeText(this, e.getMessage().toString(), Toast.LENGTH_SHORT).show();
//        }
//    }
//    @RequiresApi(api = Build.VERSION_CODES.M)
//    public PopupWindow popupWindowDogs() {
//
//        // initialize a pop up window type
//        PopupWindow popupWindow = new PopupWindow(this);
//
//        // the drop down list is a list view
//        ListView listViewDogs = new ListView(this);
//
//        // set our adapter and pass our pop up window contents
//        listViewDogs.setAdapter(dogsAdapter(popUpContents));
//        listViewDogs.setBackgroundColor(getColor(R.color.colorAccent));
//        // set the item click listener
//        listViewDogs.setOnItemClickListener(new DogsDropdownOnItemClickListener());
//
//        // some other visual settings
//        popupWindow.setFocusable(true);
//
//        popupWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
//        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
//
//        // set the list view as pop up window content
//        popupWindow.setContentView(listViewDogs);
//
//        return popupWindow;
//    }

    private ArrayAdapter<String> dogsAdapter(String dogsArray[]) {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, dogsArray) {

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                // setting the ID and text for every items in the list
                String item = getItem(position);
                String[] itemArr = item.split("::");
                String text = itemArr[0];
                //String id = itemArr[1];

                // visual settings for the list item
                TextView listItem = new TextView(EmailConfirmation.this);

                listItem.setText(text);
                //listItem.setTag(id);
                listItem.setTextSize(15);
                listItem.setPadding(10, 10, 10, 10);
                listItem.setTextColor(Color.BLACK);
                return listItem;
            }
        };

        return adapter;
    }
    public void gotoLogin(View view)
    {
        Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
    public void Confirm(View view)
    {

        progressBar.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        user_name = Name.getText().toString();
        user_email = Email.getText().toString();
        user_pass = Password.getText().toString();
        flag=true;
        if(user_name.isEmpty())
        {
            Name.setError("Please provide a user name");
            Name.requestFocus();
            flag=false;
        }
        if(user_email.isEmpty())
        {
            Email.setError("Please provide an email");
            Email.requestFocus();
            flag=false;
        }
        if(user_pass.isEmpty())
        {
            Password.setError("Please provide a password");
            Password.requestFocus();
            flag=false;
        }
        else if(!Patterns.EMAIL_ADDRESS.matcher(user_email).matches())
        {
            Email.setError("Invalid email");
            Email.requestFocus();
            flag=false;
        }
        else if(dateofb.getText().toString().isEmpty())
        {
            dateofb.setError("Enter date of birth");
            Email.requestFocus();
            flag=false;
        }
        if(flag)
        {
            progressBar.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            radioGroup = (RadioGroup) findViewById(R.id.radiogenderemailcon);
            int selectedId = radioGroup.getCheckedRadioButtonId();
            radioButton = (RadioButton) findViewById(selectedId);
            Gender = (String) radioButton.getText().toString();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ROOT_URL) //Setting the Root URL
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            PendingApi api =retrofit.create(PendingApi.class);
            Call<String> call = api.getDetails(user_name,user_email,user_pass,Gender);
            call.enqueue(new Callback<String>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    String text= response.body().toString();
                    if(text.equals("Already Registered"))
                    {

                        progressBar.setVisibility(View.GONE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        Toast.makeText(EmailConfirmation.this, "Email already registered", Toast.LENGTH_SHORT).show();
//                        Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(intent);
//                        finish();
                    }
                    else if(text.equals("Already Applied"))
                    {

                        progressBar.setVisibility(View.GONE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        Toast.makeText(EmailConfirmation.this, "Already applied. Kindly check your mail", Toast.LENGTH_SHORT).show();
                        showDialog();
                    }
                    else if(text.equals("Pending")) {

                        progressBar.setVisibility(View.GONE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        Toast.makeText(EmailConfirmation.this, "Your request is forwarded", Toast.LENGTH_SHORT).show();
                        showDialog();
                    }
                    else


                    {
                       // Toast.makeText(EmailConfirmation.this, text, Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Toast.makeText(EmailConfirmation.this, "Please try again!", Toast.LENGTH_SHORT).show();
                }
            });
        }
        progressBar.setVisibility(View.GONE);
    }

    public void Resend(View view)
    {
        user_email = Email.getText().toString();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ResendCodeApi api =retrofit.create(ResendCodeApi.class);
        Call<String> call = api.getDetails(user_email);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Toast.makeText(getApplicationContext(), "Resent Code", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Server Busy", Toast.LENGTH_SHORT).show();
            }
        });
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void showDialog()
    {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_code_enter_popup, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.editText);

        dialogBuilder.setTitle("Email Verification");
        dialogBuilder.setMessage("Enter code below");
        dialogBuilder.setCancelable(false);
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String C = edt.getText().toString();
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(ROOT_URL) //Setting the Root URL
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                ConfirmCodeApi api =retrofit.create(ConfirmCodeApi.class);
                Call<String> call = api.getDetails( user_name,user_email,user_pass,C);
                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                       // Toast.makeText(getApplicationContext(), response.body().toString(), Toast.LENGTH_SHORT).show();
                        if(response.body().toString().equals("Error"))
                        {
                            Toast.makeText(EmailConfirmation.this, "Wrong Code", Toast.LENGTH_LONG).show();
                            showDialog();
                        }
                        else if(response.body().toString().equals("Code confirmed"))
                        {
                            register();
//                            radioGroup = (RadioGroup) findViewById(R.id.radiogenderemailcon);
//                            int selectedId = radioGroup.getCheckedRadioButtonId();
//                            radioButton = (RadioButton) findViewById(selectedId);
//                            Gender = (String) radioButton.getText().toString();
//                            Intent intent = new Intent(getApplicationContext(),SignupActivity.class);
//                            intent.putExtra("name",user_name);
//                            intent.putExtra("email",user_email);
//                            intent.putExtra("password",user_pass);
//                            intent.putExtra("Gender",Gender);
//
//                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Server Busy", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }


    public void register()
    {
        user_name = Name.getText().toString();
        user_email = Email.getText().toString();
        user_pass = Password.getText().toString();
        UserDOBSeerver = dateofb.getText().toString();
        nfarzdate = namazfarzdate.getText().toString();
        SimpleDateFormat simpleDateFormat11 = new SimpleDateFormat("dd MMM yyyy");
        SimpleDateFormat simpleDateFormat22 = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        Date nfarzdatee = null;

        try {
            date = simpleDateFormat11.parse(UserDOBSeerver);
            nfarzdatee = simpleDateFormat11.parse(nfarzdate);
            UserDOB = simpleDateFormat22.format(date);
            namazfarazdate = simpleDateFormat22.format(nfarzdatee);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        SharedPreferences.Editor editor3 = sharedpreferences.edit();
editor3.putString("preuserfarzdate",namazfarazdate);
editor3.commit();
editor3.apply();
        if(UserDOB.equals("a"))
        {
            Toast.makeText(this, "Enter Your Date Of Birth First", Toast.LENGTH_SHORT).show();
            dateofb.requestFocus();
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }


        else {
            username = user_name.toString();
            userpassword = user_pass.toString();
            useremail = user_email.toString();
            Calendar calendar1 = Calendar.getInstance();

            SimpleDateFormat simpleDateFormatctime = new SimpleDateFormat("yyyy-MM-dd");

            Date ss = calendar1.getTime();


            String cdate = simpleDateFormatctime.format(ss);
            Creationtime = cdate.toString()+" "+currentDateandTime;

           // nfarzdate = namazfarzdate.getText().toString().trim();
            nstartdate = "null";
            mstartdate = "-1";
            menddate = "-1";
            // Toast.makeText(this, ""+nstartdate, Toast.LENGTH_SHORT).show();

            progressBar.setVisibility(View.VISIBLE);

            if(Gender.equals("Male")) {
                SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd-MM-yyyy");
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");



                //            startdate = simpleDateFormat.parse(nstartdate.toString());

//            calendar.setTime(startdate);
//                startnewdate = calendar1.getTime();
//                //Date changedate = simpleDateFormat.format(startnewdate);
//                cdate = simpleDateFormat.format(startnewdate);
//                Nstartdate = simpleDateFormat2.format(startnewdate);
//                //  Toast.makeText(this, "farz date = "+tenyear+"\nstart date = "+cdate, Toast.LENGTH_LONG).show();
////        List<String> dates = getDates( tenyear.toString().trim(),cdate.toString().trim());
////        for(String date:dates)
////            Log.d("date",date.toString());
//
//                diff = startnewdate.getTime() - newdate.getTime();
//                Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                TotalQadahNamaz = Days * 5;
//                Qadahfajar = TotalQadahNamaz / 5;
//                QadahZohar = TotalQadahNamaz / 5;
//                QadahAsar = TotalQadahNamaz / 5;
//                Qadahmaghrib = TotalQadahNamaz / 5;
//                Qadahisha = TotalQadahNamaz / 5;
//
//
//
//                Gson gson = new Gson();
//        String dtlist = gson.toJson(dates);
//        Log.d("array date",dtlist);
                //  Toast.makeText(this, TotalQadahNamaz+"\n"+Qadahfajar+"\n"+QadahZohar+"\n"+QadahAsar+"\n"+Qadahmaghrib+"\n"+Qadahisha, Toast.LENGTH_SHORT).show();
                // Toast.makeText(this, TotalQadahNamaz+"\n"+Qadahfajar+"\n"+QadahZohar+"\n"+QadahAsar+"\n"+Qadahmaghrib+"\n"+Qadahisha, Toast.LENGTH_SHORT).show();
                //  CalculateQadah(uid, cdate, TotalQadahNamaz, TotalQadahNamaz, Qadahfajar, QadahZohar, QadahAsar, Qadahmaghrib, Qadahisha,tenyear);

                    progressBar.setVisibility(View.GONE);

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(ROOT_URL) //Setting the Root URL
                            .addConverterFactory(ScalarsConverterFactory.create())
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                Registeronsignupapi api = retrofit.create(Registeronsignupapi.class);
                    Call<String> call = api.getDetails(username, userpassword, useremail, Gender, UserDOB, Creationtime, "null", namazfarazdate,"0","-1");
                call.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            Log.i("Response", response.body().toString());
                            progressBar.setVisibility(View.GONE);
                            if (response.isSuccessful()) {

                                if (response.body() != null) {
                                    String ret = response.body().toString().trim();
                                   // Toast.makeText(EmailConfirmation.this, ""+ret, Toast.LENGTH_SHORT).show();

                                    SharedPreferences.Editor editor1 = sharedpreferences.edit();
                                    editor1.putString("preuid",ret);
                                    editor1.apply();
                                    editor1.commit();
                                    if (ret.contains("Server Issue")) {
                                        Toast.makeText(getApplicationContext(), "Email already registered", Toast.LENGTH_SHORT).show();
                                    } else {

                                        Toast.makeText(getApplicationContext(), "Congratulations!\nAccount Created", Toast.LENGTH_LONG).show();
                                        Intent intent = new Intent(getApplicationContext(), QadahsalahCalculateActivity.class);

                                        startActivity(intent);
                                        finish();
                                    }
                                }
                                //                    else{
                                //                        Toast.makeText(SignupActivity.this, "Server issue Kindly retry later.", Toast.LENGTH_SHORT).show(); }
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(getApplicationContext(), "Congratulations!\nAccount Created", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(getApplicationContext(), QadahsalahCalculateActivity.class);
                            startActivity(intent);
                            progressBar.setVisibility(View.GONE);
                            finish();
                        }
                    });






            }
            else
            {
                SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd-MM-yyyy");
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");



                //            startdate = simpleDateFormat.parse(nstartdate.toString());

//            calendar.setTime(startdate);
//                startnewdate = calendar1.getTime();
//                //Date changedate = simpleDateFormat.format(startnewdate);
//                cdate = simpleDateFormat.format(startnewdate);
//                Nstartdate = simpleDateFormat2.format(startnewdate);
//                //  Toast.makeText(this, "farz date = "+tenyear+"\nstart date = "+cdate, Toast.LENGTH_LONG).show();
////        List<String> dates = getDates( tenyear.toString().trim(),cdate.toString().trim());
////        for(String date:dates)
////            Log.d("date",date.toString());
//
//                diff = startnewdate.getTime() - newdate.getTime();
//                Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                TotalQadahNamaz = Days * 5;
//                Qadahfajar = TotalQadahNamaz / 5;
//                QadahZohar = TotalQadahNamaz / 5;
//                QadahAsar = TotalQadahNamaz / 5;
//                Qadahmaghrib = TotalQadahNamaz / 5;
//                Qadahisha = TotalQadahNamaz / 5;
//
//
//
//                Gson gson = new Gson();
//        String dtlist = gson.toJson(dates);
//        Log.d("array date",dtlist);
                //  Toast.makeText(this, TotalQadahNamaz+"\n"+Qadahfajar+"\n"+QadahZohar+"\n"+QadahAsar+"\n"+Qadahmaghrib+"\n"+Qadahisha, Toast.LENGTH_SHORT).show();
                // Toast.makeText(this, TotalQadahNamaz+"\n"+Qadahfajar+"\n"+QadahZohar+"\n"+QadahAsar+"\n"+Qadahmaghrib+"\n"+Qadahisha, Toast.LENGTH_SHORT).show();
                //  CalculateQadah(uid, cdate, TotalQadahNamaz, TotalQadahNamaz, Qadahfajar, QadahZohar, QadahAsar, Qadahmaghrib, Qadahisha,tenyear);

                progressBar.setVisibility(View.GONE);

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(ROOT_URL) //Setting the Root URL
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                Registeronsignupapi api = retrofit.create(Registeronsignupapi.class);
                Call<String> call = api.getDetails(username, userpassword, useremail, Gender, UserDOB, Creationtime, "null", namazfarazdate,"-1","-1");
                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        Log.i("Response", response.body().toString());
                        progressBar.setVisibility(View.GONE);
                        if (response.isSuccessful()) {

                            if (response.body() != null) {
                                String ret = response.body().toString().trim();
                               // Toast.makeText(EmailConfirmation.this, ""+ret, Toast.LENGTH_SHORT).show();
                                SharedPreferences.Editor editor1 = sharedpreferences.edit();
                                editor1.putString("preuid",ret);
                                editor1.apply();
                                editor1.commit();
                                if (ret.contains("Server Issue")) {
                                    Toast.makeText(getApplicationContext(), "Email already registered", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getApplicationContext(), "Congratulations!\nAccount Created", Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(getApplicationContext(), MenscalculateActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                            //                    else{
                            //                        Toast.makeText(SignupActivity.this, "Server issue Kindly retry later.", Toast.LENGTH_SHORT).show(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), "Congratulations!\nAccount Created", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(), MenscalculateActivity.class);
                        startActivity(intent);
                        progressBar.setVisibility(View.GONE);
                        finish();
                    }
                });




            }


            }


        }
    }


class DogsDropdownOnItemClickListener implements AdapterView.OnItemClickListener {
    @Override
    public void onItemClick(AdapterView<?> arg0, View v, int arg2, long arg3) {
        Context mContext = v.getContext();
        EmailConfirmation emailConfirmation = (EmailConfirmation) mContext;
        Animation fadeInAnimation = AnimationUtils.loadAnimation(v.getContext(), android.R.anim.fade_in);
        fadeInAnimation.setDuration(10);
        v.startAnimation(fadeInAnimation);
        emailConfirmation.popupWindowDogs.dismiss();
        String selectedItemText = ((TextView) v).getText().toString();
        emailConfirmation.buttonShowDropDown.setText(selectedItemText);
    }

}



