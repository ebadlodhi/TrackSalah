package outofbox.ltd.administrator.tracksalah.POJO3;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class _0 {

    @SerializedName("User_id")
    @Expose
    private String userId;
    @SerializedName("User_name")
    @Expose
    private String userName;
    @SerializedName("User_email")
    @Expose
    private String userEmail;
    @SerializedName("User_password")
    @Expose
    private String userPassword;
    @SerializedName("User_gender")
    @Expose
    private String userGender;
    @SerializedName("User_DOB")
    @Expose
    private String userDOB;
    @SerializedName("NamazFarzDate")
    @Expose
    private String namazFarzDate;
    @SerializedName("NamazStartDate")
    @Expose
    private String namazStartDate;
    @SerializedName("Mdetails")
    @Expose
    private String mdetails;
    @SerializedName("Qadahdetails")
    @Expose
    private String qadahdetails;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserGender() {
        return userGender;
    }

    public void setUserGender(String userGender) {
        this.userGender = userGender;
    }

    public String getUserDOB() {
        return userDOB;
    }

    public void setUserDOB(String userDOB) {
        this.userDOB = userDOB;
    }

    public String getNamazFarzDate() {
        return namazFarzDate;
    }

    public void setNamazFarzDate(String namazFarzDate) {
        this.namazFarzDate = namazFarzDate;
    }

    public String getNamazStartDate() {
        return namazStartDate;
    }

    public void setNamazStartDate(String namazStartDate) {
        this.namazStartDate = namazStartDate;
    }

    public String getMdetails() {
        return mdetails;
    }

    public void setMdetails(String mdetails) {
        this.mdetails = mdetails;
    }

    public String getQadahdetails() {
        return qadahdetails;
    }

    public void setQadahdetails(String qadahdetails) {
        this.qadahdetails = qadahdetails;
    }


}
