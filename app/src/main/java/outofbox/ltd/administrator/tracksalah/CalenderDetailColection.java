package outofbox.ltd.administrator.tracksalah;

import java.util.ArrayList;

public class CalenderDetailColection {
    public String date;
    public String event_message="";
    public int namaz_count;


    public static ArrayList<CalenderDetailColection> date_collection_arr1 = new ArrayList<CalenderDetailColection>(1);
    public CalenderDetailColection(String date, String event_message,int namaz_count){

        this.date=date;
        this.event_message=event_message;
        this.namaz_count=namaz_count;
    }
}
