package outofbox.ltd.administrator.tracksalah.POJOQadahCheck;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class _0 {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("QadahID")
    @Expose
    private String qadahID;
    @SerializedName("User_id")
    @Expose
    private String userId;
    @SerializedName("NamazStartDate")
    @Expose
    private String namazStartDate;
    @SerializedName("TotalQadahNamaz")
    @Expose
    private String totalQadahNamaz;
    @SerializedName("RemainingQadahNamaz")
    @Expose
    private String remainingQadahNamaz;
    @SerializedName("Namazfajar")
    @Expose
    private String namazfajar;
    @SerializedName("NamazZohar")
    @Expose
    private String namazZohar;
    @SerializedName("NamazAsar")
    @Expose
    private String namazAsar;
    @SerializedName("NamazMaghrib")
    @Expose
    private String namazMaghrib;
    @SerializedName("NamazIsha")
    @Expose
    private String namazIsha;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getQadahID() {
        return qadahID;
    }

    public void setQadahID(String qadahID) {
        this.qadahID = qadahID;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNamazStartDate() {
        return namazStartDate;
    }

    public void setNamazStartDate(String namazStartDate) {
        this.namazStartDate = namazStartDate;
    }

    public String getTotalQadahNamaz() {
        return totalQadahNamaz;
    }

    public void setTotalQadahNamaz(String totalQadahNamaz) {
        this.totalQadahNamaz = totalQadahNamaz;
    }

    public String getRemainingQadahNamaz() {
        return remainingQadahNamaz;
    }

    public void setRemainingQadahNamaz(String remainingQadahNamaz) {
        this.remainingQadahNamaz = remainingQadahNamaz;
    }

    public String getNamazfajar() {
        return namazfajar;
    }

    public void setNamazfajar(String namazfajar) {
        this.namazfajar = namazfajar;
    }

    public String getNamazZohar() {
        return namazZohar;
    }

    public void setNamazZohar(String namazZohar) {
        this.namazZohar = namazZohar;
    }

    public String getNamazAsar() {
        return namazAsar;
    }

    public void setNamazAsar(String namazAsar) {
        this.namazAsar = namazAsar;
    }

    public String getNamazMaghrib() {
        return namazMaghrib;
    }

    public void setNamazMaghrib(String namazMaghrib) {
        this.namazMaghrib = namazMaghrib;
    }

    public String getNamazIsha() {
        return namazIsha;
    }

    public void setNamazIsha(String namazIsha) {
        this.namazIsha = namazIsha;
    }
}
