package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface UpdateExempteddatsApi {

    @FormUrlEncoded
    @POST("UpdateExemptedDate.php")
    Call<String> getDetails(@Field("Start_Dateforchange") String Start_Dateforchange, @Field("Start_daynamaz") String Start_daynamaz, @Field("End_Dateforchange") String End_Dateforchange, @Field("End_daynamaz") String End_daynamaz, @Field("Start_Date") String Start_Date, @Field("End_Date") String End_Date, @Field("userid") String userid);

}
