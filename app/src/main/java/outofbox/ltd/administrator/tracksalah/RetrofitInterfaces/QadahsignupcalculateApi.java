package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface QadahsignupcalculateApi {

    @FormUrlEncoded
    @POST("qadahcalculatesignup.php")
    Call<String> getDetails(@Field("FarzDate") String FarzDate, @Field("Namazstartdate") String Namazstartdate, @Field("days") String days, @Field("cycle") String cycle
            , @Field("userid") String userid,@Field("fajar") String fajar,@Field("duhur") String duhur,@Field("asar") String asar,@Field("maghrib") String maghrib,
                            @Field("isha") String isha);
}
