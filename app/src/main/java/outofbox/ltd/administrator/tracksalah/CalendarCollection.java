package outofbox.ltd.administrator.tracksalah;

import java.util.ArrayList;

public class CalendarCollection {

    public String date;
    public String event_message="";
    public int namaz_count;


    public static ArrayList<CalendarCollection> date_collection_arr = new ArrayList<CalendarCollection>(1);
    public CalendarCollection(String date, String event_message,int namaz_count){

        this.date=date;
        this.event_message=event_message;
        this.namaz_count=namaz_count;
    }
}
