package outofbox.ltd.administrator.tracksalah;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import outofbox.ltd.administrator.tracksalah.Databasesqlite.DatabaseHelper;
import outofbox.ltd.administrator.tracksalah.Databasesqlite.MensDatabaseHelper;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.MenscalculatesignupApi;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.addexemptedcomplete;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.addexemptedcurrent;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static outofbox.ltd.administrator.tracksalah.DailySalahActivity.NAME_NOT_SYNCED_WITH_SERVER;
import static outofbox.ltd.administrator.tracksalah.LoginActivity.loginpref;

public class MensModifyActivity extends AppCompatActivity {
ConstraintLayout menseenter;
ConstraintLayout adv;
    TextView datepicker;
    private DatePicker dpResult;
    Date Currenttime;
    private Tracker mTracker;
    String currentDateandTime;
    private int myear,mfarzyear;
    private int mmonth,mfarzmonth;
    private int mday,mfarzday;
    public static SharedPreferences sharedpreferences;
    public DatabaseHelper db;
    SharedPreferences sharedPreferences;
    public  String User_id = "";
    public  String fajar = "05:27";
    public  String duhur = "12:15";
    public  String asar = "15:30";
    public  String maghrib = "18:00";
    public  String isha = "19:00";
String[] namazname = {"Fajar","Zohar","Asar","Maghrib","Isha"};


    public static SharedPreferences sharedpreferences3;
    public static  String  mensswitchstate = "";
    public static final String ROOT_URL = "http://185.123.99.207/~android215/TrackSalah/";
    EditText cycle , noofdays;
   public static Switch Mensstartswitch;
    int uid ,status;
    String tdate,ttime;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mens_modify);
        db = new DatabaseHelper(getApplicationContext());
        this.setTitle("Mensturation Details");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sharedpreferences3 = getSharedPreferences(mensswitchstate, Context.MODE_PRIVATE);
        menseenter = findViewById(R.id.constraintLayout57);
        adv = findViewById(R.id.button10);
            Mensstartswitch = findViewById(R.id.switch2);
//            Mensstartswitch.setClickable(false);
        dpResult = (DatePicker) findViewById(R.id.dpResult5);
        datepicker = findViewById(R.id.textView309);
        cycle = findViewById(R.id.editText290);
        noofdays = findViewById(R.id.editText390);
        sharedpreferences = getSharedPreferences(loginpref, Context.MODE_PRIVATE);
        setCurrentDateOnView();
        datepicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(111);
            }
        });
        sharedPreferences = getSharedPreferences(LoginActivity.loginpref, Context.MODE_PRIVATE);


        Bundle bundle = getIntent().getExtras();
        String con = bundle.getString("condition");
//        Toast.makeText(this, "" +con, Toast.LENGTH_SHORT).show();
        if (con.equals("0"))
        {
            menseenter.setVisibility(View.GONE);
        }
        if(con.equals("1"))
        {
            adv.setVisibility(View.GONE);
        }

        adv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),MensDetailActivity.class);
                startActivity(intent);
            }
        });


        sharedpreferences3 = getSharedPreferences(mensswitchstate, Context.MODE_PRIVATE);
        boolean response = sharedpreferences3.getBoolean("enable",false);
      //  Toast.makeText(this, "aaaaaaa"+response, Toast.LENGTH_SHORT).show();
        if(response == true)
        {
            Toast.makeText(this, "Enable", Toast.LENGTH_SHORT).show();
            Mensstartswitch.setClickable(true);
            Mensstartswitch.setChecked(true);
            User_id = sharedPreferences.getString("preuid", "");
            Date current  = Calendar.getInstance().getTime();
            Calendar c = Calendar.getInstance();


            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("HH:mm", Locale.US);

            String currentdate = simpleDateFormat.format(current);
            String currenttime = simpleDateFormat1.format(current);
            try {
                c.setTime(simpleDateFormat.parse(currentdate));
                c.add(Calendar.DATE,1);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Date nextdate = c.getTime();
            Date fajrgiventime = null;
            Date duhrgiventime = null;
            Date asargiventime = null;
            Date maghribgiventime = null;
            Date ishagiventime = null;
            Date currentgiventime = null;
            String nextdaydate = simpleDateFormat.format(nextdate);
            try {
                fajrgiventime = simpleDateFormat1.parse(fajar);
                duhrgiventime = simpleDateFormat1.parse(duhur);
                asargiventime = simpleDateFormat1.parse(asar);
                maghribgiventime = simpleDateFormat1.parse(maghrib);
                ishagiventime = simpleDateFormat1.parse(isha);
                currentgiventime = simpleDateFormat1.parse(currenttime);

            } catch (ParseException e) {
                e.printStackTrace();
            }
            if(currentgiventime.after(fajrgiventime) && currentgiventime.before(duhrgiventime)) {
                for (int i = 1; i < 5; i++) {
                    String Namaztimeinsert = namazname[i];
                    String todaydateinsert = currentdate;
                    String ontimeinsert = "No";
                    String lateinsert = "No";
                    String missedinsert = "No";
                    String exemptedinsert = "Exempted";
                    db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                    // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                    registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                }
            }
            else if(currentgiventime.after(duhrgiventime) && currentgiventime.before(asargiventime)) {
                for (int i = 2; i < 5; i++) {
                    String Namaztimeinsert = namazname[i];
                    String todaydateinsert = currentdate;
                    String ontimeinsert = "No";
                    String lateinsert = "No";
                    String missedinsert = "No";
                    String exemptedinsert = "Exempted";
                    db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                    // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                    registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                }
            }
            else  if(currentgiventime.after(asargiventime) && currentgiventime.before(maghribgiventime)) {
                for (int i = 3; i < 5; i++) {
                    String Namaztimeinsert = namazname[i];
                    String todaydateinsert = currentdate;
                    String ontimeinsert = "No";
                    String lateinsert = "No";
                    String missedinsert = "No";
                    String exemptedinsert = "Exempted";
                    db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                    // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                    registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                }
            }
            else if(currentgiventime.after(maghribgiventime) && currentgiventime.before(ishagiventime)) {
                for (int i = 4; i < 5; i++) {
                    String Namaztimeinsert = namazname[i];
                    String todaydateinsert = currentdate;
                    String ontimeinsert = "No";
                    String lateinsert = "No";
                    String missedinsert = "No";
                    String exemptedinsert = "Exempted";
                    db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                    // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                    registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                }
            }
            else if(currentgiventime.after(ishagiventime) && currentgiventime.before(fajrgiventime)) {
                for (int i = 0; i < 5; i++) {
                    String Namaztimeinsert = namazname[i];
                    String todaydateinsert = nextdaydate;
                    String ontimeinsert = "No";
                    String lateinsert = "No";
                    String missedinsert = "No";
                    String exemptedinsert = "Exempted";
                    db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                    // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                    registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                }
            }
        }
        else if(response == false)
        {
            Toast.makeText(this, "Not enable", Toast.LENGTH_SHORT).show();
//            Mensstartswitch.setClickable(false);
            User_id = sharedPreferences.getString("preuid", "");
            Date current  = Calendar.getInstance().getTime();
            Calendar c = Calendar.getInstance();


            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("HH:mm", Locale.US);

            String currentdate = simpleDateFormat.format(current);
            String currenttime = simpleDateFormat1.format(current);
            try {
                c.setTime(simpleDateFormat.parse(currentdate));
                c.add(Calendar.DATE,1);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Date nextdate = c.getTime();
            Date fajrgiventime = null;
            Date duhrgiventime = null;
            Date asargiventime = null;
            Date maghribgiventime = null;
            Date ishagiventime = null;
            Date currentgiventime = null;
            String nextdaydate = simpleDateFormat.format(nextdate);
            try {
                fajrgiventime = simpleDateFormat1.parse(fajar);
                duhrgiventime = simpleDateFormat1.parse(duhur);
                asargiventime = simpleDateFormat1.parse(asar);
                maghribgiventime = simpleDateFormat1.parse(maghrib);
                ishagiventime = simpleDateFormat1.parse(isha);
                currentgiventime = simpleDateFormat1.parse(currenttime);

            } catch (ParseException e) {
                e.printStackTrace();
            }
            if(currentgiventime.after(fajrgiventime) && currentgiventime.before(duhrgiventime)) {
                for (int i = 1; i < 5; i++) {
                    String Namaztimeinsert = namazname[i];
                    String todaydateinsert = currentdate;
                    String ontimeinsert = "No";
                    String lateinsert = "No";
                    String missedinsert = "missed";
                    String exemptedinsert = "No";
                    db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                    // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                    registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                }
            }
            else if(currentgiventime.after(duhrgiventime) && currentgiventime.before(asargiventime)) {
                for (int i = 2; i < 5; i++) {
                    String Namaztimeinsert = namazname[i];
                    String todaydateinsert = currentdate;
                    String ontimeinsert = "No";
                    String lateinsert = "No";
                    String missedinsert = "missed";
                    String exemptedinsert = "No";
                    db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                    // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                    registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                }
            }
            else  if(currentgiventime.after(asargiventime) && currentgiventime.before(maghribgiventime)) {
                for (int i = 3; i < 5; i++) {
                    String Namaztimeinsert = namazname[i];
                    String todaydateinsert = currentdate;
                    String ontimeinsert = "No";
                    String lateinsert = "No";
                    String missedinsert = "missed";
                    String exemptedinsert = "No";
                    db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                    // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                    registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                }
            }
            else  if(currentgiventime.after(maghribgiventime) && currentgiventime.before(ishagiventime)) {
                for (int i = 4; i < 5; i++) {
                    String Namaztimeinsert = namazname[i];
                    String todaydateinsert = currentdate;
                    String ontimeinsert = "No";
                    String lateinsert = "No";
                    String missedinsert = "missed";
                    String exemptedinsert = "No";
                    db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                    // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                    registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                }
            }
            else  if(currentgiventime.after(ishagiventime) && currentgiventime.before(fajrgiventime)) {
                for (int i = 0; i < 5; i++) {
                    String Namaztimeinsert = namazname[i];
                    String todaydateinsert = nextdaydate;
                    String ontimeinsert = "No";
                    String lateinsert = "No";
                    String missedinsert = "missed";
                    String exemptedinsert = "No";
                    db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                    // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                    registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                }
            }

        }
        Mensstartswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(Mensstartswitch.isChecked())
                {

                    User_id = sharedPreferences.getString("preuid", "");
                    Date current  = Calendar.getInstance().getTime();
                    Calendar c = Calendar.getInstance();


                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
                    SimpleDateFormat simpleDateFormat3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                    SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("HH:mm", Locale.US);

                    String currentdate = simpleDateFormat.format(current);
                    String currenttime = simpleDateFormat1.format(current);
                    String exedatetimeformat = simpleDateFormat3.format(current);
                    try {
                        c.setTime(simpleDateFormat.parse(currentdate));
                        c.add(Calendar.DATE,1);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Date nextdate = c.getTime();
                    Date fajrgiventime = null;
                    Date duhrgiventime = null;
                    Date asargiventime = null;
                    Date maghribgiventime = null;
                    Date ishagiventime = null;
                    Date currentgiventime = null;
                String nextdaydate = simpleDateFormat.format(nextdate);
                    try {
                        fajrgiventime = simpleDateFormat1.parse(fajar);
                        duhrgiventime = simpleDateFormat1.parse(duhur);
                        asargiventime = simpleDateFormat1.parse(asar);
                        maghribgiventime = simpleDateFormat1.parse(maghrib);
                        ishagiventime = simpleDateFormat1.parse(isha);
                        currentgiventime = simpleDateFormat1.parse(currenttime);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if(currentgiventime.after(fajrgiventime) && currentgiventime.before(duhrgiventime)) {
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(ROOT_URL) //Setting the Root URL
                                .addConverterFactory(ScalarsConverterFactory.create())
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();
                        addexemptedcurrent  api = retrofit.create(addexemptedcurrent.class);
                        Call<String> call = api.getDetails(User_id,exedatetimeformat,"Duhur","Null","Null");
                        call.enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {
                              //  Toast.makeText(MensModifyActivity.this, "", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {

                            }
                        });

//                        Toast.makeText(MensModifyActivity.this, "fbd", Toast.LENGTH_SHORT).show();
                        for (int i = 1; i < 5; i++) {
                            String Namaztimeinsert = namazname[i];
                            String todaydateinsert = currentdate;
                            String ontimeinsert = "No";
                            String lateinsert = "No";
                            String missedinsert = "No";
                            String exemptedinsert = "Exempted";
                            db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                            // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                            registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                        }
                    }
                    else   if(currentgiventime.after(duhrgiventime) && currentgiventime.before(asargiventime)) {
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(ROOT_URL) //Setting the Root URL
                                .addConverterFactory(ScalarsConverterFactory.create())
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();
                        addexemptedcurrent  api = retrofit.create(addexemptedcurrent.class);
                        Call<String> call = api.getDetails(User_id,exedatetimeformat,"Asar","Null","Null");
                        call.enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {
                                //  Toast.makeText(MensModifyActivity.this, "", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {

                            }
                        });

                        for (int i = 2; i < 5; i++) {
//                            Toast.makeText(MensModifyActivity.this, "dba", Toast.LENGTH_SHORT).show();
                            String Namaztimeinsert = namazname[i];
                            String todaydateinsert = currentdate;
                            String ontimeinsert = "No";
                            String lateinsert = "No";
                            String missedinsert = "No";
                            String exemptedinsert = "Exempted";
                            db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                            // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                            registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                        }
                    }
                    else  if(currentgiventime.after(asargiventime) && currentgiventime.before(maghribgiventime)) {
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(ROOT_URL) //Setting the Root URL
                                .addConverterFactory(ScalarsConverterFactory.create())
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();
                        addexemptedcurrent  api = retrofit.create(addexemptedcurrent.class);
                        Call<String> call = api.getDetails(User_id,exedatetimeformat,"Maghrib","Null","Null");
                        call.enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {
                                //  Toast.makeText(MensModifyActivity.this, "", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {

                            }
                        });

                        for (int i = 3; i < 5; i++) {
//                            Toast.makeText(MensModifyActivity.this, "abm", Toast.LENGTH_SHORT).show();
                            String Namaztimeinsert = namazname[i];
                            String todaydateinsert = currentdate;
                            String ontimeinsert = "No";
                            String lateinsert = "No";
                            String missedinsert = "No";
                            String exemptedinsert = "Exempted";
                            db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                            // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                            registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                        }
                    }
                    else  if(currentgiventime.after(maghribgiventime) && currentgiventime.before(ishagiventime)) {
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(ROOT_URL) //Setting the Root URL
                                .addConverterFactory(ScalarsConverterFactory.create())
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();
                        addexemptedcurrent  api = retrofit.create(addexemptedcurrent.class);
                        Call<String> call = api.getDetails(User_id,exedatetimeformat,"Isha","Null","Null");
                        call.enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {
                                //  Toast.makeText(MensModifyActivity.this, "", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {

                            }
                        });

                        for (int i = 4; i < 5; i++) {

//                            Toast.makeText(MensModifyActivity.this, "mbi", Toast.LENGTH_SHORT).show();
                            String Namaztimeinsert = namazname[i];
                            String todaydateinsert = currentdate;
                            String ontimeinsert = "No";
                            String lateinsert = "No";
                            String missedinsert = "No";
                            String exemptedinsert = "Exempted";
                            db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                            // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                            registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                        }
                    }
                    else  if(currentgiventime.after(ishagiventime) && currentgiventime.before(fajrgiventime)) {
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(ROOT_URL) //Setting the Root URL
                                .addConverterFactory(ScalarsConverterFactory.create())
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();
                        addexemptedcurrent  api = retrofit.create(addexemptedcurrent.class);
                        Call<String> call = api.getDetails(User_id,exedatetimeformat,"Fajar","Null","Null");
                        call.enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {
                                //  Toast.makeText(MensModifyActivity.this, "", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {

                            }
                        });

                        for (int i = 0; i < 5; i++) {
//                            Toast.makeText(MensModifyActivity.this, "ibf", Toast.LENGTH_SHORT).show();
                            String Namaztimeinsert = namazname[i];
                            String todaydateinsert = nextdaydate;
                            String ontimeinsert = "No";
                            String lateinsert = "No";
                            String missedinsert = "No";
                            String exemptedinsert = "Exempted";
                            db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                            // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                            registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                        }
                    }




                   // Toast.makeText(getApplicationContext(), "cdate = "+currentdate+"\nctime ="+nextdaydate+"\nid ="+User_id, Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor =  sharedpreferences3.edit();
                    editor.putBoolean("enable",true);
                    editor.commit();
                    editor.apply();



                    Toast.makeText(MensModifyActivity.this, "Mensturation start.\nYou will not offer Salah until this switch ON.", Toast.LENGTH_SHORT).show();

//                    Thread thread = new Thread()
//                    {
//                        public void run()
//                        {
//                            try {
//
//                                Intent intent = new Intent(getApplicationContext(),MensService.class);
//
//                                startService(intent);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//
//                        }
//
//                    };
//
//                    thread.start();
                    Mensstartswitch.setChecked(true);

                }
                else
                {


                    Mensstartswitch.setChecked(false);
                    User_id = sharedPreferences.getString("preuid", "");
                    Date current  = Calendar.getInstance().getTime();
                    Calendar c = Calendar.getInstance();


                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
                    SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("HH:mm", Locale.US);
                    SimpleDateFormat simpleDateFormat3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);


                    String currentdate = simpleDateFormat.format(current);
                    String currenttime = simpleDateFormat1.format(current);
                    String exedatetimeformat = simpleDateFormat3.format(current);
                    try {
                        c.setTime(simpleDateFormat.parse(currentdate));
                        c.add(Calendar.DATE,1);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Date nextdate = c.getTime();
                    Date fajrgiventime = null;
                    Date duhrgiventime = null;
                    Date asargiventime = null;
                    Date maghribgiventime = null;
                    Date ishagiventime = null;
                    Date currentgiventime = null;
                    String nextdaydate = simpleDateFormat.format(nextdate);
                    try {
                        fajrgiventime = simpleDateFormat1.parse(fajar);
                        duhrgiventime = simpleDateFormat1.parse(duhur);
                        asargiventime = simpleDateFormat1.parse(asar);
                        maghribgiventime = simpleDateFormat1.parse(maghrib);
                        ishagiventime = simpleDateFormat1.parse(isha);
                        currentgiventime = simpleDateFormat1.parse(currenttime);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if(currentgiventime.after(fajrgiventime) && currentgiventime.before(duhrgiventime)) {
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(ROOT_URL) //Setting the Root URL
                                .addConverterFactory(ScalarsConverterFactory.create())
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();

                        addexemptedcomplete api = retrofit.create(addexemptedcomplete.class);
                        Call<String> call = api.getDetails(User_id,exedatetimeformat,"Duhur");
                        call.enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {

                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {

                            }
                        });

                        for (int i = 1; i < 5; i++) {
                            String Namaztimeinsert = namazname[i];
                            String todaydateinsert = currentdate;
                            String ontimeinsert = "No";
                            String lateinsert = "No";
                            String missedinsert = "missed";
                            String exemptedinsert = "No";
                            db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                            // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                            registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                        }
                    }
                    else  if(currentgiventime.after(duhrgiventime) && currentgiventime.before(asargiventime)) {
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(ROOT_URL) //Setting the Root URL
                                .addConverterFactory(ScalarsConverterFactory.create())
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();

                        addexemptedcomplete api = retrofit.create(addexemptedcomplete.class);
                        Call<String> call = api.getDetails(User_id,exedatetimeformat,"Asar");
                        call.enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {

                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {

                            }
                        });
                        for (int i = 2; i < 5; i++) {
                            String Namaztimeinsert = namazname[i];
                            String todaydateinsert = currentdate;
                            String ontimeinsert = "No";
                            String lateinsert = "No";
                            String missedinsert = "missed";
                            String exemptedinsert = "No";
                            db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                            // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                            registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                        }
                    }
                    else   if(currentgiventime.after(asargiventime) && currentgiventime.before(maghribgiventime)) {
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(ROOT_URL) //Setting the Root URL
                                .addConverterFactory(ScalarsConverterFactory.create())
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();

                        addexemptedcomplete api = retrofit.create(addexemptedcomplete.class);
                        Call<String> call = api.getDetails(User_id,exedatetimeformat,"Maghrib");
                        call.enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {

                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {

                            }
                        });
                        for (int i = 3; i < 5; i++) {
                            String Namaztimeinsert = namazname[i];
                            String todaydateinsert = currentdate;
                            String ontimeinsert = "No";
                            String lateinsert = "No";
                            String missedinsert = "missed";
                            String exemptedinsert = "No";
                            db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                            // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                            registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                        }
                    }
                    else  if(currentgiventime.after(maghribgiventime) && currentgiventime.before(ishagiventime)) {
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(ROOT_URL) //Setting the Root URL
                                .addConverterFactory(ScalarsConverterFactory.create())
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();

                        addexemptedcomplete api = retrofit.create(addexemptedcomplete.class);
                        Call<String> call = api.getDetails(User_id,exedatetimeformat,"Isha");
                        call.enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {

                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {

                            }
                        });
                        for (int i = 4; i < 5; i++) {
                            String Namaztimeinsert = namazname[i];
                            String todaydateinsert = currentdate;
                            String ontimeinsert = "No";
                            String lateinsert = "No";
                            String missedinsert = "missed";
                            String exemptedinsert = "No";
                            db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                            // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                            registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                        }
                    }
                    else  if(currentgiventime.after(ishagiventime) && currentgiventime.before(fajrgiventime)) {
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(ROOT_URL) //Setting the Root URL
                                .addConverterFactory(ScalarsConverterFactory.create())
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();

                        addexemptedcomplete api = retrofit.create(addexemptedcomplete.class);
                        Call<String> call = api.getDetails(User_id,exedatetimeformat,"Fajar");
                        call.enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {

                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {

                            }
                        });
                        for (int i = 0; i < 5; i++) {
                            String Namaztimeinsert = namazname[i];
                            String todaydateinsert = nextdaydate;
                            String ontimeinsert = "No";
                            String lateinsert = "No";
                            String missedinsert = "missed";
                            String exemptedinsert = "No";
                            db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                            // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                            registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                        }
                    }
                  //  Toast.makeText(getApplicationContext(), "cdate = "+currentdate+"\nctime ="+currenttime+"\nid ="+User_id, Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editorebad = sharedpreferences3.edit();

                    editorebad.putBoolean("enable",false);
                    editorebad.commit();
                    editorebad.apply();
//                    for (int i = 0 ; i<5;i++) {
//                        String Namaztimeinsert = namazname[i];
//                        String todaydateinsert = currentdate;
//                        String ontimeinsert = "No";
//                        String lateinsert = "No";
//                        String missedinsert = "missed";
//                        String exemptedinsert = "No";
//                        db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
//
//                        // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
//
//
//                        registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
//                    }
                    Toast.makeText(MensModifyActivity.this, "Now you can offer Salah ", Toast.LENGTH_SHORT).show();
//                    Thread thread = new Thread()
//                    {
//                        public void run()
//                        {
//                            try {
//
//                                Intent intent = new Intent(getApplicationContext(),MensService.class);
//
//                                stopService(intent);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//
//                        }
//
//                    };
//
//                    thread.start();
                    Mensstartswitch.setChecked(false);
                }
            }
        });

    }

    public  void savemodifydate(View view) {
        String userid = sharedpreferences.getString("preuid", "");
        String lastmonthdate = datepicker.getText().toString();
        String Dateofbirth = sharedpreferences.getString("preuserfarzdate", "");
        //Toast.makeText(this, ""+userid, Toast.LENGTH_SHORT).show();
        String cyclee = cycle.getText().toString().trim();
        String noofdayss = noofdays.getText().toString().trim();
        SharedPreferences.Editor editor2 = sharedpreferences.edit();
        editor2.putString("cycle", cyclee);
        editor2.putString("noofday", noofdayss);
        editor2.apply();
        editor2.commit();
        // Toast.makeText(this, "Date "+Dateofbirth, Toast.LENGTH_SHORT).show();
        if (lastmonthdate.isEmpty()) {
            datepicker.setError("Select Last month mensturation date");
            datepicker.requestFocus();
        } else if (cyclee.isEmpty()) {
            cycle.setError("Fill this field");
            cycle.requestFocus();

        } else if (noofdayss.isEmpty()) {
            noofdays.setError("Fill this field");
            noofdays.requestFocus();

        } else {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ROOT_URL) //Setting the Root URL
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            MenscalculatesignupApi api = retrofit.create(MenscalculatesignupApi.class);
            Call<String> call = api.getDetails(userid, lastmonthdate, Dateofbirth, cyclee, noofdayss);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.body().contains("Updated")) {
                        Toast.makeText(MensModifyActivity.this, "Dates Calculated.", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), EditUserProfileActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(MensModifyActivity.this, "Incorrect Data", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Toast.makeText(MensModifyActivity.this, "Network issue", Toast.LENGTH_SHORT).show();
                }
            });

        }
    }
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 111: {
                DatePickerDialog _datefarz = new DatePickerDialog(this, datePickerListenerfarz, myear, mmonth,
                        mday) {
                    @Override
                    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                        if (year > myear) {
//                            Toast.makeText(getApplicationContext(), "Year can't be greater then current Year", Toast.LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }
//                        if (monthOfYear > mmonth && year == myear) {
//                            Toast.makeText(getApplicationContext(), "Month can't be greater then current Month", Toast.LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }
//                        if (dayOfMonth > mday && year == myear && monthOfYear == mmonth) {
//                            Toast.makeText(getApplicationContext(), "Date can't be greater then current date", Toast.LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }
                    }
                };
                return _datefarz;
            }

        }


        return null;
    }
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            myear = selectedYear;
            mmonth = selectedMonth;
            mday = selectedDay;
            String sMonth = "";
            // set selected date into textview
            if (mmonth == 0)
                sMonth = "Jan";
            else if (mmonth == 1)
                sMonth = "Feb";
            else if (mmonth == 2)
                sMonth = "Mar";
            else if (mmonth == 3)
                sMonth = "Apr";
            else if (mmonth == 4)
                sMonth = "May";
            else if (mmonth == 5)
                sMonth = "Jun";
            else if (mmonth == 6)
                sMonth = "Jul";
            else if (mmonth == 7)
                sMonth = "Aug";
            else if (mmonth == 8)
                sMonth = "Sep";
            else if (mmonth == 9)
                sMonth = "Oct";
            else if (mmonth == 10)
                sMonth = "Nov";
            else if (mmonth == 11)
                sMonth = "Dec";
            datepicker.setText(new StringBuilder()
                    .append(mday).append(" ") .append(sMonth).append(" ").append(myear).append(" "));
            // set selected date into datepicker also
            dpResult.init(myear, mmonth, mday, null);

//            try {
//                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy");
//                SimpleDateFormat simpleDateFormattenyear = new SimpleDateFormat("yyyy-MM-dd");
//                mydate = simpleDateFormat.parse(tvDisplayDate.getText().toString());
//                Calendar c = Calendar.getInstance();
//                c.setTime(mydate);
//                c.add(Calendar.YEAR, 14);
//                newdate = c.getTime();
//                longdate = newdate.getTime();
//                tenyear = simpleDateFormat.format(newdate);
//                tenyearServer = simpleDateFormattenyear.format(newdate);
//                dateofb.setText(tenyear);
//                mfarzyear = Calendar.YEAR;
//                mfarzmonth = Calendar.MONTH;
//                mfarzday = Calendar.DAY_OF_MONTH;
//
//
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }


        }
    };
    public void setCurrentDateOnView() {



        final Calendar c = Calendar.getInstance();
        Currenttime = c.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        currentDateandTime = sdf.format(new Date());
        myear = c.get(Calendar.YEAR);
        mmonth = c.get(Calendar.MONTH);
        mday = c.get(Calendar.DAY_OF_MONTH);

        // set current date into textview
//        tvDisplayDate.setText(new StringBuilder()
//                // Month is 0 based, just add 1
//                .append(myear).append("-") .append(mmonth + 1).append("-").append(mday)
//        );

        // set current date into datepicker
        dpResult.init(myear, mmonth, mday, null);

    }

    private DatePickerDialog.OnDateSetListener datePickerListenerfarz = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            myear = selectedYear;
            mmonth = selectedMonth;
            mday = selectedDay;
            String sMonth = "";
            // set selected date into textview
            if (mmonth == 0)
                sMonth = "Jan";
            else if (mmonth == 1)
                sMonth = "Feb";
            else if (mmonth == 2)
                sMonth = "Mar";
            else if (mmonth == 3)
                sMonth = "Apr";
            else if (mmonth == 4)
                sMonth = "May";
            else if (mmonth == 5)
                sMonth = "Jun";
            else if (mmonth == 6)
                sMonth = "Jul";
            else if (mmonth == 7)
                sMonth = "Aug";
            else if (mmonth == 8)
                sMonth = "Sep";
            else if (mmonth == 9)
                sMonth = "Oct";
            else if (mmonth == 10)
                sMonth = "Nov";
            else if (mmonth == 11)
                sMonth = "Dec";
            datepicker.setText(new StringBuilder()
                    .append(mday).append(" ") .append(sMonth).append(" ").append(myear).append(" "));
            // set selected date into datepicker also
            dpResult.init(myear, mmonth, mday, null);

//            try {
//                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy");
//                SimpleDateFormat simpleDateFormattenyear = new SimpleDateFormat("yyyy-MM-dd");
//                mydate = simpleDateFormat.parse(dateofb.getText().toString());
//                Calendar c = Calendar.getInstance();
//                c.setTime(mydate);
//                c.add(Calendar.YEAR, 14);
//                newdate = c.getTime();
//                longdate = newdate.getTime();
//                tenyear = simpleDateFormat.format(newdate);
//                tenyearServer = simpleDateFormattenyear.format(newdate);
////                namazfarzdate.setText(tenyear);
//                mfarzyear = Calendar.YEAR;
//                mfarzmonth = Calendar.MONTH;
//                mfarzday = Calendar.DAY_OF_MONTH;

//
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//


        }
    };
}
