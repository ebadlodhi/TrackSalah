package outofbox.ltd.administrator.tracksalah;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class FiqqahActivity extends AppCompatActivity {
   SharedPreferences sharedPreferences3;
    public static  String  methodvalue = "";
    public static  String  radiotext = "";
    public static  int method =2;
    RadioGroup methodradio;
    RadioButton Shia,karachi,namerica,mwl,ummalqura,egypt,tehranuni,gulfregion,kuwait,qatar,singapor,france,turkey;
    private Tracker mTracker;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_updatebirthdate);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle("Select Fiqh");
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        methodradio = findViewById(R.id.methodradio);
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Fiqqah Selection");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("TrackSalahUser")
                .setAction("User id")
                .setLabel(DashboardActivity.Userid)
                .setValue(1)
                .build());
//        int selectedId = methodradio.getCheckedRadioButtonId();
//        selectradio = findViewById(selectedId);
//        radiotext  = (String) selectradio.getText();
        Shia = findViewById(R.id.Shia);
        karachi = findViewById(R.id.karachi);
        namerica = findViewById(R.id.namerica);
        mwl = findViewById(R.id.mwl);
        ummalqura = findViewById(R.id.ummalqura);
        tehranuni = findViewById(R.id.tehranuni);
        gulfregion = findViewById(R.id.gulfregion);
        gulfregion = findViewById(R.id.gulfregion);
        kuwait = findViewById(R.id.kuwait);
        singapor = findViewById(R.id.singapor);
        egypt = findViewById(R.id.egypt);
        france = findViewById(R.id.france);
        turkey = findViewById(R.id.turkey);






        sharedPreferences3  =getSharedPreferences(methodvalue, Context.MODE_PRIVATE);
        String mval = sharedPreferences3.getString("methodval","2");
        int val = Integer.parseInt(mval);
        if (val == 0)
            Shia.setChecked(true);
        else if (val == 1)
            karachi.setChecked(true);
        else if (val == 2)
            namerica.setChecked(true);
        else if (val == 3)
            mwl.setChecked(true);
        else if (val == 4)
            ummalqura.setChecked(true);
        else if (val == 5)
            egypt.setChecked(true);
        else if (val == 7)
            tehranuni.setChecked(true);
        else if (val == 8)
            gulfregion.setChecked(true);
        else if (val == 9)
            kuwait.setChecked(true);
        else if (val == 10)
            qatar.setChecked(true);
        else if (val == 11)
            singapor.setChecked(true);
        else if (val == 12)
            france.setChecked(true);
        else if (val == 13)
            turkey.setChecked(true);



        methodradio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedbuttton = findViewById(checkedId);
                radiotext  = (String) checkedbuttton.getText();


                if(radiotext.equals("Shia Ithna-Ansari"))
                    method = 0;
                else if(radiotext.equals("University of Islamic Sciences, Karachi"))
                    method = 1;
                else if(radiotext.equals("Islamic Society of North America"))
                    method = 2;
                else if(radiotext.equals("Muslim World League"))
                    method = 3;
                else if(radiotext.equals("Umm Al-Qura University, Makkah"))
                    method = 4;
                else if(radiotext.equals("Egyptian General Authority of Survey"))
                    method = 5;
                else if(radiotext.equals("Institute of Geophysics, University of Tehran"))
                    method = 7;
                else if(radiotext.equals("Gulf Region"))
                    method = 8;
                else if(radiotext.equals("Kuwait"))
                    method = 9;
                else if(radiotext.equals("Qatar"))
                    method = 10;
                else if(radiotext.equals("Majlis Ugama Islam Singapura, Singapore"))
                    method = 11;
                else if(radiotext.equals("Union Organization islamic de France"))
                    method = 12;
                else if(radiotext.equals("Diyanet İşleri Başkanlığı, Turkey"))
                    method = 13;
                SharedPreferences.Editor methodeditor = sharedPreferences3.edit();

                methodeditor.putString("methodval", String.valueOf(method));
                methodeditor.apply();
                methodeditor.commit();
//                String chkval = sharedPreferences3.getString("methodval","2");
//                Toast.makeText(getApplicationContext(), "id is = "+chkval , Toast.LENGTH_SHORT).show();

            }
        });

    }
}
