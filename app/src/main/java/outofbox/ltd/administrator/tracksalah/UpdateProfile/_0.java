package outofbox.ltd.administrator.tracksalah.UpdateProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class _0 {
    @SerializedName("User_name")
    @Expose
    private String userName;
    @SerializedName("User_password")
    @Expose
    private String userPassword;
    @SerializedName("User_email")
    @Expose
    private String userEmail;
    @SerializedName("Gender")
    @Expose
    private String gender;
    @SerializedName("User_DOB")
    @Expose
    private String userDOB;
    @SerializedName("NamazFarzDate")
    @Expose
    private String namazFarzDate;
    @SerializedName("MstartDate")
    @Expose
    private String mstartDate;
    @SerializedName("MendDate")
    @Expose
    private String mendDate;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUserDOB() {
        return userDOB;
    }

    public void setUserDOB(String userDOB) {
        this.userDOB = userDOB;
    }

    public String getNamazFarzDate() {
        return namazFarzDate;
    }

    public void setNamazFarzDate(String namazFarzDate) {
        this.namazFarzDate = namazFarzDate;
    }

    public String getMstartDate() {
        return mstartDate;
    }

    public void setMstartDate(String mstartDate) {
        this.mstartDate = mstartDate;
    }

    public String getMendDate() {
        return mendDate;
    }

    public void setMendDate(String mendDate) {
        this.mendDate = mendDate;
    }

}
