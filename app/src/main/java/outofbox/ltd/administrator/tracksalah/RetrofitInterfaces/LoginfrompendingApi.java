package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;


import outofbox.ltd.administrator.tracksalah.POJOFORLOGINPENDING.Example;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface LoginfrompendingApi {

    @FormUrlEncoded
    @POST("loginfrompending.php")
    Call<Example> getDetails(@Field("email") String email,@Field("password") String password);
}
