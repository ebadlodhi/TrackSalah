package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;

import outofbox.ltd.administrator.tracksalah.POJODOBforqadahcal.Example;

import outofbox.ltd.administrator.tracksalah.POJODOBforqadahcal.Example;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface DobCalculateApi {

    @FormUrlEncoded
    @POST("myProfileupdate.php")
    Call<Example> getDetails(@Field("id") String uid);

}
