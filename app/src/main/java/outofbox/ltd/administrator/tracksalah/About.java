package outofbox.ltd.administrator.tracksalah;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class About extends AppCompatActivity {

    private AdView mAdView;
    TextView text;
    private Tracker mtracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle("About Us");
        mAdView = (AdView) findViewById(R.id.adView);
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mtracker = application.getDefaultTracker();
        mtracker.setScreenName("About us");
        mtracker.send(new HitBuilders.ScreenViewBuilder().build());
        mtracker.send(new HitBuilders.EventBuilder()
                .setCategory("TrackSalahUser")
                .setAction("User id")
                .setLabel(DashboardActivity.Userid)
                .setValue(1)
                .build());
        MobileAds.initialize(this, "ca-app-pub-2825367626708661~7620129751");
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdView.setVisibility(View.VISIBLE);
            }
        });

        text = findViewById(R.id.desc);
        text.setText("Track Salah, an app developed to help you keep track of your Daily Salah activity, features special notifcation service for each namaz on your permission. You can enter each namaz as performed on time, late or missed. The tracking of missed namaz further help calculate your total qadah namaz which you can edit as you keep on performing your qadah salah(s). \n" +
                "\n" +
                "The Mosque Finder function allows you to search for nearby mosques with ease. This app also comes with a qibla locator. \n" +
                "\n" +
                "Track Salah, allows you to turn salah notifications on and off, select your fiqah from a list of provided global fiqah(s), report issues, add complaints and even share Track Salah with your friends and family. Salah timings are updated regularly according your location and fiqah giving you the ease getting exact time of each salah instead of scrolling through a list. \n" +
                "\n" +
                "Track Salah was developed by Outofbox.ltd ©. ");
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
}
