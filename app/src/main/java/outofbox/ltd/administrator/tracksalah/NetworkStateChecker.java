package outofbox.ltd.administrator.tracksalah;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import outofbox.ltd.administrator.tracksalah.DailySalahActivity;
import outofbox.ltd.administrator.tracksalah.Databasesqlite.DatabaseHelper;
import outofbox.ltd.administrator.tracksalah.Databasesqlite.POJORES;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.SyncApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class NetworkStateChecker extends BroadcastReceiver {

    //context and database helper object
    private Context context;
    private DatabaseHelper db;
    String Userid;
    String uid ,todaydate;


    @Override
    public void onReceive(Context context, Intent intent) {

        this.context = context;

        db = new DatabaseHelper(context);
        //Toast.makeText(context, "Reciever start", Toast.LENGTH_SHORT).show();

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        //if there is a network
        if (activeNetwork != null) {
            //if connected to wifi or mobile data plan
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {

                //getting all the unsynced names
                Cursor cursor = db.getUnsyncedNames();
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        do {
                            //calling the method to save the unsynced name to MySQL
                            saveName(
                                    cursor.getInt(1),
                                    cursor.getString(2),
                                    cursor.getString(3),
                                    cursor.getString(4),
                                    cursor.getString(5),
                                    cursor.getString(6),
                                    cursor.getString(7)
                            );
                        } while (cursor.moveToNext());
                    }
                }


                else{
                    Toast.makeText(context, "Database is Empty", Toast.LENGTH_SHORT).show();
                }

            }
        }
    }

    /*
     * method taking two arguments
     * name that is to be saved and id of the name from SQLite
     * if the name is successfully sent
     * we will update the status as synced in SQLite
     * */
    private void saveName(final int userid, final String todaydate, final String namazname, final String ontime , final String late , final String missed ,final String exempted) {

        Userid = Integer.toString(userid);
        String todaydateinsertt = todaydate;
        final String Namaztimeinsertt = namazname;
        String ontimeinsertt = ontime;
        String lateinsertt = late;
        String missedinsertt = missed;
        String missedexempted = exempted;

       // Toast.makeText(context, "uid = "+Userid+"\ntoday date = "+todaydateinsertt+"\nnamaz name = "+Namaztimeinsertt+"\nontime = "+ontimeinsertt+"\nlate = "+lateinsertt+"\nmiss = "+missedinsertt, Toast.LENGTH_LONG).show();

        Retrofit retrofitinsertNamazDetail = new Retrofit.Builder()
                .baseUrl(DailySalahActivity.ROOT_URL) //Setting the Root URL
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SyncApi syncApi = retrofitinsertNamazDetail.create(SyncApi.class);
        Call<POJORES> call = syncApi.getDetails(Userid,todaydateinsertt,Namaztimeinsertt,ontimeinsertt,lateinsertt,missedinsertt,missedexempted);
        call.enqueue(new Callback<POJORES>() {
            @Override
            public void onResponse(Call<POJORES> call, retrofit2.Response<POJORES> response) {
                Boolean error = response.body().getError();
                String message = response.body().getMessage().toString();

                if(error == false)
                {
                  //  DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.offeredicon);
                    boolean res =   db.updateNameStatus(userid, DailySalahActivity.NAME_SYNCED_WITH_SERVER,todaydate,namazname,ontime,late,missed);
                    if(res == true)
                    {
                        //sending the broadcast to refresh the list
                        context.sendBroadcast(new Intent(DailySalahActivity.DATA_SAVED_BROADCAST));
                       // db.deletesync();

                        }

                    else
                    {
                        Log.d("err","update name status error");
                    }

                }
                else {Log.d("server","no insert to server");}

            }

            @Override
            public void onFailure(Call<POJORES> call, Throwable t) {

                //  Toast.makeText(context, "Network error kia jammmaaaalloo", Toast.LENGTH_SHORT).show();
                Log.d("net","network error");
            }
        });







//        StringRequest stringRequest = new StringRequest(Request.Method.POST, MainActivity.URL_SAVE_NAME,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        try {
//                            JSONObject obj = new JSONObject(response);
//                            if (!obj.getBoolean("error")) {
//                                //updating the status in sqlite
//                                db.updateNameStatus(userid, MainActivity.NAME_SYNCED_WITH_SERVER);
//
//                                //sending the broadcast to refresh the list
//                                context.sendBroadcast(new Intent(MainActivity.DATA_SAVED_BROADCAST));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//
//                    }
//                }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                params.put("userid", String.valueOf(userid));
//                params.put("todaydate",todaydate);
//                params.put("namazname",namazname);
//                params.put("ontime",ontime);
//                params.put("late",late);
//                params.put("missed",missed);
//
//
//                return params;
//            }
//        };
//
//
//        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }


}