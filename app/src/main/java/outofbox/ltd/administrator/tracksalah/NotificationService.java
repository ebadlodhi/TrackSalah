package outofbox.ltd.administrator.tracksalah;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import outofbox.ltd.administrator.tracksalah.POJO2.Example;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.NamazDetailsApi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.ContentValues.TAG;

public class NotificationService extends Service implements LocationListener {
    public static String NcompareFajrOne="05:30";
    public static String NcompareFajrTwo;
    public static String NcompareZhrOne="12:35";
    public static String NcompareZhrTwo;
    public static String NcompareAsarOne="16:28";
    public static String NcompareAsarTwo;
    public static String NcompareMaghribOne="18:50";
    public static String NcompareMaghribTwo;
    public static String NcompareIshaOne="20:15";
    public static String NcompareIshaTwo;
    public  static  String Day = "";
    String getdatee,getdayy,gethijrii,gettimezonee,getfajrr,getsunrisee,getduhrr,getasrr,getsunsett,getmaghribb,getishaa,getimsakk,getfiqqahname;
    LocationManager locationManager;
    SharedPreferences sharedPreferencesmethod;
    String methodvalue ;
    Call<Example> call;
    NamazDetailsApi api;
    Double lat = 25.0700 , lng = 67.2848;
    public  static Double latti, longi;
    String res;
    Date fjartime , zohartime,asartime,maghribtime,ishatime;
    private final int INTERVAL = 30 * 1000;
    private Timer timer = new Timer();
    @Override
    public void onCreate() {
        lat = SplashActivity.latti;
        lng = SplashActivity.longi;
        sharedPreferencesmethod = getSharedPreferences(SettingsActivity.methodvalue,Context.MODE_PRIVATE);
        methodvalue = sharedPreferencesmethod.getString("methodval","2");
//        Toast.makeText(this,"Start Service",Toast.LENGTH_LONG).show();
        Log.d(TAG,"Start Service");




        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        PackageManager pm = getPackageManager();
        Intent i = pm.getLaunchIntentForPackage("outofbox.ltd.administrator.tracksalah");
        i.putExtra("run_transparent","yes");
        startActivity(i);
        timer.scheduleAtFixedRate(new TimerTask() {

            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void run() {
                // Print a log
                Log.d(TAG, "Start to do an action");
//                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//
//                Intent notificationIntent = new Intent("android.media.action.DISPLAY_NOTIFICATION");
//                notificationIntent.addCategory("android.intent.category.DEFAULT");
//
//                PendingIntent broadcast = PendingIntent.getBroadcast(getBaseContext(), 100, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
              if(internet_connection() == true) {

                  Retrofit retrofit = new Retrofit.Builder()
                          .baseUrl(NamazDetailsApi.BASE_URL)
                          .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                          .build();
                  api = retrofit.create(NamazDetailsApi.class);
                  getLocation();
                  try {

                      fjartime = simpleDateFormat.parse(NcompareFajrOne);
                      zohartime = simpleDateFormat.parse(NcompareZhrOne);
                      asartime = simpleDateFormat.parse(NcompareAsarOne);
                      maghribtime = simpleDateFormat.parse(NcompareMaghribOne);
                      ishatime = simpleDateFormat.parse(NcompareIshaOne);
                  } catch (ParseException e) {
                      e.printStackTrace();
                  }
              }
              else
              {
                  try {
                      fjartime = simpleDateFormat.parse("05:23");
                      zohartime = simpleDateFormat.parse("12:35");
                      asartime = simpleDateFormat.parse("16:28");
                      maghribtime = simpleDateFormat.parse("17:46");
                      ishatime = simpleDateFormat.parse("20:15");
                  } catch (ParseException e) {
                      e.printStackTrace();
                  }



              }

                Calendar cal = Calendar.getInstance();

                Calendar fjar = Calendar.getInstance();
                fjar.setTime(fjartime);
                Calendar zohar = Calendar.getInstance();
                zohar.setTime(zohartime);
                Calendar asar = Calendar.getInstance();
                asar.setTime(asartime);
                Calendar maghrib = Calendar.getInstance();
                maghrib.setTime(maghribtime);
                Calendar isha = Calendar.getInstance();
                isha.setTime(ishatime);


                int fjrhours = fjar.get(Calendar.HOUR_OF_DAY);
                int fjrmin = fjar.get(Calendar.MINUTE);

                int zhrhours = zohar.get(Calendar.HOUR_OF_DAY);
                int zhrmin = zohar.get(Calendar.MINUTE);

                int asrhours = asar.get(Calendar.HOUR_OF_DAY);
                int asrmin = asar.get(Calendar.MINUTE);

                int mghribhours = maghrib.get(Calendar.HOUR_OF_DAY);
                int mghribmin = maghrib.get(Calendar.MINUTE);

                int ishahours = isha.get(Calendar.HOUR_OF_DAY);
                int ishamin = isha.get(Calendar.MINUTE);

                Intent notificationIntentt = new Intent(getBaseContext(), DailySalahActivity.class);
                final Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                String formattedDate = df.format(c.getTime());
                notificationIntentt.putExtra("ClickedDate",formattedDate);
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(getBaseContext());
                stackBuilder.addParentStack(DailySalahActivity.class);
                stackBuilder.addNextIntent(notificationIntentt);
               // Notification notification=new Notification();
               // Intent notificationIntent = new Intent(getBaseContext(), NotificationPublisher.class);
                //notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, 1);
                //notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
                PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

                Log.d(TAG, "now time is = " + cal.get(Calendar.HOUR_OF_DAY) + " " + cal.get(Calendar.MINUTE) + "\n define time = " + zhrhours + " " + zhrmin);
                if (cal.get(Calendar.HOUR_OF_DAY) == fjrhours && cal.get(Calendar.MINUTE) == fjrmin) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        NotificationHelper helper = new NotificationHelper(getBaseContext());
                        Notification.Builder builder = helper.getChannelNotification("Namaz-e-FAJR Alert","Fajar Namaz Time Started");
                        builder.setContentIntent(pendingIntent);
                        helper.getManager().notify(new Random().nextInt(),builder.build());
                    }
                    else
                    {
                        long[] v = {5000,5000};
                        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        @SuppressLint("ResourceAsColor") NotificationCompat.Builder mBuilder =   new NotificationCompat.Builder(getApplicationContext())
                                .setSmallIcon(R.drawable.icon_app) // notification icon
                                .setColor(R.color.colorAccent)
                                .setContentTitle("Namaz-e-FAJR Alert") // title for notification
                                .setContentText("Fajar Namaz Time Started ") // message for notification
                                .setVibrate(v)
                                .setSound(uri)
                                .setTimeoutAfter(3000)
                                .setAutoCancel(true); // clear notification after click
                        mBuilder.setContentIntent(pendingIntent);
                        NotificationManager mNotificationManager =
                                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        mNotificationManager.notify(0, mBuilder.build());
                    }



//                    alarmManager.setExact(AlarmManager.RTC_WAKEUP,cal.getTimeInMillis() , broadcast);
//                    NotificationCompat.Builder builder = new NotificationCompat.Builder(getBaseContext());
//                    Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//                    int notifyID = 1;
//                    String CHANNEL_ID = "my_channel_01";// The id of the channel.
//                    CharSequence name = getString(R.string.channel_name);// The user-visible name of the channel.
//                    int importance = NotificationManager.IMPORTANCE_HIGH;
//                    NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
//
//                    Notification notification = builder.setContentTitle("Track Salah Reminder")
//                            .setContentText("Fajar Namaz Time Started")
//                            .setTicker("Namaz Alert!")
//                            .setSmallIcon(R.drawable.offeredicon)
//
//                            .setContentIntent(pendingIntent)
//                            .setSound(soundUri).setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 })
//                            .setLights(Color.GREEN, 3000, 3000).build();
//
//                    NotificationManager notificationManager = (NotificationManager) getBaseContext().getSystemService(Context.NOTIFICATION_SERVICE);
//                    notificationManager.createNotificationChannel(mChannel);
//                    notificationManager.notify(notifyID, notification);


                }
                else if (cal.get(Calendar.HOUR_OF_DAY) == zhrhours && cal.get(Calendar.MINUTE) == zhrmin) {
                    String daystring = cal.getDisplayName(Calendar.DAY_OF_WEEK,Calendar.LONG,Locale.getDefault());
                    if(daystring.equals("Friday")  )
                    {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            NotificationHelper helper = new NotificationHelper(getBaseContext());
                            Notification.Builder builder = helper.getChannelNotification("Namaz-e-JUMMA Alert","Namaz-e-JUMMA Time Started");
                            builder.setContentIntent(pendingIntent);
                            helper.getManager().notify(new Random().nextInt(),builder.build());
                        }
                        else
                        {
                            long[] v = {5000,5000};
                            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                            @SuppressLint("ResourceAsColor") NotificationCompat.Builder mBuilder =   new NotificationCompat.Builder(getApplicationContext())
                                    .setSmallIcon(R.drawable.icon_app) // notification icon
                                    .setColor(R.color.colorAccent)
                                    .setContentTitle("Namaz-e-JUMMA Alert") // title for notification
                                    .setContentText("Namaz-e-JUMMA Time Started ") // message for notification
                                    .setVibrate(v)
                                    .setSound(uri)
                                    .setTimeoutAfter(3000)
                                    .setAutoCancel(true); // clear notification after click
                            mBuilder.setContentIntent(pendingIntent);
                            NotificationManager mNotificationManager =
                                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                            mNotificationManager.notify(0, mBuilder.build());
                        }


                    }
                    else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            NotificationHelper helper = new NotificationHelper(getBaseContext());
                            Notification.Builder builder = helper.getChannelNotification("Namaz-e-Zohar Alert","Zohar Namaz Time Started");
                            builder.setContentIntent(pendingIntent);
                            helper.getManager().notify(new Random().nextInt(),builder.build());
                        }
                        else
                        {
                            long[] v = {5000,5000};
                            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                            @SuppressLint("ResourceAsColor") NotificationCompat.Builder mBuilder =   new NotificationCompat.Builder(getApplicationContext())
                                    .setSmallIcon(R.drawable.icon_app) // notification icon
                                    .setColor(R.color.colorAccent)
                                    .setContentTitle("Namaz-e-Zohar Alert") // title for notification
                                    .setContentText("Zohar Namaz Time Started ") // message for notification
                                    .setVibrate(v)
                                    .setSound(uri)
                                    .setTimeoutAfter(3000)
                                    .setAutoCancel(true); // clear notification after click
                            mBuilder.setContentIntent(pendingIntent);
                            NotificationManager mNotificationManager =
                                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                            mNotificationManager.notify(0, mBuilder.build());
                        }

                    }

                }

                else if (cal.get(Calendar.HOUR_OF_DAY) == asrhours && cal.get(Calendar.MINUTE) == asrmin) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        NotificationHelper helper = new NotificationHelper(getBaseContext());
                        Notification.Builder builder = helper.getChannelNotification("Namaz-e-ASAR Alert","Asar Namaz Time Started");
                        builder.setContentIntent(pendingIntent);
                        helper.getManager().notify(new Random().nextInt(),builder.build());
                    }
                    else
                    {
                        long[] v = {5000,5000};
                        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        @SuppressLint("ResourceAsColor") NotificationCompat.Builder mBuilder =   new NotificationCompat.Builder(getApplicationContext())
                                .setSmallIcon(R.drawable.icon_app) // notification icon
                                .setColor(R.color.colorAccent)
                                .setContentTitle("Namaz-e-Asar Alert") // title for notification
                                .setContentText("Asar Namaz Time Started ") // message for notification
                                .setVibrate(v)
                                .setSound(uri)
                                .setTimeoutAfter(3000)
                                .setAutoCancel(true); // clear notification after click
                        mBuilder.setContentIntent(pendingIntent);
                        NotificationManager mNotificationManager =
                                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        mNotificationManager.notify(0, mBuilder.build());
                    }



                }
                else if (cal.get(Calendar.HOUR_OF_DAY) == mghribhours && cal.get(Calendar.MINUTE) == mghribmin) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        NotificationHelper helper = new NotificationHelper(getBaseContext());
                        Notification.Builder builder = helper.getChannelNotification("Namaz-e-MAGHRIB Alert","Maghrib Namaz Time Started");
                        builder.setContentIntent(pendingIntent);
                        helper.getManager().notify(new Random().nextInt(),builder.build());
                    }
                    else
                    {
                        long[] v = {5000,5000};
                        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        @SuppressLint("ResourceAsColor") NotificationCompat.Builder mBuilder =   new NotificationCompat.Builder(getApplicationContext())
                                .setSmallIcon(R.drawable.icon_app) // notification icon
                                .setColor(R.color.colorAccent)
                                .setContentTitle("Namaz-e-MAGHRIB Alert") // title for notification
                                .setContentText("Maghrib Namaz Time Started ") // message for notification
                                .setVibrate(v)
                                .setSound(uri)
                                .setTimeoutAfter(3000)
                                .setAutoCancel(true); // clear notification after click
                        mBuilder.setContentIntent(pendingIntent);
                        NotificationManager mNotificationManager =
                                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        mNotificationManager.notify(0, mBuilder.build());
                    }



                }

                else if (cal.get(Calendar.HOUR_OF_DAY) == ishahours && cal.get(Calendar.MINUTE) == ishamin) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        NotificationHelper helper = new NotificationHelper(getBaseContext());
                        Notification.Builder builder = helper.getChannelNotification("Namaz-e-ISHA Alert","Isha Namaz Time Started");
                        builder.setContentIntent(pendingIntent);
                        helper.getManager().notify(new Random().nextInt(),builder.build());
                    }
                    else
                    {
                        long[] v = {5000,5000};
                        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        @SuppressLint("ResourceAsColor") NotificationCompat.Builder mBuilder =   new NotificationCompat.Builder(getApplicationContext())
                                .setSmallIcon(R.drawable.icon_app) // notification icon
                                .setColor(R.color.colorAccent)
                                .setContentTitle("Namaz-e-Isha Alert") // title for notification
                                .setContentText("Isha Namaz Time Started ") // message for notification
                                .setVibrate(v)
                                .setSound(uri)
                                .setTimeoutAfter(3000)
                                .setAutoCancel(true); // clear notification after click
                        mBuilder.setContentIntent(pendingIntent);
                        NotificationManager mNotificationManager =
                                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        mNotificationManager.notify(0, mBuilder.build());
                    }



                }

                else {

                    Log.d(TAG, "Time remaining for notification");
                }
            }
        }, 0, INTERVAL);



        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    boolean internet_connection(){
        //Check if connected to internet, output accordingly
        ConnectivityManager cm = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }
    void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 500000, 50, this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        latti = location.getLatitude();
        longi = location.getLongitude();
        long unixTime = System.currentTimeMillis() / 1000L;
        call = api.getHeroes("http://api.aladhan.com/v1/timings/"+unixTime+"?latitude="+latti+"&longitude="+longi+"&method="+methodvalue);
        call.enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                getdatee = response.body().getData().getDate().getReadable();
                getdayy = response.body().getData().getDate().getGregorian().getWeekday().getEn();
                gethijrii = response.body().getData().getDate().getHijri().getDate();
                gettimezonee = response.body().getData().getMeta().getTimezone();
                getfajrr = response.body().getData().getTimings().getFajr();
                getduhrr = response.body().getData().getTimings().getDhuhr();
                getasrr=response.body().getData().getTimings().getAsr();
                getmaghribb = response.body().getData().getTimings().getMaghrib();
                getishaa = response.body().getData().getTimings().getIsha();
                getsunrisee = response.body().getData().getTimings().getSunrise();
                getsunsett = response.body().getData().getTimings().getSunset();
                getfiqqahname =response.body().getData().getMeta().getMethod().getName();
                getimsakk = response.body().getData().getTimings().getImsak();
                NotificationService.NcompareFajrOne = getfajrr;
                NotificationService.NcompareZhrOne = getduhrr;
                NotificationService.NcompareAsarOne = getasrr;
                NotificationService.NcompareMaghribOne = getmaghribb;
                NotificationService.NcompareIshaOne = getishaa;
                NotificationService.Day = getdayy;
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "Error Loading\nCheck Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });


        //Toast.makeText(getApplicationContext(),"Latitude: " + location.getLatitude() + "\n Longitude: " + location.getLongitude(),Toast.LENGTH_SHORT).show();

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            latti = location.getLatitude();
            longi = location.getLongitude();
        }           catch (Exception e) {

        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
       // Toast.makeText(getApplicationContext(), "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();


    }
}

class NotificationHelper extends ContextWrapper {
    private static final String CHANNEL_ID = "PT";
    private static final String CHANNEL_NAME = "PT_Channel";
    private NotificationManager manager;
    @RequiresApi(api = Build.VERSION_CODES.O)
    public NotificationHelper(Context base) {
        super(base);
        createChannels();
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createChannels() {
        NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID,CHANNEL_NAME,
                NotificationManager.IMPORTANCE_DEFAULT);
        notificationChannel.enableLights(true);
        notificationChannel.enableVibration(true);
        notificationChannel.setLightColor(Color.GREEN);

        notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        getManager().createNotificationChannel(notificationChannel);

    }
    public NotificationManager getManager() {
        if(manager ==  null)
        {
            manager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return manager;
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    public Notification.Builder getChannelNotification(String title, String body)
    {
        return new Notification.Builder(getApplicationContext(),CHANNEL_ID)
                .setContentText(body)
                .setContentTitle(title)
                .setSmallIcon(R.drawable.icon_app)
                .setAutoCancel(true);
    }
}




