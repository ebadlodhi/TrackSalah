package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface Registeronsignupapi {

    @FormUrlEncoded
    @POST("registeronsignup.php")
    Call<String> getDetails(@Field("name") String name, @Field("password") String password, @Field("email") String email, @Field("gender") String gender, @Field("dob") String dob, @Field("creationtime") String creationtime
            , @Field("nstartdate") String nstartdate,@Field("tenyear") String tenyear, @Field("MstartDate") String MstartDate, @Field("MendDate") String MendDate);

}
