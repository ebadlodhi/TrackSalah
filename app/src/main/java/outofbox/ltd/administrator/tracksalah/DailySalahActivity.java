package outofbox.ltd.administrator.tracksalah;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;


import android.net.ConnectivityManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import outofbox.ltd.administrator.tracksalah.Databasesqlite.DatabaseHelper;
import outofbox.ltd.administrator.tracksalah.Databasesqlite.POJORES;
import outofbox.ltd.administrator.tracksalah.POJO2.Example;
import outofbox.ltd.administrator.tracksalah.POJOcalenderData.CalenderDetailsFetch;
import outofbox.ltd.administrator.tracksalah.POJOnamazdetails.NamazDetailsfetch;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.FetchCalenderData;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.FetchNamazDetailsApi;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.NamazDetailsApi;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.QadahUpdate;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.SyncApi;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.enterOldQadahNamazDetailsApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static outofbox.ltd.administrator.tracksalah.R.drawable.nonetworksalah;
import static outofbox.ltd.administrator.tracksalah.R.drawable.offeredicon;

public class DailySalahActivity extends AppCompatActivity implements LocationListener {

    SharedPreferences sharedPreferences;
    LocationManager locationManager;
    Double latti, longi;

    public static String Fajarname = "";
    public static String Zoharname = "";
    public static String Asarname = "";
    public static String Maghribname = "";
    public static String Ishanamee = "";
    SharedPreferences sharedPreferencesmethod;
    String methodvalue;
    String Userid ;
    String todaydateinsertt;
    String Namaztimeinsertt;
    String ontimeinsertt;
    String lateinsertt ;
    String missedinsertt ;

    String farzdate = "";
    String startdate = "";
    private Tracker mTracker;

    //select data variable
    String Dateselect = "";
    ProgressBar Waitprogress;

    // insert namaz detail variable

    public static String todaydateinsert = "";
    public static String Namaztimeinsert = "";
    public static String ontimeinsert = "0", lateinsert = "0", missedinsert = "0",exemptedinsert = "0";

    TextView fname, zname, aname, mname, iname;

    String getdatee, getdayy, gethijrii, gettimezonee, getfajrr, getsunrisee, getduhrr, getasrr, getsunsett, getmaghribb, getishaa, getimsakk, getfiqqahname;
    Call<Example> call;
    NamazDetailsApi api;

    String User_id = "";
    final Calendar c = Calendar.getInstance();
    public static final String inputFormat = "hh:mm";
    SimpleDateFormat inputParser = new SimpleDateFormat("kk:mm");
    SimpleDateFormat inputParser2 = new SimpleDateFormat("dd-MMM-yyyy");
    String compareFajrOne = "05:10";
    String compareFajrTwo = "06:45";
    String compareZhrOne = "12:30";
    String compareZhrTwo = "16:00";
    String compareAsarOne = "16:00";
    String compareAsarTwo = "18:30";
    String compareMaghribOne = "18:35";
    String compareMaghribTwo = "20:00";
    String compareIshaOne = "20:05";
    String compareIshaTwo = "04:30";

    StringBuilder todaydate;
    String todaydatedate = "";
    public  int count = 0;

    private Date date;
    private Date dateComparefajrOne;
    private Date dateComparefajrTwo;
    private Date todayCompare;
    private Date onScreenCompare;

    private Date dateComparezoharOne;
    private Date dateComparezoharTwo;
    private Date dateCompareasarOne;
    private Date dateCompareasarTwo;
    private Date dateComparemaghribOne;
    private Date dateComparemaghribTwo;
    private Date dateCompareishaOne;
    private Date dateCompareishaTwo;

    public static Button fajrofer, Zoharoffer, asaroffer, maghriboffer, ishaoffer;
    String sMonth;
    String dayofweak;
    ImageButton back, forward;
    Point p;
    String daynumber = "";

    static String formattedDate = "";
    String formattedDateToday;
    SimpleDateFormat df;
    DateFormat dayname;
    String daytoday;
    private Context mContext;
    private Activity mActivity;


    private RelativeLayout mRelativeLayout;
    private Button mButton;

    private PopupWindow mPopupWindow;
    private AdView mAdView;

    String[] islamic_months = {"",
            "Muḥarram","Ṣafar","Rabī‘ al-awwal","Rabī‘ ath-thānī",
            "Jumādá al-ūlá","Jumādá al-ākhirah","Rajab","Sha‘bān",
            "Ramaḍān","Shawwāl","Dhū al-Qa‘dah","Dhū al-Ḥijjah"};
    public static TextView today, tdate, hijri, fajrstart, fjrend, zohrst, zohren, asrst, asren, maghribst, maghriben, ishast, ishaen;
    int yy, mm, dd, day;
    TimePicker t1;
    TextView ontimee;
    public static final String ROOT_URL = "http://185.123.99.207/~android215/TrackSalah/";
    public static final int NAME_SYNCED_WITH_SERVER = 1;
    public static final int NAME_NOT_SYNCED_WITH_SERVER = 0;
    public static final String DATA_SAVED_BROADCAST = "com.ebad.datasaved";
    public DatabaseHelper db;
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_daily_salah);
        try {
            registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        catch (Exception e)
        {
            Log.e("error",""+e);
        }


        //        unregisterReceiver(new NetworkStateChecker());
        db = new DatabaseHelper(getApplicationContext());
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Daily Salah");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("TrackSalahUser")
                .setAction("User id")
                .setLabel(DashboardActivity.Userid)
                .setValue(1)
                .build());
        compareFajrOne = DashboardActivity.compareFajrOne;
        compareFajrTwo = DashboardActivity.compareFajrTwo;
        compareZhrOne = DashboardActivity.compareZhrOne;
        compareZhrTwo = DashboardActivity.compareZhrTwo;
        compareAsarOne = DashboardActivity.compareAsarOne;
        compareAsarTwo = DashboardActivity.compareAsarTwo;
        compareMaghribOne = DashboardActivity.compareMaghribOne;
        compareMaghribTwo = DashboardActivity.compareMaghribTwo;
        compareIshaOne = DashboardActivity.compareIshaOne;
        compareIshaTwo = DashboardActivity.compareIshaTwo;
        sharedPreferences = getSharedPreferences(LoginActivity.loginpref, Context.MODE_PRIVATE);
        User_id = sharedPreferences.getString("preuid", "");
        farzdate = sharedPreferences.getString("preuserfarzdate","");
        startdate = sharedPreferences.getString("preuserstartdate","");
        mAdView = (AdView) findViewById(R.id.adView);
        MobileAds.initialize(this, "ca-app-pub-2825367626708661~7620129751");
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdView.setVisibility(View.VISIBLE);
            }
        });
        sharedPreferencesmethod = getSharedPreferences(SettingsActivity.methodvalue, Context.MODE_PRIVATE);
        methodvalue = sharedPreferencesmethod.getString("methodval", "2");
//        TextClock tclock =  findViewById(R.id.simpleTextClock);
        Waitprogress = (ProgressBar) findViewById(R.id.waitprogress);
        Waitprogress.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

        }
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(NamazDetailsApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();
        api = retrofit.create(NamazDetailsApi.class);
        getLocation();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
//            tclock.getFormat24Hour();
        }
        today = (TextView) findViewById(R.id.tday);
        tdate = (TextView) findViewById(R.id.tdate);
        hijri = (TextView) findViewById(R.id.hdate);
        fajrstart = (TextView) findViewById(R.id.fjrstart);
        fjrend = (TextView) findViewById(R.id.fajrend);
        zohrst = (TextView) findViewById(R.id.zoharstart);
        zohren = (TextView) findViewById(R.id.zoharend);
        asrst = (TextView) findViewById(R.id.asrstart);
        asren = (TextView) findViewById(R.id.asrend);
        maghribst = (TextView) findViewById(R.id.maghribstart);
        maghriben = (TextView) findViewById(R.id.maghribend);
        ishast = (TextView) findViewById(R.id.ishastart);
        ishaen = (TextView) findViewById(R.id.ishaend);
        back = (ImageButton) findViewById(R.id.backdate);
        forward = (ImageButton) findViewById(R.id.forwarddate);

        yy = c.get(Calendar.YEAR);
        mm = c.get(Calendar.MONTH);
        dd = c.get(Calendar.DAY_OF_MONTH);
        day = c.get(Calendar.DAY_OF_WEEK);
        fajrofer = (Button) findViewById(R.id.fajrada);
        Zoharoffer = (Button) findViewById(R.id.zoharoffer);
        asaroffer = (Button) findViewById(R.id.asaroffered);
        maghriboffer = (Button) findViewById(R.id.maghriboffered);
        ishaoffer = (Button) findViewById(R.id.ishaoffered);
//    mConstraintLayout = (ConstraintLayout) findViewById(R.id.r1);
        mContext = getApplicationContext();
        // Get the activity
        mActivity = DailySalahActivity.this;
// nNamaz name assignment
        fname = (TextView) findViewById(R.id.fajr);
        zname = (TextView) findViewById(R.id.zohar);
        aname = (TextView) findViewById(R.id.asr);
        mname = (TextView) findViewById(R.id.maghrib);
        iname = (TextView) findViewById(R.id.isha);
        Fajarname = fname.getText().toString();
        Zoharname = zname.getText().toString();
        Asarname = aname.getText().toString();
        Maghribname = mname.getText().toString();
        Ishanamee = iname.getText().toString();
        // Get the widgets reference from XML layout
        if (mm == 0)
            sMonth = "January";
        else if (mm == 1)
            sMonth = "Febuary";
        else if (mm == 2)
            sMonth = "March";
        else if (mm == 3)
            sMonth = "April";
        else if (mm == 4)
            sMonth = "May";
        else if (mm == 5)
            sMonth = "June";
        else if (mm == 6)
            sMonth = "July";
        else if (mm == 7)
            sMonth = "August";
        else if (mm == 8)
            sMonth = "September";
        else if (mm == 9)
            sMonth = "October";
        else if (mm == 10)
            sMonth = "November";
        else if (mm == 11)
            sMonth = "December";
        switch (day) {
            case Calendar.SUNDAY: {
                dayofweak = "SUNDAY";
                break;
            }
            case Calendar.MONDAY: {
                dayofweak = "MONDAY";
                break;
            }
            case Calendar.TUESDAY: {
                dayofweak = "TUESDAY";
                break;
            }
            case Calendar.WEDNESDAY: {
                dayofweak = "WEDNESDAY";
                break;
            }
            case Calendar.THURSDAY: {
                dayofweak = "THURSDAY";
                break;
            }
            case Calendar.FRIDAY: {
                dayofweak = "FRIDAY";
                break;
            }
            case Calendar.SATURDAY: {
                dayofweak = "SATURDAY";
                break;
            }
        }
        // set current date into textview
//        tdate.setText(new StringBuilder()
//                // Month is 0 based, just add 1
//                .append(dd).append("-").append(sMonth).append("-")
//                .append(yy));

        todaydate = new StringBuilder()
                // Month is 0 based, just add 1
                .append(dd).append("-").append(mm).append("-")
                .append(yy);
        todaydatedate = todaydate.toString();

        df = new SimpleDateFormat("dd-MMM-yyyy");
        formattedDateToday = df.format(c.getTime());
        formattedDate = df.format(c.getTime());

        todayCompare = parseDateeb2(formattedDateToday);
        daynumber = df.format(c.getTime());

        dayname = new SimpleDateFormat("EEEE");
        DateFormat format1 = new SimpleDateFormat("d-MMM-yyyy",Locale.US);
        Bundle bundle = getIntent().getExtras();
        formattedDate = bundle.getString("ClickedDate",df.format(c.getTime()));
        count = bundle.getInt("countvalue",0);
        // Toast.makeText(getApplicationContext(), "count value = "+count, Toast.LENGTH_SHORT).show();

        cal(count);

        // Toast.makeText(mContext, User_id, Toast.LENGTH_SHORT).show();

//        SelectNamazData selectNamazData = new SelectNamazData() ;
//        selectNamazData.execute(User_id,formattedDate);


        Date date = null;
        try {
            date =  format1.parse(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.setTime(date);
        daytoday = dayname.format(date);
        tdate.setText(formattedDate,TextView.BufferType.SPANNABLE);
//TextView textView = (TextView)findViewById( R.id.TextView );
        Spannable spannable = (Spannable)tdate.getText();
//StyleSpan boldSpan = new StyleSpan(ForegroundColorSpan(Color.WHITE));
        spannable.setSpan( new ForegroundColorSpan(Color.rgb(245, 245, 245)), 2, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE );
        spannable.setSpan( new ForegroundColorSpan(Color.rgb(245, 245, 245)), 6, 7, Spannable.SPAN_INCLUSIVE_INCLUSIVE );

        Date currentt = Calendar.getInstance().getTime();
        SimpleDateFormat nn = new SimpleDateFormat("dd-MMM-yyyy",Locale.US);
        String curr = nn.format(currentt);
        String scree = tdate.getText().toString().trim();
        if(curr.equals(scree))
        {
            forward.setVisibility(View.GONE);
        }
        else
        {
            forward.setVisibility(View.VISIBLE);
        }

        today.setText(daytoday);
        FetchNamazDetails(User_id, formattedDate);
        loadNames(User_id, formattedDate);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forward.setVisibility(View.VISIBLE);
                Waitprogress.setVisibility(View.VISIBLE);
                String currentdateonscreen = tdate.getText().toString();
                DateFormat format = new SimpleDateFormat("d-MMM-yyyy",Locale.US);
                Date date = null;
                try {
                    date =  format.parse(currentdateonscreen);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                c.setTime(date);
                c.add(Calendar.DATE, -1);
                Log.d("test tag" ,"onClick:  "+currentdateonscreen+"\n"+Calendar.DATE);
                formattedDate = df.format(c.getTime());
                onScreenCompare = parseDateeb2(formattedDate);
                daytoday = dayname.format(c.getTime());
//                Toast.makeText(Main2Activity.this, "PREVIOUS DATE : "+formattedDate +"  "+daytoday, Toast.LENGTH_SHORT).show();
                tdate.setText(formattedDate,TextView.BufferType.SPANNABLE);
//TextView textView = (TextView)findViewById( R.id.TextView );
                Spannable spannable = (Spannable)tdate.getText();
//StyleSpan boldSpan = new StyleSpan(ForegroundColorSpan(Color.WHITE));
                spannable.setSpan( new ForegroundColorSpan(Color.rgb(245, 245, 245)), 2, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE );
                spannable.setSpan( new ForegroundColorSpan(Color.rgb(245, 245, 245)), 6, 7, Spannable.SPAN_INCLUSIVE_INCLUSIVE );
                today.setText(daytoday);
                count = count - 2;
                Log.d(String.valueOf(count), "");
//                Toast.makeText(mContext, count+"\n"+onScreenCompare+"\n"+todayCompare, Toast.LENGTH_SHORT).show();
                cal(count);
                todaydateinsert = formattedDate;
//                SelectNamazData selectNamazData = new SelectNamazData();
//                selectNamazData.execute(User_id, formattedDate);
                FetchNamazDetailsback(User_id,formattedDate);
                loadNames(User_id, formattedDate);
            }


        });
        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date currentt = Calendar.getInstance().getTime();
                SimpleDateFormat nn = new SimpleDateFormat("dd-MMM-yyyy",Locale.US);
                String curr = nn.format(currentt);
                String scree = tdate.getText().toString().trim();
                if(curr.equals(scree))
                {
                    forward.setVisibility(View.GONE);
                }
                else
                {
                    forward.setVisibility(View.VISIBLE);
                }

                Waitprogress.setVisibility(View.VISIBLE);
                String currentdateonscreen = tdate.getText().toString();
                DateFormat format = new SimpleDateFormat("d-MMM-yyyy",Locale.US);
                Date date = null;
                try {
                    date =  format.parse(currentdateonscreen);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                c.setTime(date);

                c.add(Calendar.DATE, 1);
                formattedDate = df.format(c.getTime());
                onScreenCompare = parseDateeb2(formattedDate);
                daytoday = dayname.format(c.getTime());
                todaydateinsert = formattedDate;
                Log.v("NEXT DATE : ", formattedDate);
                tdate.setText(formattedDate,TextView.BufferType.SPANNABLE);
//TextView textView = (TextView)findViewById( R.id.TextView );
                Spannable spannable = (Spannable)tdate.getText();
//StyleSpan boldSpan = new StyleSpan(ForegroundColorSpan(Color.WHITE));
                spannable.setSpan( new ForegroundColorSpan(Color.rgb(245, 245, 245)), 2, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE );
                spannable.setSpan( new ForegroundColorSpan(Color.rgb(245, 245, 245)), 6, 7, Spannable.SPAN_INCLUSIVE_INCLUSIVE );
                today.setText(daytoday);
                count = count + 2;
//                Toast.makeText(mContext, count+"\n"+onScreenCompare+"\n"+todayCompare, Toast.LENGTH_SHORT).show();
                cal(count);
//                SelectNamazData selectNamazData = new SelectNamazData();
//                selectNamazData.execute(User_id, formattedDate);
                FetchNamazDetailsforward(User_id,formattedDate);
                loadNames(User_id, formattedDate);
            }

        });
//        Saveinsert.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                //Toast.makeText(mContext, "Your data SAve Sucessfullly\n" + todaydateinsert + "\n" + Namaztimeinsert + "\n" + ontimeinsert + "\n" + lateinsert, Toast.LENGTH_SHORT).show();
////                InsertNamazDetails insertNamazDetails = new InsertNamazDetails(this);
//////                insertNamazDetails.execute("login", Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
////                insertNamazDetails.execute("login",todaydateinsert,Namaztimeinsert,ontimeinsert,lateinsert,missedinsert);
// SelectNamazData selectNamazData = new SelectNamazData(this);
//        selectNamazData.execute(User_id,formattedDate);
//           }
//       });


//        SelectCalenderData selectCalenderData = new SelectCalenderData();
//        selectCalenderData.execute(User_id);
        FetchCalenderDetails(User_id);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.normal_menu, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.refresh1: {
                startActivity(getIntent());
                finish();
                break;
            }
            case android.R.id.home:{
                onBackPressed();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }
//    @Override
//    protected void onResume()
//    {
//        super.onResume();
//        Bundle b = getIntent().getExtras();
//        String D = b.getString("ClickedDate",formattedDate);
//        //Toast.makeText(mContext, D, Toast.LENGTH_SHORT).show();
//        tdate.setText(D);
//        FetchNamazDetails(User_id, D);
//
////        Calendar calendar = Calendar.getInstance();
////        Date newDate = null;
////        try {
////             newDate = df.parse(b.toString());
////        } catch (ParseException e) {
////            e.printStackTrace();
////        }
////        calendar.setTime(newDate);
////    today.setText(calendar.get(Calendar.DAY_OF_MONTH));
//            loadNames(User_id, D);
//
//    }


    void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public void     cal(int count) {

        fajrofer.setBackgroundResource(offeredicon);
        Zoharoffer.setBackgroundResource(offeredicon);
        asaroffer.setBackgroundResource(offeredicon);
        maghriboffer.setBackgroundResource(offeredicon);
        ishaoffer.setBackgroundResource(offeredicon);

        if (count < 0) {

            fajrofer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    DateFormat format1 = new SimpleDateFormat("yyyy-MM-d",Locale.US);
                    DateFormat format2 = new SimpleDateFormat("dd-MMM-yyyy",Locale.US);

                    Date fdate = null;
                    Date sdate = null;
                    Date formatdate = null;
                    try {
                        fdate = format1.parse(farzdate.toString().trim());
                        sdate = format1.parse(startdate.toString().trim());
                        formatdate = format2.parse(formattedDate.toString().trim());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    Log.d("fdate", String.valueOf(fdate));
                    Log.d("sdate", String.valueOf(sdate));
                    Log.d("formatdate", String.valueOf(formatdate));

                    if(fajrofer.getText().equals("."))
                    {
                        Toast.makeText(DailySalahActivity.this, "Exepmted date can't be marked", Toast.LENGTH_SHORT).show();
                    }

                    else if(fdate.before(formatdate) && sdate.after(formatdate) )
                    {
                        QadahRangeRecord("Fajar",formattedDate);
                    }
                    else
                    {
                        if (p != null ) {
                            showPopup(DailySalahActivity.this, p, "Fajar", formattedDate);
                        }

                    }


                }


            });


            Zoharoffer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    DateFormat format1 = new SimpleDateFormat("yyyy-MM-d",Locale.US);
                    DateFormat format2 = new SimpleDateFormat("dd-MMM-yyyy",Locale.US);

                    Date fdate = null;
                    Date sdate = null;
                    Date formatdate = null;
                    try {
                        fdate = format1.parse(farzdate.toString().trim());
                        sdate = format1.parse(startdate.toString().trim());
                        formatdate = format2.parse(formattedDate.toString().trim());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if(Zoharoffer.getText().equals("."))
                    {
                        Toast.makeText(DailySalahActivity.this, "Exepmted date can't be marked", Toast.LENGTH_SHORT).show();
                    }

                    else if(fdate.before(formatdate) && sdate.after(formatdate) )
                    {
                        QadahRangeRecord("Zohar",formattedDate);
                    }
                    else
                    {
                        if (p != null) {
                            showPopup(DailySalahActivity.this, p, "Zohar", formattedDate);
                        }

                    }







                }
            });


            asaroffer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DateFormat format1 = new SimpleDateFormat("yyyy-MM-d",Locale.US);
                    DateFormat format2 = new SimpleDateFormat("dd-MMM-yyyy",Locale.US);

                    Date fdate = null;
                    Date sdate = null;
                    Date formatdate = null;
                    try {
                        fdate = format1.parse(farzdate.toString().trim());
                        sdate = format1.parse(startdate.toString().trim());
                        formatdate = format2.parse(formattedDate.toString().trim());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if(asaroffer.getText().equals("."))
                    {
                        Toast.makeText(DailySalahActivity.this, "Exepmted date can't be marked", Toast.LENGTH_SHORT).show();
                    }

                    else  if(fdate.before(formatdate) && sdate.after(formatdate) )
                    {
                        QadahRangeRecord("Asar",formattedDate);
                    }
                    else
                    {
                        if (p != null) {
                            showPopup(DailySalahActivity.this, p, "Asar", formattedDate);
                        }

                    }




                }
            });


            maghriboffer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    DateFormat format1 = new SimpleDateFormat("yyyy-MM-d",Locale.US);
                    DateFormat format2 = new SimpleDateFormat("dd-MMM-yyyy",Locale.US);

                    Date fdate = null;
                    Date sdate = null;
                    Date formatdate = null;
                    try {
                        fdate = format1.parse(farzdate.toString().trim());
                        sdate = format1.parse(startdate.toString().trim());
                        formatdate = format2.parse(formattedDate.toString().trim());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if(maghriboffer.getText().equals("."))
                    {
                        Toast.makeText(DailySalahActivity.this, "Exepmted date can't be marked", Toast.LENGTH_SHORT).show();
                    }

                    else  if(fdate.before(formatdate) && sdate.after(formatdate) )
                    {
                        QadahRangeRecord("Maghrib",formattedDate);
                    }
                    else
                    {
                        if (p != null) {
                            showPopup(DailySalahActivity.this, p, "Maghrib", formattedDate);
                        }

                    }





                }
            });


            ishaoffer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DateFormat format1 = new SimpleDateFormat("yyyy-MM-d",Locale.US);
                    DateFormat format2 = new SimpleDateFormat("dd-MMM-yyyy",Locale.US);

                    Date fdate = null;
                    Date sdate = null;
                    Date formatdate = null;
                    try {
                        fdate = format1.parse(farzdate.toString().trim());
                        sdate = format1.parse(startdate.toString().trim());
                        formatdate = format2.parse(formattedDate.toString().trim());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if(ishaoffer.getText().equals("."))
                    {
                        Toast.makeText(DailySalahActivity.this, "Exepmted date can't be marked", Toast.LENGTH_SHORT).show();
                    }

                    else if(fdate.before(formatdate) && sdate.after(formatdate) )
                    {
                        QadahRangeRecord("Isha",formattedDate);
                    }
                    else
                    {
                        if (p != null) {
                            showPopup(DailySalahActivity.this, p, "Isha", formattedDate);
                        }

                    }




                }


            });

        } else if (count == 0) {

            final boolean[] ispressed = {false};


            Calendar now = Calendar.getInstance();

            final int hour = now.get(Calendar.HOUR_OF_DAY);
            final int minute = now.get(Calendar.MINUTE);


            date = parseDateeb(Integer.toString(hour) + ":" + Integer.toString(minute));


//        final StringBuilder fjarst = new StringBuilder().append("\"").append(compareStringOne).append("\"");
//        fajrstt = fjarst.toString();
//        StringBuilder fjarend = new StringBuilder().append("\"").append(compareStringTwo).append("\"");
//
//         fajrendd = fjarend.toString();
            dateComparefajrOne = parseDateeb(compareFajrOne);
            dateComparefajrTwo = parseDateeb(compareFajrTwo);
            final TextView Fajr = (TextView) findViewById(R.id.fajr);
            fajrofer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    //Toast.makeText(mContext, compareFajrOne + "\n" + compareFajrTwo + "\n" + dateComparefajrOne + "\n" + date + "\n" + dateComparefajrTwo, Toast.LENGTH_LONG).show();
                    if (fajrofer.getText().equals(".")) {
                        Toast.makeText(DailySalahActivity.this, "Exepmted date can't be marked", Toast.LENGTH_SHORT).show();
                    } else {

                        if (dateComparefajrOne.before(date) && dateComparefajrTwo.after(date)) {

                            if (!ispressed[0]) {
                                Namaztimeinsert = "Fajar";
                                todaydateinsert = formattedDate;
                                ontimeinsert = "ontime";
                                lateinsert = "No";
                                missedinsert = "No";
                                exemptedinsert = "No";

                                db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);

                                DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);


                                registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                                // NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);

                            } else {
                                Namaztimeinsert = "Fajar";
                                todaydateinsert = formattedDate;
                                ontimeinsert = "No";
                                lateinsert = "No";
                                missedinsert = "missed";

                                exemptedinsert = "No";

                                db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                                DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworkmissed);

                                registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

//                            NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//                            fajrofer.setBackgroundResource(R.drawable.notofferedicon);
                            }

                            ispressed[0] = !ispressed[0];

                        } else if (date.before(dateComparefajrOne)) {
                            Toast.makeText(mContext, "Namaz-e-" + Fajr.getText().toString() + " time not started", Toast.LENGTH_SHORT).show();
                        } else {
                            if (p != null)
                                showPopup(DailySalahActivity.this, p, "Fajar", formattedDate);
                        }


                    }

                }
            });

            dateComparezoharOne = parseDateeb(compareZhrOne);
            dateComparezoharTwo = parseDateeb(compareZhrTwo);
            final TextView Zohar = (TextView) findViewById(R.id.zohar);
            Zoharoffer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (Zoharoffer.getText().equals(".")) {
                        Toast.makeText(DailySalahActivity.this, "Exepmted date can't be marked", Toast.LENGTH_SHORT).show();
                    } else {
                        if (dateComparezoharOne.before(date) && dateComparezoharTwo.after(date)) {
                            if (!ispressed[0]) {
                                Namaztimeinsert = "Zohar";
                                todaydateinsert = formattedDate;
                                ontimeinsert = "ontime";
                                lateinsert = "No";
                                missedinsert = "No";

                                exemptedinsert = "No";

                                db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                                DailySalahActivity.Zoharoffer.setBackgroundResource(R.drawable.nonetworksalah);

                                registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

//                            NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//
//                            Zoharoffer.setBackgroundResource(offeredicon);

                            } else {
                                Namaztimeinsert = "Zohar";
                                todaydateinsert = formattedDate;
                                ontimeinsert = "No";
                                lateinsert = "No";
                                missedinsert = "missed";
                                exemptedinsert = "No";

                                db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                                DailySalahActivity.Zoharoffer.setBackgroundResource(R.drawable.nonetworkmissed);

                                registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
//                            NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//                            Zoharoffer.setBackgroundResource(R.drawable.notofferedicon);
                            }

                            ispressed[0] = !ispressed[0];

                        } else if (date.before(dateComparezoharOne)) {

                            Toast.makeText(mContext, "Namaz-e-" + Zohar.getText().toString() + " time not started", Toast.LENGTH_SHORT).show();


                        } else {
                            if (p != null)
                                showPopup(DailySalahActivity.this, p, "Zohar", formattedDate);
                        }

                    }
                }
            });

            dateCompareasarOne = parseDateeb(compareAsarOne);
            dateCompareasarTwo = parseDateeb(compareAsarTwo);
            final TextView Asar = (TextView) findViewById(R.id.asr);

            asaroffer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Toast.makeText(mContext, compareFajrOne + "\n" + compareFajrTwo + "\n" + dateComparefajrOne + "\n" + date + "\n" + dateComparefajrTwo, Toast.LENGTH_LONG).show();
                    if (asaroffer.getText().equals(".")) {
                        Toast.makeText(DailySalahActivity.this, "Exepmted date can't be marked", Toast.LENGTH_SHORT).show();
                    } else {
                        if (dateCompareasarOne.before(date) && dateCompareasarTwo.after(date)) {
                            if (!ispressed[0]) {
                                Namaztimeinsert = "Asar";
                                todaydateinsert = formattedDate;
                                ontimeinsert = "ontime";
                                lateinsert = "No";
                                missedinsert = "No";

                                exemptedinsert = "No";

                                db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                                DailySalahActivity.asaroffer.setBackgroundResource(R.drawable.nonetworksalah);

                                registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

//                            NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//
//                            asaroffer.setBackgroundResource(offeredicon);

                            } else {
                                Namaztimeinsert = "Asar";
                                todaydateinsert = formattedDate;
                                ontimeinsert = "No";
                                lateinsert = "No";
                                missedinsert = "missed";

                                exemptedinsert = "No";

                                db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                                DailySalahActivity.asaroffer.setBackgroundResource(R.drawable.nonetworkmissed);

                                registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

//                            NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//                            asaroffer.setBackgroundResource(R.drawable.notofferedicon);
                            }

                            ispressed[0] = !ispressed[0];
                        } else if (date.before(dateCompareasarOne)) {

                            Toast.makeText(mContext, "Namaz-e-" + Asar.getText() + " time not started", Toast.LENGTH_SHORT).show();


                        } else {
                            if (p != null)
                                showPopup(DailySalahActivity.this, p, "Asar", formattedDate);
                        }


                    }
                }
            });

            dateComparemaghribOne = parseDateeb(compareMaghribOne);
            dateComparemaghribTwo = parseDateeb(compareMaghribTwo);
            final TextView Maghrib = (TextView) findViewById(R.id.maghrib);

            maghriboffer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (maghriboffer.getText().equals(".")) {
                        Toast.makeText(DailySalahActivity.this, "Exepmted date can't be marked", Toast.LENGTH_SHORT).show();
                    } else {
                        if (dateComparemaghribOne.before(date) && dateComparemaghribTwo.after(date)) {
                            if (!ispressed[0]) {
                                Namaztimeinsert = "Maghrib";
                                todaydateinsert = formattedDate;
                                ontimeinsert = "ontime";
                                lateinsert = "No";
                                missedinsert = "No";

                                exemptedinsert = "No";

                                db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                                DailySalahActivity.maghriboffer.setBackgroundResource(R.drawable.nonetworksalah);

                                registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
//                            NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//
//                            maghriboffer.setBackgroundResource(offeredicon);

                            } else {
                                Namaztimeinsert = "Maghrib";
                                todaydateinsert = formattedDate;
                                ontimeinsert = "No";
                                lateinsert = "No";
                                missedinsert = "missed";
                                exemptedinsert = "No";

                                db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                                DailySalahActivity.maghriboffer.setBackgroundResource(R.drawable.nonetworkmissed);

                                registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

//                            NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//                            maghriboffer.setBackgroundResource(R.drawable.notofferedicon);
                            }

                            ispressed[0] = !ispressed[0];
                        } else if (date.before(dateComparemaghribOne)) {

                            Toast.makeText(mContext, "Namaz-e-" + Maghrib.getText() + " time not started", Toast.LENGTH_SHORT).show();


                        } else {
                            if (p != null)
                                showPopup(DailySalahActivity.this, p, "Maghrib", formattedDate);
                        }

                    }
                }
            });

            dateCompareishaOne = parseDateeb(compareIshaOne);
            dateCompareishaTwo = parseDateeb(compareIshaTwo);
            final TextView Isha = (TextView) findViewById(R.id.isha);

            ishaoffer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ishaoffer.getText().equals(".")) {
                        Toast.makeText(DailySalahActivity.this, "Exepmted date can't be marked", Toast.LENGTH_SHORT).show();
                    } else {
                        if (dateCompareishaOne.before(date) && dateCompareishaTwo.after(date)) {
                            if (!ispressed[0]) {
                                Namaztimeinsert = "Isha";
                                todaydateinsert = formattedDate;
                                ontimeinsert = "ontime";
                                lateinsert = "No";
                                missedinsert = "No";

                                exemptedinsert = "No";

                                db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                                DailySalahActivity.ishaoffer.setBackgroundResource(R.drawable.nonetworksalah);

                                registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

//                            NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//
//                            ishaoffer.setBackgroundResource(offeredicon);

                            } else {
                                Namaztimeinsert = "Isha";
                                todaydateinsert = formattedDate;
                                ontimeinsert = "No";
                                lateinsert = "No";
                                missedinsert = "missed";
                                exemptedinsert = "No";

                                db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                                DailySalahActivity.ishaoffer.setBackgroundResource(R.drawable.nonetworkmissed);

                                registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

//                            NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//                            ishaoffer.setBackgroundResource(R.drawable.notofferedicon);
                            }

                            ispressed[0] = !ispressed[0];
                        } else if (date.before(dateCompareishaOne)) {

                            Toast.makeText(mContext, "Namaz-e-" + Isha.getText() + " time not started", Toast.LENGTH_SHORT).show();


                        } else {

                            if (p != null)
                                showPopup(DailySalahActivity.this, p, "Isha", formattedDate);


                        }
                    }
                }
            });
            Namaztimeinsert = "Isha";

        } else if (count > 0) {

            Waitprogress.setVisibility(View.VISIBLE);


//        final StringBuilder fjarst = new StringBuilder().append("\"").append(compareStringOne).append("\"");
//        fajrstt = fjarst.toString();
//        StringBuilder fjarend = new StringBuilder().append("\"").append(compareStringTwo).append("\"");
//
//         fajrendd = fjarend.toString();


            fajrofer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Toast.makeText(mContext, "Can't mark future Namaz", Toast.LENGTH_SHORT).show();

                }


            });


            Zoharoffer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Toast.makeText(mContext, "Can't mark future Namaz", Toast.LENGTH_SHORT).show();

                }
            });


            asaroffer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(mContext, "Can't mark future Namaz", Toast.LENGTH_SHORT).show();

                }
            });


            maghriboffer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Toast.makeText(mContext, "Can't mark future Namaz", Toast.LENGTH_SHORT).show();

                }
            });


            ishaoffer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Toast.makeText(mContext, "Can't mark future Namaz", Toast.LENGTH_SHORT).show();


                }


            });

        }


    }

    @Override
    public void onLocationChanged(Location location) {
        latti = location.getLatitude();
        longi = location.getLongitude();


        long unixTime = System.currentTimeMillis() / 1000L;
        call = api.getHeroes("http://api.aladhan.com/v1/timings/" + unixTime + "?latitude=" + latti + "&longitude=" + longi + "&method=" + methodvalue);


        call.enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {

                getdatee = response.body().getData().getDate().getReadable();
                getdayy = response.body().getData().getDate().getGregorian().getWeekday().getEn();
                gethijrii = response.body().getData().getDate().getHijri().getDate();
                gettimezonee = response.body().getData().getMeta().getTimezone();
                getfajrr = response.body().getData().getTimings().getFajr();
                getduhrr = response.body().getData().getTimings().getDhuhr();
                getasrr = response.body().getData().getTimings().getAsr();
                getmaghribb = response.body().getData().getTimings().getMaghrib();
                getishaa = response.body().getData().getTimings().getIsha();
                getsunrisee = response.body().getData().getTimings().getSunrise();
                getsunsett = response.body().getData().getTimings().getSunset();
                getfiqqahname = response.body().getData().getMeta().getMethod().getName();
                getimsakk = response.body().getData().getTimings().getImsak();
                String[] breakit = gethijrii.split("-");
                String to_send = breakit[0]+"-"+islamic_months[Integer.parseInt(breakit[1])]+"-"+breakit[2];
                hijri.setText(to_send);
                fajrstart.setText(getfajrr);
                fjrend.setText(getsunrisee);
                zohrst.setText(getduhrr);
                zohren.setText(getasrr);
                asrst.setText(getasrr);
                asren.setText(getsunsett);
                maghribst.setText(getmaghribb);
                maghriben.setText(getishaa);
                ishast.setText(getishaa);
                ishaen.setText(getfajrr);


            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                //  Toast.makeText(getApplicationContext(), "Namaz Times Loading Error\nPlease Check GPS and Internet ", Toast.LENGTH_SHORT).show();
            }
        });

        //Toast.makeText(getApplicationContext(),"Latitude: " + location.getLatitude() + "\n Longitude: " + location.getLongitude(),Toast.LENGTH_SHORT).show();

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            latti = location.getLatitude();
            longi = location.getLongitude();
        } catch (Exception e) {

        }


    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        // Toast.makeText(getApplicationContext(), "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();

    }


//        TabHost tabHost = (TabHost) findViewById(android.R.id.tabhost);
//        // initiate TabHost
//        tabHost.setup(this.getLocalActivityManager());
//        TabHost.TabSpec spec; // Reusable TabSpec for each tab
//        Intent intent;
//
//        spec = tabHost.newTabSpec("dailysalah"); // Create a new TabSpec using tab host
//        spec.setIndicator("Daily Salah"); // set the “HOME” as an indicator
//        // Create an Intent to launch an Activity for the tab (to be reused)
//        intent = new Intent(this, .class);
//        spec.setContent(intent);
//        tabHost.addTab(spec);
//
//
//        spec = tabHost.newTabSpec("DailyCalender"); // Create a new TabSpec using tab host
//        spec.setIndicator("Daily Calender View"); // set the “CONTACT” as an indicator
//        // Create an Intent to launch an Activity for the tab (to be reused)
//        intent = new Intent(this, DailyCalenderActivity.class);
//        spec.setContent(intent);
//        tabHost.addTab(spec);
//
//        spec = tabHost.newTabSpec("TodayNamazDetail"); // Create a new TabSpec using tab host
//        spec.setIndicator("Today Namaz Detail"); // set the “ABOUT” as an indicator
//        // Create an Intent to launch an Activity for the tab (to be reused)
//        intent = new Intent(this, DailyNamazDetailActivity.class);
//        spec.setContent(intent);
//        tabHost.addTab(spec);
//        //set tab which one you want to open first time 0 or 1 or 2
//        tabHost.setCurrentTab(0);
//        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
//            @Override
//            public void onTabChanged(String tabId) {
//                // display the name of the tab whenever a tab is changed
////                Toast.makeText(getApplicationContext(), tabId, Toast.LENGTH_SHORT).show();
//            }
//        });


    public void onWindowFocusChanged(boolean hasFocus) {

        int[] location = new int[2];
        ImageButton button = (ImageButton) findViewById(R.id.fjricon);

        // Get the x, y location and store it in the location[] array
        // location[0] = x, location[1] = y.
        button.getLocationOnScreen(location);

        //Initialize the Point with x, and y positions
        p = new Point();
        p.x = location[0];
        p.y = location[1];
    }

    // The method that displays the popup.
    private void showPopup(final Activity context, final Point p, String name, final String formattedDate) {
        int popupWidth = 750;
        int popupHeight = 1100;
        final String dateebad = formattedDate;
        final String nameebad = name;


//              t1 = (TimePicker) findViewById(R.id.timePicker1);

        // Inflate the popup_layout.xml
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popup);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.pop_up_late, viewGroup);

        // Creating the PopupWindow
        final PopupWindow popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setFocusable(true);
        ((TextView) popup.getContentView().findViewById(R.id.daytext)).setText(nameebad);
        ((TextView) popup.getContentView().findViewById(R.id.daydate)).setText(dateebad);

        // Some offset to align the popup a bit to the right, and a bit down, relative to button's position.
        int OFFSET_X = 30;
        int OFFSET_Y = 30;

        // Clear the default translucent background
        // Displaying the popup at the specified location, + offsets.
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x + OFFSET_X, p.y + OFFSET_Y);

        // Getting a reference to Close button, and close the popup when clicked.
        Button timeselect = (Button) layout.findViewById(R.id.tpicker);
        timeselect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (nameebad.equals("Fajar")) {
                    Namaztimeinsert = "Fajar";
                    todaydateinsert = formattedDate;
                    ontimeinsert = "ontime";
                    lateinsert = "No";
                    missedinsert = "No";

                    exemptedinsert = "No";

                   boolean res =  db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);

                    DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);

                    if (res == true)
                        registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));


                    //  NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);


                } else if (nameebad.equals("Zohar")) {
                    Namaztimeinsert = "Zohar";
                    todaydateinsert = formattedDate;
                    ontimeinsert = "ontime";
                    lateinsert = "No";
                    missedinsert = "No";
                    exemptedinsert = "No";

                    boolean res =  db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                    DailySalahActivity.Zoharoffer.setBackgroundResource(R.drawable.nonetworksalah);
                    if (res == true)
                        registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

                    //NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);


                    //  DailySalahActivity.Zoharoffer.setBackgroundResource(offeredicon);

                } else if (nameebad.equals("Asar")) {
                    Namaztimeinsert = "Asar";
                    todaydateinsert = formattedDate;
                    ontimeinsert = "ontime";
                    lateinsert = "No";
                    missedinsert = "No";

                    exemptedinsert = "No";

                    boolean res =  db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                    DailySalahActivity.asaroffer.setBackgroundResource(R.drawable.nonetworksalah);
                    if (res == true)
                        registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

//                    NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//
//                    DailySalahActivity.asaroffer.setBackgroundResource(offeredicon);

                } else if (nameebad.equals("Maghrib")) {
                    Namaztimeinsert = "Maghrib";
                    todaydateinsert = formattedDate;
                    ontimeinsert = "ontime";
                    lateinsert = "No";
                    missedinsert = "No";

                    exemptedinsert = "No";

                    boolean res =  db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                    DailySalahActivity.maghriboffer.setBackgroundResource(R.drawable.nonetworksalah);
                    if (res == true)
                        registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

//                    NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//
//                    DailySalahActivity.maghriboffer.setBackgroundResource(offeredicon);

                } else if (nameebad.equals("Isha")) {
                    Namaztimeinsert = "Isha";
                    todaydateinsert = formattedDate;
                    ontimeinsert = "ontime";
                    lateinsert = "No";
                    missedinsert = "No";
                    exemptedinsert = "No";

                    boolean res =  db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                    DailySalahActivity.ishaoffer.setBackgroundResource(R.drawable.nonetworksalah);
                    if (res == true)
                        registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

//                    NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//                    DailySalahActivity.ishaoffer.setBackgroundResource(offeredicon);

                }



                ontimeinsert = "1";
                popup.dismiss();
            }


        });
        Button late = (Button) layout.findViewById(R.id.latebutton);
        late.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (nameebad.equals("Fajar")) {
                    Namaztimeinsert = "Fajar";
                    todaydateinsert = formattedDate;
                    ontimeinsert = "No";
                    lateinsert = "late";
                    missedinsert = "No";
                    exemptedinsert = "No";

                    boolean res =  db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                    DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworklate);

                    registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

//                    NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//
//                    DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.lateofferedicon);

                } else if (nameebad.equals("Zohar")) {
                    Namaztimeinsert = "Zohar";
                    todaydateinsert = formattedDate;
                    ontimeinsert = "No";
                    lateinsert = "late";
                    missedinsert = "No";
                    exemptedinsert = "No";

                    boolean res =  db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                    DailySalahActivity.Zoharoffer.setBackgroundResource(R.drawable.nonetworklate);

                    registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
//                    NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//
//                    DailySalahActivity.Zoharoffer.setBackgroundResource(R.drawable.lateofferedicon);

                } else if (nameebad.equals("Asar")) {
                    Namaztimeinsert = "Asar";
                    todaydateinsert = formattedDate;
                    ontimeinsert = "No";
                    lateinsert = "late";
                    missedinsert = "No";
                    exemptedinsert = "No";

                    boolean res =  db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                    DailySalahActivity.asaroffer.setBackgroundResource(R.drawable.nonetworklate);

                    registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));


//                    NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//
//                    DailySalahActivity.asaroffer.setBackgroundResource(R.drawable.lateofferedicon);

                } else if (nameebad.equals("Maghrib")) {
                    Namaztimeinsert = "Maghrib";
                    todaydateinsert = formattedDate;
                    ontimeinsert = "No";
                    lateinsert = "late";
                    missedinsert = "No";

                    exemptedinsert = "No";

                    boolean res =  db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                    DailySalahActivity.maghriboffer.setBackgroundResource(R.drawable.nonetworklate);

                    registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

//                    NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//
//                    DailySalahActivity.maghriboffer.setBackgroundResource(R.drawable.lateofferedicon);

                } else if (nameebad.equals("Isha")) {
                    Namaztimeinsert = "Isha";
                    todaydateinsert = formattedDate;
                    ontimeinsert = "No";
                    lateinsert = "late";
                    missedinsert = "No";
                    exemptedinsert = "No";

                    boolean res =  db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                    DailySalahActivity.ishaoffer.setBackgroundResource(R.drawable.nonetworklate);

                    registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

//                    NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//
//                    DailySalahActivity.ishaoffer.setBackgroundResource(R.drawable.lateofferedicon);

                }

                lateinsert = "1";

                popup.dismiss();
            }
        });
//
        Button lastminute = (Button) layout.findViewById(R.id.lastminute);
        lastminute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (nameebad.equals("Fajar")) {
                    Namaztimeinsert = "Fajar";
                    todaydateinsert = formattedDate;
                    ontimeinsert = "No";
                    lateinsert = "No";
                    missedinsert = "missed";

                    exemptedinsert = "No";

                    boolean res =  db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                    DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworkmissed);

                    registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

//                    NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//
//                    DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.notofferedicon);

                } else if (nameebad.equals("Zohar")) {
                    Namaztimeinsert = "Zohar";
                    todaydateinsert = formattedDate;
                    ontimeinsert = "No";
                    lateinsert = "No";
                    missedinsert = "missed";
                    exemptedinsert = "No";

                    boolean res =  db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                    DailySalahActivity.Zoharoffer.setBackgroundResource(R.drawable.nonetworkmissed);

                    registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

//                    NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//
//                    DailySalahActivity.Zoharoffer.setBackgroundResource(R.drawable.notofferedicon);

                } else if (nameebad.equals("Asar")) {
                    Namaztimeinsert = "Asar";
                    todaydateinsert = formattedDate;
                    ontimeinsert = "No";
                    lateinsert = "No";
                    missedinsert = "missed";

                    exemptedinsert = "No";

                    boolean res =  db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                    DailySalahActivity.asaroffer.setBackgroundResource(R.drawable.nonetworkmissed);

                    registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

//                    NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//
//                    DailySalahActivity.asaroffer.setBackgroundResource(R.drawable.notofferedicon);

                } else if (nameebad.equals("Maghrib")) {
                    Namaztimeinsert = "Maghrib";
                    todaydateinsert = formattedDate;
                    ontimeinsert = "No";
                    lateinsert = "No";
                    missedinsert = "missed";

                    exemptedinsert = "No";

                    boolean res =  db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                    DailySalahActivity.maghriboffer.setBackgroundResource(R.drawable.nonetworkmissed);

                    registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
//                    NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//
//                    DailySalahActivity.maghriboffer.setBackgroundResource(R.drawable.notofferedicon);
                } else if (nameebad.equals("Isha")) {
                    Namaztimeinsert = "Isha";
                    todaydateinsert = formattedDate;
                    ontimeinsert = "No";
                    lateinsert = "No";
                    missedinsert = "missed";

                    exemptedinsert = "No";

                    boolean res =  db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                    DailySalahActivity.ishaoffer.setBackgroundResource(R.drawable.nonetworkmissed);

                    registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

//                    NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//
//                    DailySalahActivity.ishaoffer.setBackgroundResource(R.drawable.notofferedicon);
                }
                popup.dismiss();
            }
        });

    }
    public void runRetro(String fajardata, String zohardata, String asardata, String Maghribdata, String ishadata,String datetoday)
    {
        String Useri = sharedPreferences.getString("preuid","");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        enterOldQadahNamazDetailsApi api =retrofit.create(enterOldQadahNamazDetailsApi.class);
        SimpleDateFormat mdformat = new SimpleDateFormat("dd-MMM-yyyy",Locale.US);
        Date d =null;
        String dateaa ="";
        try {
            d = mdformat.parse(datetoday);
            mdformat = new SimpleDateFormat("yyyy-MM-dd");
            dateaa = mdformat.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        Call<String> call = api.getDetails(Useri,fajardata,zohardata,asardata,Maghribdata,ishadata,dateaa);

        Log.d("values",Useri+""+fajardata+""+zohardata+""+asardata+""+Maghribdata+""+ishadata+"\n"+dateaa);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                String ret =  response.body().toString().trim();
                Toast.makeText(DailySalahActivity.this, ""+ret, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(DailySalahActivity.this, "Please Try Again!", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void QadahRangeRecord(String name,final String formattedDate)
    {
        String nameebad = name;
        if (nameebad.equals("Fajar")) {
            Namaztimeinsert = "Fajar";
            todaydateinsert = formattedDate;
            ontimeinsert = "No";
            lateinsert = "late";
            missedinsert = "No";
            exemptedinsert = "No";

            boolean res =  db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
            DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworklate);

            runRetro("1","0","0", "0", "0",todaydateinsert);

            registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

//                    NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//
//                    DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.lateofferedicon);

        } else if (nameebad.equals("Zohar")) {
            Namaztimeinsert = "Zohar";
            todaydateinsert = formattedDate;
            ontimeinsert = "No";
            lateinsert = "late";
            missedinsert = "No";
            exemptedinsert = "No";

            boolean res =  db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
            DailySalahActivity.Zoharoffer.setBackgroundResource(R.drawable.nonetworklate);

            runRetro("0","1","0","0","0",todaydateinsert);

            registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
//                    NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//
//                    DailySalahActivity.Zoharoffer.setBackgroundResource(R.drawable.lateofferedicon);

        } else if (nameebad.equals("Asar")) {
            Namaztimeinsert = "Asar";
            todaydateinsert = formattedDate;
            ontimeinsert = "No";
            lateinsert = "late";
            missedinsert = "No";
            exemptedinsert = "No";

            boolean res =  db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
            DailySalahActivity.asaroffer.setBackgroundResource(R.drawable.nonetworklate);

            runRetro("0","0","1","0","0",todaydateinsert);

            registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));


//                    NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//
//                    DailySalahActivity.asaroffer.setBackgroundResource(R.drawable.lateofferedicon);

        } else if (nameebad.equals("Maghrib")) {
            Namaztimeinsert = "Maghrib";
            todaydateinsert = formattedDate;
            ontimeinsert = "No";
            lateinsert = "late";
            missedinsert = "No";

            exemptedinsert = "No";

            boolean res =  db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
            DailySalahActivity.maghriboffer.setBackgroundResource(R.drawable.nonetworklate);

            runRetro("0","0","0","1","0",todaydateinsert);

            registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

//                    NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//
//                    DailySalahActivity.maghriboffer.setBackgroundResource(R.drawable.lateofferedicon);

        } else if (nameebad.equals("Isha")) {
            Namaztimeinsert = "Isha";
            todaydateinsert = formattedDate;
            ontimeinsert = "No";
            lateinsert = "late";
            missedinsert = "No";
            exemptedinsert = "No";

            boolean res =  db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert,exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
            DailySalahActivity.ishaoffer.setBackgroundResource(R.drawable.nonetworklate);

            runRetro("0","0","0","0","1",todaydateinsert);

            registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

//                    NamazdetailsInsert( User_id, todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert);
//
//
//                    DailySalahActivity.ishaoffer.setBackgroundResource(R.drawable.lateofferedicon);

        }


    }

    public void onWindowtimeFocusChanged(boolean hasFocus) {

        int[] location = new int[2];
        TextView button = (TextView) findViewById(R.id.daytext);

        // Get the x, y location and store it in the location[] array
        // location[0] = x, location[1] = y.
        button.getLocationOnScreen(location);

        //Initialize the Point with x, and y positions
        p = new Point();
        p.x = location[0];
        p.y = location[1];
    }

//    private void timepickershowPopup(final Activity context, Point p, final String nameebad) {
//        int popupWidth = 1200;
//        int popupHeight = 1800;
////              t1 = (TimePicker) findViewById(R.id.timePicker1);
//
//        // Inflate the popup_layout.xml
//        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.timepopup);
//        LayoutInflater layoutInflater = (LayoutInflater) context
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View layout = layoutInflater.inflate(R.layout.popuptimepicker, viewGroup);
//
//        // Creating the PopupWindow
//        final PopupWindow popup = new PopupWindow(context);
//        popup.setContentView(layout);
//        popup.setWidth(popupWidth);
//        popup.setHeight(popupHeight);
//        popup.setFocusable(true);
//
//        // Some offset to align the popup a bit to the right, and a bit down, relative to button's position.
//        int OFFSET_X = 30;
//        int OFFSET_Y = 30;
//
//        // Clear the default translucent background
//
//
//        // Displaying the popup at the specified location, + offsets.
//        popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x + OFFSET_X, p.y + OFFSET_Y);
//
//        // Getting a reference to Close button, and close the popup when clicked.
//        Button close = (Button) layout.findViewById(R.id.timesave);
//        close.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//                if (nameebad.equals("Fajar"))
//                    Main2Activity.fajrofer.setBackgroundResource(R.drawable.offeredicon);
//                else if (nameebad.equals("Zohar"))
//                    Main2Activity.Zoharoffer.setBackgroundResource(R.drawable.offeredicon);
//                else if (nameebad.equals("Asar"))
//                    Main2Activity.asaroffer.setBackgroundResource(R.drawable.offeredicon);
//                else if (nameebad.equals("Maghrib"))
//                    Main2Activity.maghriboffer.setBackgroundResource(R.drawable.offeredicon);
//                else if (nameebad.equals("Isha"))
//                    Main2Activity.ishaoffer.setBackgroundResource(R.drawable.offeredicon);
//                ontimeinsert = "1";
//
//                popup.dismiss();
//            }
//        });
//
//
//    }


    private Date parseDateeb(String date) {

        try {

            return inputParser.parse(date);
        } catch (java.text.ParseException e) {
            return new Date(0);
        }

    }

    private Date parseDateeb2(String date) {

        try {

            return inputParser2.parse(date);
        } catch (java.text.ParseException e) {
            return new Date(0);
        }

    }

    public void calenderView(View view) {
//
//        SelectCalenderData selectCalenderData = new SelectCalenderData();
//        selectCalenderData.execute(User_id);
        FetchCalenderDetails(User_id);
        Intent intent = new Intent(getApplicationContext(), CalenderActivity.class);
        startActivity(intent);
        this.finish();
    }


    public void NamazdetailsInsert(String User_id,String  todaydateinsert,String  Namaztimeinsert,String  ontimeinsert,String  lateinsert,String  missedinsert)
    {
        Userid =User_id;
        todaydateinsertt = todaydateinsert;
        Namaztimeinsertt = Namaztimeinsert;
        ontimeinsertt = ontimeinsert;
        lateinsertt = lateinsert;
        missedinsertt = missedinsert;
        exemptedinsert = "No";

        Retrofit retrofitinsertNamazDetail = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        SyncApi syncApi = retrofitinsertNamazDetail.create(SyncApi.class);
        Call<POJORES> call = syncApi.getDetails(Userid,todaydateinsertt,Namaztimeinsertt,ontimeinsertt,lateinsertt,missedinsertt,exemptedinsert);
        call.enqueue(new Callback<POJORES>() {
            @Override
            public void onResponse(Call<POJORES> call, retrofit2.Response<POJORES> response) {
                Boolean error = response.body().getError();
                String message = response.body().getMessage().toString();

                if(error == false)
                {
                    saveNameToLocalStorage(Integer.parseInt(Userid), NAME_SYNCED_WITH_SERVER,todaydateinsertt,Namaztimeinsertt,ontimeinsertt,lateinsertt,missedinsertt,exemptedinsert);
                    fajrofer.setBackgroundResource(offeredicon);
                    registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                }
                else
                {
                    saveNameToLocalStorage(Integer.parseInt(Userid), NAME_NOT_SYNCED_WITH_SERVER,todaydateinsertt,Namaztimeinsertt,ontimeinsertt,lateinsertt,missedinsertt,exemptedinsert);
                    fajrofer.setBackgroundResource(nonetworksalah);
                }
            }
            @Override
            public void onFailure(Call<POJORES> call, Throwable t) {
                Toast.makeText(DailySalahActivity.this, "Network error ", Toast.LENGTH_SHORT).show();
                saveNameToLocalStorage(Integer.parseInt(Userid), NAME_NOT_SYNCED_WITH_SERVER,todaydateinsertt,Namaztimeinsertt,ontimeinsertt,lateinsertt,missedinsertt,exemptedinsert);
                fajrofer.setBackgroundResource(nonetworksalah);
            }
        });

    }
    private void saveNameToLocalStorage(int uid, int status ,String tdate , String nname,String otime,String l ,String miss ,String exempted) {

        boolean res =  db.addRecord(uid,tdate,nname,otime,l,miss,exempted,status);
        if(res == true)
        {
            registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

            Toast.makeText(mContext, "Data save Locally", Toast.LENGTH_SHORT).show();
        }

    }
    HashMap<String, Integer> namaz_count_per_day;
    public void FetchCalenderDetails(String Uid) {
        String uid = Uid;
        namaz_count_per_day= new HashMap<String, Integer>();
        Retrofit retrofitcalenderdetails = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        FetchCalenderData apicalenderfetch = retrofitcalenderdetails.create(FetchCalenderData.class);
        final Call<List<CalenderDetailsFetch>> callcalenderdetails = apicalenderfetch.getDetails(uid);
        callcalenderdetails.enqueue(new Callback<List<CalenderDetailsFetch>>() {
            @Override
            public void onResponse(Call<List<CalenderDetailsFetch>> call, Response<List<CalenderDetailsFetch>> response) {
                List<CalenderDetailsFetch> calenderDetailsFetches = response.body();
                if (calenderDetailsFetches.size() > 0) {
                    String[] todaydate = new String[calenderDetailsFetches.size()];
                    String[] namazname = new String[calenderDetailsFetches.size()];
                    String[] ontime = new String[calenderDetailsFetches.size()];
                    String[] late = new String[calenderDetailsFetches.size()];
                    String[] missed = new String[calenderDetailsFetches.size()];
                    String[] Exempted = new String[calenderDetailsFetches.size()];
                    CalendarCollection.date_collection_arr = new ArrayList<CalendarCollection>(1);
                    String finalDate = "";


                    //Hamza
                    for (int a = 0; a < calenderDetailsFetches.size(); a++) {
                        todaydate[a] = calenderDetailsFetches.get(a).getTodayDate();
                        ontime[a] = calenderDetailsFetches.get(a).getOntime();
                        late[a] = calenderDetailsFetches.get(a).getLate();
                        missed[a] = calenderDetailsFetches.get(a).getMissed();
                        Exempted[a] = calenderDetailsFetches.get(a).getExempted();
                    }
                    namaz_count_per_day.put(calenderDetailsFetches.get(0).getTodayDate().toString(), 0);
                    if (calenderDetailsFetches.get(0).getOntime().equals("ontime") || calenderDetailsFetches.get(0).getLate().equals("late") ) {
                        namaz_count_per_day.put(calenderDetailsFetches.get(0).getTodayDate().toString(), namaz_count_per_day.get(calenderDetailsFetches.get(0).getTodayDate()) + 1);
                    }
                    else if(calenderDetailsFetches.get(0).getExempted().equals("Exempted"))
                    {
                        namaz_count_per_day.put(calenderDetailsFetches.get(0).getTodayDate().toString(), namaz_count_per_day.get(calenderDetailsFetches.get(0).getTodayDate()) + 5);
                    }
                    int ii = 1;
                    while (ii < calenderDetailsFetches.size()) {
                        String A = todaydate[ii];
                        String B = todaydate[ii - 1];
                        if (A.equals(B)) {
                            if (calenderDetailsFetches.get(ii).getOntime().toString().equals("ontime") || calenderDetailsFetches.get(ii).getLate().toString().equals("late") ) {
                                namaz_count_per_day.put(calenderDetailsFetches.get(ii).getTodayDate(), namaz_count_per_day.get(calenderDetailsFetches.get(ii).getTodayDate()) + 1);
                            }
                            else if (calenderDetailsFetches.get(0).getExempted().equals("Exempted"))
                            {
                                namaz_count_per_day.put(calenderDetailsFetches.get(ii).getTodayDate(), namaz_count_per_day.get(calenderDetailsFetches.get(ii).getTodayDate()) + 5);
                            }

                        } else {
                            namaz_count_per_day.put(calenderDetailsFetches.get(ii).getTodayDate().toString(), 0);
                            if (calenderDetailsFetches.get(ii).getOntime().toString().equals("ontime") || calenderDetailsFetches.get(ii).getLate().toString().equals("late") ) {
                                namaz_count_per_day.put(calenderDetailsFetches.get(ii).getTodayDate(), namaz_count_per_day.get(calenderDetailsFetches.get(ii).getTodayDate()) + 1);
                            }
                            else if(calenderDetailsFetches.get(0).getExempted().equals("Exempted"))
                            {
                                namaz_count_per_day.put(calenderDetailsFetches.get(ii).getTodayDate(), namaz_count_per_day.get(calenderDetailsFetches.get(ii).getTodayDate()) + 5);
                            }
                        }
                        ii++;
                    }
                    Log.e("Data", namaz_count_per_day + "");
                    for (int i = 0; i < calenderDetailsFetches.size(); i++) {
                        String datenow = todaydate[i];
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MMM-yyyy");
                        SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
                        Date myDate = null;
                        try {
                            myDate = dateFormat1.parse(datenow);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        finalDate = dateFormat1.format(myDate);
                        CalendarCollection.date_collection_arr.add(new CalendarCollection("" + finalDate + "", "offerred", namaz_count_per_day.get(finalDate)));
                    }
                    //Toast.makeText(DailySalahActivity.this, finalDate + "\n " + calenderDetailsFetches.size(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<List<CalenderDetailsFetch>> call, Throwable t) {

            }
        });


    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
    public  void loadNames(String uid,String date)
    {
        final String formatdate = date;
        // Toast.makeText(this, ""+formatdate, Toast.LENGTH_SHORT).show();
        Cursor cursor = db.getRecord(uid);
        String tdate,namazname,ontime,late,missed;
        int uuid,status;
        fajrofer.setBackgroundResource(R.drawable.notofferedicon);
        Zoharoffer.setBackgroundResource(R.drawable.notofferedicon);
        asaroffer.setBackgroundResource(R.drawable.notofferedicon);
        maghriboffer.setBackgroundResource(R.drawable.notofferedicon);
        ishaoffer.setBackgroundResource(R.drawable.notofferedicon);
        fajrofer.setText("...");
        Zoharoffer.setText("...");
        asaroffer.setText("...");
        maghriboffer.setText("...");
        ishaoffer.setText("...");

        if (cursor.moveToFirst()) {
            do {
                Name name = new Name(
                        cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_User_id)),
                        cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_status)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_today_date)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_namaz_name)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_ontime)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_late)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_missed))
                );

                uuid = name.getUser_id();
                status = name.getStatus();
                tdate = name.getToday_date();
                namazname = name.getNamaz_name();
                ontime = name.getOntime();
                late = name.getLate();
                missed = name.getMissed();
                if (namazname.equals("Fajar") && tdate.equals(formatdate)) {
                    if (ontime.equals("ontime") && late.equals("No") && missed.equals("No") && status == 1) {
                        fajrofer.setBackgroundResource(R.drawable.offeredicon);
                    }
                    else if (ontime.equals("ontime") && late.equals("No") && missed.equals("No") && status == 0) {
                        fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                    }

                    else if (ontime.equals("No") && late.equals("late") && missed.equals("No") && status == 1) {
                        //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                        fajrofer.setBackgroundResource(R.drawable.lateofferedicon);
                    }
                    else if (ontime.equals("No") && late.equals("late") && missed.equals("No") && status == 0) {
                        //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                        fajrofer.setBackgroundResource(R.drawable.nonetworklate);
                    }
                    else if (ontime.equals("No") && late.equals("No") && missed.equals("missed")&& status == 1) {
                        //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                        fajrofer.setBackgroundResource(R.drawable.notofferedicon);
                    }

                    else if (ontime.equals("No") && late.equals("No") && missed.equals("missed")&& status == 0) {
                        //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                        fajrofer.setBackgroundResource(R.drawable.nonetworkmissed);
                    }

                }

//                    else
//                    {
//                        Toast.makeText(getApplicationContext(), "missed Run "+Dateselect, Toast.LENGTH_SHORT).show();
//
//                        fajrofer.setBackgroundResource(R.drawable.notoffered);
//                    }
                if (namazname.equals("Zohar") && tdate.equals(formatdate)) {
                    if (ontime.equals("ontime") && late.equals("No") && missed.equals("No") && status == 1) {
                        Zoharoffer.setBackgroundResource(R.drawable.offeredicon);
                    }
                    else if (ontime.equals("ontime") && late.equals("No") && missed.equals("No") && status == 0) {
                        Zoharoffer.setBackgroundResource(R.drawable.nonetworksalah);
                    }

                    else if (ontime.equals("No") && late.equals("late") && missed.equals("No") && status == 1) {
                        //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                        Zoharoffer.setBackgroundResource(R.drawable.lateofferedicon);
                    }
                    else if (ontime.equals("No") && late.equals("late") && missed.equals("No") && status == 0) {
                        //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                        Zoharoffer.setBackgroundResource(R.drawable.nonetworklate);
                    }
                    else if (ontime.equals("No") && late.equals("No") && missed.equals("missed")&& status == 1) {
                        //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                        Zoharoffer.setBackgroundResource(R.drawable.notofferedicon);
                    }

                    else if (ontime.equals("No") && late.equals("No") && missed.equals("missed")&& status == 0) {
                        //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                        Zoharoffer.setBackgroundResource(R.drawable.nonetworkmissed);
                    }
                }
//
//                else
//                {
//                    Toast.makeText(getApplicationContext(), "missed Run "+Dateselect, Toast.LENGTH_SHORT).show();
//
//                    Zoharoffer.setBackgroundResource(R.drawable.notoffered);
//                }

                if (namazname.equals("Asar") && tdate.equals(formatdate)) {
                    if (ontime.equals("ontime") && late.equals("No") && missed.equals("No") && status == 1) {
                        asaroffer.setBackgroundResource(R.drawable.offeredicon);
                    }
                    else if (ontime.equals("ontime") && late.equals("No") && missed.equals("No") && status == 0) {
                        asaroffer.setBackgroundResource(R.drawable.nonetworksalah);
                    }

                    else if (ontime.equals("No") && late.equals("late") && missed.equals("No") && status == 1) {
                        //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                        asaroffer.setBackgroundResource(R.drawable.lateofferedicon);
                    }
                    else if (ontime.equals("No") && late.equals("late") && missed.equals("No") && status == 0) {
                        //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                        asaroffer.setBackgroundResource(R.drawable.nonetworklate);
                    }
                    else if (ontime.equals("No") && late.equals("No") && missed.equals("missed")&& status == 1) {
                        //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                        asaroffer.setBackgroundResource(R.drawable.notofferedicon);
                    }

                    else if (ontime.equals("No") && late.equals("No") && missed.equals("missed")&& status == 0) {
                        //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                        asaroffer.setBackgroundResource(R.drawable.nonetworkmissed);
                    }
                }
//
//                else
//                {
//                    Toast.makeText(getApplicationContext(), "missed Run "+Dateselect, Toast.LENGTH_SHORT).show();
//
//                    asaroffer.setBackgroundResource(R.drawable.notoffered);
//                }

                if (namazname.equals("Maghrib") && tdate.equals(formatdate)) {
                    if (ontime.equals("ontime") && late.equals("No") && missed.equals("No") && status == 1) {
                        maghriboffer.setBackgroundResource(R.drawable.offeredicon);
                    }
                    else if (ontime.equals("ontime") && late.equals("No") && missed.equals("No") && status == 0) {
                        maghriboffer.setBackgroundResource(R.drawable.nonetworksalah);
                    }

                    else if (ontime.equals("No") && late.equals("late") && missed.equals("No") && status == 1) {
                        //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                        maghriboffer.setBackgroundResource(R.drawable.lateofferedicon);
                    }
                    else if (ontime.equals("No") && late.equals("late") && missed.equals("No") && status == 0) {
                        //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                        maghriboffer.setBackgroundResource(R.drawable.nonetworklate);
                    }
                    else if (ontime.equals("No") && late.equals("No") && missed.equals("missed")&& status == 1) {
                        //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                        maghriboffer.setBackgroundResource(R.drawable.notofferedicon);
                    }

                    else if (ontime.equals("No") && late.equals("No") && missed.equals("missed")&& status == 0) {
                        //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                        maghriboffer.setBackgroundResource(R.drawable.nonetworkmissed);
                    }
                }

//                else
//                {
//                    Toast.makeText(getApplicationContext(), "missed Run "+Dateselect, Toast.LENGTH_SHORT).show();
//
//                    maghriboffer.setBackgroundResource(R.drawable.notoffered);
//                }
                if (namazname.equals("Isha") && tdate.equals(formatdate)) {
                    if (ontime.equals("ontime") && late.equals("No") && missed.equals("No") && status == 1) {
                        ishaoffer.setBackgroundResource(R.drawable.offeredicon);
                    }
                    else if (ontime.equals("ontime") && late.equals("No") && missed.equals("No") && status == 0) {
                        ishaoffer.setBackgroundResource(R.drawable.nonetworksalah);
                    }

                    else if (ontime.equals("No") && late.equals("late") && missed.equals("No") && status == 1) {
                        //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                        ishaoffer.setBackgroundResource(R.drawable.lateofferedicon);
                    }
                    else if (ontime.equals("No") && late.equals("late") && missed.equals("No") && status == 0) {
                        //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                        ishaoffer.setBackgroundResource(R.drawable.nonetworklate);
                    }
                    else if (ontime.equals("No") && late.equals("No") && missed.equals("missed")&& status == 1) {
                        //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                        ishaoffer.setBackgroundResource(R.drawable.notofferedicon);
                    }

                    else if (ontime.equals("No") && late.equals("No") && missed.equals("missed")&& status == 0) {
                        //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                        ishaoffer.setBackgroundResource(R.drawable.nonetworkmissed);
                    }
                }








            } while (cursor.moveToNext());
        }
        // Waitprogress.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }
    public void FetchNamazDetails(String Uid, String date) {
        String uid = Uid;
        final String formatdate = date;


        Retrofit retrofitnamazdetails = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        FetchNamazDetailsApi apinamazfetch = retrofitnamazdetails.create(FetchNamazDetailsApi.class);
        Call<List<NamazDetailsfetch>> callnamazdetails = apinamazfetch.getDetails(uid, formatdate);
        callnamazdetails.enqueue(new Callback<List<NamazDetailsfetch>>() {
            @Override
            public void onResponse(Call<List<NamazDetailsfetch>> call, Response<List<NamazDetailsfetch>> response) {
                List<NamazDetailsfetch> namazdetails = response.body();
                String[] todaydate = new String[namazdetails.size()];
                String[] namazname = new String[namazdetails.size()];
                String[] ontime = new String[namazdetails.size()];
                String[] late = new String[namazdetails.size()];
                String[] missed = new String[namazdetails.size()];
                String[] Exempted = new String[namazdetails.size()];
                fajrofer.setBackgroundResource(R.drawable.notofferedicon);
                Zoharoffer.setBackgroundResource(R.drawable.notofferedicon);
                asaroffer.setBackgroundResource(R.drawable.notofferedicon);
                maghriboffer.setBackgroundResource(R.drawable.notofferedicon);
                ishaoffer.setBackgroundResource(R.drawable.notofferedicon);
                for (int i = 0; i < namazdetails.size(); i++) {
                    todaydate[i] = namazdetails.get(i).getTodayDate();
                    namazname[i] = namazdetails.get(i).getNamazName();
                    ontime[i] = namazdetails.get(i).getOntime();
                    late[i] = namazdetails.get(i).getLate();
                    missed[i] = namazdetails.get(i).getMissed();
                    Exempted[i] = namazdetails.get(i).getExempted();
                }

                for (int i = 0; i < namazdetails.size(); i++) {
                    if (namazname[i].equals("Fajar") && todaydate[i].equals(formatdate)) {
                        if (ontime[i].equals("ontime") && late[i].equals("No") && missed[i].equals("No")) {
                            // Toast.makeText(getApplicationContext(), "ontime Run " + formattedDate, Toast.LENGTH_SHORT).show();
                            fajrofer.setBackgroundResource(offeredicon);
                            fajrofer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("late") && missed[i].equals("No")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            fajrofer.setBackgroundResource(R.drawable.lateofferedicon);
                            fajrofer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("missed")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            fajrofer.setBackgroundResource(R.drawable.notofferedicon);
                            fajrofer.setText("..");
                        }
                        else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("No") && Exempted[i].equals("Exempted")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            fajrofer.setBackgroundResource(R.drawable.exemptedgrey);
                            fajrofer.setText(".");
                        }

                    }

//                    else
//                    {
//                        Toast.makeText(getApplicationContext(), "missed Run "+Dateselect, Toast.LENGTH_SHORT).show();
//
//                        fajrofer.setBackgroundResource(R.drawable.notoffered);
//                    }
                    if (namazname[i].equals("Zohar") && todaydate[i].equals(formatdate)) {
                        if (ontime[i].equals("ontime") && late[i].equals("No") && missed[i].equals("No")) {
                            //Toast.makeText(getApplicationContext(), "ontime Run " + formattedDate, Toast.LENGTH_SHORT).show();
                            Zoharoffer.setBackgroundResource(offeredicon);
                            Zoharoffer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("late") && missed[i].equals("No")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            Zoharoffer.setBackgroundResource(R.drawable.lateofferedicon);
                            Zoharoffer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("missed")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            Zoharoffer.setBackgroundResource(R.drawable.notofferedicon);
                            Zoharoffer.setText("..");
                        }
                        else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("No") && Exempted[i].equals("Exempted")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            Zoharoffer.setBackgroundResource(R.drawable.exemptedgrey);
                            Zoharoffer.setText(".");
                        }
                    }
//
//                else
//                {
//                    Toast.makeText(getApplicationContext(), "missed Run "+Dateselect, Toast.LENGTH_SHORT).show();
//
//                    Zoharoffer.setBackgroundResource(R.drawable.notoffered);
//                }

                    if (namazname[i].equals("Asar") && todaydate[i].equals(formatdate)) {
                        if (ontime[i].equals("ontime") && late[i].equals("No") && missed[i].equals("No")) {
                            // Toast.makeText(getApplicationContext(), "ontime Run " + formattedDate, Toast.LENGTH_SHORT).show();
                            asaroffer.setBackgroundResource(offeredicon);
                            asaroffer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("late") && missed[i].equals("No")) {
                            // Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            asaroffer.setBackgroundResource(R.drawable.lateofferedicon);
                            asaroffer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("missed")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            asaroffer.setBackgroundResource(R.drawable.notofferedicon);
                            asaroffer.setText("..");
                        }
                        else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("No") && Exempted[i].equals("Exempted")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            asaroffer.setBackgroundResource(R.drawable.exemptedgrey);
                            asaroffer.setText(".");
                        }
                    }
//
//                else
//                {
//                    Toast.makeText(getApplicationContext(), "missed Run "+Dateselect, Toast.LENGTH_SHORT).show();
//
//                    asaroffer.setBackgroundResource(R.drawable.notoffered);
//                }

                    if (namazname[i].equals("Maghrib") && todaydate[i].equals(formatdate)) {
                        if (ontime[i].equals("ontime") && late[i].equals("No") && missed[i].equals("No")) {
                            // Toast.makeText(getApplicationContext(), "ontime Run " + formattedDate, Toast.LENGTH_SHORT).show();
                            maghriboffer.setBackgroundResource(offeredicon);
                            maghriboffer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("late") && missed[i].equals("No")) {
                            // Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            maghriboffer.setBackgroundResource(R.drawable.lateofferedicon);
                            maghriboffer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("missed")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            maghriboffer.setBackgroundResource(R.drawable.notofferedicon);
                            maghriboffer.setText("..");
                        }
                        else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("No") && Exempted[i].equals("Exempted")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            maghriboffer.setBackgroundResource(R.drawable.exemptedgrey);
                            maghriboffer.setText(".");
                        }
                    }

//                else
//                {
//                    Toast.makeText(getApplicationContext(), "missed Run "+Dateselect, Toast.LENGTH_SHORT).show();
//
//                    maghriboffer.setBackgroundResource(R.drawable.notoffered);
//                }
                    if (namazname[i].equals("Isha") && todaydate[i].equals(formatdate)) {
                        if (ontime[i].equals("ontime") && late[i].equals("No") && missed[i].equals("No")) {
                            // Toast.makeText(getApplicationContext(), "ontime Run " + formattedDate, Toast.LENGTH_SHORT).show();
                            ishaoffer.setBackgroundResource(offeredicon);
                            ishaoffer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("late") && missed[i].equals("No")) {
                            // Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            ishaoffer.setBackgroundResource(R.drawable.lateofferedicon);
                            ishaoffer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("missed")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            ishaoffer.setBackgroundResource(R.drawable.notofferedicon);
                            ishaoffer.setText("..");
                        }
                        else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("No") && Exempted[i].equals("Exempted")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            ishaoffer.setBackgroundResource(R.drawable.exemptedgrey);
                            ishaoffer.setText(".");
                        }
                    }


                }
                Waitprogress.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);


            }

            @Override
            public void onFailure(Call<List<NamazDetailsfetch>> call, Throwable t) {
                Waitprogress.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                Toast.makeText(DailySalahActivity.this, "Network Issue", Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void FetchNamazDetailsback(String Uid, String date) {
        String uid = Uid;
        final String formatdate = date;
        Retrofit retrofitnamazdetails = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        FetchNamazDetailsApi apinamazfetch = retrofitnamazdetails.create(FetchNamazDetailsApi.class);
        Call<List<NamazDetailsfetch>> callnamazdetails = apinamazfetch.getDetails(uid, formatdate);
        callnamazdetails.enqueue(new Callback<List<NamazDetailsfetch>>() {
            @Override
            public void onResponse(Call<List<NamazDetailsfetch>> call, Response<List<NamazDetailsfetch>> response) {
                List<NamazDetailsfetch> namazdetails = response.body();
                String[] todaydate = new String[namazdetails.size()];
                String[] namazname = new String[namazdetails.size()];
                String[] ontime = new String[namazdetails.size()];
                String[] late = new String[namazdetails.size()];
                String[] missed = new String[namazdetails.size()];
                String[] Exempted = new String[namazdetails.size()];
                fajrofer.setBackgroundResource(R.drawable.notofferedicon);
                Zoharoffer.setBackgroundResource(R.drawable.notofferedicon);
                asaroffer.setBackgroundResource(R.drawable.notofferedicon);
                maghriboffer.setBackgroundResource(R.drawable.notofferedicon);
                ishaoffer.setBackgroundResource(R.drawable.notofferedicon);
                for (int i = 0; i < namazdetails.size(); i++) {
                    todaydate[i] = namazdetails.get(i).getTodayDate();
                    namazname[i] = namazdetails.get(i).getNamazName();
                    ontime[i] = namazdetails.get(i).getOntime();
                    late[i] = namazdetails.get(i).getLate();
                    missed[i] = namazdetails.get(i).getMissed();
                    Exempted[i] = namazdetails.get(i).getExempted();
                }
                for (int i = 0; i < namazdetails.size(); i++) {
                    if (namazname[i].equals("Fajar") && todaydate[i].equals(formatdate)) {
                        if (ontime[i].equals("ontime") && late[i].equals("No") && missed[i].equals("No")) {
                            // Toast.makeText(getApplicationContext(), "ontime Run " + formattedDate, Toast.LENGTH_SHORT).show();
                            fajrofer.setBackgroundResource(offeredicon);
                            fajrofer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("late") && missed[i].equals("No")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            fajrofer.setBackgroundResource(R.drawable.lateofferedicon);
                            fajrofer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("missed")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            fajrofer.setBackgroundResource(R.drawable.notofferedicon);
                            fajrofer.setText("..");
                        }
                        else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("No") && Exempted[i].equals("Exempted")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            fajrofer.setBackgroundResource(R.drawable.exemptedgrey);
                            fajrofer.setText(".");
                        }

                    }

//                    else
//                    {
//                        Toast.makeText(getApplicationContext(), "missed Run "+Dateselect, Toast.LENGTH_SHORT).show();
//
//                        fajrofer.setBackgroundResource(R.drawable.notoffered);
//                    }
                    if (namazname[i].equals("Zohar") && todaydate[i].equals(formatdate)) {
                        if (ontime[i].equals("ontime") && late[i].equals("No") && missed[i].equals("No")) {
                            //Toast.makeText(getApplicationContext(), "ontime Run " + formattedDate, Toast.LENGTH_SHORT).show();
                            Zoharoffer.setBackgroundResource(offeredicon);
                            Zoharoffer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("late") && missed[i].equals("No")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            Zoharoffer.setBackgroundResource(R.drawable.lateofferedicon);
                            Zoharoffer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("missed")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            Zoharoffer.setBackgroundResource(R.drawable.notofferedicon);
                            Zoharoffer.setText("..");
                        }
                        else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("No") && Exempted[i].equals("Exempted")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            Zoharoffer.setBackgroundResource(R.drawable.exemptedgrey);
                            Zoharoffer.setText(".");
                        }
                    }
//
//                else
//                {
//                    Toast.makeText(getApplicationContext(), "missed Run "+Dateselect, Toast.LENGTH_SHORT).show();
//
//                    Zoharoffer.setBackgroundResource(R.drawable.notoffered);
//                }

                    if (namazname[i].equals("Asar") && todaydate[i].equals(formatdate)) {
                        if (ontime[i].equals("ontime") && late[i].equals("No") && missed[i].equals("No")) {
                            // Toast.makeText(getApplicationContext(), "ontime Run " + formattedDate, Toast.LENGTH_SHORT).show();
                            asaroffer.setBackgroundResource(offeredicon);
                            asaroffer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("late") && missed[i].equals("No")) {
                            // Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            asaroffer.setBackgroundResource(R.drawable.lateofferedicon);
                            asaroffer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("missed")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            asaroffer.setBackgroundResource(R.drawable.notofferedicon);
                            asaroffer.setText("..");
                        }
                        else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("No") && Exempted[i].equals("Exempted")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            asaroffer.setBackgroundResource(R.drawable.exemptedgrey);
                            asaroffer.setText(".");
                        }
                    }
//
//                else
//                {
//                    Toast.makeText(getApplicationContext(), "missed Run "+Dateselect, Toast.LENGTH_SHORT).show();
//
//                    asaroffer.setBackgroundResource(R.drawable.notoffered);
//                }

                    if (namazname[i].equals("Maghrib") && todaydate[i].equals(formatdate)) {
                        if (ontime[i].equals("ontime") && late[i].equals("No") && missed[i].equals("No")) {
                            // Toast.makeText(getApplicationContext(), "ontime Run " + formattedDate, Toast.LENGTH_SHORT).show();
                            maghriboffer.setBackgroundResource(offeredicon);
                            maghriboffer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("late") && missed[i].equals("No")) {
                            // Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            maghriboffer.setBackgroundResource(R.drawable.lateofferedicon);
                            maghriboffer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("missed")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            maghriboffer.setBackgroundResource(R.drawable.notofferedicon);
                            maghriboffer.setText("..");
                        }
                        else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("No") && Exempted[i].equals("Exempted")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            maghriboffer.setBackgroundResource(R.drawable.exemptedgrey);
                            maghriboffer.setText(".");
                        }
                    }

//                else
//                {
//                    Toast.makeText(getApplicationContext(), "missed Run "+Dateselect, Toast.LENGTH_SHORT).show();
//
//                    maghriboffer.setBackgroundResource(R.drawable.notoffered);
//                }
                    if (namazname[i].equals("Isha") && todaydate[i].equals(formatdate)) {
                        if (ontime[i].equals("ontime") && late[i].equals("No") && missed[i].equals("No")) {
                            // Toast.makeText(getApplicationContext(), "ontime Run " + formattedDate, Toast.LENGTH_SHORT).show();
                            ishaoffer.setBackgroundResource(offeredicon);
                            ishaoffer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("late") && missed[i].equals("No")) {
                            // Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            ishaoffer.setBackgroundResource(R.drawable.lateofferedicon);
                            ishaoffer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("missed")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            ishaoffer.setBackgroundResource(R.drawable.notofferedicon);
                            ishaoffer.setText("..");
                        }
                        else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("No") && Exempted[i].equals("Exempted")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            ishaoffer.setBackgroundResource(R.drawable.exemptedgrey);
                            ishaoffer.setText(".");
                        }
                    }


                }
                Waitprogress.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);


            }

            @Override
            public void onFailure(Call<List<NamazDetailsfetch>> call, Throwable t) {
                Waitprogress.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                Toast.makeText(DailySalahActivity.this, "Network Issue", Toast.LENGTH_SHORT).show();


            }
        });

    }

    public void FetchNamazDetailsforward(String Uid, String date) {
        String uid = Uid;
        final String formatdate = date;
        Retrofit retrofitnamazdetails = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        FetchNamazDetailsApi apinamazfetch = retrofitnamazdetails.create(FetchNamazDetailsApi.class);
        Call<List<NamazDetailsfetch>> callnamazdetails = apinamazfetch.getDetails(uid, formatdate);
        callnamazdetails.enqueue(new Callback<List<NamazDetailsfetch>>() {
            @Override
            public void onResponse(Call<List<NamazDetailsfetch>> call, Response<List<NamazDetailsfetch>> response) {
                List<NamazDetailsfetch> namazdetails = response.body();
                String[] todaydate = new String[namazdetails.size()];
                String[] namazname = new String[namazdetails.size()];
                String[] ontime = new String[namazdetails.size()];
                String[] late = new String[namazdetails.size()];
                String[] missed = new String[namazdetails.size()];
                String[] Exempted = new String[namazdetails.size()];
                fajrofer.setBackgroundResource(R.drawable.notofferedicon);
                Zoharoffer.setBackgroundResource(R.drawable.notofferedicon);
                asaroffer.setBackgroundResource(R.drawable.notofferedicon);
                maghriboffer.setBackgroundResource(R.drawable.notofferedicon);
                ishaoffer.setBackgroundResource(R.drawable.notofferedicon);
                for (int i = 0; i < namazdetails.size(); i++) {
                    todaydate[i] = namazdetails.get(i).getTodayDate();
                    namazname[i] = namazdetails.get(i).getNamazName();
                    ontime[i] = namazdetails.get(i).getOntime();
                    late[i] = namazdetails.get(i).getLate();
                    missed[i] = namazdetails.get(i).getMissed();
                    Exempted[i] = namazdetails.get(i).getExempted();
                }
                for (int i = 0; i < namazdetails.size(); i++) {
                    if (namazname[i].equals("Fajar") && todaydate[i].equals(formatdate)) {
                        if (ontime[i].equals("ontime") && late[i].equals("No") && missed[i].equals("No")) {
                            // Toast.makeText(getApplicationContext(), "ontime Run " + formattedDate, Toast.LENGTH_SHORT).show();
                            fajrofer.setBackgroundResource(offeredicon);
                            fajrofer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("late") && missed[i].equals("No")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            fajrofer.setBackgroundResource(R.drawable.lateofferedicon);
                            fajrofer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("missed")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            fajrofer.setBackgroundResource(R.drawable.notofferedicon);
                            fajrofer.setText("..");
                        }
                        else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("No") && Exempted[i].equals("Exempted")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            fajrofer.setBackgroundResource(R.drawable.exemptedgrey);
                            fajrofer.setText(".");
                        }

                    }

//                    else
//                    {
//                        Toast.makeText(getApplicationContext(), "missed Run "+Dateselect, Toast.LENGTH_SHORT).show();
//
//                        fajrofer.setBackgroundResource(R.drawable.notoffered);
//                    }
                    if (namazname[i].equals("Zohar") && todaydate[i].equals(formatdate)) {
                        if (ontime[i].equals("ontime") && late[i].equals("No") && missed[i].equals("No")) {
                            //Toast.makeText(getApplicationContext(), "ontime Run " + formattedDate, Toast.LENGTH_SHORT).show();
                            Zoharoffer.setBackgroundResource(offeredicon);
                            Zoharoffer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("late") && missed[i].equals("No")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            Zoharoffer.setBackgroundResource(R.drawable.lateofferedicon);
                            Zoharoffer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("missed")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            Zoharoffer.setBackgroundResource(R.drawable.notofferedicon);
                            Zoharoffer.setText("..");
                        }
                        else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("No") && Exempted[i].equals("Exempted")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            Zoharoffer.setBackgroundResource(R.drawable.exemptedgrey);
                            Zoharoffer.setText(".");
                        }
                    }
//
//                else
//                {
//                    Toast.makeText(getApplicationContext(), "missed Run "+Dateselect, Toast.LENGTH_SHORT).show();
//
//                    Zoharoffer.setBackgroundResource(R.drawable.notoffered);
//                }

                    if (namazname[i].equals("Asar") && todaydate[i].equals(formatdate)) {
                        if (ontime[i].equals("ontime") && late[i].equals("No") && missed[i].equals("No")) {
                            // Toast.makeText(getApplicationContext(), "ontime Run " + formattedDate, Toast.LENGTH_SHORT).show();
                            asaroffer.setBackgroundResource(offeredicon);
                            asaroffer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("late") && missed[i].equals("No")) {
                            // Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();
                            asaroffer.setText("..");
                            asaroffer.setBackgroundResource(R.drawable.lateofferedicon);
                        } else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("missed")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            asaroffer.setBackgroundResource(R.drawable.notofferedicon);
                            asaroffer.setText("..");
                        }
                        else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("No") && Exempted[i].equals("Exempted")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            asaroffer.setBackgroundResource(R.drawable.exemptedgrey);
                            asaroffer.setText(".");
                        }
                    }
//
//                else
//                {
//                    Toast.makeText(getApplicationContext(), "missed Run "+Dateselect, Toast.LENGTH_SHORT).show();
//
//                    asaroffer.setBackgroundResource(R.drawable.notoffered);
//                }

                    if (namazname[i].equals("Maghrib") && todaydate[i].equals(formatdate)) {
                        if (ontime[i].equals("ontime") && late[i].equals("No") && missed[i].equals("No")) {
                            // Toast.makeText(getApplicationContext(), "ontime Run " + formattedDate, Toast.LENGTH_SHORT).show();
                            maghriboffer.setBackgroundResource(offeredicon);
                            maghriboffer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("late") && missed[i].equals("No")) {
                            // Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();
                            maghriboffer.setText("..");
                            maghriboffer.setBackgroundResource(R.drawable.lateofferedicon);
                        } else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("missed")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            maghriboffer.setBackgroundResource(R.drawable.notofferedicon);
                            maghriboffer.setText("..");
                        }
                        else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("No") && Exempted[i].equals("Exempted")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            maghriboffer.setBackgroundResource(R.drawable.exemptedgrey);
                            maghriboffer.setText(".");
                        }
                    }

//                else
//                {
//                    Toast.makeText(getApplicationContext(), "missed Run "+Dateselect, Toast.LENGTH_SHORT).show();
//
//                    maghriboffer.setBackgroundResource(R.drawable.notoffered);
//                }
                    if (namazname[i].equals("Isha") && todaydate[i].equals(formatdate)) {
                        if (ontime[i].equals("ontime") && late[i].equals("No") && missed[i].equals("No")) {
                            // Toast.makeText(getApplicationContext(), "ontime Run " + formattedDate, Toast.LENGTH_SHORT).show();
                            ishaoffer.setBackgroundResource(offeredicon);
                            ishaoffer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("late") && missed[i].equals("No")) {
                            // Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            ishaoffer.setBackgroundResource(R.drawable.lateofferedicon);
                            ishaoffer.setText("..");
                        } else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("missed")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            ishaoffer.setBackgroundResource(R.drawable.notofferedicon);
                            ishaoffer.setText("..");
                        }
                        else if (ontime[i].equals("No") && late[i].equals("No") && missed[i].equals("No") && Exempted[i].equals("Exempted")) {
                            //Toast.makeText(getApplicationContext(), "late Run " + formattedDate, Toast.LENGTH_SHORT).show();

                            ishaoffer.setBackgroundResource(R.drawable.exemptedgrey);
                            ishaoffer.setText(".");
                        }
                    }


                }
                Waitprogress.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);


            }

            @Override
            public void onFailure(Call<List<NamazDetailsfetch>> call, Throwable t) {
                Waitprogress.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                Toast.makeText(DailySalahActivity.this, "Network Issue", Toast.LENGTH_SHORT).show();


            }
        });
    }

    public  void reload(View view)
    {
        // Intent intent2 = new Intent(getApplicationContext(),DailySalahActivity.class);
        this.finish();
        startActivity(getIntent());
        formattedDate = null;
        finish();
    }

}

