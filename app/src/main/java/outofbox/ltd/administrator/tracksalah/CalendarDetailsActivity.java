package outofbox.ltd.administrator.tracksalah;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import outofbox.ltd.administrator.tracksalah.POJOcalenderData.CalenderDetailsFetch;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.FetchCalenderData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CalendarDetailsActivity extends AppCompatActivity {
    public GregorianCalendar cal_month1,cal_mont_next2,cal_mont_next3,cal_mont_next4,cal_mont_next5,cal_mont_next6,cal_mont_next7,cal_mont_next8,cal_mont_next9,cal_mont_next10,cal_mont_next11,cal_mont_next12, cal_month_copy;
    private CalendarDetailAdapter cal_adapter;
    private Spinner tv_month;
    SharedPreferences sharedPreferences;
    String User_id="" ;
    private Tracker mTracker;
    public static final String ROOT_URL = "http://185.123.99.207/~android215/TrackSalah/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_calender);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sharedPreferences = getSharedPreferences(LoginActivity.loginpref, Context.MODE_PRIVATE);
        User_id = sharedPreferences.getString("preuid", "");
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Salah Timings Calender");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("TrackSalahUser")
                .setAction("User id")
                .setLabel(DashboardActivity.Userid)
                .setValue(1)
                .build());
        FetchCalenderDetails("0");
        cal_month1 = (GregorianCalendar) GregorianCalendar.getInstance();

        cal_mont_next2 = (GregorianCalendar) GregorianCalendar.getInstance();
        if (cal_mont_next2.get(GregorianCalendar.MONTH) == cal_mont_next2
                .getActualMaximum(GregorianCalendar.MONTH)) {
            cal_mont_next2.set((cal_mont_next2.get(GregorianCalendar.YEAR) + 1),
                    cal_mont_next2.getActualMinimum(GregorianCalendar.MONTH), 1);
        } else {
            cal_mont_next2.set(GregorianCalendar.MONTH,
                    cal_mont_next2.get(GregorianCalendar.MONTH) + 1);
        }
        cal_mont_next3 = (GregorianCalendar) GregorianCalendar.getInstance();
        if (cal_mont_next3.get(GregorianCalendar.MONTH) == cal_mont_next3
                .getActualMaximum(GregorianCalendar.MONTH)) {
            cal_mont_next3.set((cal_mont_next3.get(GregorianCalendar.YEAR) + 2),
                    cal_mont_next3.getActualMinimum(GregorianCalendar.MONTH), 2);
        } else {
            cal_mont_next3.set(GregorianCalendar.MONTH,
                    cal_mont_next3.get(GregorianCalendar.MONTH) + 2);
        }
        cal_mont_next4 = (GregorianCalendar) GregorianCalendar.getInstance();
        if (cal_mont_next4.get(GregorianCalendar.MONTH) == cal_mont_next4
                .getActualMaximum(GregorianCalendar.MONTH)) {
            cal_mont_next4.set((cal_mont_next4.get(GregorianCalendar.YEAR) + 3),
                    cal_mont_next4.getActualMinimum(GregorianCalendar.MONTH), 3);
        } else {
            cal_mont_next4.set(GregorianCalendar.MONTH,
                    cal_mont_next4.get(GregorianCalendar.MONTH) + 3);
        }
        cal_mont_next5 = (GregorianCalendar) GregorianCalendar.getInstance();
        if (cal_mont_next5.get(GregorianCalendar.MONTH) == cal_mont_next5
                .getActualMaximum(GregorianCalendar.MONTH)) {
            cal_mont_next5.set((cal_mont_next5.get(GregorianCalendar.YEAR) + 4),
                    cal_mont_next5.getActualMinimum(GregorianCalendar.MONTH), 4);
        } else {
            cal_mont_next5.set(GregorianCalendar.MONTH,
                    cal_mont_next5.get(GregorianCalendar.MONTH) + 4);
        }
        cal_mont_next6 = (GregorianCalendar) GregorianCalendar.getInstance();
        if (cal_mont_next6.get(GregorianCalendar.MONTH) == cal_mont_next6
                .getActualMaximum(GregorianCalendar.MONTH)) {
            cal_mont_next6.set((cal_mont_next6.get(GregorianCalendar.YEAR) + 5),
                    cal_mont_next6.getActualMinimum(GregorianCalendar.MONTH), 5);
        } else {
            cal_mont_next6.set(GregorianCalendar.MONTH,
                    cal_mont_next6.get(GregorianCalendar.MONTH) + 5);
        }

        cal_mont_next7 = (GregorianCalendar) GregorianCalendar.getInstance();
        if (cal_mont_next7.get(GregorianCalendar.MONTH) == cal_mont_next7
                .getActualMaximum(GregorianCalendar.MONTH)) {
            cal_mont_next7.set((cal_mont_next7.get(GregorianCalendar.YEAR) + 6),
                    cal_mont_next7.getActualMinimum(GregorianCalendar.MONTH), 6);
        } else {
            cal_mont_next7.set(GregorianCalendar.MONTH,
                    cal_mont_next7.get(GregorianCalendar.MONTH) + 6);
        }
        cal_mont_next8 = (GregorianCalendar) GregorianCalendar.getInstance();
        if (cal_mont_next8.get(GregorianCalendar.MONTH) == cal_mont_next8
                .getActualMaximum(GregorianCalendar.MONTH)) {
            cal_mont_next8.set((cal_mont_next8.get(GregorianCalendar.YEAR) + 7),
                    cal_mont_next8.getActualMinimum(GregorianCalendar.MONTH), 7);
        } else {
            cal_mont_next8.set(GregorianCalendar.MONTH,
                    cal_mont_next8.get(GregorianCalendar.MONTH) + 7);
        }
        cal_mont_next9 = (GregorianCalendar) GregorianCalendar.getInstance();
        if (cal_mont_next9.get(GregorianCalendar.MONTH) == cal_mont_next9
                .getActualMaximum(GregorianCalendar.MONTH)) {
            cal_mont_next9.set((cal_mont_next9.get(GregorianCalendar.YEAR) + 8),
                    cal_mont_next9.getActualMinimum(GregorianCalendar.MONTH), 8);
        } else {
            cal_mont_next9.set(GregorianCalendar.MONTH,
                    cal_mont_next9.get(GregorianCalendar.MONTH) + 8);
        }
        cal_mont_next10 = (GregorianCalendar) GregorianCalendar.getInstance();
        if (cal_mont_next10.get(GregorianCalendar.MONTH) == cal_mont_next10
                .getActualMaximum(GregorianCalendar.MONTH)) {
            cal_mont_next10.set((cal_mont_next10.get(GregorianCalendar.YEAR) + 9),
                    cal_mont_next10.getActualMinimum(GregorianCalendar.MONTH), 9);
        } else {
            cal_mont_next10.set(GregorianCalendar.MONTH,
                    cal_mont_next10.get(GregorianCalendar.MONTH) + 9);
        }
        cal_mont_next11 = (GregorianCalendar) GregorianCalendar.getInstance();
        if (cal_mont_next11.get(GregorianCalendar.MONTH) == cal_mont_next11
                .getActualMaximum(GregorianCalendar.MONTH)) {
            cal_mont_next11.set((cal_mont_next11.get(GregorianCalendar.YEAR) + 10),
                    cal_mont_next11.getActualMinimum(GregorianCalendar.MONTH), 10);
        } else {
            cal_mont_next11.set(GregorianCalendar.MONTH,
                    cal_mont_next11.get(GregorianCalendar.MONTH) + 10);
        }
        cal_mont_next12 = (GregorianCalendar) GregorianCalendar.getInstance();
        if (cal_mont_next12.get(GregorianCalendar.MONTH) == cal_mont_next12
                .getActualMaximum(GregorianCalendar.MONTH)) {
            cal_mont_next12.set((cal_mont_next12.get(GregorianCalendar.YEAR) +11),
                    cal_mont_next12.getActualMinimum(GregorianCalendar.MONTH), 11);
        } else {
            cal_mont_next12.set(GregorianCalendar.MONTH,
                    cal_mont_next12.get(GregorianCalendar.MONTH) + 11);
        }


        String[] items = new String[]{android.text.format.DateFormat.format("MMMM yyyy", cal_month1).toString(),
                android.text.format.DateFormat.format("MMMM yyyy", cal_mont_next2).toString(),
                android.text.format.DateFormat.format("MMMM yyyy", cal_mont_next3).toString(),
                android.text.format.DateFormat.format("MMMM yyyy", cal_mont_next4).toString(),
                android.text.format.DateFormat.format("MMMM yyyy", cal_mont_next5).toString(),
                android.text.format.DateFormat.format("MMMM yyyy", cal_mont_next6).toString(),
                android.text.format.DateFormat.format("MMMM yyyy", cal_mont_next7).toString(),
                android.text.format.DateFormat.format("MMMM yyyy", cal_mont_next8).toString(),
                android.text.format.DateFormat.format("MMMM yyyy", cal_mont_next9).toString(),
                android.text.format.DateFormat.format("MMMM yyyy", cal_mont_next10).toString(),
                android.text.format.DateFormat.format("MMMM yyyy", cal_mont_next11).toString(),
                android.text.format.DateFormat.format("MMMM yyyy", cal_mont_next12).toString()};
        cal_month_copy = (GregorianCalendar) cal_month1.clone();

        tv_month =  findViewById(R.id.detailtv_month);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        tv_month.setPopupBackgroundResource(R.color.White);
        tv_month.setAdapter(adapter);
tv_month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position == 0) {
            cal_adapter = new CalendarDetailAdapter(getApplicationContext(), cal_month1, CalenderDetailColection.date_collection_arr1);
            GridView gridview = (GridView) findViewById(R.id.detailgv_calendar);
            gridview.setAdapter(cal_adapter);
            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v,
                                        int position, long id) {
                    ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                    String selectedGridDate = CalendarDetailAdapter.day_string.get(position);
                    String[] separatedTime = selectedGridDate.split("-");
                    String gridvalueString = separatedTime[2].replaceFirst("^0*", "");
                    int gridvalue = Integer.parseInt(gridvalueString);
                    if ((gridvalue > 10) && (position < 8)) {
                        setPreviousMonth();
                        refreshCalendar();
                    } else if ((gridvalue < 7) && (position > 28)) {
                        setNextMonth();
                        refreshCalendar();
                    }
                    ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                    ((CalendarDetailAdapter) parent.getAdapter()).getPositionList(selectedGridDate, CalendarDetailsActivity.this);
                    Intent intent = new Intent(getApplicationContext(), DailyNamazDetailActivity.class);

                    Calendar mydate = Calendar.getInstance();
                    Date todate = new Date();


                    SimpleDateFormat format = new SimpleDateFormat("d-MMM-yyyy");
                    Date griddate = null;
                    try {
                        griddate = format.parse(selectedGridDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String todate1 = format.format(todate);
                    Date todaydate2 = null;
                    try {
                        todaydate2 = format.parse(todate1);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    int count = 0;
                    mydate.setTime(griddate);
                    // Toast.makeText(CalenderActivity.this, "mydate = "+griddate+"\n\n\n\ntodate = "+todate, Toast.LENGTH_LONG).show();
                    Log.e("cheking", "mydate = " + griddate + "\n\n\n\ntodate = " + todaydate2);


//                if(griddate.before(todaydate2))
//                {
//                    long diff = todaydate2.getTime() -griddate.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = -2*Days;
//                }
//
//                else  if (griddate.after(todaydate2))
//                {
//                    long diff = griddate.getTime() -todaydate2.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = 2*Days;
//                }
//                else
//                    count = 0 ;
//
//
//
                    intent.putExtra("ClickedDateDetails", selectedGridDate);
//                intent.putExtra("countvalue",count);
                    startActivity(intent);
                    finish();
                }
            });
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }


        else if(position == 1)
        {
            cal_adapter = new CalendarDetailAdapter(getApplicationContext(), cal_mont_next2, CalenderDetailColection.date_collection_arr1);
            GridView gridview = (GridView) findViewById(R.id.detailgv_calendar);
            gridview.setAdapter(cal_adapter);
            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v,
                                        int position, long id) {
                    ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                    String selectedGridDate = CalendarDetailAdapter.day_string.get(position);
                    String[] separatedTime = selectedGridDate.split("-");
                    String gridvalueString = separatedTime[2].replaceFirst("^0*", "");
                    int gridvalue = Integer.parseInt(gridvalueString);
                    if ((gridvalue > 10) && (position < 8)) {
                        setPreviousMonth();
                        refreshCalendar();
                    } else if ((gridvalue < 7) && (position > 28)) {
                        setNextMonth();
                        refreshCalendar();
                    }
                    ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                    ((CalendarDetailAdapter) parent.getAdapter()).getPositionList(selectedGridDate, CalendarDetailsActivity.this);
                    Intent intent = new Intent(getApplicationContext(), DailyNamazDetailActivity.class);

                    Calendar mydate = Calendar.getInstance();
                    Date todate = new Date();


                    SimpleDateFormat format = new SimpleDateFormat("d-MMM-yyyy");
                    Date griddate = null;
                    try {
                        griddate = format.parse(selectedGridDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String todate1 = format.format(todate);
                    Date todaydate2 = null;
                    try {
                        todaydate2 = format.parse(todate1);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    int count = 0;
                    mydate.setTime(griddate);
                    // Toast.makeText(CalenderActivity.this, "mydate = "+griddate+"\n\n\n\ntodate = "+todate, Toast.LENGTH_LONG).show();
                    Log.e("cheking", "mydate = " + griddate + "\n\n\n\ntodate = " + todaydate2);


//                if(griddate.before(todaydate2))
//                {
//                    long diff = todaydate2.getTime() -griddate.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = -2*Days;
//                }
//
//                else  if (griddate.after(todaydate2))
//                {
//                    long diff = griddate.getTime() -todaydate2.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = 2*Days;
//                }
//                else
//                    count = 0 ;
//
//
//
                    intent.putExtra("ClickedDateDetails", selectedGridDate);
//                intent.putExtra("countvalue",count);
                    startActivity(intent);
                    finish();
                }
            });
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
        else if(position == 2)
        {
            cal_adapter = new CalendarDetailAdapter(getApplicationContext(), cal_mont_next3, CalenderDetailColection.date_collection_arr1);
            GridView gridview = (GridView) findViewById(R.id.detailgv_calendar);
            gridview.setAdapter(cal_adapter);
            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v,
                                        int position, long id) {
                    ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                    String selectedGridDate = CalendarDetailAdapter.day_string.get(position);
                    String[] separatedTime = selectedGridDate.split("-");
                    String gridvalueString = separatedTime[2].replaceFirst("^0*", "");
                    int gridvalue = Integer.parseInt(gridvalueString);
                    if ((gridvalue > 10) && (position < 8)) {
                        setPreviousMonth();
                        refreshCalendar();
                    } else if ((gridvalue < 7) && (position > 28)) {
                        setNextMonth();
                        refreshCalendar();
                    }
                    ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                    ((CalendarDetailAdapter) parent.getAdapter()).getPositionList(selectedGridDate, CalendarDetailsActivity.this);
                    Intent intent = new Intent(getApplicationContext(), DailyNamazDetailActivity.class);

                    Calendar mydate = Calendar.getInstance();
                    Date todate = new Date();


                    SimpleDateFormat format = new SimpleDateFormat("d-MMM-yyyy");
                    Date griddate = null;
                    try {
                        griddate = format.parse(selectedGridDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String todate1 = format.format(todate);
                    Date todaydate2 = null;
                    try {
                        todaydate2 = format.parse(todate1);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    int count = 0;
                    mydate.setTime(griddate);
                    // Toast.makeText(CalenderActivity.this, "mydate = "+griddate+"\n\n\n\ntodate = "+todate, Toast.LENGTH_LONG).show();
                    Log.e("cheking", "mydate = " + griddate + "\n\n\n\ntodate = " + todaydate2);


//                if(griddate.before(todaydate2))
//                {
//                    long diff = todaydate2.getTime() -griddate.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = -2*Days;
//                }
//
//                else  if (griddate.after(todaydate2))
//                {
//                    long diff = griddate.getTime() -todaydate2.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = 2*Days;
//                }
//                else
//                    count = 0 ;
//
//
//
                    intent.putExtra("ClickedDateDetails", selectedGridDate);
//                intent.putExtra("countvalue",count);
                    startActivity(intent);
                    finish();
                }
            });
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
        else if(position == 3)
        {
            cal_adapter = new CalendarDetailAdapter(getApplicationContext(), cal_mont_next4, CalenderDetailColection.date_collection_arr1);
            GridView gridview = (GridView) findViewById(R.id.detailgv_calendar);
            gridview.setAdapter(cal_adapter);
            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v,
                                        int position, long id) {
                    ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                    String selectedGridDate = CalendarDetailAdapter.day_string.get(position);
                    String[] separatedTime = selectedGridDate.split("-");
                    String gridvalueString = separatedTime[2].replaceFirst("^0*", "");
                    int gridvalue = Integer.parseInt(gridvalueString);
                    if ((gridvalue > 10) && (position < 8)) {
                        setPreviousMonth();
                        refreshCalendar();
                    } else if ((gridvalue < 7) && (position > 28)) {
                        setNextMonth();
                        refreshCalendar();
                    }
                    ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                    ((CalendarDetailAdapter) parent.getAdapter()).getPositionList(selectedGridDate, CalendarDetailsActivity.this);
                    Intent intent = new Intent(getApplicationContext(), DailyNamazDetailActivity.class);

                    Calendar mydate = Calendar.getInstance();
                    Date todate = new Date();


                    SimpleDateFormat format = new SimpleDateFormat("d-MMM-yyyy");
                    Date griddate = null;
                    try {
                        griddate = format.parse(selectedGridDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String todate1 = format.format(todate);
                    Date todaydate2 = null;
                    try {
                        todaydate2 = format.parse(todate1);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    int count = 0;
                    mydate.setTime(griddate);
                    // Toast.makeText(CalenderActivity.this, "mydate = "+griddate+"\n\n\n\ntodate = "+todate, Toast.LENGTH_LONG).show();
                    Log.e("cheking", "mydate = " + griddate + "\n\n\n\ntodate = " + todaydate2);


//                if(griddate.before(todaydate2))
//                {
//                    long diff = todaydate2.getTime() -griddate.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = -2*Days;
//                }
//
//                else  if (griddate.after(todaydate2))
//                {
//                    long diff = griddate.getTime() -todaydate2.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = 2*Days;
//                }
//                else
//                    count = 0 ;
//
//
//
                    intent.putExtra("ClickedDateDetails", selectedGridDate);
//                intent.putExtra("countvalue",count);
                    startActivity(intent);
                    finish();
                }
            });
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
        else if(position == 4)
        {
            cal_adapter = new CalendarDetailAdapter(getApplicationContext(), cal_mont_next5, CalenderDetailColection.date_collection_arr1);
            GridView gridview = (GridView) findViewById(R.id.detailgv_calendar);
            gridview.setAdapter(cal_adapter);
            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v,
                                        int position, long id) {
                    ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                    String selectedGridDate = CalendarDetailAdapter.day_string.get(position);
                    String[] separatedTime = selectedGridDate.split("-");
                    String gridvalueString = separatedTime[2].replaceFirst("^0*", "");
                    int gridvalue = Integer.parseInt(gridvalueString);
                    if ((gridvalue > 10) && (position < 8)) {
                        setPreviousMonth();
                        refreshCalendar();
                    } else if ((gridvalue < 7) && (position > 28)) {
                        setNextMonth();
                        refreshCalendar();
                    }
                    ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                    ((CalendarDetailAdapter) parent.getAdapter()).getPositionList(selectedGridDate, CalendarDetailsActivity.this);
                    Intent intent = new Intent(getApplicationContext(), DailyNamazDetailActivity.class);

                    Calendar mydate = Calendar.getInstance();
                    Date todate = new Date();


                    SimpleDateFormat format = new SimpleDateFormat("d-MMM-yyyy");
                    Date griddate = null;
                    try {
                        griddate = format.parse(selectedGridDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String todate1 = format.format(todate);
                    Date todaydate2 = null;
                    try {
                        todaydate2 = format.parse(todate1);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    int count = 0;
                    mydate.setTime(griddate);
                    // Toast.makeText(CalenderActivity.this, "mydate = "+griddate+"\n\n\n\ntodate = "+todate, Toast.LENGTH_LONG).show();
                    Log.e("cheking", "mydate = " + griddate + "\n\n\n\ntodate = " + todaydate2);


//                if(griddate.before(todaydate2))
//                {
//                    long diff = todaydate2.getTime() -griddate.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = -2*Days;
//                }
//
//                else  if (griddate.after(todaydate2))
//                {
//                    long diff = griddate.getTime() -todaydate2.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = 2*Days;
//                }
//                else
//                    count = 0 ;
//
//
//
                    intent.putExtra("ClickedDateDetails", selectedGridDate);
//                intent.putExtra("countvalue",count);
                    startActivity(intent);
                    finish();
                }
            });
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
        else if(position == 5)
        {
            cal_adapter = new CalendarDetailAdapter(getApplicationContext(), cal_mont_next6, CalenderDetailColection.date_collection_arr1);
            GridView gridview = (GridView) findViewById(R.id.detailgv_calendar);
            gridview.setAdapter(cal_adapter);
            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v,
                                        int position, long id) {
                    ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                    String selectedGridDate = CalendarDetailAdapter.day_string.get(position);
                    String[] separatedTime = selectedGridDate.split("-");
                    String gridvalueString = separatedTime[2].replaceFirst("^0*", "");
                    int gridvalue = Integer.parseInt(gridvalueString);
                    if ((gridvalue > 10) && (position < 8)) {
                        setPreviousMonth();
                        refreshCalendar();
                    } else if ((gridvalue < 7) && (position > 28)) {
                        setNextMonth();
                        refreshCalendar();
                    }
                    ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                    ((CalendarDetailAdapter) parent.getAdapter()).getPositionList(selectedGridDate, CalendarDetailsActivity.this);
                    Intent intent = new Intent(getApplicationContext(), DailyNamazDetailActivity.class);

                    Calendar mydate = Calendar.getInstance();
                    Date todate = new Date();


                    SimpleDateFormat format = new SimpleDateFormat("d-MMM-yyyy");
                    Date griddate = null;
                    try {
                        griddate = format.parse(selectedGridDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String todate1 = format.format(todate);
                    Date todaydate2 = null;
                    try {
                        todaydate2 = format.parse(todate1);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    int count = 0;
                    mydate.setTime(griddate);
                    // Toast.makeText(CalenderActivity.this, "mydate = "+griddate+"\n\n\n\ntodate = "+todate, Toast.LENGTH_LONG).show();
                    Log.e("cheking", "mydate = " + griddate + "\n\n\n\ntodate = " + todaydate2);


//                if(griddate.before(todaydate2))
//                {
//                    long diff = todaydate2.getTime() -griddate.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = -2*Days;
//                }
//
//                else  if (griddate.after(todaydate2))
//                {
//                    long diff = griddate.getTime() -todaydate2.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = 2*Days;
//                }
//                else
//                    count = 0 ;
//
//
//
                    intent.putExtra("ClickedDateDetails", selectedGridDate);
//                intent.putExtra("countvalue",count);
                    startActivity(intent);
                    finish();
                }
            });
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
        else if(position == 6)
        {
            cal_adapter = new CalendarDetailAdapter(getApplicationContext(), cal_mont_next7, CalenderDetailColection.date_collection_arr1);
            GridView gridview = (GridView) findViewById(R.id.detailgv_calendar);
            gridview.setAdapter(cal_adapter);
            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v,
                                        int position, long id) {
                    ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                    String selectedGridDate = CalendarDetailAdapter.day_string.get(position);
                    String[] separatedTime = selectedGridDate.split("-");
                    String gridvalueString = separatedTime[2].replaceFirst("^0*", "");
                    int gridvalue = Integer.parseInt(gridvalueString);
                    if ((gridvalue > 10) && (position < 8)) {
                        setPreviousMonth();
                        refreshCalendar();
                    } else if ((gridvalue < 7) && (position > 28)) {
                        setNextMonth();
                        refreshCalendar();
                    }
                    ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                    ((CalendarDetailAdapter) parent.getAdapter()).getPositionList(selectedGridDate, CalendarDetailsActivity.this);
                    Intent intent = new Intent(getApplicationContext(), DailyNamazDetailActivity.class);

                    Calendar mydate = Calendar.getInstance();
                    Date todate = new Date();


                    SimpleDateFormat format = new SimpleDateFormat("d-MMM-yyyy");
                    Date griddate = null;
                    try {
                        griddate = format.parse(selectedGridDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String todate1 = format.format(todate);
                    Date todaydate2 = null;
                    try {
                        todaydate2 = format.parse(todate1);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    int count = 0;
                    mydate.setTime(griddate);
                    // Toast.makeText(CalenderActivity.this, "mydate = "+griddate+"\n\n\n\ntodate = "+todate, Toast.LENGTH_LONG).show();
                    Log.e("cheking", "mydate = " + griddate + "\n\n\n\ntodate = " + todaydate2);


//                if(griddate.before(todaydate2))
//                {
//                    long diff = todaydate2.getTime() -griddate.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = -2*Days;
//                }
//
//                else  if (griddate.after(todaydate2))
//                {
//                    long diff = griddate.getTime() -todaydate2.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = 2*Days;
//                }
//                else
//                    count = 0 ;
//
//
//
                    intent.putExtra("ClickedDateDetails", selectedGridDate);
//                intent.putExtra("countvalue",count);
                    startActivity(intent);
                    finish();
                }
            });
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
        else if(position == 7)
        {
            cal_adapter = new CalendarDetailAdapter(getApplicationContext(), cal_mont_next8, CalenderDetailColection.date_collection_arr1);
            GridView gridview = (GridView) findViewById(R.id.detailgv_calendar);
            gridview.setAdapter(cal_adapter);
            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v,
                                        int position, long id) {
                    ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                    String selectedGridDate = CalendarDetailAdapter.day_string.get(position);
                    String[] separatedTime = selectedGridDate.split("-");
                    String gridvalueString = separatedTime[2].replaceFirst("^0*", "");
                    int gridvalue = Integer.parseInt(gridvalueString);
                    if ((gridvalue > 10) && (position < 8)) {
                        setPreviousMonth();
                        refreshCalendar();
                    } else if ((gridvalue < 7) && (position > 28)) {
                        setNextMonth();
                        refreshCalendar();
                    }
                    ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                    ((CalendarDetailAdapter) parent.getAdapter()).getPositionList(selectedGridDate, CalendarDetailsActivity.this);
                    Intent intent = new Intent(getApplicationContext(), DailyNamazDetailActivity.class);

                    Calendar mydate = Calendar.getInstance();
                    Date todate = new Date();


                    SimpleDateFormat format = new SimpleDateFormat("d-MMM-yyyy");
                    Date griddate = null;
                    try {
                        griddate = format.parse(selectedGridDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String todate1 = format.format(todate);
                    Date todaydate2 = null;
                    try {
                        todaydate2 = format.parse(todate1);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    int count = 0;
                    mydate.setTime(griddate);
                    // Toast.makeText(CalenderActivity.this, "mydate = "+griddate+"\n\n\n\ntodate = "+todate, Toast.LENGTH_LONG).show();
                    Log.e("cheking", "mydate = " + griddate + "\n\n\n\ntodate = " + todaydate2);


//                if(griddate.before(todaydate2))
//                {
//                    long diff = todaydate2.getTime() -griddate.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = -2*Days;
//                }
//
//                else  if (griddate.after(todaydate2))
//                {
//                    long diff = griddate.getTime() -todaydate2.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = 2*Days;
//                }
//                else
//                    count = 0 ;
//
//
//
                    intent.putExtra("ClickedDateDetails", selectedGridDate);
//                intent.putExtra("countvalue",count);
                    startActivity(intent);
                    finish();
                }
            });
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
        else if(position == 8)
        {
            cal_adapter = new CalendarDetailAdapter(getApplicationContext(), cal_mont_next9, CalenderDetailColection.date_collection_arr1);
            GridView gridview = (GridView) findViewById(R.id.detailgv_calendar);
            gridview.setAdapter(cal_adapter);
            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v,
                                        int position, long id) {
                    ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                    String selectedGridDate = CalendarDetailAdapter.day_string.get(position);
                    String[] separatedTime = selectedGridDate.split("-");
                    String gridvalueString = separatedTime[2].replaceFirst("^0*", "");
                    int gridvalue = Integer.parseInt(gridvalueString);
                    if ((gridvalue > 10) && (position < 8)) {
                        setPreviousMonth();
                        refreshCalendar();
                    } else if ((gridvalue < 7) && (position > 28)) {
                        setNextMonth();
                        refreshCalendar();
                    }
                    ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                    ((CalendarDetailAdapter) parent.getAdapter()).getPositionList(selectedGridDate, CalendarDetailsActivity.this);
                    Intent intent = new Intent(getApplicationContext(), DailyNamazDetailActivity.class);

                    Calendar mydate = Calendar.getInstance();
                    Date todate = new Date();


                    SimpleDateFormat format = new SimpleDateFormat("d-MMM-yyyy");
                    Date griddate = null;
                    try {
                        griddate = format.parse(selectedGridDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String todate1 = format.format(todate);
                    Date todaydate2 = null;
                    try {
                        todaydate2 = format.parse(todate1);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    int count = 0;
                    mydate.setTime(griddate);
                    // Toast.makeText(CalenderActivity.this, "mydate = "+griddate+"\n\n\n\ntodate = "+todate, Toast.LENGTH_LONG).show();
                    Log.e("cheking", "mydate = " + griddate + "\n\n\n\ntodate = " + todaydate2);


//                if(griddate.before(todaydate2))
//                {
//                    long diff = todaydate2.getTime() -griddate.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = -2*Days;
//                }
//
//                else  if (griddate.after(todaydate2))
//                {
//                    long diff = griddate.getTime() -todaydate2.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = 2*Days;
//                }
//                else
//                    count = 0 ;
//
//
//
                    intent.putExtra("ClickedDateDetails", selectedGridDate);
//                intent.putExtra("countvalue",count);
                    startActivity(intent);
                    finish();
                }
            });
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
        else if(position == 9)
        {
            cal_adapter = new CalendarDetailAdapter(getApplicationContext(), cal_mont_next10, CalenderDetailColection.date_collection_arr1);
            GridView gridview = (GridView) findViewById(R.id.detailgv_calendar);
            gridview.setAdapter(cal_adapter);
            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v,
                                        int position, long id) {
                    ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                    String selectedGridDate = CalendarDetailAdapter.day_string.get(position);
                    String[] separatedTime = selectedGridDate.split("-");
                    String gridvalueString = separatedTime[2].replaceFirst("^0*", "");
                    int gridvalue = Integer.parseInt(gridvalueString);
                    if ((gridvalue > 10) && (position < 8)) {
                        setPreviousMonth();
                        refreshCalendar();
                    } else if ((gridvalue < 7) && (position > 28)) {
                        setNextMonth();
                        refreshCalendar();
                    }
                    ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                    ((CalendarDetailAdapter) parent.getAdapter()).getPositionList(selectedGridDate, CalendarDetailsActivity.this);
                    Intent intent = new Intent(getApplicationContext(), DailyNamazDetailActivity.class);

                    Calendar mydate = Calendar.getInstance();
                    Date todate = new Date();


                    SimpleDateFormat format = new SimpleDateFormat("d-MMM-yyyy");
                    Date griddate = null;
                    try {
                        griddate = format.parse(selectedGridDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String todate1 = format.format(todate);
                    Date todaydate2 = null;
                    try {
                        todaydate2 = format.parse(todate1);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    int count = 0;
                    mydate.setTime(griddate);
                    // Toast.makeText(CalenderActivity.this, "mydate = "+griddate+"\n\n\n\ntodate = "+todate, Toast.LENGTH_LONG).show();
                    Log.e("cheking", "mydate = " + griddate + "\n\n\n\ntodate = " + todaydate2);


//                if(griddate.before(todaydate2))
//                {
//                    long diff = todaydate2.getTime() -griddate.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = -2*Days;
//                }
//
//                else  if (griddate.after(todaydate2))
//                {
//                    long diff = griddate.getTime() -todaydate2.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = 2*Days;
//                }
//                else
//                    count = 0 ;
//
//
//
                    intent.putExtra("ClickedDateDetails", selectedGridDate);
//                intent.putExtra("countvalue",count);
                    startActivity(intent);
                    finish();
                }
            });
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
        else if(position == 10)
        {
            cal_adapter = new CalendarDetailAdapter(getApplicationContext(), cal_mont_next11, CalenderDetailColection.date_collection_arr1);
            GridView gridview = (GridView) findViewById(R.id.detailgv_calendar);
            gridview.setAdapter(cal_adapter);
            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v,
                                        int position, long id) {
                    ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                    String selectedGridDate = CalendarDetailAdapter.day_string.get(position);
                    String[] separatedTime = selectedGridDate.split("-");
                    String gridvalueString = separatedTime[2].replaceFirst("^0*", "");
                    int gridvalue = Integer.parseInt(gridvalueString);
                    if ((gridvalue > 10) && (position < 8)) {
                        setPreviousMonth();
                        refreshCalendar();
                    } else if ((gridvalue < 7) && (position > 28)) {
                        setNextMonth();
                        refreshCalendar();
                    }
                    ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                    ((CalendarDetailAdapter) parent.getAdapter()).getPositionList(selectedGridDate, CalendarDetailsActivity.this);
                    Intent intent = new Intent(getApplicationContext(), DailyNamazDetailActivity.class);

                    Calendar mydate = Calendar.getInstance();
                    Date todate = new Date();


                    SimpleDateFormat format = new SimpleDateFormat("d-MMM-yyyy");
                    Date griddate = null;
                    try {
                        griddate = format.parse(selectedGridDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String todate1 = format.format(todate);
                    Date todaydate2 = null;
                    try {
                        todaydate2 = format.parse(todate1);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    int count = 0;
                    mydate.setTime(griddate);
                    // Toast.makeText(CalenderActivity.this, "mydate = "+griddate+"\n\n\n\ntodate = "+todate, Toast.LENGTH_LONG).show();
                    Log.e("cheking", "mydate = " + griddate + "\n\n\n\ntodate = " + todaydate2);


//                if(griddate.before(todaydate2))
//                {
//                    long diff = todaydate2.getTime() -griddate.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = -2*Days;
//                }
//
//                else  if (griddate.after(todaydate2))
//                {
//                    long diff = griddate.getTime() -todaydate2.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = 2*Days;
//                }
//                else
//                    count = 0 ;
//
//
//
                    intent.putExtra("ClickedDateDetails", selectedGridDate);
//                intent.putExtra("countvalue",count);
                    startActivity(intent);
                    finish();
                }
            });
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
        else if(position == 11)
        {
            cal_adapter = new CalendarDetailAdapter(getApplicationContext(), cal_mont_next12, CalenderDetailColection.date_collection_arr1);
            GridView gridview = (GridView) findViewById(R.id.detailgv_calendar);
            gridview.setAdapter(cal_adapter);
            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v,
                                        int position, long id) {
                    ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                    String selectedGridDate = CalendarDetailAdapter.day_string.get(position);
                    String[] separatedTime = selectedGridDate.split("-");
                    String gridvalueString = separatedTime[2].replaceFirst("^0*", "");
                    int gridvalue = Integer.parseInt(gridvalueString);
                    if ((gridvalue > 10) && (position < 8)) {
                        setPreviousMonth();
                        refreshCalendar();
                    } else if ((gridvalue < 7) && (position > 28)) {
                        setNextMonth();
                        refreshCalendar();
                    }
                    ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                    ((CalendarDetailAdapter) parent.getAdapter()).getPositionList(selectedGridDate, CalendarDetailsActivity.this);
                    Intent intent = new Intent(getApplicationContext(), DailyNamazDetailActivity.class);

                    Calendar mydate = Calendar.getInstance();
                    Date todate = new Date();


                    SimpleDateFormat format = new SimpleDateFormat("d-MMM-yyyy");
                    Date griddate = null;
                    try {
                        griddate = format.parse(selectedGridDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String todate1 = format.format(todate);
                    Date todaydate2 = null;
                    try {
                        todaydate2 = format.parse(todate1);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    int count = 0;
                    mydate.setTime(griddate);
                    // Toast.makeText(CalenderActivity.this, "mydate = "+griddate+"\n\n\n\ntodate = "+todate, Toast.LENGTH_LONG).show();
                    Log.e("cheking", "mydate = " + griddate + "\n\n\n\ntodate = " + todaydate2);


//                if(griddate.before(todaydate2))
//                {
//                    long diff = todaydate2.getTime() -griddate.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = -2*Days;
//                }
//
//                else  if (griddate.after(todaydate2))
//                {
//                    long diff = griddate.getTime() -todaydate2.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = 2*Days;
//                }
//                else
//                    count = 0 ;
//
//
//
                    intent.putExtra("ClickedDateDetails", selectedGridDate);
//                intent.putExtra("countvalue",count);
                    startActivity(intent);
                    finish();
                }
            });
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        cal_adapter = new CalendarDetailAdapter(getApplicationContext(), cal_month1, CalenderDetailColection.date_collection_arr1);
        GridView gridview = (GridView) findViewById(R.id.detailgv_calendar);
        gridview.setAdapter(cal_adapter);
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                String selectedGridDate = CalendarDetailAdapter.day_string.get(position);
                String[] separatedTime = selectedGridDate.split("-");
                String gridvalueString = separatedTime[2].replaceFirst("^0*", "");
                int gridvalue = Integer.parseInt(gridvalueString);
                if ((gridvalue > 10) && (position < 8)) {
                    setPreviousMonth();
                    refreshCalendar();
                } else if ((gridvalue < 7) && (position > 28)) {
                    setNextMonth();
                    refreshCalendar();
                }
                ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                ((CalendarDetailAdapter) parent.getAdapter()).getPositionList(selectedGridDate, CalendarDetailsActivity.this);
                Intent intent = new Intent(getApplicationContext(), DailyNamazDetailActivity.class);

                Calendar mydate = Calendar.getInstance();
                Date todate = new Date();


                SimpleDateFormat format = new SimpleDateFormat("d-MMM-yyyy");
                Date griddate = null;
                try {
                    griddate = format.parse(selectedGridDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String todate1 = format.format(todate);
                Date todaydate2 = null;
                try {
                    todaydate2 = format.parse(todate1);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                int count = 0;
                mydate.setTime(griddate);
                // Toast.makeText(CalenderActivity.this, "mydate = "+griddate+"\n\n\n\ntodate = "+todate, Toast.LENGTH_LONG).show();
                Log.e("cheking", "mydate = " + griddate + "\n\n\n\ntodate = " + todaydate2);


//                if(griddate.before(todaydate2))
//                {
//                    long diff = todaydate2.getTime() -griddate.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = -2*Days;
//                }
//
//                else  if (griddate.after(todaydate2))
//                {
//                    long diff = griddate.getTime() -todaydate2.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = 2*Days;
//                }
//                else
//                    count = 0 ;
//
//
//
                intent.putExtra("ClickedDate", selectedGridDate);
//                intent.putExtra("countvalue",count);
                startActivity(intent);
                finish();
            }
        });
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
});

        //cal_adapter = new CalendarDetailAdapter(getApplicationContext(), cal_mont_next2, CalenderDetailColection.date_collection_arr1);

       // tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));
//        ImageButton previous = (ImageButton) findViewById(R.id.ib_prev);
//        previous.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                setPreviousMonth();
//                refreshCalendar();
//            }
//        });
//        ImageButton next = (ImageButton) findViewById(R.id.Ib_next);
//        next.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                setNextMonth();
//                refreshCalendar();
//            }
//        });
        GridView gridview = (GridView) findViewById(R.id.detailgv_calendar);
        gridview.setAdapter(cal_adapter);
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                String selectedGridDate = CalendarDetailAdapter.day_string.get(position);
                String[] separatedTime = selectedGridDate.split("-");
                String gridvalueString = separatedTime[2].replaceFirst("^0*", "");
                int gridvalue = Integer.parseInt(gridvalueString);
                if ((gridvalue > 10) && (position < 8)) {
                    setPreviousMonth();
                    refreshCalendar();
                } else if ((gridvalue < 7) && (position > 28)) {
                    setNextMonth();
                    refreshCalendar();
                }
                ((CalendarDetailAdapter) parent.getAdapter()).setSelected(v, position);
                ((CalendarDetailAdapter) parent.getAdapter()).getPositionList(selectedGridDate, CalendarDetailsActivity.this);
                Intent intent = new Intent(getApplicationContext(), DailyNamazDetailActivity.class);

                Calendar mydate = Calendar.getInstance();
                Date todate = new Date();



                SimpleDateFormat format = new SimpleDateFormat("d-MMM-yyyy");
                Date griddate = null;
                try {
                    griddate = format.parse(selectedGridDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String todate1 = format.format(todate);
                Date todaydate2 = null;
                try {
                    todaydate2 = format.parse(todate1);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                int count = 0;
                mydate.setTime(griddate);
                // Toast.makeText(CalenderActivity.this, "mydate = "+griddate+"\n\n\n\ntodate = "+todate, Toast.LENGTH_LONG).show();
                Log.e("cheking", "mydate = "+griddate+"\n\n\n\ntodate = "+todaydate2);


//                if(griddate.before(todaydate2))
//                {
//                    long diff = todaydate2.getTime() -griddate.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = -2*Days;
//                }
//
//                else  if (griddate.after(todaydate2))
//                {
//                    long diff = griddate.getTime() -todaydate2.getTime();
//                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
//                    Log.d("Days no "," "+Days);
//                    count = 2*Days;
//                }
//                else
//                    count = 0 ;
//
//
//
                intent.putExtra("ClickedDate", selectedGridDate);
//                intent.putExtra("countvalue",count);
                startActivity(intent);
                finish();
            }
        });
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
    protected void setNextMonth() {
        if (cal_month1.get(GregorianCalendar.MONTH) == cal_month1
                .getActualMaximum(GregorianCalendar.MONTH)) {
            cal_month1.set((cal_month1.get(GregorianCalendar.YEAR) + 1),
                    cal_month1.getActualMinimum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month1.set(GregorianCalendar.MONTH,
                    cal_month1.get(GregorianCalendar.MONTH) + 1);
        }
    }
    protected void setPreviousMonth() {
        if (cal_month1.get(GregorianCalendar.MONTH) == cal_month1
                .getActualMinimum(GregorianCalendar.MONTH)) {
            cal_month1.set((cal_month1.get(GregorianCalendar.YEAR) - 1),
                    cal_month1.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month1.set(GregorianCalendar.MONTH,
                    cal_month1.get(GregorianCalendar.MONTH) - 1);
        }
    }
    public void refreshCalendar() {
        cal_adapter.refreshDays();
        cal_adapter.notifyDataSetChanged();
     //   tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));
    }
    HashMap<String, Integer> namaz_count_per_day;
    public void FetchCalenderDetails(String Uid) {
        String uid = Uid;
        namaz_count_per_day= new HashMap<String, Integer>();
        Retrofit retrofitcalenderdetails = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        FetchCalenderData apicalenderfetch = retrofitcalenderdetails.create(FetchCalenderData.class);
        final Call<List<CalenderDetailsFetch>> callcalenderdetails = apicalenderfetch.getDetails(uid);
        callcalenderdetails.enqueue(new Callback<List<CalenderDetailsFetch>>() {
            @Override
            public void onResponse(Call<List<CalenderDetailsFetch>> call, Response<List<CalenderDetailsFetch>> response) {
                List<CalenderDetailsFetch> calenderDetailsFetches = response.body();
                if (calenderDetailsFetches.size() > 0) {
                    String[] todaydate = new String[calenderDetailsFetches.size()];
                    String[] namazname = new String[calenderDetailsFetches.size()];
                    String[] ontime = new String[calenderDetailsFetches.size()];
                    String[] late = new String[calenderDetailsFetches.size()];
                    String[] missed = new String[calenderDetailsFetches.size()];
                    CalenderDetailColection.date_collection_arr1 = new ArrayList<CalenderDetailColection>(1);
                    String finalDate = "";


                    //Hamza
                    for (int a = 0; a < calenderDetailsFetches.size(); a++) {
                        todaydate[a] = calenderDetailsFetches.get(a).getTodayDate();
                        ontime[a] = calenderDetailsFetches.get(a).getOntime();
                        late[a] = calenderDetailsFetches.get(a).getLate();
                        missed[a] = calenderDetailsFetches.get(a).getMissed();
                    }
                    namaz_count_per_day.put(calenderDetailsFetches.get(0).getTodayDate().toString(), 0);
                    if (calenderDetailsFetches.get(0).getOntime().equals("ontime") || calenderDetailsFetches.get(0).getLate().equals("late")) {
                        namaz_count_per_day.put(calenderDetailsFetches.get(0).getTodayDate().toString(), namaz_count_per_day.get(calenderDetailsFetches.get(0).getTodayDate()) + 1);
                    }
                    int ii = 1;
                    while (ii < calenderDetailsFetches.size()) {
                        String A = todaydate[ii];
                        String B = todaydate[ii - 1];
                        if (A.equals(B)) {
                            if (calenderDetailsFetches.get(ii).getOntime().toString().equals("ontime") || calenderDetailsFetches.get(ii).getLate().toString().equals("late")) {
                                namaz_count_per_day.put(calenderDetailsFetches.get(ii).getTodayDate(), namaz_count_per_day.get(calenderDetailsFetches.get(ii).getTodayDate()) + 1);
                            }
                        } else {
                            namaz_count_per_day.put(calenderDetailsFetches.get(ii).getTodayDate().toString(), 0);
                            if (calenderDetailsFetches.get(ii).getOntime().toString().equals("ontime") || calenderDetailsFetches.get(ii).getLate().toString().equals("late")) {
                                namaz_count_per_day.put(calenderDetailsFetches.get(ii).getTodayDate(), namaz_count_per_day.get(calenderDetailsFetches.get(ii).getTodayDate()) + 1);
                            }
                        }
                        ii++;
                    }
                    Log.e("Data", namaz_count_per_day + "");
                    for (int i = 0; i < calenderDetailsFetches.size(); i++) {
                        String datenow = todaydate[i];
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MMM-yyyy");
                        SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
                        Date myDate = null;
                        try {
                            myDate = dateFormat1.parse(datenow);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        finalDate = dateFormat1.format(myDate);
                        CalenderDetailColection.date_collection_arr1.add(new CalenderDetailColection("" + finalDate + "", "offerred", namaz_count_per_day.get(finalDate)));
                    }
                    //Toast.makeText(DailySalahActivity.this, finalDate + "\n " + calenderDetailsFetches.size(), Toast.LENGTH_LONG).show();

                }

                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }

            @Override
            public void onFailure(Call<List<CalenderDetailsFetch>> call, Throwable t) {

                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        });
    }
}
