package outofbox.ltd.administrator.tracksalah;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import outofbox.ltd.administrator.tracksalah.POJO2.Example;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.NamazDetailsApi;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.QadahCheckRecordTImeperiodAPi;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.QadahCheckrecord;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.QadahUpdate;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;

import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class QadahSalahActivity extends AppCompatActivity implements  LocationListener {

    private static String TAG = "MainActivity";

    public static int[] yData = {20, 20, 20, 20, 20};
     String[] xData = {"Fajr", "Dhuhr" , "Asar" , "Magrib", "Isha"};
    PieChart pieChart;
    Point p;
    final String[] sele = {"LifeTime"};
    Button fajaradd, fajarsub, zoharadd, zoharsub, asaradd, asarsub, maghribadd, maghribsub, ishaadd, ishasub;
    public static TextView fajarcount, zoharcount, asarcount, maghribcount, ishacount, tday, tdate, hijridate;
    public static TextView Totalqadah, Fajarrem, Fajarqadah, Zoharrem, Zoharqadah, Asarrem, Asarqadah, Maghribrem, MaghribQadah, Isharem, Ishaqadah;
    String getdatee,getdayy,gethijrii;
    int fper,zper,aper,mper,iper;
    String fpers,zpers,apers,mpers,ipers;
    public static ProgressBar pb;
    public static String Userid = "";
    SharedPreferences sharedPreferences;
    String getuserid, gettotalqadahnamaz, getnamazfajar, getnamazzohar, getnamazasar, getnamazmaghrib, getnamazisha,getremqadahnamaz,gettotalqadahnamazz, getnamazfajarr, getnamazzoharr, getnamazasarr, getnamazmaghribb, getnamazishaa,getremqadahnamazz;
    SharedPreferences sharedPreferencesmethod;
    String methodvalue ;
    Call<Example> call;
    NamazDetailsApi api;
    private Tracker mTracker;
    LocationManager locationManager;
    private AdView mAdView;
    Double latti, longi;
    public static final String ROOT_URL = "http://185.123.99.207/~android215/TrackSalah/";
  //  Spinner fajardropdown,Zohardropdown,Asardropdown,MaghribDropdown,IshaDropdown;
  SeekBar seekBar;
  TextView value ,timeperiod;
  ImageView down;
    String fcount,zcount,acount,mcount,icount;
    String fajardata,zohardata,asardata,Maghribdata,ishadata;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_qadah_salah_new);
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Qadah Salah");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("TrackSalahUser")
                .setAction("User id")
                .setLabel(DashboardActivity.Userid)
                .setValue(1)
                .build());
        ConstraintLayout fajarlayout = findViewById(R.id.fajarlayout);
        fajarlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//               showPopup(QadahSalahActivity.this,p,"Fajar");
                showDialog("Fajar");
            }
        });
        ConstraintLayout zoharlayout = findViewById(R.id.zuhrlayout);
        zoharlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  showPopup(QadahSalahActivity.this,p,"Zuhr");
                showDialog("Dhuhr");
            }
        });
        ConstraintLayout asarlayout = findViewById(R.id.asarlayout);
        asarlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // showPopup(QadahSalahActivity.this,p,"Asar");
                showDialog("Asar");
            }
        });

        ConstraintLayout maghriblayout = findViewById(R.id.maghriblayout);
        maghriblayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // showPopup(QadahSalahActivity.this,p,"Maghrib");
                showDialog("Maghrib");
            }
        });
        ConstraintLayout ishalayout = findViewById(R.id.ishalayout);
        ishalayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showPopup(QadahSalahActivity.this,p,"Isha");
                showDialog("Isha");
            }
        });


        pieChart = (PieChart) findViewById(R.id.idPieChart);

//        Zohardropdown = findViewById(R.id.zoharcount);
//        Asardropdown = findViewById(R.id.asarcount);
//        MaghribDropdown = findViewById(R.id.maghribcount);
//        IshaDropdown = findViewById(R.id.ishacount);


//        Zohardropdown.setPopupBackgroundResource(R.color.colorAccent);
//        Asardropdown.setPopupBackgroundResource(R.color.colorAccent);
//        MaghribDropdown.setPopupBackgroundResource(R.color.colorAccent);
//        IshaDropdown.setPopupBackgroundResource(R.color.colorAccent);
//        Zohardropdown.setAdapter(adapter);
//        Asardropdown.setAdapter(adapter);
//        MaghribDropdown.setAdapter(adapter);
//        IshaDropdown.setAdapter(adapter);

        sharedPreferences = getSharedPreferences(LoginActivity.loginpref, Context.MODE_PRIVATE);
        Userid = sharedPreferences.getString("preuid", "");
        sharedPreferencesmethod = getSharedPreferences(SettingsActivity.methodvalue,Context.MODE_PRIVATE);
        methodvalue = sharedPreferencesmethod.getString("methodval","2");
       // Toast.makeText(this, Userid, Toast.LENGTH_SHORT).show();
//        mAdView = (AdView) findViewById(R.id.adView);
//        MobileAds.initialize(this, "ca-app-pub-2825367626708661~7620129751");
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);
//        fajaradd = (Button) findViewById(R.id.fajaradd);
//        fajarsub = (Button) findViewById(R.id.fajarsub);
//        zoharadd = (Button) findViewById(R.id.zoharadd);
//        zoharsub = (Button) findViewById(R.id.zoharsub);
//        asaradd = (Button) findViewById(R.id.asaradd);
//        asarsub = (Button) findViewById(R.id.asarsub);
//        maghribadd = (Button) findViewById(R.id.maghribadd);
//        maghribsub = (Button) findViewById(R.id.maghribsub);
//        ishaadd = (Button) findViewById(R.id.ishaadd);
//        ishasub = (Button) findViewById(R.id.ishasub);
       // Totalqadah = (TextView) findViewById(R.id.totalqadah);
        Fajarrem = (TextView) findViewById(R.id.fajarrem);
       // Fajarqadah = (TextView) findViewById(R.id.fajarqadah);
        Zoharrem = (TextView) findViewById(R.id.zoharrem);
       // Zoharqadah = (TextView) findViewById(R.id.zoharqadah);
        Asarrem = (TextView) findViewById(R.id.asarrem);
        //Asarqadah = (TextView) findViewById(R.id.asarqadah);
        Maghribrem = (TextView) findViewById(R.id.maghribrem);
      //  MaghribQadah = (TextView) findViewById(R.id.maghribqadah);
        Isharem = (TextView) findViewById(R.id.isharem);
        timeperiod = findViewById(R.id.timeperiod);
        down = findViewById(R.id.imageView38);
       // Ishaqadah = (TextView) findViewById(R.id.ishaqadah);

            timeperiod.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Dialog dialog = onCreateDialogVolumeChoice();
                    dialog.show();
                }
            });
        down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog dialog = onCreateDialogVolumeChoice();
                dialog.show();
            }
        });
//        fajaradd.setOnClickListener(this);
//        fajarsub.setOnClickListener(this);
//        zoharadd.setOnClickListener(this);
//        zoharsub.setOnClickListener(this);
//        asaradd.setOnClickListener(this);
//        asarsub.setOnClickListener(this);
//        maghribadd.setOnClickListener(this);
//        maghribsub.setOnClickListener(this);
//        ishaadd.setOnClickListener(this);
//        ishasub.setOnClickListener(this);
        //fajarcount = (TextView) findViewById(R.id.fajarcount);
//        zoharcount = (TextView) findViewById(R.id.zoharcount);
//        asarcount = (TextView) findViewById(R.id.asarcount);
//        maghribcount = (TextView) findViewById(R.id.maghribcount);
//        ishacount = (TextView) findViewById(R.id.ishacount);
//        tday = (TextView) findViewById(R.id.tday);
//        tdate = (TextView) findViewById(R.id.tdate);
//        hijridate = (TextView) findViewById(R.id.hijridate);
//        pb = (ProgressBar) findViewById(R.id.progressqadah);
//        pb.setVisibility(View.VISIBLE);
        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //pb.setVisibility(View.VISIBLE);
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

        }
        QadahRecordCheck(Userid);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(NamazDetailsApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();
        api = retrofit.create(NamazDetailsApi.class);
        getLocation();

        pieChart.setRotationEnabled(true);
        pieChart.setUsePercentValues(true);
        //pieChart.setHoleColor(Color.BLUE);
        //pieChart.setCenterTextColor(Color.BLACK);
        pieChart.setHoleRadius(60f);
        pieChart.setTransparentCircleAlpha(0);
        pieChart.setCenterText(" Tap to Load Qadha Salah");
        pieChart.animateX(1400);

        pieChart.setCenterTextSize(25);
        pieChart.setDrawEntryLabels(true);
        pieChart.setEntryLabelTextSize(15);

        pieChart.getDescription().setText("");
        //More options just check out the documentation!

        addDataSet(fper,zper,aper,mper,iper);
        pieChart.getLegend().setEnabled(false);

    }


    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.normal_menu, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
//            case android.R.id.est:
//            {
//                startActivity(new Intent(getApplicationContext(),QaddahEstimationActivity.class));
//                break;
//            }
//            case R.id.refresh: {
//
//                Intent intent = new Intent(getApplicationContext(),QaddahEstimationActivity.class);
//                intent.putExtra("TotalQadah",getremqadahnamaz);
//                startActivity(intent);
//                break;
//            }
            case R.id.refresh1: {
                startActivity(getIntent());
                finish();
                break;
            }

            case android.R.id.home:{
                onBackPressed();
                break;
            }


        }
        return super.onOptionsItemSelected(item);
    }

    public void SaveQadahUpdate(View view,String namazname) {
        //pb.setVisibility(View.VISIBLE);
        String namazdata = "";
        if(namazname.equals("Fajar"))
        {         namazdata = Integer.toString(Integer.parseInt(Fajarrem.getText().toString()) - Integer.parseInt(value.getText().toString()));
            fcount = value.getText().toString().trim();
            zcount = "0";
            acount = "0";
            mcount ="0";
            icount = "0";
            fajardata = Integer.toString(Integer.parseInt(getnamazfajar) - Integer.parseInt(value.getText().toString().trim()));
            zohardata = Integer.toString(Integer.parseInt(getnamazzohar) - Integer.parseInt("0"));
            asardata = Integer.toString(Integer.parseInt(getnamazasar) - Integer.parseInt("0"));
            Maghribdata = Integer.toString(Integer.parseInt(getnamazmaghrib) - Integer.parseInt("0"));
            ishadata = Integer.toString(Integer.parseInt(getnamazisha) - Integer.parseInt("0"));
        }
       else if(namazname.equals("Dhuhr"))
        {         namazdata = Integer.toString(Integer.parseInt(Zoharrem.getText().toString()) - Integer.parseInt(value.getText().toString()));
            fcount ="0";
            zcount = value.getText().toString().trim();
            acount = "0";
            mcount ="0";
            icount = "0";
            fajardata = Integer.toString(Integer.parseInt(getnamazfajar) - Integer.parseInt("0"));
            zohardata = Integer.toString(Integer.parseInt(getnamazzohar) - Integer.parseInt(value.getText().toString().trim()));
            asardata = Integer.toString(Integer.parseInt(getnamazasar) - Integer.parseInt("0"));
            Maghribdata = Integer.toString(Integer.parseInt(getnamazmaghrib) - Integer.parseInt("0"));
            ishadata = Integer.toString(Integer.parseInt(getnamazisha) - Integer.parseInt("0"));
        }
        else if(namazname.equals("Asar"))
        {         namazdata = Integer.toString(Integer.parseInt(Asarrem.getText().toString()) - Integer.parseInt(value.getText().toString()));
            fcount ="0";
            zcount = "0";
            acount = value.getText().toString().trim();
            mcount ="0";
            icount = "0";
            fajardata = Integer.toString(Integer.parseInt(getnamazfajar) - Integer.parseInt("0"));
            zohardata = Integer.toString(Integer.parseInt(getnamazzohar) - Integer.parseInt("0"));
            asardata = Integer.toString(Integer.parseInt(getnamazasar) - Integer.parseInt(value.getText().toString().trim()));
            Maghribdata = Integer.toString(Integer.parseInt(getnamazmaghrib) - Integer.parseInt("0"));
            ishadata = Integer.toString(Integer.parseInt(getnamazisha) - Integer.parseInt("0"));

        }
       else if(namazname.equals("Maghrib"))
        {         namazdata = Integer.toString(Integer.parseInt(Maghribrem.getText().toString()) - Integer.parseInt(value.getText().toString()));
            fcount ="0";
            zcount = "0";
            acount ="0";
            mcount =value.getText().toString().trim();
            icount = "0";
            fajardata = Integer.toString(Integer.parseInt(getnamazfajar) - Integer.parseInt("0"));
            zohardata = Integer.toString(Integer.parseInt(getnamazzohar) - Integer.parseInt("0"));
            asardata = Integer.toString(Integer.parseInt(getnamazasar) -Integer.parseInt("0")) ;
            Maghribdata = Integer.toString(Integer.parseInt(getnamazmaghrib) - Integer.parseInt(value.getText().toString().trim()));
            ishadata = Integer.toString(Integer.parseInt(getnamazisha) - Integer.parseInt("0"));
        }
        else if(namazname.equals("Isha"))
        {         namazdata = Integer.toString(Integer.parseInt(Isharem.getText().toString()) - Integer.parseInt(value.getText().toString()));
            fcount ="0";
            zcount = "0";
            acount = "0";
            mcount ="0";
            icount = value.getText().toString().trim();
            fajardata = Integer.toString(Integer.parseInt(getnamazfajar) - Integer.parseInt("0"));
            zohardata = Integer.toString(Integer.parseInt(getnamazzohar) - Integer.parseInt("0"));
            asardata = Integer.toString(Integer.parseInt(getnamazasar) -Integer.parseInt("0")) ;
            Maghribdata = Integer.toString(Integer.parseInt(getnamazmaghrib) - Integer.parseInt("0"));
            ishadata = Integer.toString(Integer.parseInt(getnamazisha) - Integer.parseInt(value.getText().toString().trim()));
            Toast.makeText(this, "fajardata "+fajardata+"\nzohardata "+zohardata, Toast.LENGTH_SHORT).show();
        }




        int totalQadahrem = Integer.parseInt(getremqadahnamaz) - Integer.parseInt(value.getText().toString());
        String totalqadahstring = Integer.toString(totalQadahrem);




        //        QadahUpdateRecord qadahUpdateRecord = new QadahUpdateRecord();
//        qadahUpdateRecord.execute("login", Userid, totalqadahstring, fajardata, zohardata, asardata, Maghribdata, ishadata);

     QadahRecordUpdate( Userid, totalqadahstring, fajardata, zohardata, asardata, Maghribdata, ishadata,fcount,zcount,acount,mcount,icount);
//        QadahCheckRecord qadahCheckRecord = new QadahCheckRecord();
//        qadahCheckRecord.execute("login", Userid);
        QadahRecordCheck(Userid);


    }


    public void QadahRecordUpdate(String Userid, String totalqadahstring, String fajardata, String zohardata, String asardata, String Maghribdata, String ishadata, String fcount, String zcount, String acount, String mcount, String icount)
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        QadahUpdate api =retrofit.create(QadahUpdate.class);
        Call<String> call = api.getDetails(Userid,totalqadahstring,fajardata,zohardata,asardata,Maghribdata,ishadata,fcount,zcount,acount,mcount,icount);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                String ret =  response.body().toString().trim();


                Toast.makeText(QadahSalahActivity.this, ""+ret, Toast.LENGTH_SHORT).show();
               // fajarcount.setText("0");
//                zoharcount.setText("0");
//                asarcount.setText("0");
//                maghribcount.setText("0");
//                ishacount.setText("0");

               // Qadahcount.setSelection(0);


            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(QadahSalahActivity.this, "Failed To Update Try Again!", Toast.LENGTH_SHORT).show();
            }
        });


    }

public  void QadahRecordCheck(String Uid)
{

    Retrofit retrofitcheck = new Retrofit.Builder()
            .baseUrl(ROOT_URL) //Setting the Root URL
//            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    QadahCheckrecord apichneck = retrofitcheck.create(QadahCheckrecord.class);
    Call<outofbox.ltd.administrator.tracksalah.POJOQadahCheck.Example> call = apichneck.getDetails(Uid);
    call.enqueue(new Callback<outofbox.ltd.administrator.tracksalah.POJOQadahCheck.Example>() {
        @Override
        public void onResponse(Call<outofbox.ltd.administrator.tracksalah.POJOQadahCheck.Example> call, Response<outofbox.ltd.administrator.tracksalah.POJOQadahCheck.Example> response) {


        int getres = response.body().getUserDetails().get0().getSuccess();
        if(getres == 0)
        {
            Intent intent = new Intent(getApplicationContext(), QadahcalculateActivity.class);
            startActivity(intent);
            finish();


        }



        else {
            try{
            getuserid =  response.body().getUserDetails().get0().getUserId();
            gettotalqadahnamaz = response.body().getUserDetails().get0().getTotalQadahNamaz();
            getremqadahnamaz= response.body().getUserDetails().get0().getRemainingQadahNamaz();
            getnamazfajar= response.body().getUserDetails().get0().getNamazfajar();
            getnamazzohar = response.body().getUserDetails().get0().getNamazZohar();
            getnamazasar= response.body().getUserDetails().get0().getNamazAsar();
            getnamazmaghrib = response.body().getUserDetails().get0().getNamazMaghrib();
            getnamazisha = response.body().getUserDetails().get0().getNamazIsha();
            Toast.makeText(getApplicationContext(), "Details loading...", Toast.LENGTH_SHORT).show();

          //  Totalqadah.setText(getremqadahnamaz);
            pieChart.setCenterText(" Qadha Salah \n"+getremqadahnamaz);
            Fajarrem.setText(getnamazfajar);
            Zoharrem.setText(getnamazzohar);
            Asarrem.setText(getnamazasar);
            Maghribrem.setText(getnamazmaghrib);
            Isharem.setText(getnamazisha);
            fper = (Integer.parseInt(getnamazfajar)*100)/Integer.parseInt(getremqadahnamaz);
            zper = (Integer.parseInt(getnamazzohar)*100)/Integer.parseInt(getremqadahnamaz);
            aper = (Integer.parseInt(getnamazasar)*100)/Integer.parseInt(getremqadahnamaz);
            mper = (Integer.parseInt(getnamazmaghrib)*100)/Integer.parseInt(getremqadahnamaz);
            iper = (Integer.parseInt(getnamazisha)*100)/Integer.parseInt(getremqadahnamaz);
            fpers = Integer.toString(fper)+"%";
            zpers = Integer.toString(zper)+"%";
            apers = Integer.toString(aper)+"%";
            mpers = Integer.toString(mper)+"%";
            ipers = Integer.toString(iper)+"%";


            addDataSet(fper,zper,aper,mper,iper);
//            Toast.makeText(QadahSalahActivity.this, "frecent = "+aper, Toast.LENGTH_SHORT).show();


            int count = Integer.parseInt(gettotalqadahnamaz) / 5;

//            Fajarqadah.setText(Integer.toString(count));
//            Zoharqadah.setText(Integer.toString(count));
//            Asarqadah.setText(Integer.toString(count));
//            MaghribQadah.setText(Integer.toString(count));
//            Ishaqadah.setText(Integer.toString(count));

           // pb.setVisibility(View.GONE);
        }
            catch(Exception e) {
                // This will catch any exception, because they are all descended from Exception
                Toast.makeText(QadahSalahActivity.this, "Salah is not obligatory yet.", Toast.LENGTH_SHORT).show();

        }}
        }

        @Override
        public void onFailure(Call<outofbox.ltd.administrator.tracksalah.POJOQadahCheck.Example> call, Throwable t) {
           // Toast.makeText(QadahSalahActivity.this, "Network Issue", Toast.LENGTH_SHORT).show();
        }
    });


}

    void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 500, 5, this);
        }
        catch(SecurityException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onLocationChanged(Location location) {
        latti = location.getLatitude();
        longi = location.getLongitude();
      //  pb.setVisibility(View.VISIBLE);
        //Toast.makeText(getApplicationContext(),"Latitude: " + location.getLatitude() + "\n Longitude: " + location.getLongitude(),Toast.LENGTH_SHORT).show();

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            latti = location.getLatitude();
            longi = location.getLongitude();
            long unixTime = System.currentTimeMillis() / 1000L;
            call = api.getHeroes("http://api.aladhan.com/v1/timings/"+unixTime+"?latitude="+latti+"&longitude="+longi+"&method="+methodvalue);
            call.enqueue(new Callback<Example>() {
                @Override
                public void onResponse(Call<Example> call, Response<Example> response) {

                    getdatee = response.body().getData().getDate().getReadable();
                    getdayy = response.body().getData().getDate().getGregorian().getWeekday().getEn();
                    gethijrii = response.body().getData().getDate().getHijri().getDate();

                    //QadahSalahActivity.pb.setVisibility(View.GONE);
//                    QadahSalahActivity.tdate.setText(getdatee);
//                    QadahSalahActivity.tday.setText(getdayy);
//                    QadahSalahActivity.hijridate.setText(gethijrii);

                }

                @Override
                public void onFailure(Call<Example> call, Throwable t) {

                }
            });
        }catch(Exception e)
        {

        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        //Toast.makeText(getApplicationContext(), "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();


    }


    boolean internet_connection(){
        //Check if connected to internet, output accordingly
        ConnectivityManager cm = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

//    @Override
//    public void onClick(View v) {
//
//        switch (v.getId())
//        {
//            //case R.id.fajaradd:
//            case R.id.zoharadd:
//            case R.id.asaradd:
//            case R.id.maghribadd:
//            case R.id.ishaadd:
//            {
////                if (v.getId() == R.id.fajaradd) {
//////                    int count = Integer.parseInt(fajarcount.getText().toString());
//////                    count = count + 1;
//////                    fajarcount.setText(Integer.toString(count));
////                }
//                 if (v.getId() == R.id.asaradd) {
//                    int count = Integer.parseInt(asarcount.getText().toString());
//                    count = count + 1;
//                    asarcount.setText(Integer.toString(count));
//                } else if (v.getId() == R.id.maghribadd) {
//                    int count = Integer.parseInt(maghribcount.getText().toString());
//                    count = count + 1;
//                    maghribcount.setText(Integer.toString(count));
//                } else if (v.getId() == R.id.zoharadd) {
//                    int count = Integer.parseInt(zoharcount.getText().toString());
//                    count = count + 1;
//                    zoharcount.setText(Integer.toString(count));
//                } else if (v.getId() == R.id.ishaadd) {
//                    int count = Integer.parseInt(ishacount.getText().toString());
//                    count = count + 1;
//                    ishacount.setText(Integer.toString(count));
//                }
//                break;
//            }
//            //case R.id.fajarsub:
//            case R.id.zoharsub:
//            case R.id.asarsub:
//            case R.id.maghribsub:
//            case R.id.ishasub:
//            {
////                if (v.getId() == R.id.fajarsub )
////
////                {
//////                    int check = Integer.parseInt(fajarcount.getText().toString());
//////                    if(check>0) {
////////                        int count = Integer.parseInt(fajarcount.getText().toString());
////////                        count = count - 1;
////////                        fajarcount.setText(Integer.toString(count));
//////
//////                    }
//////                    else
//////                    {
//////                        Toast.makeText(this, "Qadah offer can't be negative", Toast.LENGTH_SHORT).show();
//////                    }
////                }
//                 if (v.getId() == R.id.asarsub )
//
//                {
//                    int check = Integer.parseInt(asarcount.getText().toString());
//                    if (check > 0) {
//
//                        int count = Integer.parseInt(asarcount.getText().toString());
//                        count = count - 1;
//                        asarcount.setText(Integer.toString(count));
//                    }
//                    else
//                    {
//                        Toast.makeText(this, "Qadah offer can't be negative", Toast.LENGTH_SHORT).show();
//                    }
//                }
//                else if (v.getId() == R.id.maghribsub )
//                {   int check = Integer.parseInt(maghribcount.getText().toString());
//                    if (check > 0) {
//
//
//                        int count = Integer.parseInt(maghribcount.getText().toString());
//                        count = count - 1;
//                        maghribcount.setText(Integer.toString(count));
//                    }
//                    else
//                    {
//                        Toast.makeText(this, "Qadah offer can't be negative", Toast.LENGTH_SHORT).show();
//                    }
//
//                }  else if (v.getId() == R.id.zoharsub )
//                {
//                    int check = Integer.parseInt(zoharcount.getText().toString());
//                    if (check > 0) {
//                        int count = Integer.parseInt(zoharcount.getText().toString());
//                        count = count - 1;
//                        zoharcount.setText(Integer.toString(count));
//                    }
//                    else
//                    {
//                        Toast.makeText(this, "Qadah offer can't be negative", Toast.LENGTH_SHORT).show();
//                    }
//
//                }  else if (v.getId() == R.id.ishasub )
//                {   int check = Integer.parseInt(ishacount.getText().toString());
//                    if (check > 0) {
//
//                        int count = Integer.parseInt(ishacount.getText().toString());
//                        count = count - 1;
//                        ishacount.setText(Integer.toString(count));
//                    }
//                    else
//                    {
//                        Toast.makeText(this, "Qadah offer can't be negative", Toast.LENGTH_SHORT).show();
//                    }
//
//                }
//                break;
//            }
//
//
//
//        }
//
//    }

    public void GotoEstimate(View view)
    {

        Intent intent = new Intent(getApplicationContext(),QaddahEstimationActivity.class);
        intent.putExtra("TotalQadah",getremqadahnamaz);
        startActivity(intent);



    }

    private void addDataSet(int fper1, int zper1, int aper1, int mper1, int iper1) {
        Log.d(TAG, "addDataSet started");
        ArrayList<PieEntry> yEntrys = new ArrayList<PieEntry>();
        ArrayList<String> xEntrys = new ArrayList<>();

//        for(int i = 0; i < yData.length; i++){
//            yEntrys.add(new PieEntry(yData[i] , i));
//        }
      //  Toast.makeText(this, "fper = "+ fper1+"\nzper = "+ zper1 +"\naper = "+ aper1 +"\nmper = "+ mper1, Toast.LENGTH_SHORT).show();
        yEntrys.add(new PieEntry(fper1,"Fajr"));

        yEntrys.add(new PieEntry(zper1,"Dhuhr"));

        yEntrys.add(new PieEntry(aper1,"Asar"));

        yEntrys.add(new PieEntry(mper1,"Maghrib"));

        yEntrys.add(new PieEntry(iper1,"Isha"));

        for(int i = 1; i < xData.length; i++){
            xEntrys.add(xData[i]);
        }

        //create the data set
        PieDataSet pieDataSet = new PieDataSet(yEntrys,"");


        pieDataSet.setSliceSpace(2);
        pieDataSet.setSelectionShift(5);
//        pieDataSet.setValueTextSize(18);


        //add colors to dataset
        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(Color.rgb(70, 195, 243));
        colors.add(Color.rgb(254, 178, 32));
        colors.add(Color.rgb(140, 198, 63));
        colors.add(Color.rgb(249, 98, 159));
        colors.add(Color.rgb(134, 95, 193));


        pieDataSet.setColors(colors);

        //add legend to chart
//        Legend legend = pieChart.getLegend();
//        legend.setForm(Legend.LegendForm.CIRCLE);
//        legend.setXEntrySpace(5);
//        legend.setYEntrySpace(5);
//        legend.setPosition(Legend.LegendPosition.LEFT_OF_CHART);


        //create pie data object
        PieData pieData = new PieData(pieDataSet);

        pieData.setValueFormatter(new PercentFormatter());
        pieData.setValueFormatter(new IValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                int val = (int) value;
                String vals = Integer.toString(val)+"%";
                return vals;
            }
        });
        pieData.setValueTextSize(25f);
        pieData.setValueTextColor(Color.WHITE);
        pieData.setHighlightEnabled(true);
        pieChart.animateX(1400);
        pieChart.setData(pieData);
        pieChart.invalidate();
    }

    public void onWindowFocusChanged(boolean hasFocus) {

        int[] location = new int[2];
        ConstraintLayout fajarlayout = findViewById(R.id.fajarlayout);

        // Get the x, y location and store it in the location[] array
        // location[0] = x, location[1] = y.
        fajarlayout.getLocationOnScreen(location);

        //Initialize the Point with x, and y positions
        p = new Point();
        p.x = location[0];
        p.y = location[1];
    }
    private void showPopup(final Activity context, final Point p, String name) {
        int popupWidth = 250;
        int popupHeight = 550;

        final String nameebad = name;


//              t1 = (TimePicker) findViewById(R.id.timePicker1);

        // Inflate the popup_layout.xml
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.qadahpopup);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.qadahsavelayout, viewGroup);

        // Creating the PopupWindow
        final PopupWindow popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setFocusable(true);
      //  ((TextView) popup.getContentView().findViewById(R.id.qadahnamazname)).setText(nameebad);




        // Some offset to align the popup a bit to the right, and a bit down, relative to button's position.
        int OFFSET_X = 30;
        int OFFSET_Y = 30;

        // Clear the default translucent background
        // Displaying the popup at the specified location, + offsets.
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x + OFFSET_X, p.y + OFFSET_Y);

        // Getting a reference to Close button, and close the popup when clicked.
      //  Button timeselect = (Button) layout.findViewById(R.id.saveqadah);
      //  Qadahcount = layout.findViewById(R.id.qadahcount);
        String[] items = new String[]{"0","1", "2", "3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
//        Qadahcount.setPopupBackgroundResource(R.color.White);
//        Qadahcount.setAdapter(adapter);
//     //   timeselect.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//        SaveQadahUpdate(view,nameebad);
//
//                popup.dismiss();
//            }
//
//
//        });


    }


    public void showDialog(String name)
    {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.qadahsavelayout, null);
        dialogBuilder.setView(dialogView);
        View view = null;
        final String nameebad = name;
        seekBar = dialogView.findViewById(R.id.seekBarScale);
        value = dialogView.findViewById(R.id.value);
        if (nameebad.equals("Dhuhr"))
        {
            Drawable image = getResources().getDrawable(R.drawable.zoharthumb);
            seekBar.setThumb(image);
        }
        else  if (nameebad.equals("Asar"))
        {
            Drawable image = getResources().getDrawable(R.drawable.asarthumb);
            seekBar.setThumb(image);
        }
        else  if (nameebad.equals("Maghrib"))
        {
            Drawable image = getResources().getDrawable(R.drawable.maghribthumb);
            seekBar.setThumb(image);
        }
        else  if (nameebad.equals("Isha"))
        {
            Drawable image = getResources().getDrawable(R.drawable.ishathumb);
            seekBar.setThumb(image);
        }



        String[] items = new String[]{"0","1", "2", "3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
//        Qadahcount.setPopupBackgroundResource(R.color.White);
//        Qadahcount.setAdapter(adapter);

        dialogBuilder.setTitle(nameebad+" Qadha Salah");
        dialogBuilder.setMessage("Please enter the number of Qadah "+nameebad+" Salah you have offered");
        dialogBuilder.setCancelable(false);
        final View finalView = view;

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

            int seekValue;
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                seekValue = seekBar.getProgress();

                value.setText(""+seekValue);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                seekValue = seekBar.getProgress() ;
                value.setText(""+seekValue);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                seekValue = seekBar.getProgress() ;
                value.setText(""+seekValue);
            }
        });



        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                SaveQadahUpdate(finalView,nameebad);
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public Dialog onCreateDialogVolumeChoice() {

//Initialize the Alert Dialog
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
//Source of the data in the DIalog
        final CharSequence[] array = {"LifeTime","Last 1 Year","Last 6 Months","Last 1 Month","Last 1 week"};


// Set the dialog title
        builder.setTitle("Select Time Period")
// Specify the list array, the items to be selected by default (null for none),
// and the listener through which to receive callbacks when items are selected

                .setSingleChoiceItems(array, 0, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
// TODO Auto-generated method stub
                        sele[0] = (String) array[which];
                    }
                })

// Set the action buttons
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
// User clicked OK, so save the result somewhere
// or return them to the component that opened the dialog
                        timeperiod.setText(""+sele[0]+"");
                        if(sele[0].equals("LifeTime"))
                        {
                            QadahRecordCheck(Userid);
                            timeperiod.setText(array[0].toString()+"");
                        }
                        else if(sele[0].equals("Last 1 Year"))
                        {
                            SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
                            java.util.Date currentdate = Calendar.getInstance().getTime();
                           String  currentdatee = sd.format(currentdate);

                            Calendar cal = Calendar.getInstance();
                            java.util.Date backdate = cal.getTime();
                            cal.add(Calendar.YEAR, -1); // to get previous year add -1
                            java.util.Date backYear = cal.getTime();
                            String backyeardate = sd.format(backYear);

                            Toast.makeText(QadahSalahActivity.this, ""+currentdatee+"\n"+backyeardate, Toast.LENGTH_SHORT).show();
                            QadahRecordCheckTimePeriod(Userid,currentdatee,backyeardate);
                            timeperiod.setText(array[1].toString()+"");
                        }
                        else if(sele[0].equals("Last 6 Months"))
                        {
                            SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
                            java.util.Date currentdate = Calendar.getInstance().getTime();
                            String  currentdatee = sd.format(currentdate);

                            Calendar cal = Calendar.getInstance();
                            java.util.Date backdate = cal.getTime();
                            cal.add(Calendar.MONTH, -6); // to get previous year add -1
                            java.util.Date backYear = cal.getTime();
                            String backyeardate = sd.format(backYear);

                            Toast.makeText(QadahSalahActivity.this, ""+currentdatee+"\n"+backyeardate, Toast.LENGTH_SHORT).show();
                            QadahRecordCheckTimePeriod(Userid,currentdatee,backyeardate);
                            timeperiod.setText(array[2].toString()+"");
                        }
                        else if(sele[0].equals("Last 1 Month"))
                        {
                            SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
                            java.util.Date currentdate = Calendar.getInstance().getTime();
                            String  currentdatee = sd.format(currentdate);

                            Calendar cal = Calendar.getInstance();
                            java.util.Date backdate = cal.getTime();
                            cal.add(Calendar.MONTH, -1); // to get previous year add -1
                            java.util.Date backYear = cal.getTime();
                            String backyeardate = sd.format(backYear);

                            Toast.makeText(QadahSalahActivity.this, ""+currentdatee+"\n"+backyeardate, Toast.LENGTH_SHORT).show();
                            QadahRecordCheckTimePeriod(Userid,currentdatee,backyeardate);
                            timeperiod.setText(array[3].toString()+"");
                        }
                        else if(sele[0].equals("Last 1 week"))
                        {
                            SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
                            java.util.Date currentdate = Calendar.getInstance().getTime();
                            String  currentdatee = sd.format(currentdate);

                            Calendar cal = Calendar.getInstance();
                            java.util.Date backdate = cal.getTime();
                            cal.add(Calendar.WEEK_OF_MONTH, -1); // to get previous year add -1
                            java.util.Date backYear = cal.getTime();
                            String backyeardate = sd.format(backYear);

                            Toast.makeText(QadahSalahActivity.this, ""+currentdatee+"\n"+backyeardate, Toast.LENGTH_SHORT).show();
                            QadahRecordCheckTimePeriod(Userid,currentdatee,backyeardate);
                            timeperiod.setText(array[4].toString()+"");
                        }
//                        else if(sele[0].equals("Yesterday"))
//                        {
//                            SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
//                            java.util.Date currentdate = Calendar.getInstance().getTime();
//                            String  currentdatee = sd.format(currentdate);
//
//                            Calendar cal = Calendar.getInstance();
//                            java.util.Date backdate = cal.getTime();
//                            cal.add(Calendar.DATE, -1); // to get previous year add -1
//                            java.util.Date backYear = cal.getTime();
//                            String backyeardate = sd.format(backYear);
//
//                            Toast.makeText(QadahSalahActivity.this, ""+currentdatee+"\n"+backyeardate, Toast.LENGTH_SHORT).show();
//                            QadahRecordCheckTimePeriod(Userid,currentdatee,backyeardate);
//                        }


                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        return builder.create();
    }



    public  void QadahRecordCheckTimePeriod(String Uid,String today,String backdate )
    {

//        Retrofit retrofitcheck = new Retrofit.Builder()
//                .baseUrl(ROOT_URL) //Setting the Root URL
////            .addConverterFactory(ScalarsConverterFactory.create())
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        QadahCheckRecordTImeperiodAPi apichneck = retrofit.create(QadahCheckRecordTImeperiodAPi.class);
        Call<outofbox.ltd.administrator.tracksalah.POJOFORQADAHTIMEPERIOD.Example> call = apichneck.getDetails(Uid,today,backdate);

       call.enqueue(new Callback<outofbox.ltd.administrator.tracksalah.POJOFORQADAHTIMEPERIOD.Example>() {
           @Override
           public void onResponse(Call<outofbox.ltd.administrator.tracksalah.POJOFORQADAHTIMEPERIOD.Example> call, Response<outofbox.ltd.administrator.tracksalah.POJOFORQADAHTIMEPERIOD.Example> response) {
               gettotalqadahnamazz = response.body().getUserDetails().get0().getTotalQadahNamaz();
               getnamazfajarr = response.body().getUserDetails().get0().getNamazfajar();
               getnamazzoharr = response.body().getUserDetails().get0().getNamazZohar();
               getnamazasarr = response.body().getUserDetails().get0().getNamazAsar();
               getnamazmaghribb = response.body().getUserDetails().get0().getNamazMaghrib();
               getnamazishaa = response.body().getUserDetails().get0().getNamazIsha();

               pieChart.setCenterText(" Qadha Salah \n"+gettotalqadahnamazz);
               Fajarrem.setText(getnamazfajarr);
               Zoharrem.setText(getnamazzoharr);
               Asarrem.setText(getnamazasarr);
               Maghribrem.setText(getnamazmaghribb);
               Isharem.setText(getnamazishaa);
               fper = (Integer.parseInt(getnamazfajarr)*100)/Integer.parseInt(gettotalqadahnamazz);
               zper = (Integer.parseInt(getnamazzoharr)*100)/Integer.parseInt(gettotalqadahnamazz);
               aper = (Integer.parseInt(getnamazasarr)*100)/Integer.parseInt(gettotalqadahnamazz);
               mper = (Integer.parseInt(getnamazmaghribb)*100)/Integer.parseInt(gettotalqadahnamazz);
               iper = (Integer.parseInt(getnamazishaa)*100)/Integer.parseInt(gettotalqadahnamazz);
               fpers = Integer.toString(fper)+"%";
               zpers = Integer.toString(zper)+"%";
               apers = Integer.toString(aper)+"%";
               mpers = Integer.toString(mper)+"%";
               ipers = Integer.toString(iper)+"%";


               addDataSet(fper,zper,aper,mper,iper);
           }

           @Override
           public void onFailure(Call<outofbox.ltd.administrator.tracksalah.POJOFORQADAHTIMEPERIOD.Example> call, Throwable t) {
               Toast.makeText(QadahSalahActivity.this, ""+t, Toast.LENGTH_SHORT).show();
           }
       });


    }


}


