package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;

import outofbox.ltd.administrator.tracksalah.POJOnamazdetails.NamazDetailsfetch;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface InsertNamazDetailsApi {

    @FormUrlEncoded
    @POST("enternamazdetails.php")
    Call<String> getDetails(@Field("User_id") String User_id, @Field("Todaydate") String Todaydate, @Field("Namaztime") String Namaztime, @Field("ontime") String ontime, @Field("late") String late, @Field("missed") String missed);

}
