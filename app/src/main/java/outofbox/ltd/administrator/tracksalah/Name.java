package outofbox.ltd.administrator.tracksalah;

public class Name {
    private  int User_id,status;
    private  String today_date,namaz_name,ontime,late,missed;

    public Name(int user_id, int status, String today_date, String namaz_name, String ontime, String late, String missed) {
        User_id = user_id;
        this.status = status;
        this.today_date = today_date;
        this.namaz_name = namaz_name;
        this.ontime = ontime;
        this.late = late;
        this.missed = missed;
    }

    public int getUser_id() {
        return User_id;
    }

    public int getStatus() {
        return status;
    }

    public String getToday_date() {
        return today_date;
    }

    public String getNamaz_name() {
        return namaz_name;
    }

    public String getOntime() {
        return ontime;
    }

    public String getLate() {
        return late;
    }

    public String getMissed() {
        return missed;
    }
}
