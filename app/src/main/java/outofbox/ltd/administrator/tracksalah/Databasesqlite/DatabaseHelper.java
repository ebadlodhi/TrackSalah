package outofbox.ltd.administrator.tracksalah.Databasesqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DB_NAME ="UserNamazDetailall";
    public  static final String TABLE_NAME = "NamazDetailsall";
    public  static final String COLUMN_insert_id = "insert_id";
    public  static final String COLUMN_User_id = "User_id";
    public  static final String COLUMN_today_date = "today_date";
    public  static final String COLUMN_namaz_name = "namaz_name";
    public  static final String COLUMN_ontime = "ontime";
    public  static final String COLUMN_late = "late";
    public  static final String COLUMN_missed = "missed";
    public  static final String COLUMN_exempted = "exempted";
    public  static final String COLUMN_status = "status";

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table "+TABLE_NAME+" ("+COLUMN_insert_id+" INTEGER PRIMARY KEY AUTOINCREMENT,"+COLUMN_User_id+" INTEGER,"+COLUMN_today_date+" text,"+COLUMN_namaz_name+" text,"+COLUMN_ontime+" text,"+COLUMN_late+" text,"+COLUMN_missed+" text,"+COLUMN_exempted+" text ,"+COLUMN_status+" int )";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS "+TABLE_NAME+"";
        db.execSQL(sql);
        onCreate(db);
    }

    public boolean addRecord(int User_id,String today_date,String namaz_name,String ontime,String late,String missed,String exempted,int status ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COLUMN_User_id,User_id);
        contentValues.put(COLUMN_today_date, today_date);
        contentValues.put(COLUMN_namaz_name,namaz_name);
        contentValues.put(COLUMN_ontime,ontime);
        contentValues.put(COLUMN_late,late);
        contentValues.put(COLUMN_missed,missed);
        contentValues.put(COLUMN_exempted,exempted);
        contentValues.put(COLUMN_status,status);

         db.insert(TABLE_NAME, null, contentValues);

        db.close();
    return true;
    }

    public boolean updateNameStatus(int id, int status,String tdate,String nname,String otime,String l,String miss) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues1 = new ContentValues();
        contentValues1.put(COLUMN_status, 1);
        db.update(TABLE_NAME, contentValues1, COLUMN_User_id+ "="+ id, null);
        db.close();

        return true;
    }
    public void deletesync()
    {
        SQLiteDatabase db = this.getWritableDatabase();
       db.delete(TABLE_NAME,COLUMN_status +"="+ 1,null);
       // db.delete(TABLE_NAME,COLUMN_status +"="+ 1 +"and STR_TO_DATE('21,5,2013 extra characters','%d,%mmm,%Y'),)
        db.close();
    }

    /*
     * This method taking two arguments
     * first one is the id of the name for which
     * we have to update the sync status
     * and the second one is the status that will be changed
     * */

    /*
     * this method will give us all the name stored in sqlite
     * */
    public Cursor getRecord(String uid) {
        SQLiteDatabase db = this.getReadableDatabase();
        int userid = Integer.parseInt(uid);
        String sql = "SELECT * FROM "+TABLE_NAME+" where "+COLUMN_User_id+ " = "+userid+"";
        Cursor c = db.rawQuery(sql, null);
        return c;
    }

    /*
     * this method is for getting all the unsynced name
     * so that we can sync it with database
     * */
    public Cursor getUnsyncedNames() {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM "+TABLE_NAME+" where "+COLUMN_status+" = 0 ";
        Cursor c = db.rawQuery(sql, null);
        return c;
    }
}