package outofbox.ltd.administrator.tracksalah.POJO2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Params {
    @SerializedName("Fajr")
    @Expose
    private double fajr;
    @SerializedName("Isha")
    @Expose
    private String isha;

    public double getFajr() {
        return fajr;
    }

    public void setFajr(double fajr) {
        this.fajr = fajr;
    }

    public String getIsha() {
        return isha;
    }

    public void setIsha(String isha) {
        this.isha = isha;
    }
}
