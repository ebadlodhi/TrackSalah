package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;

import outofbox.ltd.administrator.tracksalah.POJOcalenderData.CalenderDetailsFetch;
import outofbox.ltd.administrator.tracksalah.POJOnamazdetails.NamazDetailsfetch;

import java.util.List;

import outofbox.ltd.administrator.tracksalah.POJOcalenderData.CalenderDetailsFetch;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface FetchCalenderData {

    @FormUrlEncoded
    @POST("selectcalendardata.php")
    Call<List<CalenderDetailsFetch>> getDetails(@Field("User_id") String User_id);

}
