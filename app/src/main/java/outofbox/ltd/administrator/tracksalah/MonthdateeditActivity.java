package outofbox.ltd.administrator.tracksalah;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.UpdateExempteddatsApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class MonthdateeditActivity extends AppCompatActivity implements View.OnClickListener {

    TextView mstart,fexnamaz,mend,lexnamaz,mstarta;
    Button updatemens;
    DatePicker dpResult;
    Date Currenttime;
    String sMonth;
    String currentDateandTime;
    static final int DATE_DIALOG_ID = 999;
    private int myear;
    private int mmonth;
    private int mday;
    final String[] sele = {"Fajar"};
    final String[] sele1 = {"Fajar"};
    public static final String ROOT_URL = "http://185.123.99.207/~android215/TrackSalah/";
    String Start_Dateforchange,Start_daynamaz,End_Dateforchange,End_daynamaz,Start_Date,End_Date,userid;
    SharedPreferences sharedPreferences;
    Bundle bundle;
    ProgressBar menpb;
    private Tracker mTracker;
    int id = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monthdateedit);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Mensturation Details Edit");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("TrackSalahUser")
                .setAction("User id")
                .setLabel(DashboardActivity.Userid)
                .setValue(1)
                .build());
        mstart = findViewById(R.id.mstart1);
        fexnamaz = findViewById(R.id.fexnamaz);
        menpb = findViewById(R.id.menpb);
        menpb.setVisibility(View.GONE);
        mend = findViewById(R.id.mend);
        lexnamaz = findViewById(R.id.lexnamaz);
        updatemens = findViewById(R.id.updatemens);
        sharedPreferences = getSharedPreferences(LoginActivity.loginpref, Context.MODE_PRIVATE);
        userid = sharedPreferences.getString("preuid", "");
         bundle = getIntent().getExtras();
        mstart.setText(bundle.getString("monthstartdateformated"));
        mend.setText(bundle.getString("monthenddateformated"));
        fexnamaz.setText(bundle.getString("monthstartdaynamaz"));
        lexnamaz.setText(bundle.getString("monthenddaynamaz"));
//        mstart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mstart = findViewById(R.id.mstart1);
//                setCurrentDateOnView(1);
//                addListenerOnButton();
//            }
//        });
//        mend.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mstart = findViewById(R.id.mend);
//                setCurrentDateOnView(2);
//                addListenerOnButton();
//            }
//        });

        mstart.setOnClickListener(this);
        mend.setOnClickListener(this);


        fexnamaz.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Dialog dialog1 = onCreateDialogFexnamaz();
            dialog1.show();
        }
    });
    lexnamaz.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Dialog dialog = onCreateDialogLexnamaz();
            dialog.show();
        }
    });

        updatemens.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Toast.makeText(MonthdateeditActivity.this, "Updated", Toast.LENGTH_SHORT).show();
                menpb.setVisibility(View.VISIBLE);
//                mstart = findViewById(R.id.mstart1);
                Start_Dateforchange = mstart.getText().toString();
                Start_daynamaz = fexnamaz.getText().toString();
                End_Dateforchange = mend.getText().toString();
                End_daynamaz = lexnamaz.getText().toString();
                Start_Date = bundle.getString("monthstartdateformated");
                End_Date = bundle.getString("monthenddateformated");


                SimpleDateFormat updatteformat = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
                SimpleDateFormat comingformat = new SimpleDateFormat("dd MMM yyyy",Locale.US);
                Date dateStart_Dateforchange,dateEnd_Dateforchange,dateStart_Date, dateEnd_Date = null;

                try {
                    dateStart_Dateforchange = comingformat.parse(Start_Dateforchange);
                    dateEnd_Dateforchange = comingformat.parse(End_Dateforchange);
                    dateStart_Date = comingformat.parse(Start_Date);
                    dateEnd_Date = comingformat.parse(End_Date);
                    Start_Dateforchange = updatteformat.format(dateStart_Dateforchange);
                    End_Dateforchange = updatteformat.format(dateEnd_Dateforchange);
                    Start_Date = updatteformat.format(dateStart_Date);
                    End_Date = updatteformat.format(dateEnd_Date);


                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Log.d("Start_Dateforchange",Start_Dateforchange);
                Log.d("Start_daynamaz",Start_daynamaz);
                Log.d("End_Dateforchange",End_Dateforchange);
                Log.d("End_daynamaz",End_daynamaz);
                Log.d("Start_Date",Start_Date);
                Log.d("End_Date",End_Date);




                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(ROOT_URL) //Setting the Root URL
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();


                UpdateExempteddatsApi api = retrofit.create(UpdateExempteddatsApi.class);
                Call<String> call = api.getDetails(Start_Dateforchange,Start_daynamaz,End_Dateforchange,End_daynamaz,Start_Date,End_Date,userid);
                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        String ret = response.body().toString().trim();
                     //   Toast.makeText(MonthdateeditActivity.this, ""+ret, Toast.LENGTH_SHORT).show();
                        if(ret.equals("DataUpdated"))
                        {
                            menpb.setVisibility(View.GONE);

                            //
                Intent intent = new Intent(getApplicationContext(),MensDetailActivity.class);
                startActivity(intent);
                finish();

                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Toast.makeText(MonthdateeditActivity.this, "Server Busy.", Toast.LENGTH_SHORT).show();
                        menpb.setVisibility(View.GONE);
                    }
                });



            }
        });


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }



    public void setCurrentDateOnView(int tvid) {

            mstarta = findViewById(R.id.mstart1);


        dpResult = (DatePicker) findViewById(R.id.dpResult2);
        dpResult.setVisibility(View.GONE);

        final Calendar c = Calendar.getInstance();
        SimpleDateFormat gformat = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        Date gdate = null;
        try {
             gdate = gformat.parse(mstart.getText().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.setTime(gdate);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        currentDateandTime = sdf.format(new Date());
        myear = c.get(Calendar.YEAR);
        mmonth = c.get(Calendar.MONTH);
        mday = c.get(Calendar.DAY_OF_MONTH);

        // set current date into textview
//        tvDisplayDate.setText(new StringBuilder()
//                // Month is 0 based, just add 1
//                .append(myear).append("-") .append(mmonth + 1).append("-").append(mday)
//        );

        // set current date into datepicker
        dpResult.init(myear, mmonth, mday, null);

    }
    public void setCurrentDateOnViewtwo(int tvid) {

            mstarta = findViewById(R.id.mend);

        dpResult = (DatePicker) findViewById(R.id.dpResult2);
        dpResult.setVisibility(View.GONE);

        final Calendar c = Calendar.getInstance();
        SimpleDateFormat gformat = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        Date gdate = null;
        try {
            gdate = gformat.parse(mstart.getText().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.setTime(gdate);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        currentDateandTime = sdf.format(new Date());
        myear = c.get(Calendar.YEAR);
        mmonth = c.get(Calendar.MONTH);
        mday = c.get(Calendar.DAY_OF_MONTH);

        // set current date into textview
//        tvDisplayDate.setText(new StringBuilder()
//                // Month is 0 based, just add 1
//                .append(myear).append("-") .append(mmonth + 1).append("-").append(mday)
//        );

        // set current date into datepicker
        dpResult.init(myear, mmonth, mday, null);

    }

    public void addListenerOnButton() {



        mstart.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                showDialog(DATE_DIALOG_ID);

            }

        });

    }
    public void addListenerOnButtontwo() {



        mend.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                showDialog(DATE_DIALOG_ID);

            }

        });

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                DatePickerDialog _date =   new DatePickerDialog(this, datePickerListener, myear,mmonth,
                        mday){
                    @Override
                    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                    {
//                        if (year > myear) {
//                            Toast.makeText(getApplicationContext(), "Year can't be greater then current Year", Toast.LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }
//                        if (monthOfYear > mmonth && year == myear) {
//                            Toast.makeText(getApplicationContext(), "Month can't be greater then current Month", Toast.LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }
//                        if (dayOfMonth > mday && year == myear && monthOfYear == mmonth) {
//                            Toast.makeText(getApplicationContext(), "Date can't be greater then current date", Toast.LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }
                    }
                };
                return _date;

        }
        return null;
    }
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            myear = selectedYear;
            mmonth = selectedMonth;
            mday = selectedDay;
            if (mmonth == 0)
                sMonth = "Jan";
            else if (mmonth == 1)
                sMonth = "Feb";
            else if (mmonth == 2)
                sMonth = "Mar";
            else if (mmonth == 3)
                sMonth = "Apr";
            else if (mmonth == 4)
                sMonth = "May";
            else if (mmonth == 5)
                sMonth = "Jun";
            else if (mmonth == 6)
                sMonth = "Jul";
            else if (mmonth == 7)
                sMonth = "Aug";
            else if (mmonth == 8)
                sMonth = "Sep";
            else if (mmonth == 9)
                sMonth = "Oct";
            else if (mmonth == 10)
                sMonth = "Nov";
            else if (mmonth == 11)
                sMonth = "Dec";
            // set selected date into textview

                mstarta.setText(new StringBuilder()
                        .append(mday).append(" ").append(sMonth).append(" ").append(myear).append(""));

            // set selected date into datepicker also
            dpResult.init(myear, mmonth, mday, null);

        }
    };



    public Dialog onCreateDialogFexnamaz() {

//Initialize the Alert Dialog
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
//Source of the data in the DIalog
        final CharSequence[] array = {"Fajar","Dhuhr","Asar","Maghrib","Isha"};


// Set the dialog title
        builder.setTitle("Select Namaz Name")
// Specify the list array, the items to be selected by default (null for none),
// and the listener through which to receive callbacks when items are selected

                .setSingleChoiceItems(array, 0, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
// TODO Auto-generated method stub
                        sele[0] = (String) array[which];
                    }
                })

// Set the action buttons
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
// User clicked OK, so save the result somewhere
// or return them to the component that opened the dialog

                        if(sele[0].equals("Fajar"))
                        {
                        fexnamaz.setText(sele[0]);
                        }
                        else if(sele[0].equals("Dhuhr"))
                        {
                            fexnamaz.setText(sele[0]);
                        }
                        else if(sele[0].equals("Asar"))
                        {
                            fexnamaz.setText(sele[0]);
                        }
                        else if(sele[0].equals("Maghrib"))
                        {
                            fexnamaz.setText(sele[0]);
                        }
                        else if(sele[0].equals("Isha"))
                        {
                            fexnamaz.setText(sele[0]);
                        }
//                        else if(sele[0].equals("Yesterday"))
//                        {
//                            SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
//                            java.util.Date currentdate = Calendar.getInstance().getTime();
//                            String  currentdatee = sd.format(currentdate);
//
//                            Calendar cal = Calendar.getInstance();
//                            java.util.Date backdate = cal.getTime();
//                            cal.add(Calendar.DATE, -1); // to get previous year add -1
//                            java.util.Date backYear = cal.getTime();
//                            String backyeardate = sd.format(backYear);
//
//                            Toast.makeText(QadahSalahActivity.this, ""+currentdatee+"\n"+backyeardate, Toast.LENGTH_SHORT).show();
//                            QadahRecordCheckTimePeriod(Userid,currentdatee,backyeardate);
//                        }


                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        return builder.create();
    }

    public Dialog onCreateDialogLexnamaz() {

//Initialize the Alert Dialog
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
//Source of the data in the DIalog
        final CharSequence[] array = {"Fajar","Dhuhr","Asar","Maghrib","Isha"};


// Set the dialog title
        builder.setTitle("Select Namaz Name")
// Specify the list array, the items to be selected by default (null for none),
// and the listener through which to receive callbacks when items are selected

                .setSingleChoiceItems(array, 0, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
// TODO Auto-generated method stub
                        sele1[0] = (String) array[which];
                    }
                })

// Set the action buttons
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
// User clicked OK, so save the result somewhere
// or return them to the component that opened the dialog

                        if(sele1[0].equals("Fajar"))
                        {
                            lexnamaz.setText(sele1[0]);
                        }
                        else if(sele1[0].equals("Dhuhr"))
                        {
                            lexnamaz.setText(sele1[0]);
                        }
                        else if(sele1[0].equals("Asar"))
                        {
                            lexnamaz.setText(sele1[0]);
                        }
                        else if(sele1[0].equals("Maghrib"))
                        {
                            lexnamaz.setText(sele1[0]);
                        }
                        else if(sele1[0].equals("Isha"))
                        {
                            lexnamaz.setText(sele1[0]);
                        }
//                        else if(sele[0].equals("Yesterday"))
//                        {
//                            SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
//                            java.util.Date currentdate = Calendar.getInstance().getTime();
//                            String  currentdatee = sd.format(currentdate);
//
//                            Calendar cal = Calendar.getInstance();
//                            java.util.Date backdate = cal.getTime();
//                            cal.add(Calendar.DATE, -1); // to get previous year add -1
//                            java.util.Date backYear = cal.getTime();
//                            String backyeardate = sd.format(backYear);
//
//                            Toast.makeText(QadahSalahActivity.this, ""+currentdatee+"\n"+backyeardate, Toast.LENGTH_SHORT).show();
//                            QadahRecordCheckTimePeriod(Userid,currentdatee,backyeardate);
//                        }


                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        return builder.create();
    }


    @Override
    public void onClick(View view) {
        id = 0;
         id = view.getId();
        if (id == R.id.mstart1) {
          setCurrentDateOnView(id);
               addListenerOnButton();

        } else if (id == R.id.mend) {
         setCurrentDateOnViewtwo(id);
            addListenerOnButtontwo();

        }

    }




}

