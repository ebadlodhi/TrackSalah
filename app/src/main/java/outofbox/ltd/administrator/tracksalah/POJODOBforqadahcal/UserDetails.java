package outofbox.ltd.administrator.tracksalah.POJODOBforqadahcal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserDetails {

    @SerializedName("0")
    @Expose
    private outofbox.ltd.administrator.tracksalah.POJODOBforqadahcal._0 _0;

    public outofbox.ltd.administrator.tracksalah.POJODOBforqadahcal._0 get0() {
        return _0;
    }

    public void set0(outofbox.ltd.administrator.tracksalah.POJODOBforqadahcal._0 _0) {
        this._0 = _0;
    }
}
