package outofbox.ltd.administrator.tracksalah;

public class MensResponse {
    private  int User_id,status;
    private  String today_date,today_time;

    public MensResponse(int user_id, int status, String today_date, String today_time) {
        User_id = user_id;
        this.status = status;
        this.today_date = today_date;
        this.today_time = today_time;
    }

    public int getUser_id() {
        return User_id;
    }

    public void setUser_id(int user_id) {
        User_id = user_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getToday_date() {
        return today_date;
    }

    public void setToday_date(String today_date) {
        this.today_date = today_date;
    }

    public String getToday_time() {
        return today_time;
    }

    public void setToday_time(String today_time) {
        this.today_time = today_time;
    }
}
