package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;

import java.util.List;

import outofbox.ltd.administrator.tracksalah.Exapmle;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface MendateApi {
    @FormUrlEncoded
    @POST("testfile.php")
    Call<List<Exapmle>> getDetails(@Field("User_id") String User_id,@Field("todaydate") String todaydate);
}
