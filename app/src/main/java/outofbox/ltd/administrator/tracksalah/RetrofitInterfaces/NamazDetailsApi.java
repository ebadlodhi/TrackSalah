package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;

import outofbox.ltd.administrator.tracksalah.POJO2.Example;

import java.util.Map;

import outofbox.ltd.administrator.tracksalah.POJO2.Example;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface NamazDetailsApi {
    long unixTime = System.currentTimeMillis() / 1000L;

    public static final String BASE_URL = "http://api.aladhan.com/v1/";
    @GET()
    Call<Example> getHeroes(@Url String url);
}
