package outofbox.ltd.administrator.tracksalah.POJODOBforqadahcal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class _0 {
    @SerializedName("User_DOB")
    @Expose
    private String userDOB;

    public String getUserDOB() {
        return userDOB;
    }

    public void setUserDOB(String userDOB) {
        this.userDOB = userDOB;
    }
}
