package outofbox.ltd.administrator.tracksalah;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import outofbox.ltd.administrator.tracksalah.POJO2.Example;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.NamazDetailsApi;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SplashActivity extends AppCompatActivity implements LocationListener {
    String getdatee="",getdayy="",gethijrii="",gettimezonee="",getfajrr="",getsunrisee="",getduhrr="",getasrr="",getsunsett="",getmaghribb="",getishaa="",getimsakk="",getfiqqahname="";
    LocationManager locationManager;
    public static Double latti = 52.4771894, longi = 2.0037147,altti;
    SharedPreferences sharedPreferencesmethod;
    String methodvalue ;
    Call<Example> call;
    NamazDetailsApi api;
    private Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        MobileAds.initialize(this, "ca-app-pub-2825367626708661~7620129751");

        Thread th = new Thread() {
            public void run() {

                try {

                    sleep(3000);
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                    finish();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


            }


        };
        th.start();
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Splash");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("TrackSalahUser")
                .setAction("User id")
                .setLabel(DashboardActivity.Userid)
                .setValue(1)
                .build());
        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

        }
        sharedPreferencesmethod = getSharedPreferences(SettingsActivity.methodvalue,Context.MODE_PRIVATE);
        methodvalue = sharedPreferencesmethod.getString("methodval","2");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(NamazDetailsApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();
        api = retrofit.create(NamazDetailsApi.class);
        getLocation();
    }


    void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, (LocationListener) this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        latti = location.getLatitude();
        longi = location.getLongitude();
        altti = location.getAltitude();
        long unixTime = System.currentTimeMillis() / 1000L;
        call = api.getHeroes("http://api.aladhan.com/v1/timings/"+unixTime+"?latitude="+latti+"&longitude="+longi+"&method="+methodvalue);
        call.enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {

                getdatee = response.body().getData().getDate().getReadable();
                getdayy = response.body().getData().getDate().getGregorian().getWeekday().getEn();
                gethijrii = response.body().getData().getDate().getHijri().getDate();
                gettimezonee = response.body().getData().getMeta().getTimezone();
                getfajrr = response.body().getData().getTimings().getFajr();
                getduhrr = response.body().getData().getTimings().getDhuhr();
                getasrr=response.body().getData().getTimings().getAsr();
                getmaghribb = response.body().getData().getTimings().getMaghrib();
                getishaa = response.body().getData().getTimings().getIsha();
                getsunrisee = response.body().getData().getTimings().getSunrise();
                getsunsett = response.body().getData().getTimings().getSunset();
                getfiqqahname =response.body().getData().getMeta().getMethod().getName();
                getimsakk = response.body().getData().getTimings().getImsak();

                DashboardActivity.compareFajrOne = getfajrr;
                DashboardActivity.compareFajrTwo = getsunrisee;
                DashboardActivity.compareZhrOne = getduhrr;
                DashboardActivity.compareZhrTwo = getasrr;
                DashboardActivity.compareAsarOne = getasrr;
                DashboardActivity.compareAsarTwo = getsunsett;
                DashboardActivity.compareMaghribOne = getmaghribb;
                DashboardActivity.compareMaghribTwo = getishaa;
                DashboardActivity.compareIshaOne = getishaa;
                DashboardActivity.compareIshaTwo = getfajrr;
                NotificationService.NcompareFajrOne = getfajrr;
                NotificationService.NcompareZhrOne = getduhrr;
                NotificationService.NcompareAsarOne = getasrr;
                NotificationService.NcompareMaghribOne = getmaghribb;
                NotificationService.NcompareIshaOne = getishaa;

            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
               Toast.makeText(getApplicationContext(), "Error Loading\nCheck Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });



        //Toast.makeText(getApplicationContext(),"Latitude: " + location.getLatitude() + "\n Longitude: " + location.getLongitude(),Toast.LENGTH_SHORT).show();

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            latti = location.getLatitude();
            longi = location.getLongitude();

        } catch (Exception e) {

        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(getApplicationContext(), "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();


    }
}

