package outofbox.ltd.administrator.tracksalah;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import outofbox.ltd.administrator.tracksalah.POJO3.Example;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.LoginApi;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UpdateConfirmActivity extends AppCompatActivity {
        SharedPreferences sharedPreferences;
        TextView uname ;
        EditText passwordc;
    public static String getuserpas = "";
    public static String getemail = "";
    public static final String ROOT_URL = "http://185.123.99.207/~android215/TrackSalah/";
    String name;
    private AdView mAdView;
    ProgressBar progressBar;
    private Tracker mTracker;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_confirm);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle("Edit User Profile");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Edit User Profile Login");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("TrackSalahUser")
                .setAction("User id")
                .setLabel(DashboardActivity.Userid)
                .setValue(1)
                .build());
        sharedPreferences = getSharedPreferences(LoginActivity.loginpref, Context.MODE_PRIVATE);
   name  = sharedPreferences.getString("preuseremail", "");
    uname = findViewById(R.id.unamec);
    passwordc = findViewById(R.id.passc);
    uname.append(name);
        mAdView = (AdView) findViewById(R.id.adView);
        MobileAds.initialize(this, "ca-app-pub-2825367626708661~7620129751");
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdView.setVisibility(View.VISIBLE);
            }
        });
            progressBar = findViewById(R.id.progressBar4);
            progressBar.setVisibility(View.GONE);
    }

    public void check(View view)
    {
        progressBar.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        LoginApi api =retrofit.create(LoginApi.class);
        Call<Example> call =  api.getDetails(name,passwordc.getText().toString());
        call.enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                getuserpas = response.body().getUserDetails().get0().getUserPassword();
                getemail =response.body().getUserDetails().get0().getUserEmail();
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                if ( getuserpas.equals(passwordc.getText().toString())) {
                    //  Toast.makeText(getApplicationContext(), "login failedddddddddddddd", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                    Intent intent = new Intent(getApplicationContext(), EditUserProfileActivity.class);
                    startActivity(intent);
                    finish();

                } else {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "login failed", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "No Internet", Toast.LENGTH_SHORT).show();
            }
        });




    }
}
