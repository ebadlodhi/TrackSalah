package outofbox.ltd.administrator.tracksalah;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.MensmodifycheckApi;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.MyProfileUpdate;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.UpdateUserDataApi;
import outofbox.ltd.administrator.tracksalah.UpdateProfile.Example;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class EditUserProfileActivity extends AppCompatActivity {
    String Userid = "";
    SharedPreferences sharedPreferences;
    public static final String ROOT_URL = "http://185.123.99.207/~android215/TrackSalah/";
    EditText name,pass;
   TextView email,gender,dob,salaobligatorydate,mensdate;
    ProgressBar p1;
    private AdView mAdView;
    Button update;
    String username,userpassword,useremail,genderr,UserDOB;
    ConstraintLayout menslayout;
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user_profile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sharedPreferences = getSharedPreferences(LoginActivity.loginpref, Context.MODE_PRIVATE);
        Userid = sharedPreferences.getString("preuid", "");
        name = findViewById(R.id.myname);
        email = findViewById(R.id.myemail);
salaobligatorydate = findViewById(R.id.salaobligatorydate);
mensdate = findViewById(R.id.mensdate);
        menslayout = findViewById(R.id.menslayout);
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Edit User Profile");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("TrackSalahUser")
                .setAction("User id")
                .setLabel(DashboardActivity.Userid)
                .setValue(1)
                .build());

        pass = findViewById(R.id.mypassword);
        gender = findViewById(R.id.mygender);
        dob = findViewById(R.id.mydob);
        p1 =  findViewById(R.id.progressBar2);
        mAdView = (AdView) findViewById(R.id.adView);
        MobileAds.initialize(this, "ca-app-pub-2825367626708661~7620129751");
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        update = findViewById(R.id.myupdate);
        LoadProfile(Userid);

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p1.setVisibility(View.VISIBLE);
                UpdateProfile(Userid);
            }
        });

        }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    public void LoadProfile(String userid)
    {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        MyProfileUpdate api = retrofit.create(MyProfileUpdate.class);
        Call<Example> call = api.getDetails(userid);
        call.enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                p1.setVisibility(View.GONE);
              String myname = response.body().getUserDetails().get0().getUserName();

                String myemail = response.body().getUserDetails().get0().getUserEmail();

                String mypass = response.body().getUserDetails().get0().getUserPassword();

                String mygender = response.body().getUserDetails().get0().getGender();

                String mydob = response.body().getUserDetails().get0().getUserDOB();
                final String namazfarzdate = response.body().getUserDetails().get0().getNamazFarzDate();
                final String menstart = response.body().getUserDetails().get0().getMstartDate();
                final String menend = response.body().getUserDetails().get0().getMendDate();
if(mygender.equals("Male")) {
    menslayout.setVisibility(View.GONE);
    name.setText(myname);
    email.setText(myemail);
    pass.setText(mypass);
    gender.setText(mygender);
    SimpleDateFormat comingformat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    SimpleDateFormat changeformat = new SimpleDateFormat("dd MMM yyyy", Locale.US);
Date dateofbirth = null;
String dateofbirthh = "";
    Date namazfarzdatee = null;
    String namazfarzdateeeee = "";
    try {
        dateofbirth = comingformat.parse(mydob);
        dateofbirthh = changeformat.format(dateofbirth);
        namazfarzdatee = comingformat.parse(namazfarzdate);
        namazfarzdateeeee = changeformat.format(namazfarzdatee);

    } catch (ParseException e) {
        e.printStackTrace();
    }

    dob.setText(dateofbirthh);
    salaobligatorydate.setText(namazfarzdateeeee);
}
else
{
    SimpleDateFormat comingformat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    SimpleDateFormat changeformat = new SimpleDateFormat("dd MMM yyyy", Locale.US);
    menslayout.setVisibility(View.VISIBLE);
    name.setText(myname);
    email.setText(myemail);
    pass.setText(mypass);
    gender.setText(mygender);
    Date dateofbirth = null;
    String dateofbirthh = "";
    Date namazfarzdatee = null;
    String namazfarzdateeeee = "";
    try {
        dateofbirth = comingformat.parse(mydob);
        dateofbirthh = changeformat.format(dateofbirth);
        namazfarzdatee = comingformat.parse(namazfarzdate);
        namazfarzdateeeee = changeformat.format(namazfarzdatee);

    } catch (ParseException e) {
        e.printStackTrace();
    }

    dob.setText(dateofbirthh);
    salaobligatorydate.setText(namazfarzdateeeee);
    mensdate.setText(menstart+" to "+menend+" of every month");
    menslayout.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ROOT_URL) //Setting the Root URL
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            MensmodifycheckApi apii = retrofit.create(MensmodifycheckApi.class);
            Call<String> calll = apii.getDetails(Userid);
            calll.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.isSuccessful()) {

                        if (response.body() != null) {
                            String ret = response.body().toString().trim();
                            Intent intent = new Intent(getApplicationContext(),MensModifyActivity.class);
                            intent.putExtra("condition",ret);
                            startActivity(intent);
                        }
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Toast.makeText(EditUserProfileActivity.this, "Network error.", Toast.LENGTH_SHORT).show();
                }
            });




        }
    });

}
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                p1.setVisibility(View.GONE);
                Toast.makeText(EditUserProfileActivity.this, "Network issue", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public  void UpdateProfile(String userid)
    {

        username = name.getText().toString();
        userpassword = pass.getText().toString();
        useremail = email.getText().toString();
        genderr = gender.getText().toString();
        UserDOB = dob.getText().toString();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        UpdateUserDataApi api = retrofit.create(UpdateUserDataApi.class);
        Call<String> call = api.getDetails(username,userpassword,useremail,genderr,UserDOB,userid);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {

                    if (response.body() != null) {
                        p1.setVisibility(View.GONE);
                        String ret = response.body().toString().trim();
                        if (ret.equals("Data Save")) {
                            Toast.makeText(getApplicationContext(), "Congractulations!\nAccount Details Updated", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                            startActivity(intent);
                            finish();

                        } else {
                            p1.setVisibility(View.GONE);

                            Toast.makeText(getApplicationContext(), "Server Not Responding", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                p1.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Network Problem\nTry Again!", Toast.LENGTH_SHORT).show();
            }
        });

    }

}
