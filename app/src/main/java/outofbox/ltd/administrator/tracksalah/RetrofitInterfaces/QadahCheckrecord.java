package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;

import outofbox.ltd.administrator.tracksalah.POJOQadahCheck.Example;

import outofbox.ltd.administrator.tracksalah.POJOQadahCheck.Example;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface QadahCheckrecord {
    @FormUrlEncoded
    @POST("QadahCheckRecord.php")
    Call<Example> getDetails(@Field("id") String id);

}
