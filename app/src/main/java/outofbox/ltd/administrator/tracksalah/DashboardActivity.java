package outofbox.ltd.administrator.tracksalah;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import outofbox.ltd.administrator.tracksalah.Databasesqlite.DatabaseHelper;
import outofbox.ltd.administrator.tracksalah.POJO2.Example;
import outofbox.ltd.administrator.tracksalah.POJOcalenderData.CalenderDetailsFetch;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.FetchCalenderData;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.NamazDetailsApi;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static outofbox.ltd.administrator.tracksalah.DailySalahActivity.NAME_NOT_SYNCED_WITH_SERVER;
import static outofbox.ltd.administrator.tracksalah.MensModifyActivity.mensswitchstate;

public class DashboardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,LocationListener {
    private RecyclerView recyclerView;
    public static final String ROOT_URL = "http://185.123.99.207/~android215/TrackSalah/";
    TextView uname;
   public static String Userid = "";
    public static String Gender = "";
    SharedPreferences sharedPreferences;
    SharedPreferences sharedPreferences2;
    public static String compareFajrOne = "05:04";
    public static String compareFajrTwo="06:09";
    public static String compareZhrOne="12:32";
    public static String compareZhrTwo="16:00";
    public static String compareAsarOne="16:01";
    public static String compareAsarTwo="18:54";
    public static String compareMaghribOne="18:55";
    public static String compareMaghribTwo="20:00";
    public static String compareIshaOne="20:01";
    public static String compareIshaTwo="04:00";
    LocationManager locationManager;
    public  static Double latti, longi;
    private AdView mAdView;
    public  Date date;
    SimpleDateFormat inputParser2 = new SimpleDateFormat("dd-MMM-yyyy");
    SimpleDateFormat inputParser = new SimpleDateFormat("kk:mm");
    private Date dateComparefajrOne;
    private Date dateComparefajrTwo;
    private Date todayCompare;
    private Date onScreenCompare;

    private Date dateComparezoharOne;
    private Date dateComparezoharTwo;
    private Date dateCompareasarOne;
    private Date dateCompareasarTwo;
    private Date dateComparemaghribOne;
    private Date dateComparemaghribTwo;
    private Date dateCompareishaOne;
    private Date dateCompareishaTwo;
    SharedPreferences sharedPreferencesmethod;
    String methodvalue ;
    boolean GpsStatus = false ;
    Call<Example> call;
    NamazDetailsApi api;
    String getdatee,getdayy,gethijrii,gettimezonee,getfajrr,getsunrisee,getduhrr,getasrr,getsunsett,getmaghribb,getishaa,getimsakk,getfiqqahname;
private Tracker mTracker;
    public DatabaseHelper db;
    public  String User_id = "";
    public  String fajar = "6:30";
    public  String duhur = "12:15";
    public  String asar = "15:30";
    public  String maghrib = "18:00";
    public  String isha = "19:00";
    String[] namazname = {"Fajar","Zohar","Asar","Maghrib","Isha"};


    public static SharedPreferences sharedpreferences3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_apps_black_24dp);
        setSupportActionBar(toolbar);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header=navigationView.getHeaderView(0);
        uname = header.findViewById(R.id.showusername);
        db = new DatabaseHelper(getApplicationContext());
        sharedPreferences = getSharedPreferences(LoginActivity.loginpref, Context.MODE_PRIVATE);
        Userid = sharedPreferences.getString("preuid", "");
        Gender = sharedPreferences.getString("preusergender","Male");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        //String name = this.getClass().getSimpleName();
        mTracker.setScreenName("Dashboard");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("TrackSalahUser")
                .setAction("User id")
                .setLabel(DashboardActivity.Userid)
                .setValue(1)
                .build());
        uname.setText("Welcome "+sharedPreferences.getString("preusername", ""));
       FetchCalenderDetails(Userid);
        mAdView = (AdView) findViewById(R.id.adView);
        MobileAds.initialize(this, "ca-app-pub-2825367626708661~7620129751");
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdView.setVisibility(View.VISIBLE);
            }
        });
        db = new DatabaseHelper(getApplicationContext());
        sharedpreferences3 = getSharedPreferences(mensswitchstate, Context.MODE_PRIVATE);


        sharedpreferences3 = getSharedPreferences(mensswitchstate, Context.MODE_PRIVATE);
        boolean response = sharedpreferences3.getBoolean("enable",false);
       // Toast.makeText(this, "aaaaaaa"+response, Toast.LENGTH_SHORT).show();
        if(Gender.equals("Male"))
        {

        }
        else {
            if (response == true) {
                //  Toast.makeText(this, "enable hai", Toast.LENGTH_SHORT).show();
//            Mensstartswitch.setClickable(true);
//            Mensstartswitch.setChecked(true);
                User_id = sharedPreferences.getString("preuid", "");
                Date current = Calendar.getInstance().getTime();
                Calendar c = Calendar.getInstance();


                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
                SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("HH:mm", Locale.US);

                String currentdate = simpleDateFormat.format(current);
                String currenttime = simpleDateFormat1.format(current);
                try {
                    c.setTime(simpleDateFormat.parse(currentdate));
                    c.add(Calendar.DATE, 1);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Date nextdate = c.getTime();
                Date fajrgiventime = null;
                Date duhrgiventime = null;
                Date asargiventime = null;
                Date maghribgiventime = null;
                Date ishagiventime = null;
                Date currentgiventime = null;
                String nextdaydate = simpleDateFormat.format(nextdate);
                try {
                    fajrgiventime = simpleDateFormat1.parse(fajar);
                    duhrgiventime = simpleDateFormat1.parse(duhur);
                    asargiventime = simpleDateFormat1.parse(asar);
                    maghribgiventime = simpleDateFormat1.parse(maghrib);
                    ishagiventime = simpleDateFormat1.parse(isha);
                    currentgiventime = simpleDateFormat1.parse(currenttime);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (currentgiventime.after(fajrgiventime) && currentgiventime.before(duhrgiventime)) {
                    for (int i = 1; i < 5; i++) {
                        String Namaztimeinsert = namazname[i];
                        String todaydateinsert = currentdate;
                        String ontimeinsert = "No";
                        String lateinsert = "No";
                        String missedinsert = "No";
                        String exemptedinsert = "Exempted";
                        db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                        // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                        registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    }
                } else if (currentgiventime.after(duhrgiventime) && currentgiventime.before(asargiventime)) {
                    for (int i = 2; i < 5; i++) {
                        String Namaztimeinsert = namazname[i];
                        String todaydateinsert = currentdate;
                        String ontimeinsert = "No";
                        String lateinsert = "No";
                        String missedinsert = "No";
                        String exemptedinsert = "Exempted";
                        db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                        // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                        registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    }
                } else if (currentgiventime.after(asargiventime) && currentgiventime.before(maghribgiventime)) {
                    for (int i = 3; i < 5; i++) {
                        String Namaztimeinsert = namazname[i];
                        String todaydateinsert = currentdate;
                        String ontimeinsert = "No";
                        String lateinsert = "No";
                        String missedinsert = "No";
                        String exemptedinsert = "Exempted";
                        db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                        // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                        registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    }
                } else if (currentgiventime.after(maghribgiventime) && currentgiventime.before(ishagiventime)) {
                    for (int i = 4; i < 5; i++) {
                        String Namaztimeinsert = namazname[i];
                        String todaydateinsert = currentdate;
                        String ontimeinsert = "No";
                        String lateinsert = "No";
                        String missedinsert = "No";
                        String exemptedinsert = "Exempted";
                        db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                        // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                        registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    }
                } else if (currentgiventime.after(ishagiventime) && currentgiventime.before(fajrgiventime)) {
                    for (int i = 0; i < 5; i++) {
                        String Namaztimeinsert = namazname[i];
                        String todaydateinsert = nextdaydate;
                        String ontimeinsert = "No";
                        String lateinsert = "No";
                        String missedinsert = "No";
                        String exemptedinsert = "Exempted";
                        db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                        // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                        registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    }
                }
            } else if (response == false) {
//            Toast.makeText(this, "enable nahi hai", Toast.LENGTH_SHORT).show();
//            Mensstartswitch.setClickable(false);
                User_id = sharedPreferences.getString("preuid", "");
                Date current = Calendar.getInstance().getTime();
                Calendar c = Calendar.getInstance();


                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
                SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("HH:mm", Locale.US);

                String currentdate = simpleDateFormat.format(current);
                String currenttime = simpleDateFormat1.format(current);
                try {
                    c.setTime(simpleDateFormat.parse(currentdate));
                    c.add(Calendar.DATE, 1);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Date nextdate = c.getTime();
                Date fajrgiventime = null;
                Date duhrgiventime = null;
                Date asargiventime = null;
                Date maghribgiventime = null;
                Date ishagiventime = null;
                Date currentgiventime = null;
                String nextdaydate = simpleDateFormat.format(nextdate);
                try {
                    fajrgiventime = simpleDateFormat1.parse(fajar);
                    duhrgiventime = simpleDateFormat1.parse(duhur);
                    asargiventime = simpleDateFormat1.parse(asar);
                    maghribgiventime = simpleDateFormat1.parse(maghrib);
                    ishagiventime = simpleDateFormat1.parse(isha);
                    currentgiventime = simpleDateFormat1.parse(currenttime);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (currentgiventime.after(fajrgiventime) && currentgiventime.before(duhrgiventime)) {
                    for (int i = 1; i < 5; i++) {
                        String Namaztimeinsert = namazname[i];
                        String todaydateinsert = currentdate;
                        String ontimeinsert = "No";
                        String lateinsert = "No";
                        String missedinsert = "missed";
                        String exemptedinsert = "No";
                        db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                        // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                        registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    }
                } else if (currentgiventime.after(duhrgiventime) && currentgiventime.before(asargiventime)) {
                    for (int i = 2; i < 5; i++) {
                        String Namaztimeinsert = namazname[i];
                        String todaydateinsert = currentdate;
                        String ontimeinsert = "No";
                        String lateinsert = "No";
                        String missedinsert = "missed";
                        String exemptedinsert = "No";
                        db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                        // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                        registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    }
                } else if (currentgiventime.after(asargiventime) && currentgiventime.before(maghribgiventime)) {
                    for (int i = 3; i < 5; i++) {
                        String Namaztimeinsert = namazname[i];
                        String todaydateinsert = currentdate;
                        String ontimeinsert = "No";
                        String lateinsert = "No";
                        String missedinsert = "missed";
                        String exemptedinsert = "No";
                        db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                        // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                        registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    }
                } else if (currentgiventime.after(maghribgiventime) && currentgiventime.before(ishagiventime)) {
                    for (int i = 4; i < 5; i++) {
                        String Namaztimeinsert = namazname[i];
                        String todaydateinsert = currentdate;
                        String ontimeinsert = "No";
                        String lateinsert = "No";
                        String missedinsert = "missed";
                        String exemptedinsert = "No";
                        db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                        // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                        registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    }
                } else if (currentgiventime.after(ishagiventime) && currentgiventime.before(fajrgiventime)) {
                    for (int i = 0; i < 5; i++) {
                        String Namaztimeinsert = namazname[i];
                        String todaydateinsert = nextdaydate;
                        String ontimeinsert = "No";
                        String lateinsert = "No";
                        String missedinsert = "missed";
                        String exemptedinsert = "No";
                        db.addRecord(Integer.parseInt(User_id), todaydateinsert, Namaztimeinsert, ontimeinsert, lateinsert, missedinsert, exemptedinsert, NAME_NOT_SYNCED_WITH_SERVER);
                        // DailySalahActivity.fajrofer.setBackgroundResource(R.drawable.nonetworksalah);
                        registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    }
                }

            }
        }









        sharedPreferences2 = getSharedPreferences(SettingsActivity.switchstate, MODE_PRIVATE);
        if (sharedPreferences2.contains("enable")) {

            Thread thread = new Thread() {
                public void run() {
                    try {

                        Intent intent = new Intent(getApplicationContext(), NotificationService.class);
                        startService(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            };

            thread.start();
        } else {
            Thread thread = new Thread() {
                public void run() {
                    try {

                        Intent intent = new Intent(getApplicationContext(), NotificationService.class);

                        stopService(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            };

            thread.start();
        }
//            uname = (TextView) findViewById(R.id.uname2);
//            uname.setText("Asslamualaikum " + sharedPreferences.getString("preusername", ""));
            Dateofbirth1 dateofbirth = new Dateofbirth1();
            dateofbirth.execute(Userid);

            if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

            }
        sharedPreferencesmethod = getSharedPreferences(SettingsActivity.methodvalue,Context.MODE_PRIVATE);
        methodvalue = sharedPreferencesmethod.getString("methodval","2");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(NamazDetailsApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();
        api = retrofit.create(NamazDetailsApi.class);
            getLocation();
        final boolean[] ispressed = {false};


        Calendar now = Calendar.getInstance();

        final int hour = now.get(Calendar.HOUR_OF_DAY);
        final int minute = now.get(Calendar.MINUTE);


        date = parseDateeb(Integer.toString(hour) + ":" + Integer.toString(minute));
        dateComparefajrOne = parseDateeb(compareFajrOne);
        dateComparefajrTwo = parseDateeb(compareFajrTwo);
        dateComparezoharOne = parseDateeb(compareZhrOne);
        dateComparezoharTwo = parseDateeb(compareZhrTwo);
        dateCompareasarOne = parseDateeb(compareAsarOne);
        dateCompareasarTwo = parseDateeb(compareAsarTwo);
        dateComparemaghribOne = parseDateeb(compareMaghribOne);
        dateComparemaghribTwo = parseDateeb(compareMaghribTwo);
        dateCompareishaOne = parseDateeb(compareIshaOne);
        dateCompareishaTwo = parseDateeb(compareIshaTwo);

        String farzdate = sharedPreferences.getString("preuserfarzdate","");
        String startdate = sharedPreferences.getString("preuserstartdate","");
        if(farzdate.equals("") && startdate.equals(""))
        {

            Toast.makeText(this, "Kindly Goto QADAH SALAH and calculate your QADAH NAMAZ.", Toast.LENGTH_LONG).show();

        }
        else
        {
            //Toast.makeText(this, "farz = "+farzdate+"\nstart = "+startdate, Toast.LENGTH_SHORT).show();

        }


            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (dateComparefajrOne.before(date) && dateComparefajrTwo.after(date)) {
                        Snackbar.make(view, "Fajar Namaz Time is Remaining", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                    else if (dateComparezoharOne.before(date) && dateComparezoharTwo.after(date)) {
                        Calendar cal = Calendar.getInstance() ;

                        String daystring = cal.getDisplayName(Calendar.DAY_OF_WEEK,Calendar.LONG,Locale.getDefault());
                        if (daystring.equals("Friday"))
                        {
                            Snackbar.make(view, "Namaz-e-JUMMA Time is Remaining", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();

                        }
                        else {
                            Snackbar.make(view, "Zohar Namaz Time is Remaining", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        }
                        }
                    else if (dateCompareasarOne.before(date) && dateCompareasarTwo.after(date)) {
                        Snackbar.make(view, "Asar Namaz Time is Remaining", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                    else if (dateComparemaghribOne.before(date) && dateComparemaghribTwo.after(date)) {
                        Snackbar.make(view, "Maghrib Namaz Time is Remaining", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                    else if (dateCompareishaOne.before(date) && dateCompareishaTwo.after(date)) {
                        Snackbar.make(view, "Isha Namaz Time is Remaining", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                    else
                    {
                        Snackbar.make(view, "No Namaz Time", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();


                    }



                }
            });

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

            drawer.addDrawerListener(toggle);
            toggle.syncState();

            //NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        }

    public void Dailysalah(View view)
    { if(internet_connection() == true) {
        final Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c.getTime());
        Intent intent = new Intent(getApplicationContext(), DailySalahActivity.class);
        intent.putExtra("ClickedDate", formattedDate);
        startActivity(intent);
    }
    else{
        Toast.makeText(this, "Kindly Connect your Device to Network.", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getApplicationContext(), DailySalahActivity.class);
        final Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c.getTime());
        intent.putExtra("ClickedDate",formattedDate);
        startActivity(intent);
    }
    }
    public void Qadahsalah(View view)
    {   if(internet_connection() == true) {
        Intent intent = new Intent(getApplicationContext(), QadahSalahActivity.class);
        startActivity(intent);
    }
    else
    {
        Toast.makeText(this, "Kindly Connect your Device to Network.", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getApplicationContext(), QadahSalahActivity.class);
        startActivity(intent);
    }
    }
    public void Salahtimings(View view)
    {
        Intent intent =new Intent(getApplicationContext(),DailyNamazDetailActivity.class);
        startActivity(intent);
    }
    public void MosqueFinder(View view)
    {
        Intent intent =new Intent(getApplicationContext(),MapsActivity.class);
        startActivity(intent);
    }
    public void Qiblalocator(View view)
    {
        Intent intent =new Intent(getApplicationContext(),QiblaDirectionActivity.class);
        startActivity(intent);
    }
    public void Settings(View view)
    {
        Intent intent =new Intent(getApplicationContext(),SettingsActivity.class);
        startActivity(intent);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.dashboard, menu);
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.DAILY_SALAH) {
            {
                if (internet_connection() == true) {
                    Intent intent = new Intent(getApplicationContext(), DailySalahActivity.class);
                    final Calendar c = Calendar.getInstance();
                    SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                    String formattedDate = df.format(c.getTime());
                    intent.putExtra("ClickedDate",formattedDate);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "Kindly Connect your Device to Network.", Toast.LENGTH_SHORT).show();
                }
            }
        } else if (id == R.id.RECORD_SALAH) {
            if(internet_connection() == true) {
                Intent intent = new Intent(getApplicationContext(), QadahSalahActivity.class);
                startActivity(intent);
            }
            else
            {
                Toast.makeText(this, "Kindly Connect your Device to Network.", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.TODAY_SALAH_TIME) {
            Intent intent = new Intent(getApplicationContext(), DailyNamazDetailActivity.class);
            startActivity(intent);
        } else if (id == R.id.MOSQUE_FINDER) {
            Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
            startActivity(intent);
        } else if (id == R.id.QIBLA_LOCATOR) {
            Intent intent = new Intent(getApplicationContext(), QiblaDirectionActivity.class);
            startActivity(intent);
        }
     else if (id == R.id.share) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            String title = "Share Track Salah with Freinds";
            String link = "https://play.google.com/store/apps/details?id="+getApplicationContext().getPackageName();
            intent.putExtra(Intent.EXTRA_SUBJECT,title);
            intent.putExtra(Intent.EXTRA_TEXT,link);
            startActivity(Intent.createChooser(intent,"Share TrackSalah using")) ;
    }
        else if (id == R.id.report) {
            String[] TO = {"salam@tracksalah.com"};
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setData(Uri.parse("mailto:"));
            emailIntent.setType("text/plain");
            emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);

            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Report for Track Salah");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");
            emailIntent.setPackage("com.google.android.gm");
            try {
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));

            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(DashboardActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
            }
        }
        else if (id == R.id.about) {
            Intent intent = new Intent(getApplicationContext(), About.class);
            startActivity(intent);
        }
        else if (id == R.id.logout) {

            SharedPreferences.Editor editor2 = sharedPreferences.edit();
            editor2.clear();
            editor2.apply();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finish();

        }
        else if (id == R.id.setting) {


            Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(intent);


        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
          locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 500000, 50, this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        latti = location.getLatitude();
        longi = location.getLongitude();
        long unixTime = System.currentTimeMillis() / 1000L;
        call = api.getHeroes("http://api.aladhan.com/v1/timings/"+unixTime+"?latitude="+latti+"&longitude="+longi+"&method="+methodvalue);
        call.enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {

                getdatee = response.body().getData().getDate().getReadable();
                getdayy = response.body().getData().getDate().getGregorian().getWeekday().getEn();
                gethijrii = response.body().getData().getDate().getHijri().getDate();
                gettimezonee = response.body().getData().getMeta().getTimezone();
                getfajrr = response.body().getData().getTimings().getFajr();
                getduhrr = response.body().getData().getTimings().getDhuhr();
                getasrr=response.body().getData().getTimings().getAsr();
                getmaghribb = response.body().getData().getTimings().getMaghrib();
                getishaa = response.body().getData().getTimings().getIsha();
                getsunrisee = response.body().getData().getTimings().getSunrise();
                getsunsett = response.body().getData().getTimings().getSunset();
                getfiqqahname =response.body().getData().getMeta().getMethod().getName();
                getimsakk = response.body().getData().getTimings().getImsak();

                DashboardActivity.compareFajrOne = getfajrr;
                DashboardActivity.compareFajrTwo = getsunrisee;
                DashboardActivity.compareZhrOne = getduhrr;
                DashboardActivity.compareZhrTwo = getasrr;
                DashboardActivity.compareAsarOne = getasrr;
                DashboardActivity.compareAsarTwo = getsunsett;
                DashboardActivity.compareMaghribOne = getmaghribb;
                DashboardActivity.compareMaghribTwo = getishaa;
                DashboardActivity.compareIshaOne = getishaa;
                DashboardActivity.compareIshaTwo = getfajrr;
//                NotificationService.NcompareFajrOne = getfajrr;
//                NotificationService.NcompareZhrOne = getduhrr;
//                NotificationService.NcompareAsarOne = getasrr;
//                NotificationService.NcompareMaghribOne = getmaghribb;
//                NotificationService.NcompareIshaOne = getishaa;







            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
               // Toast.makeText(getApplicationContext(), "Error Loading\nCheck Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });


        //Toast.makeText(getApplicationContext(),"Latitude: " + location.getLatitude() + "\n Longitude: " + location.getLongitude(),Toast.LENGTH_SHORT).show();

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            latti = location.getLatitude();
            longi = location.getLongitude();
        } catch (Exception e) {

        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        //
        //Toast.makeText(getApplicationContext(), "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();


    }
    boolean internet_connection(){
        //Check if connected to internet, output accordingly
        ConnectivityManager cm = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }
    private Date parseDateeb2(String date) {

        try {

            return inputParser2.parse(date);
        } catch (java.text.ParseException e) {
            return new Date(0);
        }

    }
    private Date parseDateeb(String date) {

        try {

            return inputParser.parse(date);
        } catch (java.text.ParseException e) {
            return new Date(0);
        }

    }
    HashMap<String, Integer> namaz_count_per_day;
    public void FetchCalenderDetails(String Uid) {
        String uid = Uid;
        namaz_count_per_day= new HashMap<String, Integer>();
        Retrofit retrofitcalenderdetails = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        FetchCalenderData apicalenderfetch = retrofitcalenderdetails.create(FetchCalenderData.class);
        final Call<List<CalenderDetailsFetch>> callcalenderdetails = apicalenderfetch.getDetails(uid);
        callcalenderdetails.enqueue(new Callback<List<CalenderDetailsFetch>>() {
            @Override
            public void onResponse(Call<List<CalenderDetailsFetch>> call, Response<List<CalenderDetailsFetch>> response) {
                List<CalenderDetailsFetch> calenderDetailsFetches = response.body();
                if (calenderDetailsFetches.size() > 0) {
                    String[] todaydate = new String[calenderDetailsFetches.size()];
                    String[] namazname = new String[calenderDetailsFetches.size()];
                    String[] ontime = new String[calenderDetailsFetches.size()];
                    String[] late = new String[calenderDetailsFetches.size()];
                    String[] missed = new String[calenderDetailsFetches.size()];
                    String[] Exempted = new String[calenderDetailsFetches.size()];
                    CalendarCollection.date_collection_arr = new ArrayList<CalendarCollection>(1);
                    String finalDate = "";


                    //Hamza
                    for (int a = 0; a < calenderDetailsFetches.size(); a++) {
                        todaydate[a] = calenderDetailsFetches.get(a).getTodayDate();
                        ontime[a] = calenderDetailsFetches.get(a).getOntime();
                        late[a] = calenderDetailsFetches.get(a).getLate();
                        missed[a] = calenderDetailsFetches.get(a).getMissed();
                        Exempted[a] = calenderDetailsFetches.get(a).getExempted();
                    }
                    namaz_count_per_day.put(calenderDetailsFetches.get(0).getTodayDate().toString(), 0);
                    if (calenderDetailsFetches.get(0).getOntime().equals("ontime") || calenderDetailsFetches.get(0).getLate().equals("late") ) {
                        namaz_count_per_day.put(calenderDetailsFetches.get(0).getTodayDate().toString(), namaz_count_per_day.get(calenderDetailsFetches.get(0).getTodayDate()) + 1);
                    }
                    else if(calenderDetailsFetches.get(0).getExempted().equals("Exempted"))
                    {
                        namaz_count_per_day.put(calenderDetailsFetches.get(0).getTodayDate().toString(), namaz_count_per_day.get(calenderDetailsFetches.get(0).getTodayDate()) + 5);
                    }
                    int ii = 1;
                    while (ii < calenderDetailsFetches.size()) {
                        String A = todaydate[ii];
                        String B = todaydate[ii - 1];
                        if (A.equals(B)) {
                            if (calenderDetailsFetches.get(ii).getOntime().toString().equals("ontime") || calenderDetailsFetches.get(ii).getLate().toString().equals("late") ) {
                                namaz_count_per_day.put(calenderDetailsFetches.get(ii).getTodayDate(), namaz_count_per_day.get(calenderDetailsFetches.get(ii).getTodayDate()) + 1);
                            }
                            else if (calenderDetailsFetches.get(0).getExempted().equals("Exempted"))
                            {
                                namaz_count_per_day.put(calenderDetailsFetches.get(ii).getTodayDate(), namaz_count_per_day.get(calenderDetailsFetches.get(ii).getTodayDate()) + 5);
                            }

                        } else {
                            namaz_count_per_day.put(calenderDetailsFetches.get(ii).getTodayDate().toString(), 0);
                            if (calenderDetailsFetches.get(ii).getOntime().toString().equals("ontime") || calenderDetailsFetches.get(ii).getLate().toString().equals("late") ) {
                                namaz_count_per_day.put(calenderDetailsFetches.get(ii).getTodayDate(), namaz_count_per_day.get(calenderDetailsFetches.get(ii).getTodayDate()) + 1);
                            }
                            else if(calenderDetailsFetches.get(0).getExempted().equals("Exempted"))
                            {
                                namaz_count_per_day.put(calenderDetailsFetches.get(ii).getTodayDate(), namaz_count_per_day.get(calenderDetailsFetches.get(ii).getTodayDate()) + 5);
                            }
                        }
                        ii++;
                    }
                    Log.e("Data", namaz_count_per_day + "");
                    for (int i = 0; i < calenderDetailsFetches.size(); i++) {
                        String datenow = todaydate[i];
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MMM-yyyy");
                        SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
                        Date myDate = null;
                        try {
                            myDate = dateFormat1.parse(datenow);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                            finalDate = dateFormat1.format(myDate);
                        CalendarCollection.date_collection_arr.add(new CalendarCollection("" + finalDate + "", "offerred", namaz_count_per_day.get(finalDate)));
                    }
                    //Toast.makeText(DailySalahActivity.this, finalDate + "\n " + calenderDetailsFetches.size(), Toast.LENGTH_LONG).show();

                }

                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }

            @Override
            public void onFailure(Call<List<CalenderDetailsFetch>> call, Throwable t) {

                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        });
    }
}

