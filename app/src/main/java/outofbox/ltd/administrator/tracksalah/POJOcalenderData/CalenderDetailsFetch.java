package outofbox.ltd.administrator.tracksalah.POJOcalenderData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CalenderDetailsFetch {

    @SerializedName("today_date")
    @Expose
    private String todayDate;
    @SerializedName("namaz_name")
    @Expose
    private String namazName;
    @SerializedName("ontime")
    @Expose
    private String ontime;
    @SerializedName("late")
    @Expose
    private String late;
    @SerializedName("missed")
    @Expose
    private String missed;
    @SerializedName("Exempted")
    @Expose
    private String exempted;

    public String getTodayDate() {
        return todayDate;
    }

    public void setTodayDate(String todayDate) {
        this.todayDate = todayDate;
    }

    public String getNamazName() {
        return namazName;
    }

    public void setNamazName(String namazName) {
        this.namazName = namazName;
    }

    public String getOntime() {
        return ontime;
    }

    public void setOntime(String ontime) {
        this.ontime = ontime;
    }

    public String getLate() {
        return late;
    }

    public void setLate(String late) {
        this.late = late;
    }

    public String getMissed() {
        return missed;
    }

    public void setMissed(String missed) {
        this.missed = missed;
    }

    public String getExempted() {
        return exempted;
    }

    public void setExempted(String exempted) {
        this.exempted = exempted;
    }
}
