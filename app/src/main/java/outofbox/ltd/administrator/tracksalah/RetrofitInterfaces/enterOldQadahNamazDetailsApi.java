package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface enterOldQadahNamazDetailsApi {
    @FormUrlEncoded
    @POST("enterOldQadahNamaDetails.php")
    Call<String> getDetails(@Field("id") String  id,
                            @Field("fajardata") String fajardata,
                            @Field("zohardata") String zohardata,
                            @Field("asardata") String asardata,
                            @Field("Maghribdata") String Maghribdata,
                            @Field("ishadata") String ishadata,
                            @Field("daytoday") String daytoday);
}
