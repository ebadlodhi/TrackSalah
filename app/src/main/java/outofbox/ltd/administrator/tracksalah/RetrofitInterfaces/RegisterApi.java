package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;

import outofbox.ltd.administrator.tracksalah.POJO3.Example;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;


public interface RegisterApi {
    @FormUrlEncoded
    @POST("registerservice.php")
    Call<String> getDetails(@Field("name") String name, @Field("password") String password,@Field("email") String email,@Field("gender") String gender,@Field("dob") String dob,@Field("creationtime") String creationtime
    ,@Field("cdate") String cdate, @Field("TotalQadahNamaz") String RemainingQadahNamaz,@Field("RemainingQadahNamaz") String TotalQadahNamaz,
                            @Field("Qadahfajar") String Qadahfajar,@Field("QadahZohar") String QadahZohar,@Field("QadahAsar") String QadahAsar
    ,@Field("Qadahmaghrib") String Qadahmaghrib,@Field("Qadahisha") String Qadahisha,@Field("tenyear") String tenyear,@Field("MstartDate") String MstartDate,@Field("MendDate") String MendDate);
}
