package outofbox.ltd.administrator.tracksalah;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.MenscalculatesignupApi;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.skipmensApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static outofbox.ltd.administrator.tracksalah.LoginActivity.loginpref;

public class MenscalculateActivity extends AppCompatActivity {
    TextView datepicker;
    private DatePicker dpResult;
    Date Currenttime;
    String currentDateandTime;
    private int myear,mfarzyear;
    private int mmonth,mfarzmonth;
    private int mday,mfarzday;
    public static SharedPreferences sharedpreferences;
    public static final String ROOT_URL = "http://185.123.99.207/~android215/TrackSalah/";
    EditText cycle , noofdays;
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menscalculate);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle("Mensulation Days");
        dpResult = (DatePicker) findViewById(R.id.dpResult2);
        datepicker = findViewById(R.id.textView30);
        cycle = findViewById(R.id.editText2);
        noofdays = findViewById(R.id.editText3);
        sharedpreferences = getSharedPreferences(loginpref, Context.MODE_PRIVATE);
        setCurrentDateOnView();
        datepicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(111);
            }
        });
    }

public  void savemdate(View view) {
    String userid = sharedpreferences.getString("preuid", "");
    String lastmonthdate = datepicker.getText().toString();
    String Dateofbirth = sharedpreferences.getString("preuserfarzdate", "");
    //Toast.makeText(this, ""+userid, Toast.LENGTH_SHORT).show();
    String cyclee = cycle.getText().toString().trim();
    String noofdayss = noofdays.getText().toString().trim();
    SharedPreferences.Editor editor2 = sharedpreferences.edit();
    editor2.putString("cycle", cyclee);
    editor2.putString("noofday", noofdayss);
    editor2.apply();
    editor2.commit();
    // Toast.makeText(this, "Date "+Dateofbirth, Toast.LENGTH_SHORT).show();
    if (lastmonthdate.isEmpty()) {
        datepicker.setError("Select Last month mensturation date");
        datepicker.requestFocus();
    } else if (cyclee.isEmpty()) {
        cycle.setError("Fill this field");
        cycle.requestFocus();

    } else if (noofdayss.isEmpty()) {
        noofdays.setError("Fill this field");
        noofdays.requestFocus();

    } else {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        MenscalculatesignupApi api = retrofit.create(MenscalculatesignupApi.class);
        Call<String> call = api.getDetails(userid, lastmonthdate, Dateofbirth, cyclee, noofdayss);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.body().contains("Updated")) {
                    Intent intent = new Intent(getApplicationContext(), QadahsalahCalculateActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(MenscalculateActivity.this, "Incorrect Data", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(MenscalculateActivity.this, "Network issue", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 111: {
                DatePickerDialog _datefarz = new DatePickerDialog(this, datePickerListenerfarz, myear, mmonth,
                        mday) {
                    @Override
                    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                        if (year > myear) {
//                            Toast.makeText(getApplicationContext(), "Year can't be greater then current Year", Toast.LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }
//                        if (monthOfYear > mmonth && year == myear) {
//                            Toast.makeText(getApplicationContext(), "Month can't be greater then current Month", Toast.LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }
//                        if (dayOfMonth > mday && year == myear && monthOfYear == mmonth) {
//                            Toast.makeText(getApplicationContext(), "Date can't be greater then current date", Toast.LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }
                    }
                };
                return _datefarz;
            }

        }


        return null;
    }
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            myear = selectedYear;
            mmonth = selectedMonth;
            mday = selectedDay;
            String sMonth = "";
            // set selected date into textview
            if (mmonth == 0)
                sMonth = "Jan";
            else if (mmonth == 1)
                sMonth = "Feb";
            else if (mmonth == 2)
                sMonth = "Mar";
            else if (mmonth == 3)
                sMonth = "Apr";
            else if (mmonth == 4)
                sMonth = "May";
            else if (mmonth == 5)
                sMonth = "Jun";
            else if (mmonth == 6)
                sMonth = "Jul";
            else if (mmonth == 7)
                sMonth = "Aug";
            else if (mmonth == 8)
                sMonth = "Sep";
            else if (mmonth == 9)
                sMonth = "Oct";
            else if (mmonth == 10)
                sMonth = "Nov";
            else if (mmonth == 11)
                sMonth = "Dec";
            datepicker.setText(new StringBuilder()
                    .append(mday).append(" ") .append(sMonth).append(" ").append(myear).append(" "));
            // set selected date into datepicker also
            dpResult.init(myear, mmonth, mday, null);

//            try {
//                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy");
//                SimpleDateFormat simpleDateFormattenyear = new SimpleDateFormat("yyyy-MM-dd");
//                mydate = simpleDateFormat.parse(tvDisplayDate.getText().toString());
//                Calendar c = Calendar.getInstance();
//                c.setTime(mydate);
//                c.add(Calendar.YEAR, 14);
//                newdate = c.getTime();
//                longdate = newdate.getTime();
//                tenyear = simpleDateFormat.format(newdate);
//                tenyearServer = simpleDateFormattenyear.format(newdate);
//                dateofb.setText(tenyear);
//                mfarzyear = Calendar.YEAR;
//                mfarzmonth = Calendar.MONTH;
//                mfarzday = Calendar.DAY_OF_MONTH;
//
//
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }


        }
    };
    public void setCurrentDateOnView() {



        final Calendar c = Calendar.getInstance();
        Currenttime = c.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        currentDateandTime = sdf.format(new Date());
        myear = c.get(Calendar.YEAR);
        mmonth = c.get(Calendar.MONTH);
        mday = c.get(Calendar.DAY_OF_MONTH);

        // set current date into textview
//        tvDisplayDate.setText(new StringBuilder()
//                // Month is 0 based, just add 1
//                .append(myear).append("-") .append(mmonth + 1).append("-").append(mday)
//        );

        // set current date into datepicker
        dpResult.init(myear, mmonth, mday, null);

    }

    private DatePickerDialog.OnDateSetListener datePickerListenerfarz = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            myear = selectedYear;
            mmonth = selectedMonth;
            mday = selectedDay;
            String sMonth = "";
            // set selected date into textview
            if (mmonth == 0)
                sMonth = "Jan";
            else if (mmonth == 1)
                sMonth = "Feb";
            else if (mmonth == 2)
                sMonth = "Mar";
            else if (mmonth == 3)
                sMonth = "Apr";
            else if (mmonth == 4)
                sMonth = "May";
            else if (mmonth == 5)
                sMonth = "Jun";
            else if (mmonth == 6)
                sMonth = "Jul";
            else if (mmonth == 7)
                sMonth = "Aug";
            else if (mmonth == 8)
                sMonth = "Sep";
            else if (mmonth == 9)
                sMonth = "Oct";
            else if (mmonth == 10)
                sMonth = "Nov";
            else if (mmonth == 11)
                sMonth = "Dec";
            datepicker.setText(new StringBuilder()
                    .append(mday).append(" ") .append(sMonth).append(" ").append(myear).append(" "));
            // set selected date into datepicker also
            dpResult.init(myear, mmonth, mday, null);

//            try {
//                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy");
//                SimpleDateFormat simpleDateFormattenyear = new SimpleDateFormat("yyyy-MM-dd");
//                mydate = simpleDateFormat.parse(dateofb.getText().toString());
//                Calendar c = Calendar.getInstance();
//                c.setTime(mydate);
//                c.add(Calendar.YEAR, 14);
//                newdate = c.getTime();
//                longdate = newdate.getTime();
//                tenyear = simpleDateFormat.format(newdate);
//                tenyearServer = simpleDateFormattenyear.format(newdate);
////                namazfarzdate.setText(tenyear);
//                mfarzyear = Calendar.YEAR;
//                mfarzmonth = Calendar.MONTH;
//                mfarzday = Calendar.DAY_OF_MONTH;

//
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//


        }
    };


    public void skipmens(View view)
    {
        String userid = sharedpreferences.getString("preuid","");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        skipmensApi api =retrofit.create(skipmensApi.class);
        Call<String> call = api.getDetails(userid);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.body().contains("Updated"))
                {
                    Intent intent = new Intent(getApplicationContext(),QadahsalahCalculateActivity.class);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    Toast.makeText(MenscalculateActivity.this, "Incorrect Data", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(MenscalculateActivity.this, "Network issue", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
