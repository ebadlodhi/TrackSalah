package outofbox.ltd.administrator.tracksalah;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import outofbox.ltd.administrator.tracksalah.POJO2.Example;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.FetchEmptyCalenderData;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.NamazDetailsApi;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DailyNamazDetailActivity extends AppCompatActivity implements LocationListener {

    public static TextView getdate,getday,gethijri,gettimezone,getfajr,getsunrise,getduhr,getasr,getsunset,getmaghrib,getisha,getimsak,getfiqqah,getislamicmonth;
    String getdatee,getdayy,gethijrii,gettimezonee,getfajrr,getsunrisee,getduhrr,getasrr,getsunsett,getmaghribb,getishaa,getimsakk,getfiqqahname,islamicmonth;
    String[] islamic_months = {"",
            "Muḥarram","Ṣafar","Rabī‘ al-awwal","Rabī‘ ath-thānī",
            "Jumādá al-ūlá","Jumādá al-ākhirah","Rajab","Sha‘bān",
            "Ramaḍān","Shawwāl","Dhū al-Qa‘dah","Dhū al-Ḥijjah"};
    public static ProgressBar pb;
    LocationManager locationManager;
    Criteria criteria ;
    boolean GpsStatus = false ;
    String Holder;
    Location location;
    public  static final int RequestPermissionCode  = 1 ;

    ConstraintLayout detailcalenderopen;
    Double latti = 24.8607,longi = 67.0011;
    private AdView mAdView;
    SharedPreferences sharedPreferencesmethod;
    String methodvalue ;
    String select_date ="";
    Call<Example> call;
    NamazDetailsApi api;
    private Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_daily_namaz_detail);


        detailcalenderopen = findViewById(R.id.detailcalenderopen);

        detailcalenderopen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),CalendarDetailsActivity.class);
                startActivity(intent);
                finish();
            }
        });




        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Salah Timings");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("TrackSalahUser")
                .setAction("User id")
                .setLabel(DashboardActivity.Userid)
                .setValue(1)
                .build());
        sharedPreferencesmethod = getSharedPreferences(SettingsActivity.methodvalue,Context.MODE_PRIVATE);
        methodvalue = sharedPreferencesmethod.getString("methodval","2");
        mAdView = (AdView) findViewById(R.id.adView);
        MobileAds.initialize(this, "ca-app-pub-2825367626708661~7620129751");
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdView.setVisibility(View.VISIBLE);
            }
        });
        getdate = (TextView) findViewById(R.id.getdate);
        getday= (TextView) findViewById(R.id.getday);
        gethijri= (TextView) findViewById(R.id.gethijridate);
        gettimezone=findViewById(R.id.timezone);
        getfajr= (TextView) findViewById(R.id.getfajr);
//        getsunrise= (TextView) findViewById(R.id.getsrise);
        getduhr= (TextView) findViewById(R.id.getzohar);
        getasr= (TextView) findViewById(R.id.getasr);
//        getsunset= (TextView) findViewById(R.id.getsset);
        getmaghrib= (TextView) findViewById(R.id.getmaghrib);
        getisha= (TextView) findViewById(R.id.getisha);
//        getimsak= (TextView) findViewById(R.id.getimsak);
//        getfiqqah = findViewById(R.id.getfiqqah);
//        getislamicmonth = findViewById(R.id.getislamicmonth);
        pb = (ProgressBar) findViewById(R.id.progressBar3);
        pb.setVisibility(View.GONE);
       // DailyNamazDetailActivity.gettimezone.setText("aaa");
        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            pb.setVisibility(View.VISIBLE);
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

        }

        Bundle bundle1 = getIntent().getExtras();
        try {
            select_date = bundle1.getString("ClickedDateDetails","a").trim();

        }
        catch (Exception e)
        {
            select_date = "a";
        }
        if (select_date.equals("a"))
        {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            criteria = new Criteria();

            Holder = locationManager.getBestProvider(criteria, false);
            CheckGpsStatus();
            if(GpsStatus == true) {
                if (Holder != null) {
                    if (ActivityCompat.checkSelfPermission(
                            DailyNamazDetailActivity.this,
                            Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                            &&
                            ActivityCompat.checkSelfPermission(DailyNamazDetailActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                                    != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    location = locationManager.getLastKnownLocation(Holder);
                    locationManager.requestLocationUpdates(Holder, 99999, 7, DailyNamazDetailActivity.this);
                }
            }else {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                        .setTitle("Please Enable GPS First")
                        .setCancelable(false);
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(DailyNamazDetailActivity.this, DashboardActivity.class);
                        startActivity(intent);
                    }
                });
                builder.setPositiveButton("Enable", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                });
                final AlertDialog alert = builder.create();
                alert.show();
                // take the user to the phone gps settings and then start the asyncronous logic.

                Toast.makeText(DailyNamazDetailActivity.this, "Please Enable GPS First", Toast.LENGTH_LONG).show();

            }
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(NamazDetailsApi.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                    .build();
            api = retrofit.create(NamazDetailsApi.class);
            getLocation();

        }
        else
        {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(NamazDetailsApi.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                    .build();
            api = retrofit.create(NamazDetailsApi.class);
            getdate.setText(select_date);
            DateFormat format1 = new SimpleDateFormat("d-MMM-yyyy",Locale.US);
            Date date = null;
            try {
                date = format1.parse(select_date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            DateFormat dayname = new SimpleDateFormat("EEEE");
            DateFormat monthnumber = new SimpleDateFormat("MM");
            String mnum = monthnumber.format(date);
            DateFormat daynum = new SimpleDateFormat("d");
            String dnum = daynum.format(date);
            DateFormat yearnum = new SimpleDateFormat("yyyy");
            String ynum = yearnum.format(date);
            Toast.makeText(this, dnum+"-"+mnum+"-"+ynum, Toast.LENGTH_SHORT).show();
            String datetoday = dnum+"-"+mnum+"-"+ynum;
            String daytoday = dayname.format(date);
            getday.setText(daytoday);
            Retrofit retrofit2 = new Retrofit.Builder()
                    .baseUrl(NamazDetailsApi.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                    .build();
            FetchEmptyCalenderData emptyapi = retrofit2.create(FetchEmptyCalenderData.class);

            latti = SplashActivity.latti;
            longi = SplashActivity.longi;
          //  long unixTime = Integer.parseInt(select_date) / 1000L;
         //   call = api.getHeroes( "http://api.aladhan.com/v1/calendar?latitude="+latti+"&longitude="+longi+"&method="+methodvalue+"&month="+mnum+"&year="+2018);
            call = api.getHeroes( "http://api.aladhan.com/v1/timings/"+datetoday+"?latitude="+latti+"&longitude="+longi+"&method="+methodvalue);
            call.enqueue(new Callback<Example>() {
                @Override
                public void onResponse(Call<Example> call, Response<Example> response) {
                    getdatee = response.body().getData().getDate().getReadable();
                    getdayy = response.body().getData().getDate().getGregorian().getWeekday().getEn();
                    gethijrii = response.body().getData().getDate().getHijri().getDate();
                    gettimezonee = response.body().getData().getMeta().getTimezone();
                    getfajrr = response.body().getData().getTimings().getFajr();
                    getduhrr = response.body().getData().getTimings().getDhuhr();
                    getasrr=response.body().getData().getTimings().getAsr();
                    getmaghribb = response.body().getData().getTimings().getMaghrib();
                    getishaa = response.body().getData().getTimings().getIsha();
                    getsunrisee = response.body().getData().getTimings().getSunrise();
                    getsunsett = response.body().getData().getTimings().getSunset();
                    getfiqqahname =response.body().getData().getMeta().getMethod().getName();
                    getimsakk = response.body().getData().getTimings().getImsak();
                    islamicmonth = response.body().getData().getDate().getHijri().getMonth().getEn();
                    gettimezonee = response.body().getData().getMeta().getTimezone();
                    String[] breakit = gethijrii.split("-");
                    String to_send = breakit[0]+"-"+islamic_months[Integer.parseInt(breakit[1])]+"-"+breakit[2];
                    DailyNamazDetailActivity.getdate.setText(getdatee);
                    DailyNamazDetailActivity.getday.setText(getdayy);
                    DailyNamazDetailActivity.gethijri.setText(to_send);
                    DailyNamazDetailActivity.getfajr.setText(getfajrr+" - "+getsunrisee);
                    //DailyNamazDetailActivity.getsunrise.setText(getsunrisee);
                    DailyNamazDetailActivity.getduhr.setText(getduhrr+" - "+getasrr);
                    DailyNamazDetailActivity.getasr.setText(getasrr+" - "+getsunsett);
                    DailyNamazDetailActivity.gettimezone.setText(gettimezonee);
                    //Log.d("message",gettimezonee);

                   // gettimezone.setText("aaa");
                    DailyNamazDetailActivity.getmaghrib.setText(getmaghribb+" - "+getishaa);
                    DailyNamazDetailActivity.getisha.setText(getishaa+" - "+getimsakk);
                    //DailyNamazDetailActivity.getimsak.setText(getimsakk);
                    //getislamicmonth.setText(islamicmonth);
                    pb.setVisibility(View.GONE);
                   // Toast.makeText(getApplicationContext(), "Timexone"+gettimezonee, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<Example> call, Throwable t) {

                }
            });

        }



    }
    public void CheckGpsStatus(){

        locationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);

        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

    }

    public void EnableRuntimePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(DailyNamazDetailActivity.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION))
        {

            Toast.makeText(DailyNamazDetailActivity.this,"ACCESS_FINE_LOCATION permission allows us to Access GPS in app", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(DailyNamazDetailActivity.this,new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION}, RequestPermissionCode);

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.normal_menu, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.refresh1: {
                refresh();
                break;
            }
            case android.R.id.home:{
                onBackPressed();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }


    public void refresh()
    {

        getLocation();

    }
    void getLocation() {
        pb.setVisibility(View.VISIBLE);
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 9999999, 50, this);
        }
        catch(SecurityException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onLocationChanged(Location location) {
        latti = location.getLatitude();
        longi = location.getLongitude();





        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            latti = location.getLatitude();
            longi = location.getLongitude();
            long unixTime = System.currentTimeMillis() / 1000L;
            call = api.getHeroes("http://api.aladhan.com/v1/timings/"+unixTime+"?latitude="+latti+"&longitude="+longi+"&method="+methodvalue);
            String url = "http://api.aladhan.com/v1/timings/"+unixTime+"?latitude="+latti+"&longitude="+longi+"&method="+methodvalue;
          //  Toast.makeText(this, "+ "+url, Toast.LENGTH_SHORT).show();


            call.enqueue(new Callback<Example>() {
                @Override
                public void onResponse(Call<Example> call, Response<Example> response) {

                    getdatee = response.body().getData().getDate().getReadable();
                    getdayy = response.body().getData().getDate().getGregorian().getWeekday().getEn();
                    gethijrii = response.body().getData().getDate().getHijri().getDate();
                    gettimezonee = response.body().getData().getMeta().getTimezone();
                    getfajrr = response.body().getData().getTimings().getFajr();
                    getduhrr = response.body().getData().getTimings().getDhuhr();
                    getasrr=response.body().getData().getTimings().getAsr();
                    getmaghribb = response.body().getData().getTimings().getMaghrib();
                    getishaa = response.body().getData().getTimings().getIsha();
                    getsunrisee = response.body().getData().getTimings().getSunrise();
                    getsunsett = response.body().getData().getTimings().getSunset();
                    getfiqqahname =response.body().getData().getMeta().getMethod().getName();
                    getimsakk = response.body().getData().getTimings().getImsak();
                    islamicmonth = response.body().getData().getDate().getHijri().getMonth().getEn();
                    String[] breakit = gethijrii.split("-");
                    String to_send = breakit[0]+"-"+islamic_months[Integer.parseInt(breakit[1])]+"-"+breakit[2];
                    DailyNamazDetailActivity.getdate.setText(getdatee);
                    DailyNamazDetailActivity.getday.setText(getdayy);
                    DailyNamazDetailActivity.gethijri.setText(to_send);
                    DailyNamazDetailActivity.gettimezone.setText(gettimezonee);
                    //DailyNamazDetailActivity.getfiqqah.setText(getfiqqahname);
                    DailyNamazDetailActivity.getfajr.setText(getfajrr+" - "+getsunrisee);
                    //DailyNamazDetailActivity.getsunrise.setText(getsunrisee);
                    DailyNamazDetailActivity.getduhr.setText(getduhrr+" - "+getasrr);
                    DailyNamazDetailActivity.getasr.setText(getasrr+" - "+getsunsett);
                    //DailyNamazDetailActivity.getsunset.setText(getsunsett);
                    DailyNamazDetailActivity.getmaghrib.setText(getmaghribb+" - "+getishaa);
                    DailyNamazDetailActivity.getisha.setText(getishaa+" - "+getimsakk);
                    //DailyNamazDetailActivity.getimsak.setText(getimsakk);
                    //getislamicmonth.setText(islamicmonth);
                    pb.setVisibility(View.GONE);




                }

                @Override
                public void onFailure(Call<Example> call, Throwable t) {
                    pb.setVisibility(View.GONE);
                   Toast.makeText(getApplicationContext(), "Error Loading\nKindly Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            });
        }catch(Exception e)
        {

        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
      //  Toast.makeText(getApplicationContext(), "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();


    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(DailyNamazDetailActivity.this,"Permission Granted, Now your application can access GPS.", Toast.LENGTH_LONG).show();

                } else {

                    Toast.makeText(DailyNamazDetailActivity.this,"Permission Canceled, Now your application cannot access GPS.", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }
}
