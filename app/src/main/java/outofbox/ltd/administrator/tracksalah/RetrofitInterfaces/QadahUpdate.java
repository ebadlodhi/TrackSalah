package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface QadahUpdate {
    @FormUrlEncoded
    @POST("QadahUpdateRecord.php")
    Call<String> getDetails(@Field("id") String id, @Field("totalQadahrem") String totalQadahrem, @Field("fajardata") String fajardata,
                            @Field("zohardata") String zohardata, @Field("asardata") String asardata, @Field("Maghribdata") String Maghribdata, @Field("ishadata") String ishadata
            , @Field("fcount") String fcount
            , @Field("zcount") String zcount
            , @Field("acount") String acount
            , @Field("mcount") String mcount
            , @Field("icount") String icount

    );


}
