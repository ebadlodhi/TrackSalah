package outofbox.ltd.administrator.tracksalah;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.MendateApi;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.deleteexemptedlistapi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class MensDetailActivity extends AppCompatActivity {
    public static final String ROOT_URL = "http://185.123.99.207/~android215/TrackSalah/";
    List<Exapmle> list = new ArrayList<Exapmle>();
    String[] startdatee,enddatee,startdayname,enddayname,formatedstartdatee,formatedenddatee;
    ListView listView;
    String Userid = "";
    SharedPreferences sharedPreferences;
    private Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mens_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    listView = findViewById(R.id.menslistview);

        sharedPreferences = getSharedPreferences(LoginActivity.loginpref, Context.MODE_PRIVATE);
        Userid = sharedPreferences.getString("preuid", "");


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        MendateApi subscribedplanApi = retrofit.create(MendateApi.class);
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Mensturation Details");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("TrackSalahUser")
                .setAction("User id")
                .setLabel(DashboardActivity.Userid)
                .setValue(1)
                .build());
        Date c = Calendar.getInstance().getTime();
        final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        final SimpleDateFormat dfformated = new SimpleDateFormat("dd MMM yyyy");
        String formattedDate = df.format(c);

        Call<List<Exapmle>> call = subscribedplanApi.getDetails(Userid,formattedDate);
        call.enqueue(new Callback<List<Exapmle>>() {
            @Override
            public void onResponse(Call<List<Exapmle>> call, Response<List<Exapmle>> response) {
                list = response.body();
                startdatee = new String[list.size()];
                enddatee = new String[list.size()];
                startdayname= new String[list.size()];
                enddayname= new String[list.size()];
                formatedstartdatee= new String[list.size()];
                formatedenddatee = new String[list.size()];

                for(int i = 0; i<list.size();i++)
                {

                    startdatee[i]  =list.get(i).getStartDate();
                    enddatee[i] = list.get(i).getEndDate();
                    startdayname[i] = list.get(i).getStartDaynamaz();
                    enddayname[i] = list.get(i).getEndDaynamaz();
                    Date comingstartdate = null;
                    Date comingenddate = null;

                    try {
                         comingstartdate = df.parse(startdatee[i]);
                        comingenddate = df.parse(enddatee[i]);
                        formatedstartdatee[i] = dfformated.format(comingstartdate);
                        formatedenddatee[i] = dfformated.format(comingenddate);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

//                    Toast.makeText(GoalHistoryActivity.this, ""+Dates[i], Toast.LENGTH_SHORT).show();

                }
                AppAdapter appAdapter = new AppAdapter(getApplicationContext(),list);

                listView.setAdapter(appAdapter);

            }


            @Override
            public void onFailure(Call<List<Exapmle>> call, Throwable t) {

            }
        });


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }



    class AppAdapter extends ArrayAdapter<Exapmle>
    {
        private List<Exapmle> items;

        public AppAdapter(@NonNull Context context, List<Exapmle> itemss) {
            super( context,R.layout.custommenslayout,itemss);

            this.items = itemss;
        }


        @Override
        public int getCount() {
            return items.size();
        }


        @NonNull
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder vh;
            View v = convertView;

            if(v == null) {
                vh = new ViewHolder();
                LayoutInflater li = LayoutInflater.from(getApplicationContext());
                v = li.inflate(R.layout.custommenslayout,null);
//            Example app = items.get(position);
//            Log.d("position",Integer.toString(position));
//            if(app != null) {

                vh.mstartdate = v.findViewById(R.id.mstartdate);
                vh.menddate = v.findViewById(R.id.menddate);
                vh.medit = v.findViewById(R.id.medit);
                vh.mdelete = v.findViewById(R.id.imageButton6);
                vh.menslist = v.findViewById(R.id.mensconstraint);

                v.setTag(vh);
            }
            else{
                vh = (ViewHolder) convertView.getTag();
            }
//              TextView comentt = v.findViewById(R.id.textView10);
            vh.mstartdate.setText(formatedstartdatee[position]);
            vh.menddate.setText(formatedenddatee[position]);
            vh.medit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getApplicationContext(),MonthdateeditActivity.class);
                    intent.putExtra("monthstartdate",items.get(position).getStartDate());
                    intent.putExtra("monthenddate",items.get(position).getEndDate());
                    intent.putExtra("monthstartdateformated",formatedstartdatee[position]);
                    intent.putExtra("monthenddateformated",formatedenddatee[position]);
                    intent.putExtra("monthstartdaynamaz",items.get(position).getStartDaynamaz());
                    intent.putExtra("monthenddaynamaz",items.get(position).getEndDaynamaz());
                    startActivity(intent);
                    finish();
                }
            });
            vh.mdelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(ROOT_URL) //Setting the Root URL
                            .addConverterFactory(ScalarsConverterFactory.create())
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    //
                   Toast.makeText(MensDetailActivity.this, ""+Userid+"\n"+startdatee[position]+"\n"+enddatee[position], Toast.LENGTH_SHORT).show();

                    deleteexemptedlistapi api = retrofit.create(deleteexemptedlistapi.class);
                    Call<String> call = api.getDetails(Userid,startdatee[position],enddatee[position]);
                    call.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            Toast.makeText(MensDetailActivity.this, "deleted", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(),MensDetailActivity.class);
                            startActivity(intent);
                            finish();
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {

                        }
                    });
                    Toast.makeText(MensDetailActivity.this, "deleted", Toast.LENGTH_SHORT).show();
                }
            });





            // vh.imageView.setImageResource(images[position]);

            //                comentt.setText(comment[position]);



            return v;
        }



    }
    static class ViewHolder
    {
        // ImageView imageView;
        TextView mstartdate;
        ImageButton medit,mdelete;
        TextView menddate;
        ConstraintLayout menslist;


    }

}
