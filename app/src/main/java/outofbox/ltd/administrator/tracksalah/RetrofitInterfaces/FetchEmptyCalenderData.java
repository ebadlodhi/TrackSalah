package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;

import java.util.List;

import outofbox.ltd.administrator.tracksalah.POJO2.Example;
import outofbox.ltd.administrator.tracksalah.POJOcalenderData.CalenderDetailsFetch;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface FetchEmptyCalenderData {
    long unixTime = System.currentTimeMillis() / 1000L;

    public static final String BASE_URL = "http://api.aladhan.com/v1/";
    @GET()
    Call<Example> getHeroes(@Url String url);
}
