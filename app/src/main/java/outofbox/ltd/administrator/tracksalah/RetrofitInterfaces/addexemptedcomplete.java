package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface addexemptedcomplete {
    @FormUrlEncoded
    @POST("addexemptedcomleteadd.php")
    Call<String> getDetails(@Field("userid") String userid,  @Field("enaddate") String enaddate
            , @Field("enddatenamazname") String enddatenamazname);
}
