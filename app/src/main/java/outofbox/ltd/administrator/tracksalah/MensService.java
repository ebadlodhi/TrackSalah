package outofbox.ltd.administrator.tracksalah;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.CursorAdapter;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import outofbox.ltd.administrator.tracksalah.Databasesqlite.MensDatabaseHelper;

public class MensService extends Service {
    private final int INTERVAL = 3 * 1000;
    private Timer timer = new Timer();
    public MensDatabaseHelper db;
    SharedPreferences sharedPreferences;
    public  String User_id = "";
    int uid ,status;
    String tdate,ttime;
    public MensService() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        db = new MensDatabaseHelper(getApplicationContext());
        sharedPreferences = getSharedPreferences(LoginActivity.loginpref, Context.MODE_PRIVATE);
        User_id = sharedPreferences.getString("preuid", "");
        Date current  = Calendar.getInstance().getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("HH:mm", Locale.US);
        String currentdate = simpleDateFormat.format(current);
        String currenttime = simpleDateFormat1.format(current);
       // Toast.makeText(this, "cdate = "+currentdate+"\nctime ="+currenttime+"\nid ="+User_id, Toast.LENGTH_SHORT).show();
//       db.addRecord(Integer.parseInt(User_id), currentdate, currenttime, 1);
//    Cursor c = db.getRecord(User_id);
//    if(c != null) {
//        if (c.moveToFirst()) {
//            do {
//                MensResponse mensResponse = new MensResponse(
//                        c.getInt(c.getColumnIndex(MensDatabaseHelper.COLUMN_User_id)),
//                        c.getInt(c.getColumnIndex(MensDatabaseHelper.COLUMN_status)),
//                        c.getString(c.getColumnIndex(MensDatabaseHelper.COLUMN_today_date)),
//                        c.getString(c.getColumnIndex(MensDatabaseHelper.COLUMN_today_time))
//                );
//                try {
//                    uid = mensResponse.getUser_id();
//                    status = mensResponse.getStatus();
//                    tdate = mensResponse.getToday_date();
//                    ttime = mensResponse.getToday_time();
//                } catch (NullPointerException e) {
//                    uid = 0;
//                    status = -1;
//                    tdate = null;
//                    ttime = null;
//                }
//                if (uid == 0 && status == -1 && tdate == null && ttime == null) {
//
//                    boolean res = db.addRecord(Integer.parseInt(User_id), currentdate, currenttime, 1);
//                    if (res == true) {
//                        Toast.makeText(this, "added", Toast.LENGTH_SHORT).show();
//                    } else {
//                        Toast.makeText(this, "notadded", Toast.LENGTH_SHORT).show();
//                    }
//                } else {
//                    Toast.makeText(this, "record found", Toast.LENGTH_SHORT).show();
//                }
//            }
//            while (c.moveToNext());
//        }
//    }
//    else
//    {
//        boolean res = db.addRecord(Integer.parseInt(User_id), currentdate, currenttime, 1);
//        if (res == true) {
//            Toast.makeText(this, "added", Toast.LENGTH_SHORT).show();
//        } else {
//            Toast.makeText(this, "notadded", Toast.LENGTH_SHORT).show();
//        }
//    }

//
        timer.scheduleAtFixedRate(new TimerTask() {

            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void run() {
                Log.d("Chal rahi ahi","Runnniiinnnngggg");

            }
        }, 0, INTERVAL);


        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "band hogae", Toast.LENGTH_SHORT).show();
        timer.cancel();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
