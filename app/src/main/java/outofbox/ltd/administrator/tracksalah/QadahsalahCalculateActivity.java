package outofbox.ltd.administrator.tracksalah;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.QadahsignupcalculateApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static outofbox.ltd.administrator.tracksalah.LoginActivity.loginpref;

public class QadahsalahCalculateActivity extends AppCompatActivity {
ConstraintLayout namazstartConstraint;
TextView namazStartDate;
    Calendar calendar;
    TextView namazfarzdate;

    TimePickerDialog timePickerDialog;
    int currentHour;
    int currentMinute;
    public static SharedPreferences sharedpreferences;
    private DatePicker dpResult;
    Date Currenttime;
    String currentDateandTime;
    private int myear,mfarzyear;
    private int mmonth,mfarzmonth;
    private int mday,mfarzday;
    String userid = "";
    String fajar = "0",duhur= "0",asar= "0",maghrib= "0",isha= "0";
    public static final String ROOT_URL = "http://185.123.99.207/~android215/TrackSalah/";
    CheckBox checkfajar  ,checkduhur,checkasar,checkmaghrib,checkisha;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qadah_calculate_activity);
        namazstartConstraint=findViewById(R.id.namazstartdate);
        this.setTitle("Qadah Calculate");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        dpResult = (DatePicker) findViewById(R.id.dpResult24);
        sharedpreferences = getSharedPreferences(loginpref, Context.MODE_PRIVATE);
        namazStartDate=findViewById(R.id.startDatetxt);
        namazfarzdate = findViewById(R.id.editText_date);
        checkfajar = findViewById(R.id.checkFajr);
        checkduhur = findViewById(R.id.checkZohar);
        checkasar = findViewById(R.id.checkasar);
        checkmaghrib = findViewById(R.id.checkMaghrib);
        checkisha = findViewById(R.id.checkIsha);


        String nfrz = "";
String nfrzshow = "";
nfrz = sharedpreferences.getString("preuserfarzdate","2018-11-11");
userid = sharedpreferences.getString("preuid","");


        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        SimpleDateFormat simpleDateFormatcoming = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        Date coming  = null;
        try {
            coming = simpleDateFormatcoming.parse(nfrz);
            nfrzshow = simpleDateFormat.format(coming);
            namazfarzdate.setText(""+nfrzshow);
        } catch (ParseException e) {
            Log.e("error",""+e)  ;
        }




        setCurrentDateOnView();
        namazstartConstraint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(111);
            }
        });
    }


    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 111: {
                DatePickerDialog _datefarz = new DatePickerDialog(this, datePickerListenerfarz, myear, mmonth,
                        mday) {
                    @Override
                    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                        if (year > myear) {
//                            Toast.makeText(getApplicationContext(), "Year can't be greater then current Year", Toast.LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }
//                        if (monthOfYear > mmonth && year == myear) {
//                            Toast.makeText(getApplicationContext(), "Month can't be greater then current Month", Toast.LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }
//                        if (dayOfMonth > mday && year == myear && monthOfYear == mmonth) {
//                            Toast.makeText(getApplicationContext(), "Date can't be greater then current date", Toast.LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }
                    }
                };
                return _datefarz;
            }

        }


        return null;
    }
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            myear = selectedYear;
            mmonth = selectedMonth;
            mday = selectedDay;
            String sMonth = "";
            // set selected date into textview
            if (mmonth == 0)
                sMonth = "Jan";
            else if (mmonth == 1)
                sMonth = "Feb";
            else if (mmonth == 2)
                sMonth = "Mar";
            else if (mmonth == 3)
                sMonth = "Apr";
            else if (mmonth == 4)
                sMonth = "May";
            else if (mmonth == 5)
                sMonth = "Jun";
            else if (mmonth == 6)
                sMonth = "Jul";
            else if (mmonth == 7)
                sMonth = "Aug";
            else if (mmonth == 8)
                sMonth = "Sep";
            else if (mmonth == 9)
                sMonth = "Oct";
            else if (mmonth == 10)
                sMonth = "Nov";
            else if (mmonth == 11)
                sMonth = "Dec";
            namazStartDate.setText(new StringBuilder()
                    .append(mday).append(" ") .append(sMonth).append(" ").append(myear).append(" "));
            // set selected date into datepicker also
            dpResult.init(myear, mmonth, mday, null);

//            try {
//                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy");
//                SimpleDateFormat simpleDateFormattenyear = new SimpleDateFormat("yyyy-MM-dd");
//                mydate = simpleDateFormat.parse(tvDisplayDate.getText().toString());
//                Calendar c = Calendar.getInstance();
//                c.setTime(mydate);
//                c.add(Calendar.YEAR, 14);
//                newdate = c.getTime();
//                longdate = newdate.getTime();
//                tenyear = simpleDateFormat.format(newdate);
//                tenyearServer = simpleDateFormattenyear.format(newdate);
//                dateofb.setText(tenyear);
//                mfarzyear = Calendar.YEAR;
//                mfarzmonth = Calendar.MONTH;
//                mfarzday = Calendar.DAY_OF_MONTH;
//
//
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }


        }
    };
    public void setCurrentDateOnView() {



        final Calendar c = Calendar.getInstance();
        Currenttime = c.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        currentDateandTime = sdf.format(new Date());
        myear = c.get(Calendar.YEAR);
        mmonth = c.get(Calendar.MONTH);
        mday = c.get(Calendar.DAY_OF_MONTH);
        String sMonth = "";
        // set selected date into textview
        if (mmonth == 0)
            sMonth = "Jan";
        else if (mmonth == 1)
            sMonth = "Feb";
        else if (mmonth == 2)
            sMonth = "Mar";
        else if (mmonth == 3)
            sMonth = "Apr";
        else if (mmonth == 4)
            sMonth = "May";
        else if (mmonth == 5)
            sMonth = "Jun";
        else if (mmonth == 6)
            sMonth = "Jul";
        else if (mmonth == 7)
            sMonth = "Aug";
        else if (mmonth == 8)
            sMonth = "Sep";
        else if (mmonth == 9)
            sMonth = "Oct";
        else if (mmonth == 10)
            sMonth = "Nov";
        else if (mmonth == 11)
            sMonth = "Dec";
        // set current date into textview
        namazStartDate.setText(new StringBuilder()
                // Month is 0 based, just add 1
                .append(mday).append(" ") .append(sMonth).append(" ").append(myear)
        );

        // set current date into datepicker
        dpResult.init(myear, mmonth, mday, null);

    }

    private DatePickerDialog.OnDateSetListener datePickerListenerfarz = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            myear = selectedYear;
            mmonth = selectedMonth;
            mday = selectedDay;
            String sMonth = "";
            // set selected date into textview
            if (mmonth == 0)
                sMonth = "Jan";
            else if (mmonth == 1)
                sMonth = "Feb";
            else if (mmonth == 2)
                sMonth = "Mar";
            else if (mmonth == 3)
                sMonth = "Apr";
            else if (mmonth == 4)
                sMonth = "May";
            else if (mmonth == 5)
                sMonth = "Jun";
            else if (mmonth == 6)
                sMonth = "Jul";
            else if (mmonth == 7)
                sMonth = "Aug";
            else if (mmonth == 8)
                sMonth = "Sep";
            else if (mmonth == 9)
                sMonth = "Oct";
            else if (mmonth == 10)
                sMonth = "Nov";
            else if (mmonth == 11)
                sMonth = "Dec";
            namazStartDate.setText(new StringBuilder()
                    .append(mday).append(" ") .append(sMonth).append(" ").append(myear).append(" "));
            // set selected date into datepicker also
            dpResult.init(myear, mmonth, mday, null);

//            try {
//                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy");
//                SimpleDateFormat simpleDateFormattenyear = new SimpleDateFormat("yyyy-MM-dd");
//                mydate = simpleDateFormat.parse(dateofb.getText().toString());
//                Calendar c = Calendar.getInstance();
//                c.setTime(mydate);
//                c.add(Calendar.YEAR, 14);
//                newdate = c.getTime();
//                longdate = newdate.getTime();
//                tenyear = simpleDateFormat.format(newdate);
//                tenyearServer = simpleDateFormattenyear.format(newdate);
////                namazfarzdate.setText(tenyear);
//                mfarzyear = Calendar.YEAR;
//                mfarzmonth = Calendar.MONTH;
//                mfarzday = Calendar.DAY_OF_MONTH;

//
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//


        }
    };
 public  void qadahcal(View view)
 {
     if(!checkfajar.isChecked())
     {
         fajar = "1";
     }

     if(!checkduhur.isChecked())
     {
         duhur = "1";
     }
     if(!checkasar.isChecked())
     {
         asar = "1";
     }
     if(!checkmaghrib.isChecked())
     {
         maghrib = "1";
     }
     if(!checkisha.isChecked())
     {
         isha = "1";
     }
            String cycl= sharedpreferences.getString("cycle","0");
            String noofdays = sharedpreferences.getString("noofday","0");
            String FarzDate  = namazfarzdate.getText().toString();
            String Namazstartdate = namazStartDate.getText().toString();

     SharedPreferences.Editor editor2 = sharedpreferences.edit();
     editor2.putString("preuserfarzdate",FarzDate);
     editor2.putString("preuserstartdate",Namazstartdate);
     editor2.apply();
     editor2.commit();


     Retrofit retrofit = new Retrofit.Builder()
             .baseUrl(ROOT_URL) //Setting the Root URL
             .addConverterFactory(ScalarsConverterFactory.create())
             .addConverterFactory(GsonConverterFactory.create())
             .build();
     QadahsignupcalculateApi api = retrofit.create(QadahsignupcalculateApi.class);
     Call<String> call  = api.getDetails(FarzDate,Namazstartdate,noofdays,cycl,userid,fajar,duhur,asar,maghrib,isha);
     call.enqueue(new Callback<String>() {
         @Override
         public void onResponse(Call<String> call, Response<String> response) {
             if(response.body().contains("Qadahcalculated"))
             {
                 Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                 startActivity(intent);
                 finish();
             }
             else
             {
                 Toast.makeText(QadahsalahCalculateActivity.this, "Incorrect Data", Toast.LENGTH_SHORT).show();
             }
         }

         @Override
         public void onFailure(Call<String> call, Throwable t) {

         }
     });

    // Toast.makeText(this, "userid = "+userid+"\nFarzDate "+FarzDate+"\nstart date"+Namazstartdate+"\ncycle "+cycl+"\nno of days "+noofdays, Toast.LENGTH_SHORT).show();
 }

 public void skip(View view)
 {

     String cycl= sharedpreferences.getString("cycle","0");
     String noofdays = sharedpreferences.getString("noofday","0");
     String FarzDate  = namazfarzdate.getText().toString();

SimpleDateFormat simpleDateFormat = new SimpleDateFormat("DD MMM yyy",Locale.US);
     Date currentTime = Calendar.getInstance().getTime();
     String sendtime = simpleDateFormat.format(currentTime);

     String Namazstartdate = sendtime;

     Retrofit retrofit = new Retrofit.Builder()
             .baseUrl(ROOT_URL) //Setting the Root URL
             .addConverterFactory(ScalarsConverterFactory.create())
             .addConverterFactory(GsonConverterFactory.create())
             .build();
     QadahsignupcalculateApi api = retrofit.create(QadahsignupcalculateApi.class);
     Call<String> call  = api.getDetails(FarzDate,Namazstartdate,noofdays,cycl,userid,fajar,duhur,asar,maghrib,isha);
     call.enqueue(new Callback<String>() {
         @Override
         public void onResponse(Call<String> call, Response<String> response) {
             if(response.body().contains("Qadahcalculated"))
             {
                 Intent intent = new Intent(getApplicationContext(),DashboardActivity.class);
                 startActivity(intent);
                 finish();
             }
             else
             {
                 Toast.makeText(QadahsalahCalculateActivity.this, "Incorrect Data", Toast.LENGTH_SHORT).show();
             }
         }

         @Override
         public void onFailure(Call<String> call, Throwable t) {

         }
     });
 }
}
