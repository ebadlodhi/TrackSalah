package outofbox.ltd.administrator.tracksalah;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class Dateofbirth1 extends AsyncTask<String,Void,String>
{
    public static String res ="",getUserDOB="" ;


    @Override
    protected String doInBackground(String... params) {

        String Useridd = params[0];
        try {

            URL url = new URL("http://185.123.99.207/~android215/TrackSalah/selectDOBforqadahcalculate.php");
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            OutputStream outputStream = httpURLConnection.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            String postdata = URLEncoder.encode("id", "UTF-8") + "=" + URLEncoder.encode(Useridd, "UTF-8");
            bufferedWriter.write(postdata);
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStream.close();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String line = "";

            while (line != null) {

                line = bufferedReader.readLine();
                res = res + line;

            }
            JSONObject json = null;
            try {
                json = new JSONObject(res);
                JSONObject myobject = json.getJSONObject("UserDetails");
                JSONObject myobject2 =myobject.getJSONObject("0");

                getUserDOB = myobject2.getString("User_DOB");


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);


    }



}



