package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;

import outofbox.ltd.administrator.tracksalah.POJO3.Example;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

import outofbox.ltd.administrator.tracksalah.POJO3.*;

public interface LoginApi {
  @FormUrlEncoded
    @POST("logintest.php")
  Call<Example>   getDetails(@Field("name") String email, @Field("password") String password);
}
