package outofbox.ltd.administrator.tracksalah;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class SettingsActivity extends AppCompatActivity {
Switch switch1;
    SharedPreferences sharedPreferences2;
    private Tracker mTracker;
public static  String  switchstate = "";
    public static  String  methodvalue = "";
    public static  String  radiotext = "";
public static  int method =1;
RadioGroup methodradio;
RadioButton Shia,karachi,namerica,mwl,ummalqura,egypt,tehranuni,gulfregion,kuwait,qatar,singapor,france,turkey;
    private AdView mAdView;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        switch1 = findViewById(R.id.switch1);
        mAdView = (AdView) findViewById(R.id.adView);
        MobileAds.initialize(this, "ca-app-pub-2825367626708661~7620129751");
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdView.setVisibility(View.VISIBLE);
            }
        });
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Settings");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("TrackSalahUser")
                .setAction("User id")
                .setLabel(DashboardActivity.Userid)
                .setValue(1)
                .build());
        
 sharedPreferences2 = getSharedPreferences(switchstate, Context.MODE_PRIVATE);
if(sharedPreferences2.contains("enable"))
{
    switch1.setChecked(true);
    Thread thread = new Thread()
    {
        public void run()
        {
            try {

                Intent intent = new Intent(getApplicationContext(),NotificationService.class);
                AnalyticsApplication application = (AnalyticsApplication) getApplication();
                mTracker = application.getDefaultTracker();
                mTracker.setScreenName("Notification");
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("TrackSalahUser")
                        .setAction("User id")
                        .setLabel(DashboardActivity.Userid)
                        .setValue(1)
                        .build());
                startService(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    };

    thread.start();
}
else if(sharedPreferences2.contains("notenable"))
{
    switch1.setChecked(false);
    Thread thread = new Thread()
{
    public void run()
    {
        try {

            Intent intent = new Intent(getApplicationContext(),NotificationService.class);

            stopService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

};

    thread.start();

}



        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if(switch1.isChecked())
                {
                    SharedPreferences.Editor editor =  sharedPreferences2.edit();

                    editor.putBoolean("enable",true);
                    editor.commit();
                    Toast.makeText(SettingsActivity.this, "You Will recieve all namaz time start notification.", Toast.LENGTH_SHORT).show();

                    Thread thread = new Thread()
                        {
                        public void run()
                        {
                            try {

                                Intent intent = new Intent(getApplicationContext(),NotificationService.class);

                                startService(intent);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                    };

                        thread.start();
                        switch1.setChecked(true);
                }
                else
                {

                    SharedPreferences.Editor editorebad = sharedPreferences2.edit();
                    editorebad.remove("enable");
                    editorebad.putBoolean("notenable",false);
                    editorebad.commit();
                    Toast.makeText(SettingsActivity.this, "No Notification will be shown ", Toast.LENGTH_SHORT).show();
                    Thread thread = new Thread()
                    {
                        public void run()
                        {
                            try {

                                Intent intent = new Intent(getApplicationContext(),NotificationService.class);

                                stopService(intent);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                    };

                    thread.start();
                    switch1.setChecked(false);
                }
            }
        });


    }

    public  void fiqahactivity(View view)
    {
        Intent intent = new Intent(getApplicationContext(),FiqqahActivity.class);
        startActivity(intent);


    }
    public  void userprofileedit(View view)
    {

        Intent intent = new Intent(getApplicationContext(),EditUserProfileActivity.class);
        startActivity(intent);

    }
    public void shareit(View view)
    {
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Share");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("TrackSalahUser")
                .setAction("User id")
                .setLabel(DashboardActivity.Userid)
                .setValue(1)
                .build());
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        String title = "Share Track Salah with Freinds";
//        String link = "https://www.dropbox.com/s/n5kvq7slfs71abo/app-debug.apk?dl=1";
        String link = "https://play.google.com/store/apps/details?id="+getApplicationContext().getPackageName();
        intent.putExtra(Intent.EXTRA_SUBJECT,title);
        intent.putExtra(Intent.EXTRA_TEXT,link);
        startActivity(Intent.createChooser(intent,"Share TrackSalah using")) ;

    }
    public void AboutUs(View view)
    {

        Intent intent = new Intent(getApplicationContext(),About.class);
        startActivity(intent);
    }
    public  void emailit(View view)
    {
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Email to Report");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("TrackSalahUser")
                .setAction("User id")
                .setLabel(DashboardActivity.Userid)
                .setValue(1)
                .build());
        String[] TO = {"salam@tracksalah.com"};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);

        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Report for Track Salah");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");
        emailIntent.setPackage("com.google.android.gm");

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));

        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(SettingsActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }

    }

}

