package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;


import outofbox.ltd.administrator.tracksalah.POJOnamazdetails.NamazDetailsfetch;

import java.util.List;

import outofbox.ltd.administrator.tracksalah.POJOnamazdetails.NamazDetailsfetch;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface FetchNamazDetailsApi {

    @FormUrlEncoded
    @POST("selectnamazdetails.php")
    Call<List<NamazDetailsfetch>> getDetails(@Field("User_id") String User_id, @Field("Todaydate") String Todaydate);

}
