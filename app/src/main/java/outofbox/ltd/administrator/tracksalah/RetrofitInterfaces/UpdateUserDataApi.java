package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface UpdateUserDataApi {
    @FormUrlEncoded
    @POST("SaveProfileUpdate.php")
    Call<String> getDetails(@Field("name") String name, @Field("password") String password, @Field("email") String email, @Field("gender") String gender, @Field("dob") String dob, @Field("id") String id);

}
