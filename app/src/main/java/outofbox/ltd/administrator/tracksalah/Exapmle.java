package outofbox.ltd.administrator.tracksalah;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Exapmle {
    @SerializedName("Start_Date")
    @Expose
    private String startDate;
    @SerializedName("Start_daynamaz")
    @Expose
    private String startDaynamaz;
    @SerializedName("End_Date")
    @Expose
    private String endDate;
    @SerializedName("End_daynamaz")
    @Expose
    private String endDaynamaz;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartDaynamaz() {
        return startDaynamaz;
    }

    public void setStartDaynamaz(String startDaynamaz) {
        this.startDaynamaz = startDaynamaz;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndDaynamaz() {
        return endDaynamaz;
    }

    public void setEndDaynamaz(String endDaynamaz) {
        this.endDaynamaz = endDaynamaz;
    }
}
