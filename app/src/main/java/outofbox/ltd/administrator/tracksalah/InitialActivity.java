package outofbox.ltd.administrator.tracksalah;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class InitialActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent().hasExtra("run_transparent")){
            finish();
        } else {
            startActivity(new Intent(this, SplashActivity.class));
            finish();
        }



    }
}
