package outofbox.ltd.administrator.tracksalah;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import outofbox.ltd.administrator.tracksalah.POJO3.Example;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.ForgotPasswordCodeRequestApi;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.ForgotPasswordConfirmCodeApi;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.LoginApi;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.LoginfrompendingApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class LoginActivity extends AppCompatActivity {

    //Hello//
    String res ="";
    String uname = "";
    String password = "";
    EditText username, userpassword;
    ProgressBar p1;
    public static String getuserid = "";
    public static String getusername = "";
    public static String getuseremail ;
    public static String getuserpassword = "";
    public static String getusergender = "";
    public static  String getuserdob="";
    public static  String getfarzdate = "";
    public  static  String getstartdate = "";
    public  static  String mdetail = "";
    public  static  String qadahdetail = "";
    Button Login;
    private Tracker mTracker;
    TextView signuptext, forgot;
    public static  String loginpref = "" ;
    public static  String SPUseremail = "";
    public static  String SPpassword = "";
    public static  String SPuserid = "";
    public static  String SPusername = "";
   public static SharedPreferences sharedpreferences;
    private AdView mAdView;
    public static final String ROOT_URL = "http://185.123.99.207/~android215/TrackSalah/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            getSupportActionBar().hide();
            AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Login");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("TrackSalahUser")
                .setAction("User id")
                .setLabel(DashboardActivity.Userid)
                .setValue(1)
                .build());
        setContentView(R.layout.activity_login);
        username = (TextInputEditText) findViewById(R.id.full_email);
        userpassword = (TextInputEditText) findViewById(R.id.passs);
        p1 = (ProgressBar) findViewById(R.id.progressBar);
        Login = (Button) findViewById(R.id.loginbutton);
        signuptext = (TextView) findViewById(R.id.signuptext);
        forgot = (TextView) findViewById(R.id.forgotPass);
        p1.setVisibility(View.GONE);

        Spannable word = new SpannableString(" Sign up");
        word.setSpan(new ForegroundColorSpan(Color.rgb(212,92,84)), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        signuptext.append(word);

        mAdView = (AdView) findViewById(R.id.adView);
        MobileAds.initialize(this, "ca-app-pub-2825367626708661~7620129751");
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdView.setVisibility(View.VISIBLE);
            }
        });

//        sharedpreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        sharedpreferences = getSharedPreferences(loginpref, Context.MODE_PRIVATE);
        String preusermdetail = sharedpreferences.getString("preusermdetails","");
        String preqadahdetail = sharedpreferences.getString("preuserqadahdetails","");
        if (sharedpreferences.contains("preuseremail")&& sharedpreferences.contains("prepassword") && preusermdetail.contains("0")&& preqadahdetail.contains("0")) {
            Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(intent);
            finish();


        }

        else if(sharedpreferences.contains("preuseremail")&& sharedpreferences.contains("prepassword") && preusermdetail.contains("-1")&& preqadahdetail.contains("-1"))
        {
            Intent intent = new Intent(getApplicationContext(), MenscalculateActivity.class);
            startActivity(intent);

        }
        else if(sharedpreferences.contains("preuseremail")&& sharedpreferences.contains("prepassword") && preusermdetail.contains("0")&& preqadahdetail.contains("-1"))
        {
            Intent intent = new Intent(getApplicationContext(), QadahsalahCalculateActivity.class);
            startActivity(intent);

        }

        else {
            if (internet_connection()) {
                Login.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        uname = username.getText().toString();
                        password = userpassword.getText().toString();
                        if(!Patterns.EMAIL_ADDRESS.matcher(uname).matches())
                        {
                            username.setError("Invalid email");
                            username.requestFocus();
                        }
                        if(uname.equals("") && password.equals("")) {

                            if(uname.isEmpty())
                            {
                                username.setError("No email found");
                                username.requestFocus();
                            }
                            if(password.isEmpty())
                            {
                                userpassword.setError("No password found");
                                userpassword.requestFocus();
                            }
                            //Toast.makeText(LoginActivity.this, "Invalid Username or password", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            p1.setVisibility(View.VISIBLE);
//                            BackgroundCLass backgroundCLass = new BackgroundCLass(LoginActivity.this);
//                            backgroundCLass.execute("login", uname, password);
                            insertUser();

                            // insertUser();
                            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        }
                    }
                });
            }
            else {
                Toast.makeText(getApplicationContext(), "No network.", Toast.LENGTH_SHORT).show();
            }

        }



        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uname = username.getText().toString();
                password = userpassword.getText().toString();
                if(!Patterns.EMAIL_ADDRESS.matcher(uname).matches())
                {
                    username.setError("Invalid email");
                    username.requestFocus();
                }
                if(uname.equals("") && password.equals("")) {

                    if(uname.isEmpty())
                    {
                        username.setError("No email found");
                        username.requestFocus();
                    }
                    if(password.isEmpty())
                    {
                        userpassword.setError("No password found");
                        userpassword.requestFocus();
                    }
                    //Toast.makeText(LoginActivity.this, "Invalid Username or password", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    p1.setVisibility(View.VISIBLE);
//                            BackgroundCLass backgroundCLass = new BackgroundCLass(LoginActivity.this);
//                            backgroundCLass.execute("login", uname, password);
                    insertUser();

                    // insertUser();
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }
            }
        });
    }


    public void forgotPassPopUp(View view)
    {
        showDialog();
    }
    public void showDialog()
    {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_forgot_password_layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setTitle("Please confirm your email");
        dialogBuilder.setMessage("You will receive a code via the email you provide.");
        dialogBuilder.setCancelable(false);
        dialogBuilder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                final android.support.design.widget.TextInputEditText FE = (TextInputEditText) dialogView.findViewById(R.id.email_field);
                String abc = FE.getText().toString().trim();
                if(abc.isEmpty())
                {
                    showDialog();
                    Toast.makeText(LoginActivity.this, "No email found", Toast.LENGTH_SHORT).show();
                }
                else if(!Patterns.EMAIL_ADDRESS.matcher(abc).matches())
                {
                    showDialog();
                    Toast.makeText(LoginActivity.this, "Invalid Email", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    p1.setVisibility(View.VISIBLE);
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    sendCode(abc);
                }
            }
        });
        dialogBuilder.setNegativeButton("Go back", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }
    public void sendCode(final String incoming_email)
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ForgotPasswordCodeRequestApi api =retrofit.create(ForgotPasswordCodeRequestApi.class);
        Call<String> call = api.getDetails(incoming_email);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()) {
                  //  Toast.makeText(LoginActivity.this, response.body().toString().trim(), Toast.LENGTH_SHORT).show();
                    if( response.body().toString().contains("Code"))
                    {
                      //  Toast.makeText(LoginActivity.this, ""+response.body().toString(), Toast.LENGTH_SHORT).show();
                        p1.setVisibility(View.GONE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        showCodeConfirmDialog(incoming_email);
                    }
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Network issue Kindly retry later.", Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void showCodeConfirmDialog(final String incoming)
    {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_forgot_pass_enter_new_pass, null);
        dialogBuilder.setView(dialogView);
//        final EditText edt = (EditText) dialogView.findViewById(R.id.editText);
        dialogBuilder.setTitle("Please provide the code we sent");
        dialogBuilder.setMessage("Enter code and new password below");
        dialogBuilder.setCancelable(false);
        dialogBuilder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                p1.setVisibility(View.VISIBLE);
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                final android.support.design.widget.TextInputEditText Code = (TextInputEditText) dialogView.findViewById(R.id.code_editText);
                final android.support.design.widget.TextInputEditText Pass = (TextInputEditText) dialogView.findViewById(R.id.pass_editText);
                final android.support.design.widget.TextInputEditText RePass = (TextInputEditText) dialogView.findViewById(R.id.repass_editText);

                String C = Code.getText().toString();
                String P1 = Pass.getText().toString();
                String P2 = RePass.getText().toString();
                if(P1.equals(P2))
                {
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(ROOT_URL) //Setting the Root URL
                            .addConverterFactory(ScalarsConverterFactory.create())
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    ForgotPasswordConfirmCodeApi api =retrofit.create(ForgotPasswordConfirmCodeApi.class);
                    Call<String> call = api.getDetails(incoming,C,P1);
                    call.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            if(response.body().toString().contains("Error"))
                            {
                                Snackbar snackbar = Snackbar
                                        .make(dialogView, "Wrong Code", Snackbar.LENGTH_LONG);
                                snackbar.show();
                                showCodeConfirmDialog(incoming);
                            }
                            else if(response.body().toString().contains("Code confirmed"))
                            {
                                Toast.makeText(LoginActivity.this, "Password updated", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                                p1.setVisibility(View.GONE);
                                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Server Busy", Toast.LENGTH_SHORT).show();
                            p1.setVisibility(View.GONE);
                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        }
                    });
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Passwords do not match", Toast.LENGTH_SHORT).show();
                    showCodeConfirmDialog(incoming);
                }
            }
        });
        dialogBuilder.setNegativeButton("Go back", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                showDialog();
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }
    public  void insertUser()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        LoginApi api =retrofit.create(LoginApi.class);
        Call<Example> call =  api.getDetails(username.getText().toString(),userpassword.getText().toString());
        call.enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                getuserid = response.body().getUserDetails().get0().getUserId();
                getusername =response.body().getUserDetails().get0().getUserName();
                getuseremail = response.body().getUserDetails().get0().getUserEmail();
                getuserpassword = response.body().getUserDetails().get0().getUserPassword();
                getusergender = response.body().getUserDetails().get0().getUserGender();
                getuserdob = response.body().getUserDetails().get0().getUserDOB();
                getfarzdate = response.body().getUserDetails().get0().getNamazFarzDate();
                getstartdate = response.body().getUserDetails().get0().getNamazStartDate();
                mdetail = response.body().getUserDetails().get0().getMdetails();
                qadahdetail = response.body().getUserDetails().get0().getQadahdetails();
                SharedPreferences.Editor editor1 = sharedpreferences.edit();
                editor1.putString("preuseremail",getuseremail);
                editor1.putString("prepassword",getuserpassword);
                editor1.putString("preuid",getuserid);
                editor1.putString("preusername",getusername);
                editor1.putString("preusergender",getusergender);
                editor1.putString("preuserfarzdate",getfarzdate);
                editor1.putString("preuserstartdate",getstartdate);
                editor1.putString("preusermdetails",mdetail);
                editor1.putString("preuserqadahdetails",qadahdetail);
                editor1.putString("preuserdob",getuserdob);
                editor1.apply();
                editor1.commit();
                p1.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            //   Toast.makeText(LoginActivity.this, ""+mdetail+"\n"+qadahdetail+"\n"+getuseremail, Toast.LENGTH_SHORT).show();
                if (getuseremail.equals(uname) && getuserpassword.equals(password) && mdetail.contains("0") && qadahdetail.contains("0")) {
                    Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                    startActivity(intent);
                    finish();
                }
                if (getuseremail.equals(uname) && getuserpassword.equals(password) && mdetail.contains("1") && qadahdetail.contains("0")) {
                    Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                    startActivity(intent);
                    finish();
                }
                else if(getuseremail.equals(uname) && getuserpassword.equals(password) && mdetail.contains("0") && qadahdetail.contains("-1") ) {
                    Intent intent = new Intent(getApplicationContext(), QadahsalahCalculateActivity.class);
                    intent.putExtra("farzdate",getfarzdate);
                    startActivity(intent);

                }
             else if(getuseremail.equals(uname) && getuserpassword.equals(password) && mdetail.contains("-1") && qadahdetail.contains("-1") ) {
                Intent intent = new Intent(getApplicationContext(), MenscalculateActivity.class);
                intent.putExtra("farzdate",getfarzdate);
                startActivity(intent);

            }
            }
            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                p1.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                Toast.makeText(LoginActivity.this, "Signup first", Toast.LENGTH_SHORT).show();

//                Retrofit retrofit1 = new Retrofit.Builder()
//                        .baseUrl(ROOT_URL) //Setting the Root URL
//                        .addConverterFactory(GsonConverterFactory.create())
//                        .build();
//                LoginfrompendingApi api =retrofit1.create(LoginfrompendingApi.class);
//Call<outofbox.ltd.administrator.tracksalah.POJOFORLOGINPENDING.Example> exampleCall = api.getDetails(username.getText().toString().trim(),userpassword.getText().toString());
//exampleCall.enqueue(new Callback<outofbox.ltd.administrator.tracksalah.POJOFORLOGINPENDING.Example>() {
//    @Override
//    public void onResponse(Call<outofbox.ltd.administrator.tracksalah.POJOFORLOGINPENDING.Example> call, Response<outofbox.ltd.administrator.tracksalah.POJOFORLOGINPENDING.Example> response) {
//        try {
//            getusername = response.body().getUserDetails().get0().getName();
//            getuseremail = response.body().getUserDetails().get0().getEmail();
//            getuserpassword = response.body().getUserDetails().get0().getPassword();
//            getusergender = response.body().getUserDetails().get0().getGender();
//            Intent intent = new Intent(getApplicationContext(),SignupActivity.class);
//            intent.putExtra("name",getusername);
//            intent.putExtra("email",getuseremail);
//            intent.putExtra("password",getuserpassword);
//            intent.putExtra("Gender",getusergender);
//            startActivity(intent);
//
//        }
//        catch (NullPointerException e)
//        {
//            Toast.makeText(LoginActivity.this, "Password or Email Incorrect", Toast.LENGTH_SHORT).show();
//        }
//
//    }
//
//    @Override
//    public void onFailure(Call<outofbox.ltd.administrator.tracksalah.POJOFORLOGINPENDING.Example> call, Throwable t) {
//        Toast.makeText(LoginActivity.this, "Network issue", Toast.LENGTH_SHORT).show();
//    }
//});
                }
        });
//        String name = response.body().getName();
//        String getuseremail = response.body().getEmail();
//        String  getuserpassword= response.body().getPassword();
//        String  getusergender= response.body().getGender();
//        Intent intent = new Intent(getApplicationContext(),SignupActivity.class);
//        intent.putExtra("name",name);
//        intent.putExtra("email",getuseremail);
//        intent.putExtra("password",getuserpassword);
//        intent.putExtra("Gender",getusergender);
//        startActivity(intent);
//        Toast.makeText(LoginActivity.this, "Incomplete Details Complete it First.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }

    boolean internet_connection(){
        //Check if connected to internet, output accordingly
        ConnectivityManager cm = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    public void signup(View view) {
        Intent intent = new Intent(getApplicationContext(),EmailConfirmation.class);
        startActivity(intent);
    }


}
