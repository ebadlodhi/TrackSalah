package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;

import outofbox.ltd.administrator.tracksalah.POJOFORQADAHTIMEPERIOD.Example;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface QadahCheckRecordTImeperiodAPi {

    @FormUrlEncoded
    @POST("Qadhacheckrecordtimeperiod.php")
    Call<Example> getDetails(@Field("id") String id, @Field("curretntdate") String curretntdate, @Field("backdate") String backdate);
}
