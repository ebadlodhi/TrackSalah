package outofbox.ltd.administrator.tracksalah;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import outofbox.ltd.administrator.tracksalah.POJOcalenderData.CalenderDetailsFetch;
import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.FetchCalenderData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static java.util.Calendar.DATE;

public class CalenderActivity extends AppCompatActivity {
    public GregorianCalendar cal_month, cal_month_copy;
    private CalendarAdapter cal_adapter;
    private TextView tv_month;
    SharedPreferences sharedPreferences;
    String User_id="" ;
    private Tracker mTracker;
    public static final String ROOT_URL = "http://185.123.99.207/~android215/TrackSalah/";
    public ArrayList<CalendarCollection> date_collection_arr  = new ArrayList<>(1);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sharedPreferences = getSharedPreferences(LoginActivity.loginpref, Context.MODE_PRIVATE);
        User_id = sharedPreferences.getString("preuid", "");
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());
        FetchCalenderDetails(User_id);
        cal_month = (GregorianCalendar) GregorianCalendar.getInstance();
        cal_month_copy = (GregorianCalendar) cal_month.clone();
        cal_adapter = new CalendarAdapter(getApplicationContext(), cal_month, CalendarCollection.date_collection_arr);
        tv_month = (TextView) findViewById(R.id.tv_month);
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));
        ImageButton previous = (ImageButton) findViewById(R.id.ib_prev);
        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPreviousMonth();
                refreshCalendar();
            }
        });
        ImageButton next = (ImageButton) findViewById(R.id.Ib_next);
        next.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setNextMonth();
                refreshCalendar();
            }
        });
        GridView gridview = (GridView) findViewById(R.id.gv_calendar);
        gridview.setAdapter(cal_adapter);

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                ((CalendarAdapter) parent.getAdapter()).setSelected(v, position);
                String selectedGridDate = CalendarAdapter.day_string
                        .get(position);
                String[] separatedTime = selectedGridDate.split("-");
                String gridvalueString = separatedTime[2].replaceFirst("^0*", "");
                int gridvalue = Integer.parseInt(gridvalueString);
                if ((gridvalue > 10) && (position < 8)) {
                    setPreviousMonth();
                    refreshCalendar();
                } else if ((gridvalue < 7) && (position > 28)) {
                    setNextMonth();
                    refreshCalendar();
                }
                ((CalendarAdapter) parent.getAdapter()).setSelected(v, position);
                ((CalendarAdapter) parent.getAdapter()).getPositionList(selectedGridDate, CalenderActivity.this);
                Intent intent = new Intent(getApplicationContext(), DailySalahActivity.class);

                Calendar mydate = Calendar.getInstance();
                Date todate = new Date();



                SimpleDateFormat format = new SimpleDateFormat("d-MMM-yyyy");
                Date griddate = null;
                try {
                     griddate = format.parse(selectedGridDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String todate1 = format.format(todate);
                Date todaydate2 = null;
                try {
                    todaydate2 = format.parse(todate1);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                int count = 0;
                mydate.setTime(griddate);
               // Toast.makeText(CalenderActivity.this, "mydate = "+griddate+"\n\n\n\ntodate = "+todate, Toast.LENGTH_LONG).show();
                Log.e("cheking", "mydate = "+griddate+"\n\n\n\ntodate = "+todaydate2);


                if(griddate.before(todaydate2))
                {
                    long diff = todaydate2.getTime() -griddate.getTime();
                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                    Log.d("Days no "," "+Days);
                    count = -2*Days;
                }

                else  if (griddate.after(todaydate2))
                {
                    long diff = griddate.getTime() -todaydate2.getTime();
                    int Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                    Log.d("Days no "," "+Days);
                    count = 2*Days;
                }
                else
                    count = 0 ;



                intent.putExtra("ClickedDate", selectedGridDate);
                intent.putExtra("countvalue",count);
                startActivity(intent);
                finish();
            }
        });
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
    protected void setNextMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month
                .getActualMaximum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) + 1),
                    cal_month.getActualMinimum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) + 1);
        }
    }
    protected void setPreviousMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month
                .getActualMinimum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) - 1),
                    cal_month.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) - 1);
        }
    }
    public void refreshCalendar() {
        cal_adapter.refreshDays();
        cal_adapter.notifyDataSetChanged();
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));
    }
    HashMap<String, Integer> namaz_count_per_day;
    public void FetchCalenderDetails(String Uid) {
        String uid = Uid;
        namaz_count_per_day= new HashMap<String, Integer>();
        Retrofit retrofitcalenderdetails = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        FetchCalenderData apicalenderfetch = retrofitcalenderdetails.create(FetchCalenderData.class);
        final Call<List<CalenderDetailsFetch>> callcalenderdetails = apicalenderfetch.getDetails(uid);
        callcalenderdetails.enqueue(new Callback<List<CalenderDetailsFetch>>() {
            @Override
            public void onResponse(Call<List<CalenderDetailsFetch>> call, Response<List<CalenderDetailsFetch>> response) {
                List<CalenderDetailsFetch> calenderDetailsFetches = response.body();
                if (calenderDetailsFetches.size() > 0) {
                    String[] todaydate = new String[calenderDetailsFetches.size()];
                    String[] namazname = new String[calenderDetailsFetches.size()];
                    String[] ontime = new String[calenderDetailsFetches.size()];
                    String[] late = new String[calenderDetailsFetches.size()];
                    String[] missed = new String[calenderDetailsFetches.size()];
                    String[] Exempted = new String[calenderDetailsFetches.size()];
                    CalendarCollection.date_collection_arr = new ArrayList<CalendarCollection>(1);
                    String finalDate = "";


                    //Hamza
                    for (int a = 0; a < calenderDetailsFetches.size(); a++) {
                        todaydate[a] = calenderDetailsFetches.get(a).getTodayDate();
                        ontime[a] = calenderDetailsFetches.get(a).getOntime();
                        late[a] = calenderDetailsFetches.get(a).getLate();
                        missed[a] = calenderDetailsFetches.get(a).getMissed();
                        Exempted[a] = calenderDetailsFetches.get(a).getExempted();

                        namaz_count_per_day.put(calenderDetailsFetches.get(a).getTodayDate().toString(), 0);
                        if(calenderDetailsFetches.get(a).getExempted().equals("Exempted"))
                        {
                            namaz_count_per_day.put(calenderDetailsFetches.get(a).getTodayDate().toString(), namaz_count_per_day.get(calenderDetailsFetches.get(a).getTodayDate()) + 50);
                          //  Toast.makeText(CalenderActivity.this, "aaaaaaaaaaa     "+namaz_count_per_day, Toast.LENGTH_SHORT).show();
                        }
                      else  if (calenderDetailsFetches.get(a).getOntime().equals("ontime") || calenderDetailsFetches.get(0).getLate().equals("late") ) {
                            namaz_count_per_day.put(calenderDetailsFetches.get(a).getTodayDate().toString(), namaz_count_per_day.get(calenderDetailsFetches.get(a).getTodayDate()) + 1);
                          //  Toast.makeText(CalenderActivity.this, "bbbbbbbbbb     "+namaz_count_per_day, Toast.LENGTH_SHORT).show();
                        }
                    }



                    int ii = 1;
                    while (ii < calenderDetailsFetches.size()) {
                        String A = todaydate[ii];
                        String B = todaydate[ii - 1];
                        if (A.equals(B)) {
                            namaz_count_per_day.put(calenderDetailsFetches.get(ii).getTodayDate().toString(), 0);
                            if (calenderDetailsFetches.get(ii).getOntime().toString().equals("ontime") || calenderDetailsFetches.get(ii).getLate().toString().equals("late") ) {
                                namaz_count_per_day.put(calenderDetailsFetches.get(ii).getTodayDate(), namaz_count_per_day.get(calenderDetailsFetches.get(ii).getTodayDate()) + 1);
                            }
                         else  if (calenderDetailsFetches.get(ii).getExempted().equals("Exempted"))
                            {
                                namaz_count_per_day.put(calenderDetailsFetches.get(ii).getTodayDate(), namaz_count_per_day.get(calenderDetailsFetches.get(ii).getTodayDate()) + 50);
                            }

                        } else {
                            namaz_count_per_day.put(calenderDetailsFetches.get(ii).getTodayDate().toString(), 0);
                            if (calenderDetailsFetches.get(ii).getOntime().toString().equals("ontime") || calenderDetailsFetches.get(ii).getLate().toString().equals("late") ) {
                                namaz_count_per_day.put(calenderDetailsFetches.get(ii).getTodayDate(), namaz_count_per_day.get(calenderDetailsFetches.get(ii).getTodayDate()) + 1);
                            }
                           else if(calenderDetailsFetches.get(ii).getExempted().equals("Exempted"))
                            {
                                namaz_count_per_day.put(calenderDetailsFetches.get(ii).getTodayDate(), namaz_count_per_day.get(calenderDetailsFetches.get(ii).getTodayDate()) + 50);
                            }
                        }
                        ii++;
                    }
                    Log.e("Data", namaz_count_per_day + "");
                    for (int i = 0; i < calenderDetailsFetches.size(); i++) {
                        String datenow = todaydate[i];
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MMM-yyyy");
                        SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
                        Date myDate = null;
                        try {
                            myDate = dateFormat1.parse(datenow);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        finalDate = dateFormat1.format(myDate);
                        CalendarCollection.date_collection_arr.add(new CalendarCollection("" + finalDate + "", "offerred", namaz_count_per_day.get(finalDate)));
                  //  Log.e("loopaaaaaaaa",i+" "+namaz_count_per_day.get(finalDate));
                    }

                 //  Toast.makeText(CalenderActivity.this, finalDate + "\n " + calenderDetailsFetches.size(), Toast.LENGTH_LONG).show();

                }

                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }

            @Override
            public void onFailure(Call<List<CalenderDetailsFetch>> call, Throwable t) {

                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        });
    }
}
