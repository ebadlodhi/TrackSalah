package outofbox.ltd.administrator.tracksalah;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import outofbox.ltd.administrator.tracksalah.RetrofitInterfaces.CalculateQadahApi;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class QadahcalculateActivity extends AppCompatActivity {

    private DatePicker dpResult;
    static final int DATE_DIALOG_ID = 999;
    Date Currenttime;
    String currentDateandTime;
    private int myear;
    private int mmonth;
    private int mday;
    public EditText datepicker;
    String Userid = "";
    Date mydate = null;
    Date newdate = null;
    long longdate;
    String tenyear = "";
    Date startdate = null;
    Date startnewdate = null;
    String Nstartdate = "";
    SharedPreferences sharedPreferences;
    String Gender;

    public static TextView DOB, tenyeardate;
    public static EditText oferstartdate;
    long diff;
    int Days,TotalQadahNamaz,Qadahfajar,QadahZohar,QadahAsar,Qadahmaghrib,Qadahisha = 0;
    public static final String ROOT_URL = "http://185.123.99.207/~android215/TrackSalah/";
    TextView mtext;
    Spinner mcount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qadahcalculate);
        datepicker = (EditText) findViewById(R.id.oferstartdate);
        DOB = (TextView) findViewById(R.id.birthdate);
        tenyeardate = (TextView) findViewById(R.id.tenyeardate);
        oferstartdate = (EditText) findViewById(R.id.oferstartdate);
        mtext = findViewById(R.id.mtext);
        mcount = findViewById(R.id.mcount);
        sharedPreferences = getSharedPreferences(LoginActivity.loginpref, Context.MODE_PRIVATE);
        Userid = sharedPreferences.getString("preuid", "");
        Gender = sharedPreferences.getString("preusergender","");
        String[] items = new String[]{ "3","4","5","6","7","8","9","10"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        mcount.setPopupBackgroundResource(R.color.colorAccent);
        mcount.setAdapter(adapter);
        Toast.makeText(this, ""+Gender, Toast.LENGTH_SHORT).show();


        if(Gender.equals("Male")) {

            mtext.setVisibility(View.GONE);
            mcount.setVisibility(View.GONE);
            String getUserDOB = Dateofbirth1.getUserDOB;
            DOB.setText(getUserDOB);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

            setCurrentDateOnView();
            addListenerOnButton();

            try {
                mydate = simpleDateFormat.parse(getUserDOB);
                Calendar c = Calendar.getInstance();
                c.setTime(mydate);
                c.add(Calendar.YEAR, 14);
                newdate = c.getTime();
                longdate = newdate.getTime();
                tenyear = simpleDateFormat.format(newdate);
                String dob = simpleDateFormat.format(mydate);

//                List<Date> dates = getDates("2000-10-23", "2014-10-23");
//                List<String> dates = getDates(dob.toString().trim(), tenyear.toString().trim());
//                for(String date:dates)
//                    Log.d("date",date.toString());
                tenyeardate.setText(tenyear);


            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else
        {

            String getUserDOB = Dateofbirth1.getUserDOB;
            DOB.setText(getUserDOB);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

            setCurrentDateOnView();
            addListenerOnButton();

            try {
                mydate = simpleDateFormat.parse(getUserDOB);
                Calendar c = Calendar.getInstance();
                c.setTime(mydate);
                c.add(Calendar.YEAR, 14);
                newdate = c.getTime();
                longdate = newdate.getTime();
                tenyear = simpleDateFormat.format(newdate);


                tenyeardate.setText(tenyear);



            } catch (ParseException e) {
                e.printStackTrace();
            }


        }
    }

    public void SaveQadahData(View view)  {
        if(Gender.equals("Male")) {
            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String uid = Userid;
//        String nstartdate = Nstartdate;
//        int TotalDays = Days;
//
//

            Calendar calendar = Calendar.getInstance();
            try {
                startdate = simpleDateFormat2.parse(datepicker.getText().toString());

                calendar.setTime(startdate);
                startnewdate = calendar.getTime();
                //Date changedate = simpleDateFormat.format(startnewdate);
                String cdate = simpleDateFormat.format(startnewdate);
                Nstartdate = simpleDateFormat2.format(startnewdate);
                Toast.makeText(this, "farz date = "+tenyear+"\nstart date = "+cdate, Toast.LENGTH_LONG).show();
                SharedPreferences.Editor editor1 = LoginActivity.sharedpreferences.edit();
                editor1.putString("preuserfarzdate",tenyear);
                editor1.putString("preuserstartdate",cdate);
                editor1.apply();
                editor1.commit();
                List<String> dates = getDates( tenyear.toString().trim(),cdate.toString().trim());
                for(String date:dates)
                    Log.d("date",date.toString());

                diff = startnewdate.getTime() - newdate.getTime();
                Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                TotalQadahNamaz = Days * 5;
                Qadahfajar = TotalQadahNamaz / 5;
                QadahZohar = TotalQadahNamaz / 5;
                QadahAsar = TotalQadahNamaz / 5;
                Qadahmaghrib = TotalQadahNamaz / 5;
                Qadahisha = TotalQadahNamaz / 5;
                Gson gson = new Gson();
                String dtlist = gson.toJson(dates);
                Log.d("array date",dtlist);
                CalculateQadah(uid, cdate, TotalQadahNamaz, TotalQadahNamaz, Qadahfajar, QadahZohar, QadahAsar, Qadahmaghrib, Qadahisha,tenyear);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            //Toast.makeText(this, "User id = "+uid+"\nNstart ="+Nstartdate.toString()+"\ndays ="+Days, Toast.LENGTH_SHORT).show();
        }
        else
        {
            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String uid = Userid;
//        String nstartdate = Nstartdate;
//        int TotalDays = Days;
//
//

            Calendar calendar = Calendar.getInstance();
            try {
                startdate = simpleDateFormat2.parse(datepicker.getText().toString());

                calendar.setTime(startdate);
                startnewdate = calendar.getTime();
                //Date changedate = simpleDateFormat.format(startnewdate);
                String cdate = simpleDateFormat.format(startnewdate);
                Nstartdate = simpleDateFormat2.format(startnewdate);
                Toast.makeText(this, "farz date = "+tenyear+"\nstart date = "+cdate, Toast.LENGTH_LONG).show();
                SharedPreferences.Editor editor1 = LoginActivity.sharedpreferences.edit();
                editor1.putString("preuserfarzdate",tenyear);
                editor1.putString("preuserstartdate",cdate);
                editor1.apply();
                editor1.commit();
                List<String> dates = getDates( tenyear.toString().trim(),cdate.toString().trim());
                for(String date:dates)
                    Log.d("date",date.toString());
                diff = startnewdate.getTime() - newdate.getTime();
                Days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                String mdayss = mcount.getSelectedItem().toString();
                int mdays = Integer.parseInt(mdayss);
                int minyear = mdays*12;
                int totyear = Days/365;
                int totminday = minyear*totyear;
                int Totalqadahmin = Days-totminday;
                TotalQadahNamaz = Totalqadahmin * 5;
                Qadahfajar = TotalQadahNamaz / 5;
                QadahZohar = TotalQadahNamaz / 5;
                QadahAsar = TotalQadahNamaz / 5;
                Qadahmaghrib = TotalQadahNamaz / 5;
                Qadahisha = TotalQadahNamaz / 5;
                Gson gson = new Gson();
                String dtlist = gson.toJson(dates);
                Log.d("array date",dtlist);
                CalculateQadah(uid, cdate, TotalQadahNamaz, TotalQadahNamaz, Qadahfajar, QadahZohar, QadahAsar, Qadahmaghrib, Qadahisha,tenyear);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }

    private static ArrayList<String> getDates(String dateString1, String dateString2)
    {
        ArrayList<String> dates = new ArrayList<String>();
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = df1.parse(dateString1);
            date2 = df1.parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);


        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while(!cal1.after(cal2))
        {
            Date pdate = cal1.getTime();
            String sdate = df1.format(pdate);
            dates.add(sdate.toString());
            cal1.add(Calendar.DATE, 1);
        }
        return dates;
    }




    public void setCurrentDateOnView() {

        // tvDisplayDate = (TextView) findViewById(R.id.date);
        dpResult = (DatePicker) findViewById(R.id.dobselect);
        dpResult.setVisibility(View.GONE);

        final Calendar c = Calendar.getInstance();
        Currenttime = c.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        currentDateandTime = sdf.format(new Date());
        myear = c.get(Calendar.YEAR);
        mmonth = c.get(Calendar.MONTH);
        mday = c.get(Calendar.DAY_OF_MONTH);

        // set current date into textview
        datepicker.setText(new StringBuilder()
                // Month is 0 based, just add 1
                .append(mday).append("-") .append(mmonth + 1).append("-").append(myear)
        );

        // set current date into datepicker
        dpResult.init(myear, mmonth, mday, null);


    }

    public void addListenerOnButton() {



        datepicker.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                showDialog(DATE_DIALOG_ID);

            }

        });

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                DatePickerDialog _date =   new DatePickerDialog(this, datePickerListener, myear,mmonth,
                        mday){
                    @Override
                    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                    {
//                        if (year > myear) {
//                            Toast.makeText(getApplicationContext(), "Year can't be greater then current Year", LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }
//                        if (monthOfYear > mmonth && year == myear) {
//                            Toast.makeText(getApplicationContext(), "Month can't be greater then current Month", LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }
//                        if (dayOfMonth > mday && year == myear && monthOfYear == mmonth) {
//                            Toast.makeText(getApplicationContext(), "Date can't be greater then current date", LENGTH_SHORT).show();
//                            view.updateDate(myear, mmonth, mday);
//                        }
                    }
                };
                return _date;
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            myear = selectedYear;
            mmonth = selectedMonth;
            mday = selectedDay;

            // set selected date into textview

            datepicker.setText(new StringBuilder()
                    .append(mday).append("-") .append(mmonth + 1).append("-").append(myear));

            // set selected date into datepicker also
            dpResult.init(myear, mmonth, mday, null);

        }
    };

    public  void CalculateQadah(String uid,String Nstartdate,int TotalQadahNamaz,int RemQadahNamaz,int Qadahfajar,int QadahZohar,int QadahAsar,int Qadahmaghrib,int Qadahisha,String dates)
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        CalculateQadahApi  api = retrofit.create(CalculateQadahApi.class);
        Call<String> call = api.getDetails(uid,Nstartdate,TotalQadahNamaz,RemQadahNamaz,Qadahfajar,QadahZohar,QadahAsar,Qadahmaghrib,Qadahisha,dates);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                String ret =  response.body().toString().trim();
                Toast.makeText(QadahcalculateActivity.this, ""+ret, Toast.LENGTH_SHORT).show();
                if(ret.equals("DataInserted"))
                {
                    Toast.makeText(getApplicationContext(),"Congractulations!\nQadah Namaz Calculated",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getApplicationContext(),QadahSalahActivity.class);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    Toast.makeText(QadahcalculateActivity.this, "Qadah Namaz Not Calculate Try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(QadahcalculateActivity.this, "Network Issue", Toast.LENGTH_SHORT).show();
            }
        });




    }


}



