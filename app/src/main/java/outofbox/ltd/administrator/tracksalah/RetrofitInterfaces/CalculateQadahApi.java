package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface CalculateQadahApi {

    @FormUrlEncoded
    @POST("QadahInsertRecord.php")
    Call<String> getDetails(@Field("uid") String uid, @Field("Namazstartdate") String Namazstartdate, @Field("Totalqadahnamaz") int Totalqadahnamaz,
                            @Field("Remainingqadahnamaz") int Remainingqadahnamaz, @Field("Qadahfajar") int Qadahfajar, @Field("QadahZohar") int QadahZohar,
                            @Field("QadahAsar") int QadahAsar, @Field("Qadahmaghrib") int Qadahmaghrib, @Field("QadahIsha") int QadahIsha, @Field("DateList") String dates);

}
