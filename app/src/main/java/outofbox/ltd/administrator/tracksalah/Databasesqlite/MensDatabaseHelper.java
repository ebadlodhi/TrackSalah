package outofbox.ltd.administrator.tracksalah.Databasesqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MensDatabaseHelper extends SQLiteOpenHelper {


    public static final String DB_NAME ="UserMensDetail";
    public  static final String TABLE_NAME = "Mensdetail";
    public  static final String COLUMN_insert_id = "insert_id";
    public  static final String COLUMN_User_id = "User_id";
    public  static final String COLUMN_today_date = "today_date";
    public  static final String COLUMN_today_time = "today_time";
    public  static final String COLUMN_status = "status";

    public MensDatabaseHelper(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table "+TABLE_NAME+" ("+COLUMN_insert_id+" INTEGER PRIMARY KEY AUTOINCREMENT,"+COLUMN_User_id+" INTEGER,"+COLUMN_today_date+" text,"+COLUMN_today_time+" text,"+COLUMN_status+" int )";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        String sql = "DROP TABLE IF EXISTS "+TABLE_NAME+"";
        db.execSQL(sql);
        onCreate(db);
    }

    public boolean addRecord(int User_id,String today_date,String todaytime,int status ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COLUMN_User_id,User_id);
        contentValues.put(COLUMN_today_date, today_date);
        contentValues.put(COLUMN_today_time,todaytime);
        contentValues.put(COLUMN_status,status);


        db.insert(TABLE_NAME, null, contentValues);

        db.close();
        return true;
    }


    public boolean updateNameStatusOn(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues1 = new ContentValues();
        contentValues1.put(COLUMN_status, 1);
        db.update(TABLE_NAME, contentValues1, COLUMN_User_id+ "="+ id, null);
        db.close();

        return true;
    }
    public boolean updateNameStatusOff(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues1 = new ContentValues();
        contentValues1.put(COLUMN_status, 0);
        db.update(TABLE_NAME, contentValues1, COLUMN_User_id+ "="+ id, null);
        db.close();

        return true;
    }
    public Cursor getRecord(String uid) {
        SQLiteDatabase db = this.getReadableDatabase();
        int userid = Integer.parseInt(uid);
        String sql = "SELECT * FROM "+TABLE_NAME+" where "+COLUMN_User_id+ " = "+userid+"";
        Cursor c = db.rawQuery(sql, null);
        return c;
    }

    public Cursor getUnsyncedNames() {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM "+TABLE_NAME+" where "+COLUMN_status+" = 0 " ;
        Cursor c = db.rawQuery(sql, null);
        return c;
    }

}
