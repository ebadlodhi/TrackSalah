package outofbox.ltd.administrator.tracksalah.RetrofitInterfaces;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface skipmensApi {

    @FormUrlEncoded
    @POST("skipmens.php")
    Call<String> getDetails(@Field("userid") String userid);
}
